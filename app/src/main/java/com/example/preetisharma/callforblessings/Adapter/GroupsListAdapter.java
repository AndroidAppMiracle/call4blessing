package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupListingModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.GroupWallHomeScreen;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/21/2017.
 */

public class GroupsListAdapter extends RecyclerView.Adapter<GroupsListAdapter.DataViewHolder> implements Filterable {


    private Context mContext;
    private List<GroupListingModal.ListBean> myGroupsList = new ArrayList<>();
    private FriendFilter friendFilter;

    public GroupsListAdapter(Context mContext, List<GroupListingModal.ListBean> list) {
        this.myGroupsList = list;
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_joy_music_view_all, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {

        try {
            holder.atv_Name.setText(myGroupsList.get(position).getGroup_name());


            Glide
                    .with(mContext)
                    .load(myGroupsList.get(position).getGroup_icon())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.mProgress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            // image ready, hide progress now
                            holder.mProgress.setVisibility(View.GONE);
                            return false;   // return false if you want Glide to handle everything else.
                        }
                    })
                    /*.diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image*/
                    .centerCrop()
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(holder.atv_AlbumCover);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (myGroupsList.size() != 0) {
            return myGroupsList.size();
        } else {
            return 0;
        }

    }

    public GroupListingModal.ListBean getItem(int position) {
        return myGroupsList.get(position);
    }


    @Override
    public Filter getFilter() {
        if (friendFilter == null) {
            friendFilter = new FriendFilter();
        }

        return friendFilter;
    }


    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.atv_Name)
        AppCompatTextView atv_Name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.movie_progress)
        ProgressBar mProgress;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/


            ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, GroupWallHomeScreen.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.GROUP_ID, String.valueOf(myGroupsList.get(getAdapterPosition()).getId()));
                    i.putExtras(b);
                    mContext.startActivity(i);

                   /* if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myGroupsList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*/
                }
            });

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    GroupDeleteModal groupDeleteModal;

                    switch (tag) {
                        case APIServerResponse.GROUP_DELETE:
                            groupDeleteModal = (GroupDeleteModal) response.body();
                            if (groupDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast("Group Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            } else {
                                ((BaseActivity) mContext).showToast(groupDeleteModal.getMessage(), Toast.LENGTH_SHORT);
                            }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
           /* switch (tag) {
                case APIServerResponse.DELETEEVENT:
                    ((BaseActivity) mContext).showToast(throwable.getMessage().toString(), Toast.LENGTH_SHORT);

            }*/
        }
    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        myGroupsList.remove(position);
        notifyItemRemoved(position);
    }


    /**
     * Custom filter for friend list
     * Filter content in friend list according to the search text
     */
    private class FriendFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                //ArrayList<User> tempList = new ArrayList<User>();
                List<GroupListingModal.ListBean> tempList = new ArrayList<>();

                // search content in friend list
                for (GroupListingModal.ListBean user : myGroupsList) {
                    if (user.getGroup_name().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(user);
                    }
                }


                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = myGroupsList.size();
                filterResults.values = myGroupsList;
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         *
         * @param constraint text
         * @param results    filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            // Now we have to inform the adapter about the new list filtered
            if (results.count == 0) {

            } else {
                myGroupsList = (List<GroupListingModal.ListBean>) results.values;
                notifyDataSetChanged();
            }

        }
    }
}