package com.example.preetisharma.callforblessings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.QuestionsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.QuestionsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/6/2017.
 */

public class QuestionsActivity extends BaseActivity {
    @BindView(R.id.recyler_view_questions)
    RecyclerView recyler_view_questions;
    QuestionsAdapter _questionsAdapter;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_next)
    AppCompatTextView txtvw_next;
    public List<QuestionsModal.ListBean> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.questions_layout);
        updateStatusBar();
        ButterKnife.bind(this);
        getmPrefs();
        txtvw_header_title.setText("Just For Information");
        img_view_change_password.setVisibility(View.GONE);

        if (isConnectedToInternet()) {
            ServerAPI.getInstance().questions(APIServerResponse.QUESTIONS, this);
            showLoading();
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


    }


    @OnClick(R.id.txtvw_next)
    void submit() {

        if (_questionsAdapter.getcount() <= 0 && _questionsAdapter == null) {
            showToast("Please answer 3 questions correct", Toast.LENGTH_SHORT);
        } else if (_questionsAdapter.getcount() < 3) {
            showToast("Must  answer atleast 3 questions", Toast.LENGTH_SHORT);
        } else if (_questionsAdapter.getcount() >= 3) {
            Intent i = new Intent(this, PlayAudioActivity.class);
            startActivity(i);
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onSuccess(int tag, Response response) {
        hideLoading();
        QuestionsModal questions;
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.QUESTIONS:

                        Log.e("message", "Message" + response.toString());
                        questions = (QuestionsModal) response.body();


                        if (questions.getStatus().equals("1")) {
                            Log.e("List data", "List data" + questions.getList().size() + "Questions" + questions.getList().get(0).getQuestion_title());
                            list = questions.getList();
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        _questionsAdapter = new QuestionsAdapter(this, list);
        recyler_view_questions.setItemAnimator(new DefaultItemAnimator());
        recyler_view_questions.setAdapter(_questionsAdapter);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyler_view_questions.setLayoutManager(mLayoutManager);
    }

    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        switch (tag) {
            case APIServerResponse.REGISTER:
                System.out.println("Error");
                break;
        }
    }
}
