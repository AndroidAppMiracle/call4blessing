package com.example.preetisharma.callforblessings.demo;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BooksBuyFreeBookModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import retrofit2.Response;

/**
 * Created by satoti.garg on 5/29/2017.
 */

public class BooksBuyFree extends BaseActivity implements APIServerResponse {


    AppCompatTextView txtvw_amount, txtvw_pay, txtvw_header_title;
    AppCompatEditText edt_txt_first_name, edt_txt_last_name, edt_txt_email, edt_txt_city, edt_txt_zipcode, edt_txt_mobile_number, edt_txt_street_address;
    AppCompatImageView img_view_change_password, img_view_back;
    String bookId /*bookAmount, bookName*/;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_payment_details);

        getIntentInfo();
        instantiateViews();
        updateToolbar();
        instantiatePayButton();


    }


    public void getIntentInfo() {
        if (getIntent().getExtras() != null) {
            bookId = getIntent().getExtras().getString(Constants.BOOK_ID);
            //bookAmount = getIntent().getExtras().getString(Constants.AMOUNT);
            // bookName = getIntent().getExtras().getString(Constants.BOOK_DESC);
        }
    }

    private void instantiateViews() {

        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);

        txtvw_amount = (AppCompatTextView) findViewById(R.id.txtvw_amount);
        txtvw_pay = (AppCompatTextView) findViewById(R.id.txtvw_pay);
        edt_txt_first_name = (AppCompatEditText) findViewById(R.id.edt_txt_first_name);
        edt_txt_last_name = (AppCompatEditText) findViewById(R.id.edt_txt_last_name);
        edt_txt_email = (AppCompatEditText) findViewById(R.id.edt_txt_email);
        edt_txt_city = (AppCompatEditText) findViewById(R.id.edt_txt_city);
        edt_txt_zipcode = (AppCompatEditText) findViewById(R.id.edt_txt_zipcode);
        edt_txt_mobile_number = (AppCompatEditText) findViewById(R.id.edt_txt_mobile_number);
        edt_txt_street_address = (AppCompatEditText) findViewById(R.id.edt_txt_street_address);

    }


    public void updateToolbar() {
        txtvw_header_title.setText("Payment Details");
        txtvw_amount.setText(R.string.free);
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void instantiatePayButton() {
        try {

            txtvw_pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isConnectedToInternet()) {
                        if (validation()) {
                            //Call Api
                            if (txtvw_pay.isEnabled()) {
                                txtvw_pay.setEnabled(false);
                            }
                            showLoading();
                            ServerAPI.getInstance().booksBuyFreeBook(APIServerResponse.BOOK_BUY_FREE_BOOK, getUserSessionId(), bookId, edt_txt_first_name.getText().toString().trim(), edt_txt_last_name.getText().toString().trim(), edt_txt_email.getText().toString().trim(), edt_txt_city.getText().toString().trim(), edt_txt_zipcode.getText().toString().trim(), edt_txt_mobile_number.getText().toString().trim(), edt_txt_street_address.getText().toString().trim(), BooksBuyFree.this);

                        }
                    } else {

                        showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean validation() {
        if (edt_txt_first_name.getText().toString().isEmpty()) {
            showToast(Constants.FIRST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_first_name.setError(Constants.FIRST_NAMENOT_BLANK);
            return false;
        } else if (edt_txt_last_name.getText().toString().isEmpty()) {
            showToast(Constants.LAST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_last_name.setError(Constants.LAST_NAMENOT_BLANK);
            return false;
        } else if (edt_txt_email.getText().toString().isEmpty()) {
            showToast(Constants.EMAIL_NOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_email.setError(Constants.EMAIL_NOT_BLANK);
            return false;
        } else if (Character.isDigit(edt_txt_email.getText().charAt(0))) {
            showToast(Constants.EMAIL_START, Toast.LENGTH_SHORT);
            return false;
        } else if (!isValidEmail(edt_txt_email.getText().toString())) {
            showToast(Constants.EMAIL_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_city.getText().toString().isEmpty()) {
            showToast("City should not be blank", Toast.LENGTH_SHORT);
            edt_txt_city.setError("City should not be blank");
            return false;
        } else if (edt_txt_zipcode.getText().toString().isEmpty()) {
            showToast("Zipcode should not be blank", Toast.LENGTH_SHORT);
            edt_txt_zipcode.setError("Zipcode should not be blank");
            return false;
        } else if (edt_txt_mobile_number.getText().toString().isEmpty()) {
            showToast(Constants.PHONE_NUMBER_NOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_mobile_number.setError(Constants.PHONE_NUMBER_NOT_BLANK);
            return false;
        } else if (edt_txt_mobile_number.getText().toString().length() < 10) {
            showToast(Constants.PHONE_NUMBER_LENGTH, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_street_address.getText().toString().isEmpty()) {
            showToast("Street Address should not be blank", Toast.LENGTH_SHORT);
            return false;
        } else {
            return true;
        }


    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {

            hideLoading();
            if (response.isSuccessful()) {

                BooksBuyFreeBookModal booksBuyFreeBookModal;

                switch (tag) {

                    case APIServerResponse.BOOK_BUY_FREE_BOOK:

                        booksBuyFreeBookModal = (BooksBuyFreeBookModal) response.body();

                        if (booksBuyFreeBookModal.getStatus().equalsIgnoreCase("1")) {
                            //showToast(booksBuyFreeBookModal.getMessage(), Toast.LENGTH_LONG);
                            onBookBuySuccessDialog("Message", booksBuyFreeBookModal.getMessage());

                        }
                        break;
                }

                if (!txtvw_pay.isEnabled()) {
                    txtvw_pay.setEnabled(true);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }


    private void onBookBuySuccessDialog(String dialogName, String dialogMessage) {

        try {
            //final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(dialogName);
            dialogBuilder.setMessage(dialogMessage);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    BooksBuyFree.this.finish();
                }
            });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
