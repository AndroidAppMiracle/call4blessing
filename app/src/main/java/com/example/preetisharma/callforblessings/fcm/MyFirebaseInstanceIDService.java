package com.example.preetisharma.callforblessings.fcm;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.preetisharma.callforblessings.Utils.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private SharedPreferences mPrefs;
    Context context;
    public static String refreshedToken;

    public static FirebaseInstanceIdService getInstance() {
        return new FirebaseInstanceIdService();
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        context = this.context;
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("", "Refreshed token: " + refreshedToken);
        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.


        Log.e("Token", "---" + token);
        mPrefs = getSharedPreferences(Constants.DEVICE_ID, MODE_PRIVATE);
        mPrefs.edit().putString(Constants.FCM_TOKEN, token).commit();

    }

}
