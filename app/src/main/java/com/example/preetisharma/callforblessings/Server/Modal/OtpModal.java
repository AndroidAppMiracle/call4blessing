package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by preeti.sharma on 1/31/2017.
 */

public class OtpModal {
    /**
     * status : 1
     * message : Otp send successfully
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
