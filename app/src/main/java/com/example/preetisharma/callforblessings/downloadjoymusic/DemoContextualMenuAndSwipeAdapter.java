package com.example.preetisharma.callforblessings.downloadjoymusic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.NotificationsAdapter;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationsModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.DemoSinglePostView;
import com.example.preetisharma.callforblessings.demo.EventsDetailsActivityNew;
import com.example.preetisharma.callforblessings.demo.SinglePostView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 7/28/2017.
 */

public class DemoContextualMenuAndSwipeAdapter extends RecyclerView.Adapter<DemoContextualMenuAndSwipeAdapter.DataViewHolder> {
    public List<NotificationsModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;
    private SparseBooleanArray selectedItems;
    /*private static int currentSelectedIndex = -1;*/

    public DemoContextualMenuAndSwipeAdapter(Activity mContext, List<NotificationsModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
        selectedItems = new SparseBooleanArray();
    }


 /*   public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }*/

  /*  public void addAllItems(List<MyBooksModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }*/


    @Override
    public DemoContextualMenuAndSwipeAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.demodemoadapter_contextual, parent, false);
        DemoContextualMenuAndSwipeAdapter.DataViewHolder dataView = new DemoContextualMenuAndSwipeAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DemoContextualMenuAndSwipeAdapter.DataViewHolder holder, int position) {


        holder.tv_notification_text.setText(list.get(position).getText());
        holder.tv_notification_date.setText(list.get(position).getCreated_time());
        Glide.with(mContext).load(list.get(position).getUserInfo().getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).crossFade()/*.override(64, 64)*/.into(holder.aiv_notification_image);

        holder.itemView.setActivated(selectedItems.get(position, false));

       /* holder.aiv_notification_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(holder.getAdapterPosition()).getUserInfo().getIs_friend() == 1) {
                    Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUserInfo().getId()));
                    b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                    b.putString(Constants.PRIVACY_WALL, list.get(holder.getAdapterPosition()).getUserInfo().getWALL());
                    b.putString(Constants.PRIVACY_ABOUT_INFO, list.get(holder.getAdapterPosition()).getUserInfo().getabout_info());
                    b.putString(Constants.PRIVACY_FRIEND_REQUEST, list.get(holder.getAdapterPosition()).getUserInfo().getfriend_request());
                    b.putString(Constants.PRIVACY_MESSAGE, list.get(holder.getAdapterPosition()).getUserInfo().getMESSAGE());
                    i.putExtras(b);
                    mContext.startActivity(i);
                } else if (list.get(holder.getAdapterPosition()).getUserInfo().getIs_friend() == 0) {
                    Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUserInfo().getId()));
                    b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    b.putInt(Constants.IS_FRIEND, Constants.USER_NOT_FRIEND);
                    b.putString(Constants.PRIVACY_WALL, list.get(holder.getAdapterPosition()).getUserInfo().getWALL());
                    b.putString(Constants.PRIVACY_ABOUT_INFO, list.get(holder.getAdapterPosition()).getUserInfo().getabout_info());
                    b.putString(Constants.PRIVACY_FRIEND_REQUEST, list.get(holder.getAdapterPosition()).getUserInfo().getfriend_request());
                    b.putString(Constants.PRIVACY_MESSAGE, list.get(holder.getAdapterPosition()).getUserInfo().getMESSAGE());
                    i.putExtras(b);
                    mContext.startActivity(i);
                }
            }
        });*/

       /* holder.ll_notification_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.FRIEND_REQUEST)) {


                    if (list.get(holder.getAdapterPosition()).getUserInfo().getRequest_respond().equalsIgnoreCase("No") && list.get(holder.getAdapterPosition()).getUserInfo().getIs_friend() == 1) {
                        Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUserInfo().getId()));
                        b.putBoolean(Constants.TIMELINE_ENABLED, false);
                        b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                        i.putExtras(b);
                        mContext.startActivity(i);
                    } else {

                        if (list.get(holder.getAdapterPosition()).getUserInfo().getRequest_respond().equalsIgnoreCase("No") && list.get(holder.getAdapterPosition()).getUserInfo().getIs_friend() == 0) {

                        } else {

                        }
                        Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUserInfo().getId()));
                        b.putBoolean(Constants.TIMELINE_ENABLED, false);
                        b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                        i.putExtras(b);
                        mContext.startActivity(i);
                    }


                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.POST_COMMENT)) {


                    Intent i = new Intent(mContext, DemoSinglePostView.class);
                    *//*Intent i = new Intent(mContext, SinglePostView.class);*//*
                    Bundle b = new Bundle();
                    //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                    //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                    b.putString(Constants.POSTID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                    i.putExtras(b);
                    mContext.startActivity(i);


                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.POST_LIKE)) {

                    Intent i = new Intent(mContext, DemoSinglePostView.class);
                    *//*Intent i = new Intent(mContext, SinglePostView.class);*//*
                    Bundle b = new Bundle();
                    //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                    //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                    b.putString(Constants.POSTID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                    i.putExtras(b);
                    mContext.startActivity(i);

                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.POST_SHARE)) {

                    Intent i = new Intent(mContext, DemoSinglePostView.class);
                    *//*Intent i = new Intent(mContext, SinglePostView.class);*//*
                    Bundle b = new Bundle();
                    //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                    //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                    b.putString(Constants.POSTID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                    i.putExtras(b);
                    mContext.startActivity(i);

                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.EVENT_INVITE)) {


                    if (list.get(holder.getAdapterPosition()).getUserInfo().getRequest_respond().equalsIgnoreCase("NO")) {
                        Intent i = new Intent(mContext, EventsDetailsActivityNew.class);
                        Bundle b = new Bundle();
                        //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                        //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                        //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                        b.putString(Constants.EVENT_ID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                        b.putString(Constants.EVENT_TYPE_FLAG, "Event Invites");
                        b.putString(Constants.EVENT_INVITE_STATUS, "PENDING");
                        i.putExtras(b);
                        mContext.startActivity(i);
                    } else {
                        Intent i = new Intent(mContext, EventsDetailsActivityNew.class);
                        Bundle b = new Bundle();
                        //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                        //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                        //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                        b.putString(Constants.EVENT_ID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                        b.putString(Constants.EVENT_TYPE_FLAG, "Event Invites");
                        b.putString(Constants.EVENT_INVITE_STATUS, "ACCEPTED");
                        i.putExtras(b);
                        mContext.startActivity(i);
                    }


                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.POST)) {
                    *//*Intent i = new Intent(mContext, DemoSinglePostView.class);*//*
                    Intent i = new Intent(mContext, SinglePostView.class);
                    Bundle b = new Bundle();
                    //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                    //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                    b.putString(Constants.POSTID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                    i.putExtras(b);
                    mContext.startActivity(i);
                }
            }
        });*/


    }


    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    public void toggleSelection(int pos) {
        /*currentSelectedIndex = pos;*/
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            /*animationItemsIndex.delete(pos);*/
        } else {
            selectedItems.put(pos, true);
            /*animationItemsIndex.put(pos, true);*/
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        /*reverseAllAnimations = true;*/
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        list.remove(position);
        //resetCurrentIndex();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_notification_text)
        AppCompatTextView tv_notification_text;

        @BindView(R.id.aiv_notification_image)
        AppCompatImageView aiv_notification_image;

        @BindView(R.id.tv_notification_date)
        AppCompatTextView tv_notification_date;


        @BindView(R.id.ll_notification_content)
        LinearLayout ll_notification_content;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }
}
