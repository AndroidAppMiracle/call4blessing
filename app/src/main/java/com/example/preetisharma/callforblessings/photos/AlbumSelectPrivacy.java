package com.example.preetisharma.callforblessings.photos;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satoti.garg on 7/14/2017.
 */

public class AlbumSelectPrivacy extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;


    @BindView(R.id.rg_album_Privacy)
    RadioGroup rg_album_Privacy;
    @BindView(R.id.rb_public)
    RadioButton rb_public;
    @BindView(R.id.rb_friends)
    RadioButton rb_friends;
    @BindView(R.id.rb_only_me)
    RadioButton rb_only_me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_select_privacy);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText(R.string.album_select_privacy);


       /* int selectedId=rg_album_Privacy.getCheckedRadioButtonId();
        radioSexButton=(RadioButton)findViewById(selectedId);
        Toast.makeText(MainActivity.this,radioSexButton.getText(),Toast.LENGTH_SHORT).show();
*/

    }

    @OnClick(R.id.img_view_back)
    public void toolbarBack() {

        AlbumSelectPrivacy.this.finish();

    }
}
