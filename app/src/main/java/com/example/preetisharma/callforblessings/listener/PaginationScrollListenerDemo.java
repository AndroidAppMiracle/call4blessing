package com.example.preetisharma.callforblessings.listener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by satoti.garg on 6/28/2017.
 */

public abstract class PaginationScrollListenerDemo extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;
    private LinearLayout linearLayout;

    /**
     * @param layoutManager
     */
    public PaginationScrollListenerDemo(LinearLayoutManager layoutManager, LinearLayout linearLayout) {
        this.layoutManager = layoutManager;
        this.linearLayout = linearLayout;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (layoutManager.findViewByPosition(firstVisibleItemPosition).getTop() == 0 && firstVisibleItemPosition == 0) {
            Log.i("isFirstChild", "Yes");
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            Log.i("isFirstChild", "No");
            linearLayout.setVisibility(View.GONE);
        }

        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= getTotalPageCount()) {
                loadMoreItems();
            }
        }


    }

    protected abstract void loadMoreItems();

    public abstract int getTotalPageCount();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();

}
