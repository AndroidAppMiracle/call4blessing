package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.FilterModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.UpdateGroupParcebleModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/12/2017.
 */

public class UpdateGroupAdapter extends RecyclerView.Adapter<UpdateGroupAdapter.DataViewHolder> {


    private Context mContext;
    private List<UpdateGroupParcebleModal> myFriends = new ArrayList<>();
    private FilterModal modal;
    //private ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> taggedFriendsDemo = new ArrayList<>();
    private ArrayList<UpdateGroupParcebleModal> taggedFriendsIds = new ArrayList<>();
    private ArrayList<UpdateGroupParcebleModal> groupMembers = new ArrayList<>();
    //private ArrayList<UpdateGroupParcebleModal> allFriends = new ArrayList<>();
    private ArrayList<UpdateGroupParcebleModal> myFriendsAll = new ArrayList<>();

    public UpdateGroupAdapter(Context mContext, ArrayList<UpdateGroupParcebleModal> list) {
        this.mContext = mContext;
        this.groupMembers = list;
        this.myFriendsAll.addAll(groupMembers);
    }


    @Override
    public UpdateGroupAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_create_group_add_friends_layout, parent, false);
        UpdateGroupAdapter.DataViewHolder dataView = new UpdateGroupAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }


    @Override
    public void onBindViewHolder(final UpdateGroupAdapter.DataViewHolder holder, int position) {

        try {

            holder.txtvw_friends_request_name.setText(groupMembers.get(position).getName());

            if (groupMembers.get(position).isGroupMember()) {
                holder.checkbox_select_friend.setChecked(true);
                taggedFriendsIds.add(groupMembers.get(holder.getAdapterPosition()));
            } else {
                holder.checkbox_select_friend.setChecked(false);
            }


            holder.checkbox_select_friend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        Log.i("isChecked", "Checked");
                        taggedFriendsIds.add(groupMembers.get(holder.getAdapterPosition()));
                        Log.i("isChecked size", "" + taggedFriendsIds.size());

                    } else {
                        Log.i("isChecked", "notChecked");
                        taggedFriendsIds.remove(groupMembers.get(holder.getAdapterPosition()));
                        Log.i("isChecked size", "" + taggedFriendsIds.size());
                    }


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* public ArrayList<FilterModal> unselectList() {
        FilterModal filterModal = new FilterModal();
        filterModal.setFriendsArray(myTaggedFriends);
        return myTaggedFriends;

    }*/

    /*public ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> selectedList() {

        ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> list = new ArrayList<>();
        list = taggedFriendsDemo;
        return list;
    }*/

    public ArrayList<UpdateGroupParcebleModal> selectedListIds() {
        ArrayList<UpdateGroupParcebleModal> list = new ArrayList<>();
        list.addAll(taggedFriendsIds);
        return list;
    }

    @Override
    public int getItemCount() {
        if (groupMembers.size() != 0) {
            return groupMembers.size();
        } else {
            return 0;
        }

    }


    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;
        @BindView(R.id.checkbox_select_friend)
        AppCompatCheckBox checkbox_select_friend;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @Override
        public void onSuccess(int tag, Response response) {


        }

        @Override
        public void onError(int tag, Throwable throwable) {

        }
    }


    public void notifyDataList(List<UpdateGroupParcebleModal> list) {
        Log.d("notifyData ", list.size() + "");
        this.myFriends = list;
        notifyDataSetChanged();
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        myFriends.clear();
        if (charText.length() == 0) {
            myFriends.addAll(myFriendsAll);
        } else {
            for (UpdateGroupParcebleModal wp : myFriendsAll) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    myFriends.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }


}

