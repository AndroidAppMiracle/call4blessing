package com.example.preetisharma.callforblessings.Server;


import com.example.preetisharma.callforblessings.Server.Modal.AddEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.AlarmRequestAcceptModal;
import com.example.preetisharma.callforblessings.Server.Modal.AlarmRequestCountModal;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.AmenPrayerModal;
import com.example.preetisharma.callforblessings.Server.Modal.BookDetailsModal;
import com.example.preetisharma.callforblessings.Server.Modal.BookPaymentModal;
import com.example.preetisharma.callforblessings.Server.Modal.BooksBuyFreeBookModal;
import com.example.preetisharma.callforblessings.Server.Modal.BooksLanguagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.BooksListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.BuyMusicDownloadedModal;
import com.example.preetisharma.callforblessings.Server.Modal.ChangePasswordModal;
import com.example.preetisharma.callforblessings.Server.Modal.ChangeProfileCoverPicModal;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.CommentsListModal;
import com.example.preetisharma.callforblessings.Server.Modal.CreateGroupModal;
import com.example.preetisharma.callforblessings.Server.Modal.CreateGroupPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.DeleteEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoHomeModal;
import com.example.preetisharma.callforblessings.Server.Modal.EditEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.EditUserPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventDetailsModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventsInvitesModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventsListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.FetchProfileModal;
import com.example.preetisharma.callforblessings.Server.Modal.ForgotPasswordModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestAcceptedModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.GalleryModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallPostLike;
import com.example.preetisharma.callforblessings.Server.Modal.GroupListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupPostDetailCommentsModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupPostViewModal;
import com.example.preetisharma.callforblessings.Server.Modal.HaleTestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.HelpRequestListModal;
import com.example.preetisharma.callforblessings.Server.Modal.HelpRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.HomeWallModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicMyDownloadsModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSearchModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSongsModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionCheckModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionPlansModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllAlbumModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllSongsModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.LoginModal;
import com.example.preetisharma.callforblessings.Server.Modal.LogoutModal;
import com.example.preetisharma.callforblessings.Server.Modal.MessagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyBooksModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyEventsCreatedModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyInvitationRequestAcceptReject;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationsModal;
import com.example.preetisharma.callforblessings.Server.Modal.OtpModal;
import com.example.preetisharma.callforblessings.Server.Modal.PostDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerIndexModal;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.ProfileModal;
import com.example.preetisharma.callforblessings.Server.Modal.QuestionsModal;
import com.example.preetisharma.callforblessings.Server.Modal.RegisterModal;
import com.example.preetisharma.callforblessings.Server.Modal.RemoveProfileModal;
import com.example.preetisharma.callforblessings.Server.Modal.SendEventInvitationModal;
import com.example.preetisharma.callforblessings.Server.Modal.SendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsGetValueModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsMainModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsSetModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.SinglePostViewModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModalNew;
import com.example.preetisharma.callforblessings.Server.Modal.TimeLineModal;
import com.example.preetisharma.callforblessings.Server.Modal.UPloadBookModal;
import com.example.preetisharma.callforblessings.Server.Modal.UnfriendAFriendModal;
import com.example.preetisharma.callforblessings.Server.Modal.UpdateGroupModal;
import com.example.preetisharma.callforblessings.Server.Modal.UploadPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.UserListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.ValidRegisterationModal;
import com.example.preetisharma.callforblessings.Server.Modal.VideoOnWallModal;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


public interface APIReference {

    String TOKEN = "a152e84173914146e4bc4f391sd0f686ebc4f31";

    /* @Headers("token:" + TOKEN)*/
    @FormUrlEncoded
    @POST("api/user/register")
    Call<RegisterModal> registration(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/user/check-user")
    Call<ValidRegisterationModal> validregistration(@FieldMap Map<String, String> params);


    @FormUrlEncoded
    @POST("api/user/generate-otp")
    Call<OtpModal> generateOTP(@FieldMap Map<String, String> params);


    @FormUrlEncoded
    @POST("api/user/verify-otp")
    Call<OtpModal> verifyOTP(@FieldMap Map<String, String> params);

    @GET("api/user/questions")
    Call<QuestionsModal> questions();

    @FormUrlEncoded
    @POST("api/user/login")
    Call<LoginModal> login(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/user/update")
    Call<ProfileModal> saveProfile(@Header("sessionkey") String header, @FieldMap Map<String, String> params);

    @GET("api/upload/gallery")
    Call<GalleryModal> galleryList(@Header("sessionkey") String header, @Query("type") String type);

    @Multipart
    @POST("api/user/update")
    Call<ProfileModal> saveProfileWithPic(@Header("sessionkey") String header, @Part("UserModel[email]") RequestBody email, @Part("ProfileModel[firstname]") RequestBody firstName, @Part("ProfileModel[lastname]") RequestBody userLastName, @Part("ProfileModel[gender]") RequestBody gender, @Part("ProfileModel[DOB]") RequestBody dob, @Part("ProfileModel[phone]") RequestBody phoneNumber, @Part MultipartBody.Part profile_pic);

    @GET("api/user/home" + "?")
    Call<HomeWallModal> user_home(@Header("sessionkey") String header, @Query("page") String pagenumber);

   /* @GET("api/user/profile")
    Call<TimeLineModal> user_timeLine(@Header("sessionkey") String header);*/

    @FormUrlEncoded
    @POST("api/prayer/request-prayer")
    Call<PrayerRequestModal> request_prayer(@FieldMap Map<String, String> params);

    @Multipart
    @POST("api/prayer/request-prayer")
    Call<PrayerRequestModal> request_prayer_with_image(@Part("Prayer[name]") RequestBody email, @Part("Prayer[title]") RequestBody firstName, @Part("Prayer[desc]") RequestBody userLastName, @Part("Prayer[video_link]") RequestBody gender, @Part MultipartBody.Part profile_pic);

    @FormUrlEncoded
    @POST("api/testimony/request-testimony")
    Call<PrayerRequestModal> request_testimony(@FieldMap Map<String, String> params);

    @Multipart
    @POST("api/testimony/request-testimony")
    Call<PrayerRequestModal> request_testimony_with_image(@Part("Testimony[name]") RequestBody email, @Part("Testimony[title]") RequestBody firstName, @Part("Testimony[desc]") RequestBody userLastName, @Part("Testimony[video_link]") RequestBody gender, @Part MultipartBody.Part profile_pic);


    @FormUrlEncoded
    @POST("api/post/like")
    Call<LikeModal> like(@Header("sessionkey") String headersessionkey, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/post/share")
    Call<ShareModal> share(@Header("sessionkey") String headersessionkey, @FieldMap Map<String, String> params);

    @GET("api/post/get" + "?")
    Call<CommentsListModal> fetchComments(@Header("sessionkey") String header, @Query("id") String postId, @Query("type") String postType);

    @GET("api/post/get" + "?")
    Call<SinglePostViewModal> getSinglePost(@Header("sessionkey") String header, @Query("id") String postId, @Query("type") String postType);

    @FormUrlEncoded
    @POST("api/post/comment")
    Call<CommentModal> comment(@Header("sessionkey") String headersessionkey, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/user/change-password")
    Call<ChangePasswordModal> changePassword(@Header("sessionkey") String headersessionkey, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/user/recover")
    Call<ForgotPasswordModal> forgotPassword(@Field("recovery-form[email]") String emailID);

    @Multipart
    @POST("api/post/create")
    Call<UploadPostModal> create_post_with_image(@Header("sessionkey") String header, @Part("PostMode[post_on]") RequestBody userId, @Part("PostModel[post_content]") RequestBody content, @Part("PostMedia[media_type]") RequestBody mediaType, @Part("PostModel[post_type]") RequestBody postType, @Part("tag_list") RequestBody taggedList, @Part MultipartBody.Part profile_pic);

    @Multipart
    @POST("api/post/create")
    Call<UploadPostModal> create_post_with_video(@Header("sessionkey") String header, @Part("PostMode[post_on]") RequestBody userId, @Part("PostModel[post_content]") RequestBody content, @Part("PostMedia[media_type]") RequestBody mediaType, @Part("PostModel[post_type]") RequestBody postType, @Part("tag_list") RequestBody taggedList, @Part MultipartBody.Part thumbnail_pic, @Part MultipartBody.Part profile_pic);

    @FormUrlEncoded
    @POST("api/post/create")
    Call<UploadPostModal> create_post_without_image(@Header("sessionkey") String headersessionkey, @FieldMap Map<String, String> params);

    @GET("api/prayer/index")
    Call<PrayerIndexModal> getPrayers(@Header("sessionkey") String headersessionkey);

    @GET("api/testimony/index")
    Call<TestimonyModal> getTestimony(@Header("sessionkey") String headersessionkey);

    @GET("api/testimony/index")
    Call<TestimonyModalNew> getTestimonyNew(@Header("sessionkey") String headersessionkey);

    @FormUrlEncoded
    @POST("api/post/amen")
    Call<AmenPrayerModal> amenPrayerPost(@Header("sessionkey") String headersessionkey, @Field("prayer_id") String prayerID);

    @FormUrlEncoded
    @POST("api/post/hale")
    Call<HaleTestimonyModal> haleTestimonyPost(@Header("sessionkey") String headersessionkey, @Field("testimony_id") String testimonyID);

    @GET("api/user/logout")
    Call<LogoutModal> logout(@Header("sessionkey") String header);

    @GET("api/user/check")
    Call<FetchProfileModal> fetchUserProfile(@Header("sessionkey") String header);

    @Multipart
    @POST("api/event/create")
    Call<AddEventModal> createEventWithImage(@Header("sessionkey") String headersessionkey, @Part("Event[event_name]") RequestBody eventName, @Part("Event[event_date]") RequestBody eventDate, @Part("Event[event_location]") RequestBody event_location, @Part("Event[event_desc]") RequestBody event_desc, @Part MultipartBody.Part eventPic);

    @FormUrlEncoded
    @POST("api/event/create")
    Call<AddEventModal> createEvent(@Header("sessionkey") String headersessionkey, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/event/delete")
    Call<DeleteEventModal> deleteEvent(@Header("sessionkey") String headersessionkey, @FieldMap Map<String, String> params);

    @GET("api/upload/books-list")
    Call<BooksListingModal> booksListing(@Header("sessionkey") String header);

    @GET("api/user/index")
    Call<UserListingModal> userListing(@Header("sessionkey") String header);

    @GET("api/event/index")
    Call<EventsListingModal> eventsListing();

    @FormUrlEncoded
    @POST("api/help-request/request-help")
    Call<HelpRequestModal> createHelpRequest(@FieldMap Map<String, String> params);


    @Multipart
    @POST("api/help-request/request-help")
    Call<HelpRequestModal> createHelpRequestWithImage(@Part("HelpRequest[name]") RequestBody email, @Part("HelpRequest[title]") RequestBody firstName, @Part("HelpRequest[description]") RequestBody userLastName, @Part("HelpRequest[video_link]") RequestBody gender, @Part("HelpRequest[email]") RequestBody emaidId, @Part("HelpRequest[phone_number]") RequestBody phoneNumber, @Part("HelpRequest[address]") RequestBody address, @Part MultipartBody.Part profile_pic);


    @GET("api/help-request/index")
    Call<HelpRequestListModal> getHelpRequest(@Header("sessionkey") String header);

    @FormUrlEncoded
    @POST("api/upload/create")
    Call<UPloadBookModal> uploadBooks(@Header("sessionkey") String header, @FieldMap Map<String, String> params);

    @Multipart
    @POST("api/upload/create")
    Call<UPloadBookModal> uploadBooksWithImage(@Header("sessionkey") String header, @Part("Book[name]") RequestBody bookName, @Part("Book[payment_type]") RequestBody bookType, @Part("Book[description]") RequestBody bookDescription, @Part("Book[author_name]") RequestBody author_name, @Part("Book[language]") RequestBody language, @Part("Book[year]") RequestBody year, @Part("Book[amount]") RequestBody bookAmount, @Part("Book[author_other]") RequestBody otherUser, @Part MultipartBody.Part book_coverpic, @Part MultipartBody.Part book_pic1, @Part MultipartBody.Part bookPic2, @Part MultipartBody.Part bookPic3, @Part MultipartBody.Part bookPic4, @Part MultipartBody.Part bookPic5, @Part MultipartBody.Part bookPic6);

    @GET("api/upload/book-detail" + "?")
    Call<BookDetailsModal> getBookDetails(@Header("sessionkey") String header, @Query("id") String bookID);

    @GET("api/user/get")
    Call<TimeLineModal> userTimeLine(@Header("sessionkey") String header, @Query("id") String userId, @Query("page") String pagenumber);

    @FormUrlEncoded
    @POST("api/friend/send-request")
    Call<SendRequestModal> sendRequest(@Header("sessionkey") String header, @FieldMap Map<String, String> params);


    @GET("api/friend/friend-requests")
    Call<FriendRequestModal> friendRequestListing(@Header("sessionkey") String header);

    @GET("api/friend/index")
    Call<MyFriendsModal> friendsListing(@Header("sessionkey") String header);

    @FormUrlEncoded
    @POST("api/friend/request-action")
    Call<FriendRequestAcceptedModal> requestAccepted(@Header("sessionkey") String header, @FieldMap Map<String, String> params);


    @GET("api/event/my")
    Call<MyEventsCreatedModal> myCreatedEvents(@Header("sessionkey") String header);

    @GET("api/event/invites")
    Call<EventsInvitesModal> getEventsInvites(@Header("sessionkey") String header);

    @Multipart
    @POST("api/user/update-pic")
    Call<ChangeProfileCoverPicModal> changeProfileOrCoverPic(@Header("sessionkey") String header, @Part MultipartBody.Part profilePic, @Part MultipartBody.Part coverPic);

    @FormUrlEncoded
    @POST("api/user/remove-pic")
    Call<RemoveProfileModal> removeProfileOrCoverPic(@Header("sessionkey") String header, @FieldMap Map<String, String> params);

    @GET("api/event/get" + "?")
    Call<EventDetailsModal> getEventDetails(@Header("sessionkey") String header, @Query("id") String postID);

    @FormUrlEncoded
    @POST("api/event/send-invite")
    Call<SendEventInvitationModal> sendEventInvite(@Header("sessionkey") String header, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/event/update" + "?")
    Call<EditEventModal> editEventWithoutPic(@Header("sessionkey") String header, @Query("id") String eventID, @FieldMap Map<String, String> params);


    @Multipart
    @POST("api/event/update" + "?")
    Call<EditEventModal> editEventWithPic(@Header("sessionkey") String header, @Query("id") String eventID, @Part("Event[event_name]") RequestBody eventName, @Part("Event[event_date]") RequestBody eventDate, @Part("Event[event_location]") RequestBody eventLocation, @Part("Event[event_desc]") RequestBody eventDescr, @Part MultipartBody.Part profile_pic);


    @FormUrlEncoded
    @POST("api/event/invite-action")
    Call<MyInvitationRequestAcceptReject> acceptOrRejectEventInvite(@Header("sessionkey") String header, @Field("event_id") String eventID, @Field("action") String action);

    @GET("api/user/alarm-req-accept")
    Call<AlarmRequestAcceptModal> alarmAccept(@Header("sessionkey") String header);

    @GET("api/user/req-count")
    Call<AlarmRequestCountModal> alarmRequestCount(@Header("sessionkey") String header);

    @Multipart
    @POST("api/group/create")
    Call<CreateGroupModal> createGroup(@Header("sessionkey") String header, @Part("Groups[group_name]") RequestBody groupName,
                                       @Part("Groups[group_description]") RequestBody groupDescr,
                                       @Part("Groups[is_active]") RequestBody isActive, @Part("GroupsJoin[user_id]") RequestBody friendsJoined,
                                       @Part MultipartBody.Part groupPic);


    @GET("api/group/index")
    Call<GroupListingModal> getGroupsList(@Header("sessionkey") String header);

    @FormUrlEncoded
    @POST("api/group/delete")
    Call<GroupDeleteModal> deleteGroup(@Header("sessionkey") String header, @Field("group_id") String groupID);

    @FormUrlEncoded
    @POST("api/post/delete")
    Call<PostDeleteModal> deletePost(@Header("sessionkey") String header, @Field("post_id") String postID);

    @FormUrlEncoded
    @POST("api/friend/unfriend")
    Call<UnfriendAFriendModal> unfriend(@Header("sessionkey") String header, @Field("friend_id") String friendID);

    @GET("api/upload/joy-music")
    Call<JoyMusicModal> getjoyMusicList(@Header("sessionkey") String header);

    @GET("api/upload/album-detail" + "?")
    Call<AlbumListingModal> getAlbumListing(@Header("sessionkey") String header, @Query("id") String postId);


   /* ////////////////////
    @GET("api/upload/album-detail" + "?")
    Call<AlbumListingModalExoMediaDemo> getAlbumListingExoDemo(@Header("sessionkey") String header, @Query("id") String postId);
    /////////////////////////*/


    @Multipart
    @POST("api/post/update" + "?")
    Call<EditUserPostModal> editUserPost(@Header("sessionkey") String header, @Query("id") String postID, @Part("PostModel[post_on]") RequestBody userId, @Part("PostModel[post_content]") RequestBody content, @Part("PostMedia[media_type]") RequestBody mediaType, @Part("PostModel[post_type]") RequestBody postType, @Part("tag_list") RequestBody taggedList, @Part MultipartBody.Part profile_pic);


    @FormUrlEncoded
    @POST("api/post/update" + "?")
    Call<EditUserPostModal> editUserPostWithoutImage(@Header("sessionkey") String headersessionkey, @Query("id") String postID, @FieldMap Map<String, String> params);


    @GET("api/upload/language-list")
    Call<BooksLanguagesModal> getBooksLanguages(@Header("sessionkey") String header);

    @FormUrlEncoded
    @POST("api/payment/generate-hash" + "?" + "id")
    Call<BookPaymentModal> getBookpaymentHash(@Header("sessionkey") String headersessionkey, @Query("id") String bookId, @Query("type") String type, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/payment/donation")
    Call<BookPaymentModal> getDonationPaymentHash(@Header("sessionkey") String sessionKey, @FieldMap Map<String, String> params);

    @GET("api/upload/my-books")
    Call<MyBooksModal> getMyBooks(@Header("sessionkey") String headersessionkey);

    @GET("api/user/messages")
    Call<MessagesModal> getMessages(@Header("sessionkey") String headersessionkey);

    @GET("api/user/notifications")
    Call<NotificationsModal> getNotifications(@Header("sessionkey") String headersessionkey);

    @GET("api/upload/search-song" + "?" + "q")
    Call<JoyMusicSearchModal> searchMusic(@Header("sessionkey") String headersessionkey, @Query("q") String query);

    @GET("api/upload/songs-list")
    Call<JoyMusicSongsModal> getJoyMusicSongs(@Header("sessionkey") String sessionkey);

    /*@GET("api/upload/view-all-songs" + "?" + "id" + "&" + "page")*/
    @GET("api/upload/view-all-songs")
    Call<JoyMusicViewAllSongsModal> getAllSongsList(@Header("sessionkey") String sessionKey, @QueryMap Map<String, String> queries);


    @GET("api/upload/view-all")
    Call<JoyMusicViewAllAlbumModal> getAllAlbumsList(@Header("sessionkey") String sessionKey, @QueryMap Map<String, String> queries);

    @GET("api/user/get-plans")
    Call<JoyMusicSubscriptionPlansModal> getJoyMusicSubscriptionPlans(@Header("sessionkey") String sessionKey);

    @GET("api/user/check-plan")
    Call<JoyMusicSubscriptionCheckModal> checkJoyMusicSubscription(@Header("sessionkey") String sessionKey);


    @GET("api/payment/buy-music")
    Call<BuyMusicDownloadedModal> buyDownloadMusic(@Header("sessionkey") String sessionKey, @QueryMap Map<String, String> queries);


    @GET("api/user/my-downloads")
    Call<JoyMusicMyDownloadsModal> myDownloads(@Header("sessionkey") String sessionKey);

    @FormUrlEncoded
    @POST("api/payment/free-book")
    Call<BooksBuyFreeBookModal> buyFreeBook(@Header("sessionkey") String sessionKey, @Query("id") String bookId, @FieldMap Map<String, String> params);

    @GET("api/user/home")
    Call<DemoHomeModal> getHomePosts(@Header("sessionkey") String sessionKey, @Query("page") String pageNumber);

    @GET("api/group/get")
    Call<GroupHomeWallModal> getGroupHomeWallPosts(@Header("sessionkey") String sessionKey, @Query("id") String groupId, @Query("page") String pageNumber);

    @FormUrlEncoded
    @POST("api/group/create-post")
    Call<CreateGroupPostModal> createGroupPostWithText(@Header("sessionkey") String sessionKey, @Query("id") String groupId, @FieldMap Map<String, String> params);

    @Multipart
    @POST("api/group/create-post")
    Call<CreateGroupPostModal> createGroupPostWithImageOrVideo(@Header("sessionkey") String sessionKey, @Query("id") String groudID, @Part("GroupWallModel[wall_description]") RequestBody postText, @Part MultipartBody.Part groupPicOrVideo, @Part("GroupWallModel[attachment_type]") RequestBody postType, @Part MultipartBody.Part videoThumbnail);


    @GET("api/group/post-like")
    Call<GroupHomeWallPostLike> likeGroupPost(@Header("sessionkey") String sessionKey, @Query("id") String groupPostId);

    @GET("api/group/post-detail")
    Call<GroupPostDetailCommentsModal> getGroupPostDetailAndComments(@Header("sessionkey") String sessionKey, @Query("id") String groupPostId);

    @FormUrlEncoded
    @POST("api/group/post-comment")
    Call<GroupCommentModal> commentOnGroupPost(@Header("sessionkey") String sessionKey, @Query("id") String groupPostId, @Field("GroupPostCommentModel[comment]") String comment);


    @Multipart
    @POST("api/group/update")
    Call<UpdateGroupModal> updateGroup(@Header("sessionkey") String sessionKey, @Query("id") String groupId, @Part("Groups[group_name]") RequestBody groupName,
                                       @Part("Groups[group_description]") RequestBody groupDescr,
                                       @Part("Groups[is_active]") RequestBody isActive, @Part("GroupsJoin[user_id]") RequestBody friendsJoined,
                                       @Part MultipartBody.Part groupPic);


    @GET("api/upload/video-on-wall")
    Call<VideoOnWallModal> getVideoOnWall(@Header("sessionkey") String sessionKey);

    @GET("api/group/post-detail")
    Call<GroupPostViewModal> viewGroupPost(@Header("sessionkey") String sessionKey, @Query("id") String postId);

    @GET("api/user/get-settings-type")
    Call<SettingsMainModal> getAllSettingsType(@Header("sessionkey") String sessionKey);

    @FormUrlEncoded
    @POST("api/user/set-settings")
    Call<SettingsSetModal> setSettings(@Header("sessionkey") String sessionKey, @FieldMap Map<String, String> params);

    @GET("api/user/get-settings")
    Call<SettingsGetValueModal> getSettingValue(@Header("sessionkey") String sessionKey, @Query("id") String settingId);


   /* @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("login")
    Call<Login> login(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("set_current_location")
    Call<Register> set_location(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_shop_list")
    Call<ShopList> get_shop_list(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_shop_details")
    Call<ShopDetails> get_shop_details(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("product_like")
    Call<Register> product_like(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_product_details")
    Call<ProductData> get_product_details(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("add_to_cart")
    Call<Register> add_to_cart(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("user_wishlist")
    Call<Register> user_wishlist(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("order_product")
    Call<Register> order_product(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_user_wishlist")
    Call<Wishlist> get_user_wishlist(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_cart")
    Call<CartData> get_cart(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_order_list")
    Call<OrderList> get_order_list(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("cancel_order")
    Call<Register> cancel_order(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("return_order")
    Call<Register> return_order(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("report_issue_on_order")
    Call<Register> report_issue_on_order(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_categories")
    Call<CategoryType> get_categories(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_user_profile")
    Call<Profile> get_user_profile(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("apply_referral_code")
    Call<Register> apply_referral_code(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @Multipart
    @POST("update_profile")
    Call<Register> update_profile(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part profile_pic);


    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("update_profile")
    Call<Register> update_profile_without_pic(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_paid_bill_list")
    Call<BillList> get_paid_bill_list(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("myshop")
    Call<SellerShopDetails> myshop(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("seller_delete_product")
    Call<Register> seller_delete_product(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("get_best_selling_product")
    Call<BestSelling> get_best_selling_product(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("edit_product")
    Call<Register> edit_product(@FieldMap Map<String, String> params);


    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("lock_product")
    Call<Register> lock_product(@FieldMap Map<String, String> params);


    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("seller_myorder")
    Call<MyOrder> seller_myorder(@FieldMap Map<String, String> params);


    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("new_order_list")
    Call<NewOrderList> new_order_list(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_custom_order_list")
    Call<CustomOrder> get_custom_order_list(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("edit_shop")
    Call<Register> editshop(@FieldMap Map<String, String> params);

    @Multipart
    @POST("upload_image")
    Call<Register> uploadFile(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part file);

    @Multipart
    @POST("upload_finance_doc")
    Call<Register> upload_finance_doc(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part file);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("update_order")
    Call<Register> update_order(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_invoice_number")
    Call<Register> get_invoice_number(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("get_product_feedback")
    Call<Feedback> get_product_feedback(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("forward_bill")
    Call<Register> forward_bill(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("apply_finance")
    Call<Register> finance_apply(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("pay_to_popular")
    Call<Register> pay_to_popular(@FieldMap Map<String, String> params);

    @Headers("token:" + TOKEN)
    @FormUrlEncoded
    @POST("get_popular_dates")
    Call<Register> get_popular_dates(@FieldMap Map<String, String> params);
*/
}
