package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 1/31/2017.
 */

public class VideosFragment extends Fragment{
    public static VideosFragment newInstance() {

        VideosFragment fragment = new VideosFragment();
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseActivity) getActivity()).dismissDialog();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.notifications, container, false);
        ButterKnife.bind(getActivity());
        ((BaseActivity) getActivity()).getmPrefs();

        return rootView;
    }
}
