package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 5/10/2017.
 */

public class JoyMusicSubscriptionPlansModal {


    /**
     * status : 1
     * list : [{"id":1,"amount":"1","offer_amount":"","time_period":"1 Month Plan"},{"id":2,"amount":"2","offer_amount":"","time_period":"3 Month Plan"},{"id":3,"amount":"3","offer_amount":"","time_period":"6 Month Plan"},{"id":4,"amount":"4","offer_amount":"","time_period":"1 Year Plan"}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 1
         * amount : 1
         * offer_amount :
         * time_period : 1 Month Plan
         */

        private int id;
        private String amount;
        private String offer_amount;
        private String time_period;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getOffer_amount() {
            return offer_amount;
        }

        public void setOffer_amount(String offer_amount) {
            this.offer_amount = offer_amount;
        }

        public String getTime_period() {
            return time_period;
        }

        public void setTime_period(String time_period) {
            this.time_period = time_period;
        }
    }
}
