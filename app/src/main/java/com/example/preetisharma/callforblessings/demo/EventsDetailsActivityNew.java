package com.example.preetisharma.callforblessings.demo;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.HomeActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DeleteEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventDetailsModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyInvitationRequestAcceptReject;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/8/2017.
 */

public class EventsDetailsActivityNew extends BaseActivity implements APIServerResponse {

    String postID, eventTypeFlag, eventInviteStatus, notifyFlag;
    ImageView event_image;
    Toolbar main_toolbar;
    FloatingActionButton fab_event_status;
    AppCompatTextView tv_event_host_info, tv_event_location, txtvw_event_invite_all_count, txtvw_event_invite_accepted_count,
            txtvw_event_invite_pending_count, tv_event_detail_description, tv_event_detail_description_time;

    CollapsingToolbarLayout main_collapsing_bar_tab_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_details_new);
        main_collapsing_bar_tab_layout = (CollapsingToolbarLayout) findViewById(R.id.main_collapsing_bar_tab_layout);
        main_collapsing_bar_tab_layout.setTitle("");

        if (getIntent().getExtras() != null) {
            postID = getIntent().getExtras().getString(Constants.EVENT_ID);
            eventTypeFlag = getIntent().getExtras().getString(Constants.EVENT_TYPE_FLAG);
            eventInviteStatus = getIntent().getExtras().getString(Constants.EVENT_INVITE_STATUS);
            if (getIntent().getExtras().getString("Notification_Flag") != null && !getIntent().getExtras().getString("Notification_Flag").equalsIgnoreCase("")) {
                notifyFlag = getIntent().getExtras().getString("Notification_Flag");
            } else {
                notifyFlag = "";
            }
            //showToast(notifyFlag, Toast.LENGTH_LONG);
        }

        main_toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        event_image = (ImageView) findViewById(R.id.event_image);
        fab_event_status = (FloatingActionButton) findViewById(R.id.fab_event_status);
        tv_event_host_info = (AppCompatTextView) findViewById(R.id.tv_event_host_info);
        tv_event_location = (AppCompatTextView) findViewById(R.id.tv_event_location);
        txtvw_event_invite_all_count = (AppCompatTextView) findViewById(R.id.txtvw_event_invite_all_count);
        txtvw_event_invite_accepted_count = (AppCompatTextView) findViewById(R.id.txtvw_event_invite_accepted_count);
        txtvw_event_invite_pending_count = (AppCompatTextView) findViewById(R.id.txtvw_event_invite_pending_count);
        tv_event_detail_description = (AppCompatTextView) findViewById(R.id.tv_event_detail_description);
        tv_event_detail_description_time = (AppCompatTextView) findViewById(R.id.tv_event_detail_description_time);

        if (eventTypeFlag.equals("My Events")) {
            /*fab_event_status.setVisibility(View.INVISIBLE);*/
            Glide.with(EventsDetailsActivityNew.this).load(R.drawable.ic_overflow_more).placeholder(R.drawable.placeholder_callforblessings).into(fab_event_status);
        } else {
            /*fab_event_status.setVisibility(View.VISIBLE);*/
            if (eventInviteStatus.equals("ACCEPTED")) {
                Glide.with(EventsDetailsActivityNew.this).load(R.drawable.ic_going).placeholder(R.drawable.placeholder_callforblessings)/*.centerCrop()*/.into(fab_event_status);
            } else if (eventInviteStatus.equals("REJECTED")) {
                Glide.with(EventsDetailsActivityNew.this).load(R.drawable.ic_not_going).placeholder(R.drawable.placeholder_callforblessings)/*.centerCrop()*/.into(fab_event_status);
            } else {
                Glide.with(EventsDetailsActivityNew.this).load(R.drawable.ic_pending).placeholder(R.drawable.placeholder_callforblessings)/*.centerCrop()*/.into(fab_event_status);
            }
        }

        try {
            setSupportActionBar(main_toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            main_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventsDetailsActivityNew.this.finish();
                }
            });

            fab_event_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //accept reject event invite
                    if (eventTypeFlag.equals("My Events")) {
                        extraDetailsDialog(R.drawable.ic_edit, R.drawable.delete);
                    } else {
                        if (eventInviteStatus.equals("PENDING")) {
                            extraAcceptRejetcDetailsDialog(R.drawable.ic_accept, R.drawable.ic_reject);
                        } else if (eventInviteStatus.equals("ACCEPTED")) {
                            showToast("Invite already accepted.", Toast.LENGTH_SHORT);
                        } else if (eventInviteStatus.equals("REJECTED")) {
                            showToast("Invite already rejected.", Toast.LENGTH_SHORT);
                        }

                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getEventDetails(APIServerResponse.EVENT_DETAILS, getUserSessionId(), postID, this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {

            if (response.isSuccessful()) {

                EventDetailsModal eventDetailsModal;
                MyInvitationRequestAcceptReject myInvitationRequestAcceptReject;
                DeleteEventModal deleteEvent;
                Resources resources = getResources();

                switch (tag) {
                    case APIServerResponse.EVENT_DETAILS:
                        eventDetailsModal = (EventDetailsModal) response.body();
                        if (eventDetailsModal.getStatus().equals("1")) {

                            tv_event_location.setText(eventDetailsModal.getDetail().getEvent_location());
                            main_collapsing_bar_tab_layout.setTitle(eventDetailsModal.getDetail().getEvent_name());
                            main_toolbar.setTitle(eventDetailsModal.getDetail().getEvent_name());
                            tv_event_detail_description.setText(eventDetailsModal.getDetail().getEvent_desc());
                            tv_event_detail_description_time.setText(eventDetailsModal.getDetail().getEvent_date());
                            txtvw_event_invite_all_count.setText(eventDetailsModal.getDetail().getAll_count());
                            txtvw_event_invite_accepted_count.setText(eventDetailsModal.getDetail().getAccepted_count());
                            txtvw_event_invite_pending_count.setText(eventDetailsModal.getDetail().getPending_count());

                            String text = String.format(resources.getString(R.string.two_parameters), eventDetailsModal.getDetail().getUser_info().getProfile_details().getFirstname(), eventDetailsModal.getDetail().getUser_info().getProfile_details().getLastname());
                            tv_event_host_info.setText(text);
                            getSupportActionBar().setTitle(eventDetailsModal.getDetail().getEvent_name());
                            if (!eventDetailsModal.getDetail().getEvent_photo().equals("")) {
                                Glide.with(EventsDetailsActivityNew.this).load(eventDetailsModal.getDetail().getEvent_photo()).thumbnail(0.1f)/*.placeholder(R.drawable.placeholder_callforblessings)*/.into(event_image);

                            } else {
                                Glide.with(EventsDetailsActivityNew.this).load(R.drawable.placeholder_callforblessings).into(event_image);
                            }

                            hideLoading();
                        } else {
                            hideLoading();
                            showToast(response.message(), Toast.LENGTH_SHORT);
                        }

                        break;
                    case APIServerResponse.EVENT_INVITE_REQUEST_RESPONSE:

                        myInvitationRequestAcceptReject = (MyInvitationRequestAcceptReject) response.body();
                        if (myInvitationRequestAcceptReject.getStatus().equals("1")) {
                            if (myInvitationRequestAcceptReject.getMessage().contains("ACCEPTED")) {
                                showToast(myInvitationRequestAcceptReject.getMessage(), Toast.LENGTH_LONG);
                                Glide.with(EventsDetailsActivityNew.this).load(R.drawable.ic_going).placeholder(R.drawable.placeholder)/*.centerCrop()*/.into(fab_event_status);
                                finish();

                            } else {
                                showToast(myInvitationRequestAcceptReject.getMessage(), Toast.LENGTH_LONG);
                                EventsDetailsActivityNew.this.finish();
                            }
                        } else {
                            showToast("Unable to respond to event invite.", Toast.LENGTH_LONG);
                        }
                        hideLoading();
                        break;

                    case APIServerResponse.DELETEEVENT:
                        deleteEvent = (DeleteEventModal) response.body();
                        if (deleteEvent.getStatus().equals("1")) {
                            showToast("Event Deleted", Toast.LENGTH_SHORT);
                            EventsDetailsActivityNew.this.finish();
                        }
                        hideLoading();
                        break;
                }

            } else {
                hideLoading();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }


    private void extraDetailsDialog(int imageAcceptOrEdit, int imageRejectOrDelete) {
        try {
            final Dialog dialog = new Dialog(EventsDetailsActivityNew.this);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            //dialog.setTitle(R.string.title_activity_product_detail);
            dialog.setContentView(R.layout.custom_dialog_event_invite);

            FloatingActionButton fab_event_accept = (FloatingActionButton) dialog.findViewById(R.id.fab_event_accept);
            FloatingActionButton fab_event_reject = (FloatingActionButton) dialog.findViewById(R.id.fab_event_reject);
            Glide.with(EventsDetailsActivityNew.this).load(imageAcceptOrEdit).into(fab_event_accept);
            Glide.with(EventsDetailsActivityNew.this).load(imageRejectOrDelete).into(fab_event_reject);
            AppCompatTextView txtvw_event_invitation_cancel = (AppCompatTextView) dialog.findViewById(R.id.txtvw_event_invitation_cancel);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/


            txtvw_event_invitation_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            fab_event_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //accept_event or Edit  event

                        Intent i = new Intent(EventsDetailsActivityNew.this, EventEditActivity.class);
                        i.putExtra(Constants.EVENT_ID, postID);
                        startActivity(i);
                        dialog.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            fab_event_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //reject_event or delete even

                        if (isConnectedToInternet()) {
                            dialog.dismiss();
                            ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, getUserSessionId(), postID, EventsDetailsActivityNew.this);
                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void extraAcceptRejetcDetailsDialog(int imageAcceptOrEdit, int imageRejectOrDelete) {
        try {
            final Dialog dialog = new Dialog(EventsDetailsActivityNew.this);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            //dialog.setTitle(R.string.title_activity_product_detail);
            dialog.setContentView(R.layout.custom_dialog_event_invite);

            FloatingActionButton fab_event_accept = (FloatingActionButton) dialog.findViewById(R.id.fab_event_accept);
            FloatingActionButton fab_event_reject = (FloatingActionButton) dialog.findViewById(R.id.fab_event_reject);
            Glide.with(EventsDetailsActivityNew.this).load(imageAcceptOrEdit).into(fab_event_accept);
            Glide.with(EventsDetailsActivityNew.this).load(imageRejectOrDelete).into(fab_event_reject);
            AppCompatTextView txtvw_event_invitation_cancel = (AppCompatTextView) dialog.findViewById(R.id.txtvw_event_invitation_cancel);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/


            txtvw_event_invitation_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            fab_event_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //accept_event or Edit  event
                        if (isConnectedToInternet()) {
                            showLoading();
                            dialog.dismiss();
                            ServerAPI.getInstance().acceptOrRejectEventInvitation(APIServerResponse.EVENT_INVITE_REQUEST_RESPONSE, getUserSessionId(), postID, "ACCEPTED", EventsDetailsActivityNew.this);
                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            fab_event_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //reject_event or delete even

                        if (isConnectedToInternet()) {
                            showLoading();
                            dialog.dismiss();
                            ServerAPI.getInstance().acceptOrRejectEventInvitation(APIServerResponse.EVENT_INVITE_REQUEST_RESPONSE, getUserSessionId(), postID, "REJECTED", EventsDetailsActivityNew.this);
                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (eventTypeFlag.equals("My Events")) {
            /*fab_event_status.setVisibility(View.INVISIBLE);*/
            ServerAPI.getInstance().getEventDetails(APIServerResponse.EVENT_DETAILS, getUserSessionId(), postID, this);
        }
    }

    @Override
    public void onBackPressed() {


        if (!notifyFlag.equalsIgnoreCase("") && notifyFlag.equals("true")) {
            Intent i = new Intent(EventsDetailsActivityNew.this, HomeActivity.class);
            startActivity(i);
            finish();
        } else {
            super.onBackPressed();
        }

    }
}
