package com.example.preetisharma.callforblessings;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.WindowManager;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.ForgotPasswordModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by satoti.garg on 1/17/2017.
 */

public class ForgotPassword extends BaseActivity implements APIServerResponse {

    @BindView(R.id.edt_txt_email)
    AppCompatEditText edt_txt_email;
    @BindView(R.id.txtvw_retrieve)
    AppCompatTextView txtvw_retrieve;

    private int RC_INTERNET = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @OnClick(R.id.txtvw_retrieve)
    public void retrievePassword() {

        try {
            if (Validation(edt_txt_email.getText().toString())) {

                if (EasyPermissions.hasPermissions(this, Manifest.permission.INTERNET)) {
                    if (isConnectedToInternet()) {
                        showLoading();
                        ServerAPI.getInstance().forgotPassword(APIServerResponse.CHANGE_PASSWORD, edt_txt_email.getText().toString().trim(), this);
                    } else {
                        showSnack("Not connected to Internet ");
                    }
                } else {
                    EasyPermissions.requestPermissions(this, getString(R.string.internet_access),
                            RC_INTERNET, Manifest.permission.INTERNET);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean Validation(String email) {
        if (email.isEmpty()) {
            showToast(Constants.EMAIL_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (!isValidEmail(email)) {
            showToast(Constants.EMAIL_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        }
        return true;


    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {
            hideLoading();
            if (response.isSuccessful()) {
                ForgotPasswordModal forgotPassword = (ForgotPasswordModal) response.body();
                if (forgotPassword.getStatus().equals("1")) {
                    showToast(forgotPassword.getMessage(), Toast.LENGTH_SHORT);
                    Intent i = new Intent(ForgotPassword.this, SignInActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    showToast(forgotPassword.getMessage(), Toast.LENGTH_SHORT);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        switch (tag) {
            case APIServerResponse.FORGOT_PASSWORD:
                System.out.println("Error");
                break;
        }
    }
}
