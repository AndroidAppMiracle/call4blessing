package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 2/28/2017.
 */

public class BookDetailsModal {


    /**
     * status : 1
     * detail : {"id":32,"name":"Miracle","author_name":"Miracle","author_other":"Miracle Technologies","description":"Miracle Studios","payment_type":"FREE","language":"English","year":"2017","status":0,"amount":0,"created_at":"2017-05-03","file":[{"name":"http://www.call4blessing.com/uploads/books/1493812856Chrysanthemum.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Desert.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Hydrangeas.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Jellyfish.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Koala.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Penguins.jpg"}],"book_cover":"http://www.call4blessing.com/uploads/books/1493812856Lighthouse.jpg","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"2001-11-17","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1492681953Calvin_Harris_and_Disciples_-_How_Deep_Is_Your_Love.png"},"is_friend":0,"request_respond":"NO"},"purchased_by_current_user":"false"}
     */

    private String status;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * id : 32
         * name : Miracle
         * author_name : Miracle
         * author_other : Miracle Technologies
         * description : Miracle Studios
         * payment_type : FREE
         * language : English
         * year : 2017
         * status : 0
         * amount : 0
         * created_at : 2017-05-03
         * file : [{"name":"http://www.call4blessing.com/uploads/books/1493812856Chrysanthemum.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Desert.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Hydrangeas.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Jellyfish.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Koala.jpg"},{"name":"http://www.call4blessing.com/uploads/books/1493812856Penguins.jpg"}]
         * book_cover : http://www.call4blessing.com/uploads/books/1493812856Lighthouse.jpg
         * user_info : {"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"2001-11-17","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1492681953Calvin_Harris_and_Disciples_-_How_Deep_Is_Your_Love.png"},"is_friend":0,"request_respond":"NO"}
         * purchased_by_current_user : false
         */

        private int id;
        private String name;
        private String author_name;
        private String author_other;
        private String description;
        private String payment_type;
        private String language;
        private String year;
        private int status;
        private String amount;
        private String created_at;
        private String book_cover;
        private UserInfoBean user_info;
        private String purchased_by_current_user;
        private List<FileBean> file;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAuthor_name() {
            return author_name;
        }

        public void setAuthor_name(String author_name) {
            this.author_name = author_name;
        }

        public String getAuthor_other() {
            return author_other;
        }

        public void setAuthor_other(String author_other) {
            this.author_other = author_other;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getBook_cover() {
            return book_cover;
        }

        public void setBook_cover(String book_cover) {
            this.book_cover = book_cover;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public String getPurchased_by_current_user() {
            return purchased_by_current_user;
        }

        public void setPurchased_by_current_user(String purchased_by_current_user) {
            this.purchased_by_current_user = purchased_by_current_user;
        }

        public List<FileBean> getFile() {
            return file;
        }

        public void setFile(List<FileBean> file) {
            this.file = file;
        }

        public static class UserInfoBean {
            /**
             * id : 214
             * username : plash.jindal
             * email : plash.jindal@emptask.com
             * updated : 1
             * profile_details : {"firstname":"Palash","lastname":"Jindal","d_o_b":"2001-11-17","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1492681953Calvin_Harris_and_Disciples_-_How_Deep_Is_Your_Love.png"}
             * is_friend : 0
             * request_respond : NO
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : Palash
                 * lastname : Jindal
                 * d_o_b : 2001-11-17
                 * phone : 1234567890
                 * gender : MALE
                 * location :
                 * country : Spain
                 * state : Madrid
                 * cover_pic : http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg
                 * profile_pic : http://www.call4blessing.com/uploads/profilepic/1492681953Calvin_Harris_and_Disciples_-_How_Deep_Is_Your_Love.png
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }

        public static class FileBean {
            /**
             * name : http://www.call4blessing.com/uploads/books/1493812856Chrysanthemum.jpg
             */

            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
