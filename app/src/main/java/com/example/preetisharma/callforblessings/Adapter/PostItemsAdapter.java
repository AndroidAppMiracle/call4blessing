package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.PostDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;
import com.example.preetisharma.callforblessings.demo.EditUserPostActivity;
import com.example.preetisharma.callforblessings.demo.SharePostActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 9/19/2017.
 */

public class PostItemsAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{


    Activity mContext;
    public PostItemsAdapter(Activity mContext) {

        this.mContext = mContext;



    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.homescreen_item_adapter_layout, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder /*implements APIServerResponse*/ {
        @BindView(R.id.txtvw_tagged_friends)
        AppCompatTextView txtvw_tagged_friends;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;
        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;
        @BindView(R.id.viewholder_image)
        AppCompatImageView viewholder_image;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;
        @BindView(R.id.tv_changed_profile_cover_pic)
        AppCompatTextView tv_changed_profile_cover_pic;
        @BindView(R.id.video_play)
        AppCompatImageView video_play;

        //Shared Post
        @BindView(R.id.card_shared_post)
        CardView card_shared_post;
        @BindView(R.id.img_vw_user_profile_shared)
        AppCompatImageView img_vw_user_profile_shared;
        @BindView(R.id.txtvw_user_name_shared)
        AppCompatTextView txtvw_user_name_shared;
        @BindView(R.id.txtvw_time_stamp_shared)
        AppCompatTextView txtvw_time_stamp_shared;
        @BindView(R.id.txtvw_post_details_shared)
        AppCompatTextView txtvw_post_details_shared;
        @BindView(R.id.viewholder_image_shared)
        AppCompatImageView viewholder_image_shared;

        @BindView(R.id.tv_changed_profile_cover_pic_shared)
        AppCompatTextView tv_changed_profile_cover_pic_shared;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.iv_more_options)
        public void deleteeditPopup() {

            try {
           //     createPopupMenuOption();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

/*

        @OnClick(R.id.txtvw_like)
        public void likePost() {

            if (txtvw_like.isEnabled()) {
                txtvw_like.setEnabled(false);
            }
            liked = list.get(getAdapterPosition()).getLike_flag();
            likescount = list.get(getAdapterPosition()).getLike_count();

            if (liked.equalsIgnoreCase("Not Liked")) {
                int imgResource = R.drawable.ic_liked;
                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) mContext).isConnectedToInternet()) {

                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), "POST", HomeWallAdapter.DataViewHolder.this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }

            } else {
                int imgResource = R.drawable.prayer_like_icon;
                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) mContext).isConnectedToInternet()) {

                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), "POST", HomeWallAdapter.DataViewHolder.this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {

            if (list.size() > 0 && list.get(getAdapterPosition()).getMedia().size() > 0) {
                if (list.get(getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Image")) {
                    openConfirmationDialog(String.valueOf(list.get(getAdapterPosition()).getId()), list.get(getAdapterPosition()).getPost_content(), list.get(getAdapterPosition()).getPosted_at(), list.get(getAdapterPosition()).getMedia().get(0).getFile(), "", "");
                } else if (list.get(getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Video")) {
                    openConfirmationDialog(String.valueOf(list.get(getAdapterPosition()).getId()), list.get(getAdapterPosition()).getPost_content(), list.get(getAdapterPosition()).getPosted_at(), "", list.get(getAdapterPosition()).getMedia().get(0).getFile(), list.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());

                }
            } else {
                openConfirmationDialog(String.valueOf(list.get(getAdapterPosition()).getId()), list.get(getAdapterPosition()).getPost_content(), list.get(getAdapterPosition()).getPosted_at(), "", "", "");

            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentPost() {


            Intent i = new Intent(mContext, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(list.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            i.putExtras(b);
            mContext.startActivity(i);


        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                LikeModal likeModal;
                ShareModal shareModal;
                CommentModal commentModal;
                PostDeleteModal postDeleteModal;
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    switch (tag) {
                        case APIServerResponse.LIKE:

                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like.isEnabled()) {
                                    txtvw_like.setEnabled(true);
                                }
                                int imgResource = R.drawable.ic_liked;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue += 1;
                                liked = "liked";
                                list.get(getAdapterPosition()).setLike_flag("liked");
                                list.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));
                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                                } else {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                                }
                            }

                            break;
                        case APIServerResponse.UNLIKE:
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like.isEnabled()) {
                                    txtvw_like.setEnabled(true);
                                }
                                int imgResource = R.drawable.prayer_like;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue--;
                                liked = "not liked";
                                list.get(getAdapterPosition()).setLike_flag("not liked");
                                list.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                                } else {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                                }
                            }
                            break;
                        case APIServerResponse.SHARE:

                            shareModal = (ShareModal) response.body();
                            if (shareModal.getStatus().equals("1")) {
                                */
/*shareModal.getDetail().*//*

                                ((BaseActivity) mContext).showToast("Shared successfully", Toast.LENGTH_SHORT);

                                notifyDataSetChanged();
                            }

                            break;
                        case APIServerResponse.COMMENT:

                            commentModal = (CommentModal) response.body();
                            if (commentModal.getStatus().equals("1")) {
                                */
/*shareModal.getDetail().*//*

                                ((BaseActivity) mContext).showToast("Success", Toast.LENGTH_SHORT);
                                notifyDataSetChanged();
                            }

                            break;

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }

                            break;
                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

*/

 /*       public void openConfirmationDialog(String postId, String post_text, String timeStamp, String post_image, String postVideoUrl, String postVideoThumbnail) {

            Intent shareDialog = new Intent(mContext, SharePostActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.POSTTYPE, "POST");
            b.putString(Constants.POSTIMAGE, post_image);
            b.putString(Constants.POSTTEXT, post_text);
            b.putString(Constants.POSTTIMESTAMP, timeStamp);
            b.putString(Constants.POSTID, postId);
            b.putString(Constants.POSTVIDEO, postVideoUrl);
            b.putString(Constants.POSTTHUMBNAIL, postVideoThumbnail);
            shareDialog.putExtras(b);
            mContext.startActivity(shareDialog);


        }
*/
 /*       @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.LIKE:
                    System.out.println("Error");
                    break;
                case APIServerResponse.SHARE:
                    System.out.println("Error");
                    break;
            }
        }
*/

   /*     public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(mContext, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) mContext).isConnectedToInternet()) {
                                    ((BaseActivity) mContext).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), HomeWallAdapter.DataViewHolder.this);
                                } else {
                                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(mContext, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(list.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, list.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + list.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, list.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(list.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, list.get(getAdapterPosition()).getPost_type());
                                if (list.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(list.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                mContext.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        public void refresh(int position) {
            Log.e("Refreshed Position", "Pos" + position);
            list.remove(position);
            notifyItemRemoved(position);
        }

       *//* txtvw_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServerAPI.getInstance().share(APIServerResponse.SHARE, new BaseActivity().getUserSessionId(), String.valueOf(list.get(position).getId()), "POST", "Shared Text", DataViewHolder);
            }
        });*/
    }
}
