package com.example.preetisharma.callforblessings.Utils;

/**
 * Created by preeti.sharma on 1/9/2017.
 */

public class DataHolder {


    public static String getFirst_name() {
        return first_name;
    }

    public static void setFirst_name(String first_name) {
        DataHolder.first_name = first_name;
    }

    public static String getLast_name() {
        return last_name;
    }

    public static void setLast_name(String last_name) {
        DataHolder.last_name = last_name;
    }

    public static String getPhone_number() {
        return phone_number;
    }

    public static void setPhone_number(String phone_number) {
        DataHolder.phone_number = phone_number;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        DataHolder.email = email;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        DataHolder.password = password;
    }

    public static String getGender() {
        return gender;
    }

    public static void setGender(String gender) {
        DataHolder.gender = gender;
    }

    public static String getDate_of_birth() {
        return date_of_birth;
    }

    public static void setDate_of_birth(String date_of_birth) {
        DataHolder.date_of_birth = date_of_birth;
    }

    public static String getSession_key() {
        return session_key;
    }

    public static void setSession_key(String session_key) {
        DataHolder.session_key = session_key;
    }

    private static String session_key;
    private static String first_name;
    private static String last_name;
    private static String phone_number;
    private static String email;
    private static String password;
    private static String gender;
    private static String date_of_birth;

    public static String getProfilePic() {
        return profilePic;
    }

    public static void setProfilePic(String profilePic) {
        DataHolder.profilePic = profilePic;
    }

    private static String profilePic;


}
