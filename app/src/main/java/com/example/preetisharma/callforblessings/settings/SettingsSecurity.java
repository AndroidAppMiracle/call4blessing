package com.example.preetisharma.callforblessings.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.ChangePassword;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satoti.garg on 7/20/2017.
 */

public class SettingsSecurity extends BaseActivity {

    @BindView(R.id.rv_security_settings)
    RecyclerView rv_security_settings;

    @BindView(R.id.atv_change_password)
    AppCompatTextView atv_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_security);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Security Settings");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }

    @OnClick(R.id.atv_change_password)
    public void changePassword()
    {
        Intent intent=new Intent(this,ChangePassword.class);
        startActivity(intent);
    }
}
