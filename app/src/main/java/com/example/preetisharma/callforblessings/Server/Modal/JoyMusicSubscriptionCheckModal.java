package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 5/12/2017.
 */

public class JoyMusicSubscriptionCheckModal {


    /**
     * status : 0
     * valid : false
     */

    private String status;
    private String valid;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }
}
