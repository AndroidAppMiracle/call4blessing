package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 5/9/2017.
 */

public class JoyMusicViewAllAlbumModal {


    /**
     * status : 1
     * list : [{"id":2,"name":"Calvin Harris n Disciples","description":"Calvin Harris n Disciples","language":"English","payment_type":"FREE","amount":"","created_at":"3 weeks ago","is_featured":"NO","album_cover_image":"http://call4blessing.com/uploads/album/1493275729Lighthouse.jpg","is_downloaded":"false"}]
     * page : 1
     * totalPages : 1
     */

    private String status;
    private int page;
    private int totalPages;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 2
         * name : Calvin Harris n Disciples
         * description : Calvin Harris n Disciples
         * language : English
         * payment_type : FREE
         * amount :
         * created_at : 3 weeks ago
         * is_featured : NO
         * album_cover_image : http://call4blessing.com/uploads/album/1493275729Lighthouse.jpg
         * is_downloaded : false
         */

        private int id;
        private String name;
        private String description;
        private String language;
        private String payment_type;
        private String amount;
        private String created_at;
        private String is_featured;
        private String album_cover_image;
        private String is_downloaded;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getIs_featured() {
            return is_featured;
        }

        public void setIs_featured(String is_featured) {
            this.is_featured = is_featured;
        }

        public String getAlbum_cover_image() {
            return album_cover_image;
        }

        public void setAlbum_cover_image(String album_cover_image) {
            this.album_cover_image = album_cover_image;
        }

        public String getIs_downloaded() {
            return is_downloaded;
        }

        public void setIs_downloaded(String is_downloaded) {
            this.is_downloaded = is_downloaded;
        }
    }
}
