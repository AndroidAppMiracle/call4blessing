package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by preeti.sharma on 1/6/2017.
 */

public class QuestionsModal {
    /**
     * status : 1
     * list : [{"id":5,"question_title":"Are you believe in Nature?","option_a":"Yes","option_b":"No","option_c":"","option_d":"","question_type":"OPTIONAL","right_answer":""},{"id":4,"question_title":"Are you believe in Help Others?","option_a":"Yes","option_b":"No","option_c":"","option_d":"","question_type":"OPTIONAL","right_answer":""},{"id":3,"question_title":"Are you believe in Social prayers?","option_a":"Yes","option_b":"No","option_c":"","option_d":"","question_type":"OPTIONAL","right_answer":""},{"id":2,"question_title":"Are you believe in God?","option_a":"Yes ","option_b":"No","option_c":"","option_d":"","question_type":"OPTIONAL","right_answer":""},{"id":1,"question_title":"Are you interested in prayers?","option_a":"Yes","option_b":"No","option_c":"","option_d":"","question_type":"OPTIONAL","right_answer":""}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 5
         * question_title : Are you believe in Nature?
         * option_a : Yes
         * option_b : No
         * option_c :
         * option_d :
         * question_type : OPTIONAL
         * right_answer :
         */

        private int id;
        private String question_title;
        private String option_a;
        private String option_b;
        private String option_c;
        private String option_d;
        private String question_type;
        private String right_answer;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getQuestion_title() {
            return question_title;
        }

        public void setQuestion_title(String question_title) {
            this.question_title = question_title;
        }

        public String getOption_a() {
            return option_a;
        }

        public void setOption_a(String option_a) {
            this.option_a = option_a;
        }

        public String getOption_b() {
            return option_b;
        }

        public void setOption_b(String option_b) {
            this.option_b = option_b;
        }

        public String getOption_c() {
            return option_c;
        }

        public void setOption_c(String option_c) {
            this.option_c = option_c;
        }

        public String getOption_d() {
            return option_d;
        }

        public void setOption_d(String option_d) {
            this.option_d = option_d;
        }

        public String getQuestion_type() {
            return question_type;
        }

        public void setQuestion_type(String question_type) {
            this.question_type = question_type;
        }

        public String getRight_answer() {
            return right_answer;
        }

        public void setRight_answer(String right_answer) {
            this.right_answer = right_answer;
        }
    }
}
