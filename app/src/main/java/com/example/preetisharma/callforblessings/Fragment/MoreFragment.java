package com.example.preetisharma.callforblessings.Fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.ProfileActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.LogoutModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.SignInActivity;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.BookTabs;
import com.example.preetisharma.callforblessings.demo.DonationActivity;
import com.example.preetisharma.callforblessings.demo.EventsTabs;
import com.example.preetisharma.callforblessings.demo.GalleryActivity;
import com.example.preetisharma.callforblessings.demo.GroupsList;
import com.example.preetisharma.callforblessings.demo.JoyMusicActivity;
import com.example.preetisharma.callforblessings.demo.MapsActivity;
import com.example.preetisharma.callforblessings.demo.MyMessages;
import com.example.preetisharma.callforblessings.demo.SettingOptionListActivity;
import com.example.preetisharma.callforblessings.demo.SettingsMainActivity;
import com.example.preetisharma.callforblessings.pages.CreatePageFirstActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/12/2017.
 */

public class MoreFragment extends Fragment implements APIServerResponse {
    /*  @BindView(R.id.txtvw_prayer)
      AppCompatTextView txtvw_prayer;
      @BindView(R.id.txtvw_testimony)
      AppCompatTextView txtvw_testimony;*/
    @BindView(R.id.txtvw_update_profile)
    AppCompatTextView txtvw_update_profile;
    @BindView(R.id.txtvw_logout)
    AppCompatTextView txtvw_logout;
    @BindView(R.id.txtvw_events)
    AppCompatTextView txtvw_events;
    @BindView(R.id.txtvw_books)
    AppCompatTextView txtvw_books;
    @BindView(R.id.txtvw_gallery)
    AppCompatTextView txtvw_gallery;
    @BindView(R.id.txtvw_joy_music)
    AppCompatTextView txtvw_joy_music;
    @BindView(R.id.txtvw_groups)
    AppCompatTextView txtvw_groups;
    //txtvw_create_page
    @BindView(R.id.txtvw_create_page)
    AppCompatTextView txtvw_create_page;
    @BindView(R.id.txtvw_messages)
    AppCompatTextView txtvw_messages;
    @BindView(R.id.txtvw_Donations)
    AppCompatTextView txtvw_Donations;

    @BindView(R.id.txtvw_settings)
    AppCompatTextView txtvw_settings;
  /*  @BindView(R.id.txtvw_helpRequest)
    AppCompatTextView txtvw_helpRequest;*/

    public static MoreFragment newInstance() {

        MoreFragment fragment = new MoreFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_more, container, false);
        ButterKnife.bind(this, rootView);
        ((BaseActivity) getActivity()).getmPrefs();

        Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.create_page);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * 0.5),
                (int) (drawable.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd = new ScaleDrawable(drawable, 0, 50, 20);
        txtvw_create_page.setCompoundDrawables(sd.getDrawable(), null, null, null);


        txtvw_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    ((BaseActivity) getActivity()).showLoading();
                    ServerAPI.getInstance().logOut(APIServerResponse.LOGOUT, ((BaseActivity) getActivity()).getUserSessionId(), MoreFragment.this);
                } else {
                    ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        });

        return rootView;
    }

    @OnClick(R.id.txtvw_create_page)
    public void createPageTv() {
        Intent intent = new Intent(getActivity(), CreatePageFirstActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtvw_gallery)
    public void txtvw_gallery() {
        /*Intent intent = new Intent(getActivity(), DemoVideoIntent.class);*/
        Intent intent = new Intent(getActivity(), GalleryActivity.class);
        startActivity(intent);

    }

    @OnClick(R.id.txtvw_joy_music)
    public void txtvw_joy_music() {
        Intent intent = new Intent(getActivity(), JoyMusicActivity.class);
        startActivity(intent);

    }

    @OnClick(R.id.txtvw_Donations)
    public void giveDonation() {
        /*Intent intent = new Intent(getActivity(), DemoContextualMenuAndSwipe.class);*/
        Intent intent = new Intent(getActivity(), DonationActivity.class);
        startActivity(intent);
    }

    /*@OnClick(R.id.txtvw_prayer)
    public void prayerListing() {
        Intent intent = new Intent(getActivity(), PrayerActivity.class);
        startActivity(intent);


    }


*/
    @OnClick(R.id.txtvw_groups)
    public void groups() {
        /*Intent groups = new Intent(getActivity(), DemoHomeScrollingActivity.class);*/
        /*Intent groups = new Intent(getActivity(), DemoSearchGroup.class);*/
        Intent groups = new Intent(getActivity(), GroupsList.class);
        startActivity(groups);
    }

    @OnClick(R.id.txtvw_nearby_church)
    public void churchListing() {
        Intent intent = new Intent(getActivity(), MapsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtvw_books)
    public void booksListing() {
        Intent intent = new Intent(getActivity(), BookTabs.class);
        /*Intent intent = new Intent(getActivity(), DemoHomeScrollingActivity.class);*/
        /*Intent intent = new Intent(getActivity(), BooksListingActivity.class);*/
        startActivity(intent);
    }

    @OnClick(R.id.txtvw_events)
    public void createEvents() {
        Intent intent = new Intent(getActivity(), EventsTabs.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtvw_messages)
    public void myMessages() {
        //Messages Activity Intent
        Intent intentMessages = new Intent(getActivity(), MyMessages.class);
        startActivity(intentMessages);
    }

    @OnClick(R.id.txtvw_update_profile)
    public void profileUpdate() {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        startActivity(intent);

    }

    @OnClick(R.id.txtvw_settings)
    public void settings() {
        Intent intent = new Intent(getActivity(),/* SettingsMainActivity*/SettingOptionListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseActivity) getActivity()).dismissDialog();
    }


    @Override
    public void onSuccess(int tag, Response response) {
        LogoutModal logout = null;

        if (response.isSuccessful()) {
            try {
                ((BaseActivity) getActivity()).hideLoading();
                switch (tag) {
                    case APIServerResponse.LOGOUT:

                        logout = (LogoutModal) response.body();
                        if (logout.getStatus().equals("1")) {

                            ((BaseActivity) getActivity()).showToast("successfully logged out!", Toast.LENGTH_SHORT);
                            ((BaseActivity) getActivity()).clearPreferences();

                            Intent i = new Intent(getActivity(), SignInActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            //getActivity().finish();

                        } else {
                            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
