package com.example.preetisharma.callforblessings.demo;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GroupsListAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GroupListingModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by satoti.garg on 6/5/2017.
 */

public class DemoSearchGroup extends BaseActivity implements APIServerResponse, SearchView.OnQueryTextListener {

    RecyclerView rv_groups;

    List<GroupListingModal.ListBean> groupList = new ArrayList<>();
    GroupsListAdapter groupsListAdapter;
    Toolbar toolbar;
    AppCompatImageView img_view_back;
    AppCompatImageView img_view_change_password;
    AppCompatTextView txtvw_header_title;
    private SearchView searchView;
    private MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rv_groups = (RecyclerView) findViewById(R.id.rv_groups);
        rv_groups.setLayoutManager(new GridLayoutManager(this, 2));
        rv_groups.setItemAnimator(new DefaultItemAnimator());
        //rv_groups.setAdapter(groupsListAdapter);

        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setImageResource(R.drawable.ic_add);
        txtvw_header_title.setText("Groups");

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getGroupsList(APIServerResponse.GROUP_LISTING, getUserSessionId(), this);
        } else {
            showToast("Please check your internet connection.", Toast.LENGTH_LONG);
        }

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img_view_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DemoSearchGroup.this, CreateGroupActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {

            GroupListingModal groupListingModal;
            if (response.isSuccessful()) {

                switch (tag) {

                    case APIServerResponse.GROUP_LISTING:
                        groupListingModal = (GroupListingModal) response.body();
                        if (groupListingModal.getStatus().equals("1")) {
                            groupList = groupListingModal.getList();
                            groupsListAdapter = new GroupsListAdapter(DemoSearchGroup.this, groupList);
                            rv_groups.setAdapter(groupsListAdapter);


                        } else {
                            showToast("No groups created.", Toast.LENGTH_LONG);
                        }

                        hideLoading();
                        break;
                }

            } else {
                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            hideLoading();
            if (tag == APIServerResponse.GROUP_LISTING) {
                throwable.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.
                getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        groupsListAdapter.getFilter().filter(newText);

        return true;
    }
}
