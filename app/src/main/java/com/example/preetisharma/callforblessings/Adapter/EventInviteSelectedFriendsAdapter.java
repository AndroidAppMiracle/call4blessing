package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventInviteSelectedFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/9/2017.
 */

public class EventInviteSelectedFriendsAdapter extends RecyclerView.Adapter<EventInviteSelectedFriendsAdapter.DataViewHolder> {


    private Context mContext;
    //private List<EventInviteSelectedFriendsModal> myEventInvitesFriendsSelectedList = new ArrayList<>();
    private List<EventInviteSelectedFriendsModal> userInfoBeanList;
    private HashMap<String, String> mList;
    private LinearLayout ll_invites;

    public EventInviteSelectedFriendsAdapter(Context mContext, /*HashMap<String, String> list,*/ List<EventInviteSelectedFriendsModal> userList, LinearLayout llInvites) {
        //this.mList = list;
        this.mContext = mContext;
        this.ll_invites = llInvites;
        this.userInfoBeanList = userList;
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_selected_friends_to_invite, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {

        try {

            holder.txtvw_name.setText(userInfoBeanList.get(position).getFriend_name());
            /*Glide.with(mContext).load(myEventInvitesFriendsSelectedList.get(position).getProfile_pic()).placeholder(R.drawable.placeholder).centerCrop().into(holder.iv_close_icon);*/

            holder.iv_close_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //notifyDataSetChanged();
                        if (!userInfoBeanList.isEmpty()) {

                            refresh(position);
                            //notifyData(userInfoBeanList);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (userInfoBeanList.size() != 0) {
            return userInfoBeanList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.txtvw_name)
        AppCompatTextView txtvw_name;

        @BindView(R.id.iv_close_icon)
        AppCompatImageView iv_close_icon;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/
        }


    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        //userInfoBeanList.remove(position);
        userInfoBeanList.remove(position);
        //notifyItemRemoved(userInfoBeanList.size() - 1);
        notifyItemRemoved(position);
        if (userInfoBeanList.isEmpty()) {
            ll_invites.setVisibility(View.GONE);
        } else {
            ll_invites.setVisibility(View.VISIBLE);
        }

    }


    public void notifyData(List<EventInviteSelectedFriendsModal> userInfoBeanList) {
        Log.d("notifyData ", userInfoBeanList.size() + "");
        this.userInfoBeanList = userInfoBeanList;
        notifyDataSetChanged();
    }
}

