package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.BooksAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BooksListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.QuestionsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class BooksListingActivity extends BaseActivity implements APIServerResponse {
    /*@BindView(R.id.books_recycler_view)*/
    RecyclerView books_recycler_view;
    //BooksAdapter booksAdapter;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    public List<QuestionsModal.ListBean> list;
    private List<BooksListingModal.ListBean> mList;
    BooksAdapter _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.books_listing);
        updateStatusBar();
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtvw_header_title.setText("Books");

        img_view_change_password.setVisibility(View.VISIBLE);
        img_view_back.setVisibility(View.VISIBLE);
        img_view_back.setImageResource(R.drawable.ic_back);
        img_view_change_password.setImageResource(R.drawable.ic_add);

        books_recycler_view = (RecyclerView) findViewById(R.id.books_recycler_view);

        //RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        books_recycler_view.setLayoutManager(new GridLayoutManager(this, 2));


        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BooksListingActivity.this.finish();
            }
        });
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().booksList(APIServerResponse.LIST_BOOK, getUserSessionId(), this);
            showLoading();
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        img_view_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addEvent = new Intent(BooksListingActivity.this, AddBookActivity.class);
                startActivity(addEvent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isConnectedToInternet()) {

            ServerAPI.getInstance().booksList(APIServerResponse.LIST_BOOK, getUserSessionId(), this);
            showLoading();
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {


        hideLoading();

        BooksListingModal booksModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.LIST_BOOK:

                    {
                        booksModal = (BooksListingModal) response.body();


                        if (booksModal.getStatus().equals("1")) {
                            if (booksModal.getList().size() > 0) {
                                mList = booksModal.getList();
                                _adapter = new BooksAdapter(this, mList);
                                books_recycler_view.setAdapter(_adapter);

                            }
                            break;
                        } else {
                            showToast("No Books Found", Toast.LENGTH_SHORT);
                        }

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

        hideLoading();
    }
}
