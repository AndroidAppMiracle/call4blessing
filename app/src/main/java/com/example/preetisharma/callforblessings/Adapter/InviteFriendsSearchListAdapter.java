package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kshitiz Bali on 2/14/2017.
 */

public class InviteFriendsSearchListAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<MyFriendsModal.ListBean> originalList;
    private List<MyFriendsModal.ListBean> suggestions = new ArrayList<>();
    private Filter filter = new CustomFilter();

    /**
     * @param context      Context
     * @param originalList Original list used to compare in constraints.
     */
    public InviteFriendsSearchListAdapter(Context context, List<MyFriendsModal.ListBean> originalList) {
        this.context = context;
        this.originalList = originalList;
    }
    public String getItemName(int position) {
        return suggestions.get(position).getUser_info().getProfile_details().getFirstname() + " " + suggestions.get(position).getUser_info().getProfile_details().getLastname();
    }
    @Override
    public int getCount() {
        return suggestions.size(); // Return the size of the suggestions list.
    }

    @Override
    public Object getItem(int position) {
        return suggestions.get(position).getUser_info().getProfile_details().getFirstname();
    }


    @Override
    public long getItemId(int position) {
        return suggestions.get(position).getUser_info().getId();
    }

    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.textview_item,
                    parent,
                    false);
            holder = new ViewHolder();
            holder.autoText = (TextView) convertView.findViewById(R.id.viewholder_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.autoText.setText(suggestions.get(position).getUser_info().getProfile_details().getFirstname());

        return convertView;
    }


    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class ViewHolder {
        TextView autoText;
    }

   /* public int getisFriend(int pos) {
        if (suggestions.get(pos).getIs_friend() == 0) {
            return 0;
        } else if (suggestions.get(pos).getIs_friend() == 1) {
            return 1;
        } else if (suggestions.get(pos).getRequest_respond() == "NO") {
            return 2;
        }

        return 0;
    }*/

    /**
     * Our Custom Filter Class.
     */
    private class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();
            if (originalList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                for (int i = 0; i < originalList.size(); i++) {
                    if (originalList.get(i).getUser_info().getProfile_details().getFirstname().toLowerCase().contains(constraint) && originalList.get(i).getUser_info().getProfile_details().getFirstname().toLowerCase().startsWith(constraint.toString())) { // Compare item in original list if it contains constraints.
                        suggestions.add(originalList.get(i)); // If TRUE add item in Suggestions.
                    }
                }
            }
            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                if(constraint!=null&&constraint.length()!=0)
                {
                    ((BaseActivity) context).showToast("No results found", Toast.LENGTH_SHORT);
                }
                notifyDataSetInvalidated();
            }
        }
    }


}
