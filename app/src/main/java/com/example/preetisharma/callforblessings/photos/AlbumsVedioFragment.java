package com.example.preetisharma.callforblessings.photos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.DemoDataAlbumsAdapter;
import com.example.preetisharma.callforblessings.Adapter.DemoDataVedioAlbumsAdapter;
import com.example.preetisharma.callforblessings.Fragment.GalleryVideosFragment;
import com.example.preetisharma.callforblessings.GalleyVedioActivity;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 7/17/2017.
 */

public class AlbumsVedioFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    List<DemoDataModal> albumList = new ArrayList<>();

    //int[] images = {R.drawable.ic_album_placeholder};

    @BindView(R.id.rv_albums)
    RecyclerView rv_albums;

    DemoDataVedioAlbumsAdapter adapter;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    static int var;

    @Override

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_albums, container, false);
        ButterKnife.bind(this, rootView);
        var=0;
        swipe_refresh_layout.setOnRefreshListener(this);
        rv_albums.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_albums.setItemAnimator(new DefaultItemAnimator());
        //rv_albums.addItemDecoration(new DividerItemDecoration(getActivity(), GridLayoutManager.VERTICAL));


        //rv_albums.setNestedScrollingEnabled(false);

        swipe_refresh_layout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        addDummyAlbums();
                    }
                }
        );


        ItemClickSupport.addTo(rv_albums).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                if (adapter.getSelectedItemCount() > 0 && position != 0) {
                    enableActionMode(position);
                } else if (position != 0) {
                    Intent i = new Intent(getActivity(), GalleyVedioActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.ALBUM_NAME, albumList.get(position).getName());
                    i.putExtras(b);
                    startActivity(i);
                }

            }
        }).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                /*if (position != 0) {
                    enableActionMode(position);
                }*/

                return true;
            }
        });


        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            /*swipe_refresh_layout.post(
                    new Runnable() {
                        @Override
                        public void run() {
                            addDummyAlbums();
                        }
                    }
            );*/

            //addDummyAlbums();

            //Load Data
        } else {
            //No Loading of Data
        }
    }


    public void addDummyAlbums() {


        if (!albumList.isEmpty()) {
            albumList.clear();
        }

        for (int i = 0; i < 2; i++) {

            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setId(i);

          /*  if(i<=3)
            {
                if(i==1) {
                    demoDataModal.setName("Profile Picture");
                }else if(i==2)
                {
                    demoDataModal.setName("Cover Photos");
                }else
                {
                    demoDataModal.setName("TimeLine Photos ");
                }
            }else
            {
                demoDataModal.setName("Album " + (var+1));
                var++;
            }*/
            demoDataModal.setName("Vedio album " + (i+1));
            demoDataModal.setAlbumArt("https://upgradeit.ro/wp-content/themes/typegrid/img/placeholder.png");
            demoDataModal.setImage("https://upgradeit.ro/wp-content/themes/typegrid/img/placeholder.png");
            albumList.add(demoDataModal);
        }
        swipe_refresh_layout.setRefreshing(false);

        adapter = new DemoDataVedioAlbumsAdapter(getActivity(), albumList);
        rv_albums.setAdapter(adapter);

    }

    @Override
    public void onRefresh() {
        addDummyAlbums();
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_delete, menu);

            // disable swipe refresh if action mode is enabled
            swipe_refresh_layout.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete_item:
                    // delete all the selected messages
                    deleteMessages();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.clearSelections();
            swipe_refresh_layout.setEnabled(true);
            actionMode = null;
            /*recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });*/
        }
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = ((BaseActivity) getActivity()).startSupportActionMode(actionModeCallback);
            //actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void deleteMessages() {
        List<Integer> selectedItemPositions =
                adapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            adapter.removeData(selectedItemPositions.get(i));
        }
        adapter.notifyDataSetChanged();
    }
}
