package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.HelpRequestListModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;
import com.example.preetisharma.callforblessings.demo.ShareDialogActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 1/23/2017.
 */

public class HelpRequestAdapter extends RecyclerView.Adapter<HelpRequestAdapter.DataViewHolder> {

    public List<HelpRequestListModal.ListBean> mList = new ArrayList<>();
    Activity mContext;
    private boolean expand = false;
    private static String likescount, commentsCount;
    private static String liked = "not liked";

    public HelpRequestAdapter(Activity mContext, List<HelpRequestListModal.ListBean> list) {
        mList = list;
        this.mContext = mContext;
        ((BaseActivity) mContext).hideKeyboard();
    }


    public void deleteItem(int index) {
        mList.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<HelpRequestListModal.ListBean> items) {
        mList.addAll(items);
        notifyDataSetChanged();
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.help_request_item, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, int position) {


        holder.txtvw_help_of_the_title.setText(mList.get(position).getTitle());
        holder.txtvw_help_of_the_day_description.setText(mList.get(position).getDesc());
        holder.txtvw_help_of_the_day_reach_count.setText(mList.get(position).getReach_count() + " " + "Reached");
        holder.txtvw_help_of_the_day_comments_count.setText(mList.get(position).getComment_count() + "   " + "Comment");
        holder.txtvw_help_of_the_day_willing_count.setText(mList.get(position).getLike_count() + " " + "Willing");
        liked = mList.get(position).getLike_flag();
        if (liked.equalsIgnoreCase("Liked")) {
            int imgResource = R.drawable.ic_i_will_selected;
            holder.txtvw_i_will.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);

        } else {
            int imgResource = R.drawable.ic_help_request_will;
            holder.txtvw_i_will.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);

        }
       /* if (position == 0) {
            holder.child_layout.setVisibility(View.VISIBLE);
        } else {
            holder.child_layout.setVisibility(View.GONE);
        }*/
       /* holder.txtvw_help_of_the_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!expand) {
                    expand = true;
                    holder.child_layout.setVisibility(View.VISIBLE);
                } else {
                    holder.child_layout.setVisibility(View.GONE);
                    expand = false;
                }

            }
        });*/
        holder.txtvw_help_of_the_day_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Comments position", "Position" + holder.getAdapterPosition());
                Intent i = new Intent(mContext, CommentsListingActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.COMMENTS, String.valueOf(mList.get(holder.getAdapterPosition()).getId()));
                b.putString(Constants.POSTTYPE, Constants.HELP_REQUEST);
                i.putExtras(b);
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {
        //Prayer List
        @BindView(R.id.txtvw_help_of_the_title)
        AppCompatTextView txtvw_help_of_the_title;
        @BindView(R.id.child_prayer_of_the_day_layout)
        LinearLayout child_layout;
        @BindView(R.id.txtvw_help_of_the_day_description)
        AppCompatTextView txtvw_help_of_the_day_description;
        @BindView(R.id.txtvw_help_of_the_day_share)
        AppCompatTextView txtvw_help_of_the_day_share;
        @BindView(R.id.txtvw_help_of_the_day_comment)
        AppCompatTextView txtvw_help_of_the_day_comment;

        @BindView(R.id.txtvw_help_of_the_day_reach_count)
        AppCompatTextView txtvw_help_of_the_day_reach_count;
        @BindView(R.id.txtvw_help_of_the_day_comments_count)
        AppCompatTextView txtvw_help_of_the_day_comments_count;
        @BindView(R.id.txtvw_help_of_the_day_willing_count)
        AppCompatTextView txtvw_help_of_the_day_willing_count;
        @BindView(R.id.txtvw_i_will)
        AppCompatTextView txtvw_i_will;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }


        @OnClick(R.id.txtvw_help_of_the_day_share)
        public void helpOfDayShare() {

            openConfirmationDialog(String.valueOf(mList.get(getAdapterPosition()).getId()), ((BaseActivity) mContext).getFirstName(), ((BaseActivity) mContext).getUserImage(), mList.get(getAdapterPosition()).getTitle(), "");
        }

        Dialog dialog;

        public void openConfirmationDialog(final String help_id, String userName, String profilePic, String post_text, String post_image) {


            dialog = new Dialog(mContext);

            Intent shareDialog = new Intent(mContext, ShareDialogActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.POSTTYPE, "HELP_REQUEST");
            b.putString(Constants.POSTIMAGE, post_image);
            b.putString(Constants.POSTTEXT, post_text);
            b.putString(Constants.POSTID, help_id);
            shareDialog.putExtras(b);
            mContext.startActivity(shareDialog);


        }

        @OnClick(R.id.txtvw_i_will)
        public void likePrayer() {

            if (txtvw_i_will.isEnabled()){
                txtvw_i_will.setEnabled(false);
            }

            liked = mList.get(getAdapterPosition()).getLike_flag();
            likescount = mList.get(getAdapterPosition()).getLike_count();


            if (liked.equalsIgnoreCase("Not Liked")) {
                int imgResource = R.drawable.ic_i_will_selected;
                txtvw_i_will.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);

                ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(mList.get(getAdapterPosition()).getId()), "HELP_REQUEST", DataViewHolder.this);
            } else {
                int imgResource = R.drawable.ic_help_request_will;
                txtvw_i_will.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(mList.get(getAdapterPosition()).getId()), "HELP_REQUEST", DataViewHolder.this);

            }
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {

                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    LikeModal likeModal;
                    ShareModal shareModal;

                    switch (tag) {
                        case APIServerResponse.LIKE: {
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_i_will.isEnabled()){
                                    txtvw_i_will.setEnabled(true);
                                }
                                int imgResource = R.drawable.ic_i_will_selected;
                                txtvw_i_will.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(mList.get(getAdapterPosition()).getLike_count());
                                likescountValue++;
                                liked = "liked";
                                mList.get(getAdapterPosition()).setLike_flag("liked");
                                mList.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));
                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_help_of_the_day_willing_count.setText(String.valueOf(likescountValue) + " " + "Willing");
                                } else {
                                    txtvw_help_of_the_day_willing_count.setText(String.valueOf(likescountValue) + " " + "Willing");
                                }

                            }



                        }
                        break;
                        case APIServerResponse.UNLIKE:
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_i_will.isEnabled()){
                                    txtvw_i_will.setEnabled(true);
                                }
                                int imgResource = R.drawable.ic_help_request_will;
                                txtvw_i_will.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue--;
                                liked = "not liked";
                                mList.get(getAdapterPosition()).setLike_flag("not liked");
                                mList.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_help_of_the_day_willing_count.setText(String.valueOf(likescountValue) + " " + "Willing");
                                } else {
                                    txtvw_help_of_the_day_willing_count.setText(String.valueOf(likescountValue) + " " + "Willing");
                                }
                            }

                            break;
                        case APIServerResponse.SHARE:

                            shareModal = (ShareModal) response.body();
                            if (shareModal.getStatus().equals("1")) {
                                /*shareModal.getDetail().*/

                                ((BaseActivity) mContext).showToast("Shared successfully", Toast.LENGTH_SHORT);
                                int count = Integer.parseInt((mList.get(getAdapterPosition()).getReach_count())) + 1;
                                mList.get(getAdapterPosition()).setReach_count(String.valueOf(count));
                                dialog.dismiss();
                                notifyDataSetChanged();
                            }

                            break;
                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int tag, Throwable throwable) {
            try {
                ((BaseActivity) mContext).hideLoading();
                throwable.printStackTrace();
                switch (tag) {
                    case APIServerResponse.LIKE:
                        System.out.println("Error");
                        break;

                    case APIServerResponse.AMEN_PRAYER:
                        System.out.println("Error");
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
