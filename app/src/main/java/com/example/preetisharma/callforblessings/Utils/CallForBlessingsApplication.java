package com.example.preetisharma.callforblessings.Utils;

import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.devbrackets.android.exomedia.ExoMedia;
import com.example.preetisharma.callforblessings.file.FileUtils;
import com.example.preetisharma.callforblessings.joymusicplayer.PlaylistManager;
import com.example.preetisharma.callforblessings.joymusicplayer.PlaylistManagerSongs;
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;

import okhttp3.OkHttpClient;

/**
 * Created by preeti.sharma on 1/17/2017.
 */

public class CallForBlessingsApplication extends MultiDexApplication {

    //private  AppCompatActivity _activity;
    public static CallForBlessingsApplication _application;
    //private FlowOrganizer _flow;
    private static PlaylistManager playlistManager;
    private static PlaylistManagerSongs playlistManagerSongs;


    public CallForBlessingsApplication getInstanse() {
        if (_application == null) {
            _application = new CallForBlessingsApplication();
        }
        return _application;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        enableStrictMode();
        super.onCreate();

        FileUtils.createApplicationFolder();
        _application = this;
        playlistManager = new PlaylistManager();
        playlistManagerSongs = new PlaylistManagerSongs();
        //LeakCanary.install(this);

        configureExoMedia();
    }


    @Override
    public void onTerminate() {
        super.onTerminate();

        _application = null;
        playlistManager = null;
        playlistManagerSongs = null;
    }

    public static PlaylistManager getPlaylistManager() {
        return playlistManager;
    }

    public static PlaylistManagerSongs getPlaylistManagerSongs() {
        return playlistManagerSongs;
    }

    public static CallForBlessingsApplication getApplication() {
        return _application;
    }

    private void enableStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()
                .penaltyLog()
                .build());

        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .build());
    }

    private void configureExoMedia() {
        // Registers the media sources to use the OkHttp client instead of the standard Apache one
        // Note: the OkHttpDataSourceFactory can be found in the ExoPlayer extension library `extension-okhttp`
        ExoMedia.setHttpDataSourceFactoryProvider(new ExoMedia.HttpDataSourceFactoryProvider() {
            @NonNull
            @Override
            public HttpDataSource.BaseFactory provide(@NonNull String userAgent, @Nullable TransferListener<? super DataSource> listener) {
                return new OkHttpDataSourceFactory(new OkHttpClient(), userAgent, listener);
            }
        });
    }
}
