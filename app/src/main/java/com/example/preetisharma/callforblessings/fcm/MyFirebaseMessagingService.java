package com.example.preetisharma.callforblessings.fcm;

/**
 * Created by tanuja.gupta on 10/13/2016.
 */

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.HomeActivity;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.SignInActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.ChatActivity;
import com.example.preetisharma.callforblessings.demo.DemoSinglePostView;
import com.example.preetisharma.callforblessings.demo.DirectMessageWebActivity;
import com.example.preetisharma.callforblessings.demo.GroupSinglePostView;
import com.example.preetisharma.callforblessings.demo.GroupWallHomeScreen;
import com.example.preetisharma.callforblessings.demo.SinglePostView;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;


/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    SharedPreferences sp;
    public int NOTIFICATION_ID;
    private static final String TAG = "MyFcmListenerService";
    private SharedPreferences mPrefs;
    String userId;
    String message;
    Random random;


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        mPrefs = getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);

        if (remoteMessage.getData().size() > 0) {

            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            random = new Random();
            NOTIFICATION_ID = random.nextInt(1000) + 1;
            Log.d("NOTIFICATION ID", "" + NOTIFICATION_ID);


            if (remoteMessage.getData().get("action").equalsIgnoreCase("alarm")) {
                //sendNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("action"));
                Intent intentActivity = new Intent(this, NotifyAlertDialog.class).putExtra("NOTIFICATION_MESSAGE", remoteMessage.getData().get("message"));
                intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                this.startActivity(intentActivity);
            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("send-invite")) {
                //invite
                sendNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"));
                /*sendNotification(remoteMessage.getData().get("message"), remoteMessage.getData().get("action"));*/
                Log.i("onject_id ", remoteMessage.getData().get("object_id"));

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("request-action")) {
                //accepted Friend requested
                sendNotificationFriendAccepted(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"));
                Log.i("onject_id ", remoteMessage.getData().get("object_id"));

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("send-request")) {

                sendNotificationFriendRequest(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"));
                //friend request
                Log.i("onject_id ", remoteMessage.getData().get("object_id"));
            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("like")) {
                //Send to postView
                sendNotificationPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Like");
            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("post-like")) {
                //Send to postView
                sendNotificationPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Like");
            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("comment")) {
                //Send to postView
                sendNotificationPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Comment");
            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("post-comment")) {
                //Send to postView
                sendNotificationPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Comment");
            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("create")) {
                //Send to postView
                sendNotificationPostCreate(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Post on your wall.");
            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("post")) {
                //Send to postView
                sendNotificationPostCreate(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Post on your wall.");
            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("update-post") || remoteMessage.getData().get("action").equalsIgnoreCase("update")) {
                sendNotificationPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Update");

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("share") || remoteMessage.getData().get("action").equalsIgnoreCase("post-share")) {
                sendNotificationPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Share");

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("send")) {
                //Send To Chat
                sendNotificationChatMessage(remoteMessage.getData().get("message"), remoteMessage.getData().get("from_id"), "Message");

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("group-create")) {

                //Group Home Wall
                sendNotificationCreateGroup(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Create Group");

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("group-update")) {

                //Group Home Wall
                sendNotificationCreateGroup(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Create Group");

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("group-post-like") || remoteMessage.getData().get("action").equalsIgnoreCase("post-group-like")) {

                //Group Post View
                sendNotificationGroupPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Group Like");

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("group-post-comment") || remoteMessage.getData().get("action").equalsIgnoreCase("post-comment-group")) {

                //Group Post View
                sendNotificationGroupPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Group Comment");

            } else if (remoteMessage.getData().get("action").equalsIgnoreCase("post-share")) {

                //Group Post View
                sendNotificationPostLikeComment(remoteMessage.getData().get("message"), remoteMessage.getData().get("object_id"), "Group Share");
            }

        }


        if (remoteMessage.getNotification() != null) {

            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

       /* Intent i = new Intent(MyFirebaseMessagingService.this, NotifyAlertDialog.class);
        startActivity(i);*/


    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String Action) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, com.example.preetisharma.callforblessings.demo.EventsDetailsActivityNew.class);
                in.putExtra(Constants.EVENT_ID, Action);
                in.putExtra(Constants.EVENT_TYPE_FLAG, "Event Invites");
                in.putExtra(Constants.EVENT_INVITE_STATUS, "PENDING");
                in.putExtra("Notification_Flag", "true");
                //in.setAction(Intent.ACTION_MAIN);
                //in.addCategory(Intent.CATEGORY_LAUNCHER);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

               /* in.putExtra(Constants.EVENT_ID, Action);*/
             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                /*| PendingIntent.FLAG_ONE_SHOT*/);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Event Invite")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);

                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Event Invite")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        } else {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, com.example.preetisharma.callforblessings.demo.EventsDetailsActivityNew.class);
                in.putExtra(Constants.EVENT_ID, Action);
                in.putExtra(Constants.EVENT_TYPE_FLAG, "Event Invites");
                in.putExtra(Constants.EVENT_INVITE_STATUS, "PENDING");
                in.putExtra("Notification_Flag", "true");

                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
               /* in.putExtra(Constants.EVENT_ID, Action);*/
             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Event Invite")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Event Invite")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }


    private void sendNotificationFriendRequest(String messageBody, String Action) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, MaterialUpConceptActivity.class);
                in.putExtra(Constants.SHARED_PREF_USER_ID, Action);
                in.putExtra(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Friend Request")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Friend Request")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        } else {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, MaterialUpConceptActivity.class);
                in.putExtra(Constants.SHARED_PREF_USER_ID, Action);
                in.putExtra(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Friend Request")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Friend Request")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }


    private void sendNotificationFriendAccepted(String messageBody, String Action) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, MaterialUpConceptActivity.class);
                in.putExtra(Constants.SHARED_PREF_USER_ID, Action);
                in.putExtra(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Request Accepted")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Request Accepted")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        } else {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, MaterialUpConceptActivity.class);
                in.putExtra(Constants.SHARED_PREF_USER_ID, Action);
                in.putExtra(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Request Accepted")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle("Request Accepted")
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }


    private void sendNotificationPostLikeComment(String messageBody, String ActionID, String title) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, DemoSinglePostView.class);
                in.putExtra(Constants.POSTID, ActionID);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);


                /*PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);*/
                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);
                /*PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        in, 0);*/
                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        } else {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, DemoSinglePostView.class);
                in.putExtra(Constants.POSTID, ActionID);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                /*PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);*/
                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                /*PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        in, 0);*/

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)

                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }


    private void sendNotificationGroupPostLikeComment(String messageBody, String ActionID, String title) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, GroupSinglePostView.class);
                in.putExtra(Constants.GROUP_ID, ActionID);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);
                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)

                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        } else {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, GroupSinglePostView.class);
                in.putExtra(Constants.GROUP_ID, ActionID);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setContentIntent(contentIntent);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }

    private void sendNotificationPostCreate(String messageBody, String ActionID, String title) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, SinglePostView.class);
                in.putExtra(Constants.POSTID, ActionID);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);

                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        } else {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, SinglePostView.class);
                in.putExtra(Constants.POSTID, ActionID);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }


    private void sendNotificationChatMessage(String messageBody, String ActionID, String title) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, DirectMessageWebActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.FRIEND_ID, ActionID);
                /*in.putExtra(Constants.POSTID, ActionID);*/
                b.putString("Notification_Flag", "true");
                in.putExtras(b);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        } else {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, ChatActivity.class);
                /*in.putExtra(Constants.POSTID, ActionID);*/
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }


    private void sendNotificationCreateGroup(String messageBody, String ActionID, String title) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, GroupWallHomeScreen.class);
                in.putExtra(Constants.GROUP_ID, ActionID);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        } else {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, GroupWallHomeScreen.class);
                in.putExtra(Constants.GROUP_ID, ActionID);
                in.putExtra("Notification_Flag", "true");
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

             /*in.putExtra("Notif", messageBody);*/
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(HomeActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, NOTIFICATION_ID,
                        in, PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                /*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*/
                        .setAutoCancel(true)
                        .setSound(uri)
                        //.setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody));
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }


    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_notofication : R.drawable.ic_notofication;
    }

    private boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                //                Log.e("app",appPackageName);
                return true;
            }
        }
        return false;
    }


/*

    private void sendNotification(String messageBody, String Action) {
        message = messageBody;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager mNotificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (mPrefs.getBoolean(Constants.SHARED_PREF_NAME, false)) {
                Intent in = new Intent(this, ALarmDialogActivity.class).putExtra("message", message);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
             */
/*in.putExtra("Notif", messageBody);*//*

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(ALarmDialogActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(in);
                PendingIntent contentIntent = stackBuilder
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                */
/*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*//*

                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

            } else {
                Intent in = new Intent(this, SignInActivity.class);
                in.putExtra("Notif", messageBody);
                PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                        in, 0);

                int numMessages = 0;
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        this).setSmallIcon(R.drawable.ic_notofication)
                        .setContentTitle(getString(R.string.app_name))
                        .setColor(Color.parseColor("#FF660000"))
                */
/*.setStyle(new NotificationCompat.BigTextStyle().bigText("dfds"))*//*

                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentTitle(message)
                        .setContentText(message)
                        .setNumber(++numMessages);
                mBuilder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());


            }
        }
    }
*/


}
