package com.example.preetisharma.callforblessings.photos;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.PhotosTabsAdapter;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

/**
 * Created by satoti.garg on 7/17/2017.
 */

public class PhotosTabs extends BaseActivity implements TabLayout.OnTabSelectedListener {

    ViewPager viewPagerPhotos;
    AppCompatTextView txtvw_header_title;
    AppCompatImageView img_view_back;
    AppCompatImageView img_view_change_password;
    PhotosTabsAdapter photosTabsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_tabs);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_phots);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setVisibility(View.GONE);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        txtvw_header_title.setText("Photos");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotosTabs.this.finish();

            }
        });

        /*tabLayout.addTab(tabLayout.newTab().setText("My Photos"));*/
        tabLayout.addTab(tabLayout.newTab().setText("Uploaded"));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.albums));

        viewPagerPhotos = (ViewPager) findViewById(R.id.viewPagerPhotos);

        photosTabsAdapter = new PhotosTabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPagerPhotos.setAdapter(photosTabsAdapter);
        viewPagerPhotos.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPagerPhotos.setOffscreenPageLimit(0);
        tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPagerPhotos.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
