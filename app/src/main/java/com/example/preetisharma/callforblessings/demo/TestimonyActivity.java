/*
package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.TestimonyAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.HaleTestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

*/
/**
 * Created by Kshitiz Bali on 1/19/2017.
 *//*


public class TestimonyActivity extends BaseActivity implements APIServerResponse {


    @BindView(R.id.testimony_list)
    RecyclerView testimony_list;

    List<TestimonyModal.ListBean> lisAllTestimonial;
    @BindView(R.id.txtvw_testimony_of_the_title)
    AppCompatTextView txtvw_testimony_of_the_title;
    @BindView(R.id.txtvw_testimony_of_the_day_description)
    AppCompatTextView txtvw_testimony_of_the_day_description;
    @BindView(R.id.txtvw_testimony_of_the_day_likes_count)
    AppCompatTextView txtvw_testimony_of_the_day_likes_count;
    @BindView(R.id.txtvw_testimony_of_the_day_comments_count)
    AppCompatTextView txtvw_testimony_of_the_day_comments_count;
    @BindView(R.id.txtvw_testimony_of_the_day_like)
    AppCompatTextView txtvw_testimony_of_the_day_like;
    @BindView(R.id.txtvw_testimony_of_the_day_comment)
    AppCompatTextView txtvw_testimony_of_the_day_comment;
    @BindView(R.id.txtvw_testimony_of_the_day_amen)
    AppCompatTextView txtvw_testimony_of_the_day_amen;
    @BindView(R.id.txtvw_testimony_of_the_day_halle_count)
    AppCompatTextView txtvw_testimony_of_the_day_halle_count;
    TestimonyAdapter adapter;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private String prayerId;
    TestimonyModal testimonyModal;
    String likescount, hallecount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testimony_tab);
        ButterKnife.bind(this);


        try {
            setSupportActionBar(toolbar);
            txtvw_header_title.setText("Testimony");
            img_view_change_password.setVisibility(View.GONE);
            showLoading();
            if (isConnectedToInternet()) {
                ServerAPI.getInstance().getTestimony(APIServerResponse.TESTIMONY_INDEX, getUserSessionId(), this);
            } else {
                showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.txtvw_testimony_of_the_day_comment)
    public void comment_prayer_of_day() {
        Intent i = new Intent(this, CommentsListingActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.COMMENTS, String.valueOf(testimonyModal.getTestimony_of_day().getId()));
        b.putString(Constants.POSTTYPE, Constants.TESTIMONY);
        i.putExtras(b);
        startActivity(i);
    }

    @OnClick(R.id.img_view_back)
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            lisAllTestimonial = new ArrayList<>();

            */
/*AmenPrayerModal amenPrayerModal;*//*

            HaleTestimonyModal haleTestimonyModal;
            LikeModal likeModal;
            hideLoading();

            if (response.isSuccessful()) {

                switch (tag) {
                    case APIServerResponse.LIKE:

                    {
                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {
                            showToast("Testimony Liked", Toast.LENGTH_SHORT);
                        }
                        int imgResource = R.drawable.ic_liked;
                        txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        int likescountValue = Integer.parseInt(likescount);
                        likescountValue++;
                        if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + "Like");
                        } else {
                            txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + "Likes");
                        }
                    }
                    break;
                    case APIServerResponse.HALE_TESTIMONY: {
                        haleTestimonyModal = (HaleTestimonyModal) response.body();
                        if (haleTestimonyModal.getStatus().equals("1")) {
                            showToast("Hallelujah", Toast.LENGTH_SHORT);
                        }

                        int likescountValue = Integer.parseInt(hallecount);
                        likescountValue++;
                        if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(likescountValue) + "Hallelujah");
                        } else {
                            txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(likescountValue) + "Hallelujah");
                        }
                    }
                    break;

                    case APIServerResponse.TESTIMONY_INDEX:
                        try {
                            testimonyModal = (TestimonyModal) response.body();
                            if (testimonyModal.getStatus().equals("1")) {
                                if (testimonyModal.getTestimony_of_day().getLike_flag().equalsIgnoreCase("Not liked")) {
                                    int imgResource = R.drawable.prayer_like_icon;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                } else {
                                    int imgResource = R.drawable.ic_liked;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                }
                                txtvw_testimony_of_the_title.setText(testimonyModal.getTestimony_of_day().getTitle());
                                txtvw_testimony_of_the_day_description.setText(testimonyModal.getTestimony_of_day().getDesc());
                                prayerId = String.valueOf(testimonyModal.getTestimony_of_day().getId());
                                lisAllTestimonial = testimonyModal.getList();

                            }
                            if (Integer.parseInt(testimonyModal.getTestimony_of_day().getLike_count()) == 0 || Integer.parseInt(testimonyModal.getTestimony_of_day().getLike_count()) == 1) {
                                txtvw_testimony_of_the_day_like.setText(testimonyModal.getTestimony_of_day().getLike_count() + "Like");
                            } else {
                                txtvw_testimony_of_the_day_like.setText(testimonyModal.getTestimony_of_day().getLike_count() + "Likes");
                            }
                            if (Integer.parseInt(testimonyModal.getTestimony_of_day().getComment_count()) == 0 || Integer.parseInt(testimonyModal.getTestimony_of_day().getComment_count()) == 1) {
                                txtvw_testimony_of_the_day_comments_count.setText(testimonyModal.getTestimony_of_day().getComment_count() + "Comment");
                            } else {
                                txtvw_testimony_of_the_day_comments_count.setText(testimonyModal.getTestimony_of_day().getComment_count() + "Comments");
                            }
                            if (Integer.parseInt(testimonyModal.getTestimony_of_day().getHallelujah_count()) == 0 || Integer.parseInt(testimonyModal.getTestimony_of_day().getHallelujah_count()) == 1) {
                                txtvw_testimony_of_the_day_halle_count.setText(testimonyModal.getTestimony_of_day().getHallelujah_count() + "Hallelujah");
                            } else {
                                txtvw_testimony_of_the_day_halle_count.setText(testimonyModal.getTestimony_of_day().getHallelujah_count() + "Hallelujahs");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        likescount = testimonyModal.getTestimony_of_day().getLike_count();
                        adapter = new TestimonyAdapter(TestimonyActivity.this, lisAllTestimonial);
                        testimony_list.setAdapter(adapter);

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(TestimonyActivity.this);
                        testimony_list.setLayoutManager(mLayoutManager);
                        testimony_list.setItemAnimator(new DefaultItemAnimator());
                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {

            hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.TESTIMONY_INDEX:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.txtvw_testimony_of_the_day_amen)
    public void haleTestimonyOfTheDay() {
        showLoading();
        ServerAPI.getInstance().haleTestimonyPost(APIServerResponse.HALE_TESTIMONY, getUserSessionId(), prayerId, this);
    }

    @OnClick(R.id.txtvw_testimony_of_the_day_like)
    public void likeTestimonyOfTheDay() {
        if (!testimonyModal.getTestimony_of_day().getLike_flag().equalsIgnoreCase("Not liked")) {
            int imgResource = R.drawable.ic_liked;
            txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().like(APIServerResponse.LIKE, getUserSessionId(), String.valueOf(testimonyModal.getTestimony_of_day().getId()), "TESTIMONY", this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }

        } else {
            int imgResource = R.drawable.prayer_like_icon;
            txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().like(APIServerResponse.LIKE, getUserSessionId(), String.valueOf(testimonyModal.getTestimony_of_day().getId()), "TESTIMONY", this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }
    }


}
*/
