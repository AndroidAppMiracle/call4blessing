package com.example.preetisharma.callforblessings.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.settings.general_setting.UpdateGeneralSettingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satoti.garg on 9/20/2017.
 */

public class SettingsGeneral extends BaseActivity {


    @BindView(R.id.atv_general_setting_name)
    AppCompatTextView atv_general_setting_name;

    @BindView(R.id.atv_setting_value)
    AppCompatTextView atv_setting_value;

    @BindView(R.id.cv_setting_item)
    CardView cv_setting_item;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_general);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("General Settings");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @OnClick(R.id.txtvw_update)
    public void updateSetting()
    {
        Intent intent = new Intent(SettingsGeneral.this, UpdateGeneralSettingActivity.class);
        startActivity(intent);
    }

}
