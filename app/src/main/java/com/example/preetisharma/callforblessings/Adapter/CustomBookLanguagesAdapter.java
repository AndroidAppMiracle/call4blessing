package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.BooksLanguagesModal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 3/8/2017.
 */

//public class CustomBookLanguagesAdapter


public class CustomBookLanguagesAdapter extends ArrayAdapter<BooksLanguagesModal.ListBean> {

    private LayoutInflater flater;

    public CustomBookLanguagesAdapter(Activity context, int resouceId, int textviewId, List<BooksLanguagesModal.ListBean> list) {

        super(context, resouceId, textviewId, list);
//        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return rowview(convertView, position);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return rowview(convertView, position);
    }

    private View rowview(View convertView, int position) {

        BooksLanguagesModal.ListBean rowItem = getItem(position);

        viewHolder holder;
        View rowview = convertView;
        if (rowview == null) {

            holder = new viewHolder();
            flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowview = flater.inflate(R.layout.adapter_layout_custom_spinner_item, null, false);

            holder.tv_language = (TextView) rowview.findViewById(R.id.tv_language);
            rowview.setTag(holder);
        } else {
            holder = (viewHolder) rowview.getTag();
        }
        if (rowItem != null && !rowItem.getLanguage().equals("")) {
            holder.tv_language.setText(rowItem.getLanguage());
        }


        return rowview;
    }

    private class viewHolder {
        TextView tv_language;
    }
}



