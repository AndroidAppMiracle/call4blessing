package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.CreateGroupAddFriendsAdapter;
import com.example.preetisharma.callforblessings.Adapter.UpdateGroupAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CreateGroupModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusIsGroupCreatedModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusUpdateGroup;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.UpdateGroupModal;
import com.example.preetisharma.callforblessings.Server.Modal.UpdateGroupParcebleModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.FileUtils;
import com.example.preetisharma.callforblessings.Utils.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/21/2017.
 */

public class CreateGroupActivity extends BaseActivity implements APIServerResponse {


    Toolbar toolbar;

    //AppCompatTextView txtvw_create_group;
    AppCompatImageView img_view_back;
    AppCompatImageView img_view_change_password;
    @BindView(R.id.edt_txt_group_name)
    AppCompatEditText edt_txt_group_name;
    @BindView(R.id.edt_txt_group_description)
    AppCompatEditText edt_txt_group_description;
    //@BindView(R.id.edt_txt_group_status)
    //AppCompatEditText edt_txt_group_status;
    @BindView(R.id.img_view_event_pic_upload)
    AppCompatImageView img_view_event_pic_upload;
    @BindView(R.id.img_view_event_pic)
    AppCompatImageView img_view_event_pic;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.txtvw_no_friends)
    AppCompatTextView txtvw_no_friends;

    //Search Items
    @BindView(R.id.aiv_clear)
    AppCompatImageView aiv_clear;
    AppCompatEditText aet_search;

    @BindView(R.id.rv_tagging_friends_recycler_view)
    RecyclerView rv_tagging_friends_recycler_view;

    CreateGroupAddFriendsAdapter createGroupAddFriendsAdapter;
    UpdateGroupAdapter updateGroupAdapter;
    List<MyFriendsModal.ListBean> friendsList = new ArrayList<>();

    static final int PICK_IMAGE_ = 1;
    static final int REQUEST_IMAGE_CAPTURE = 2;
    static final int RC_CAMERA_PERM_AND_STORAGE = 3;
    static final int RC_GALLERY_PERM_AND_STORAGE = 4;

    String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String[] galleryPerms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private String mCurrentPhotoPath;

    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private String groupFunction = "";
    private String groupName = "", groupDesc = "", groupId = "", groupPic = "";
    ArrayList<UpdateGroupParcebleModal> groupMembers = new ArrayList<>();

    ArrayList<UpdateGroupParcebleModal> allFriendsMembers = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            groupFunction = extras.getString(Constants.GROUP_FUNCTION, Constants.GROUP_CREATE);
            Log.i("groupFunction", groupFunction);
        }


        rv_tagging_friends_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        rv_tagging_friends_recycler_view.setItemAnimator(new DefaultItemAnimator());

        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);

        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setVisibility(View.GONE);
        aet_search = (AppCompatEditText) findViewById(R.id.aet_search);
        //txtvw_create_group = (AppCompatTextView) findViewById(R.id.txtvw_create_group);

        if (groupFunction.equalsIgnoreCase(Constants.GROUP_CREATE)) {

            txtvw_header_title.setText(R.string.new_group);

        } else if (groupFunction.equalsIgnoreCase(Constants.GROUP_UPDATE)) {

            txtvw_header_title.setText(getString(R.string.update_group));

            if (getIntent().getExtras() != null) {
                Bundle extras = getIntent().getExtras();
                groupName = extras.getString(Constants.GROUP_NAME, Constants.GROUP_UPDATE);
                edt_txt_group_name.setText(groupName);
                groupDesc = extras.getString(Constants.GROUP_DESC, Constants.GROUP_UPDATE);
                edt_txt_group_description.setText(groupDesc);
                groupId = extras.getString(Constants.GROUP_ID, Constants.GROUP_ID);
                groupPic = extras.getString(Constants.GROUP_PIC, Constants.GROUP_PIC);
                Glide.with(CreateGroupActivity.this).load(groupPic).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(img_view_event_pic);
                Log.i("groupFunction", groupFunction);
            }

            if (getIntent().getExtras().getParcelableArrayList(Constants.GROUP_MEMBERS) != null && !getIntent().getExtras().getParcelableArrayList(Constants.GROUP_MEMBERS).isEmpty()) {
                groupMembers = getIntent().getExtras().getParcelableArrayList(Constants.GROUP_MEMBERS);
            }

        }

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                CreateGroupActivity.this.finish();
            }
        });

        if (isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, getUserSessionId(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);

        }



        /*txtvw_create_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if (isConnectedToInternet()) {
                        showLoading();
                      //  ServerAPI.getInstance().createGroup(APIServerResponse.CREATE_GROUP,edt_txt_group_name.getText().toString() );
                        //create group api
                    } else {
                        showToast("Please check your internet connection.", Toast.LENGTH_LONG);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/

        aet_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (groupFunction.equalsIgnoreCase(Constants.GROUP_UPDATE)) {
                    String queryText = aet_search.getText().toString().toLowerCase(Locale.getDefault());
                    updateGroupAdapter.filter(queryText);
                } else {
                    String queryText = aet_search.getText().toString().toLowerCase(Locale.getDefault());
                    createGroupAddFriendsAdapter.filter(queryText);
                }


            }
        });

        aet_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if (groupFunction.equalsIgnoreCase(Constants.GROUP_UPDATE)) {

                        String queryText = aet_search.getText().toString().toLowerCase(Locale.getDefault());
                        updateGroupAdapter.filter(queryText);

                    } else {
                        String queryText = aet_search.getText().toString().toLowerCase(Locale.getDefault());
                        createGroupAddFriendsAdapter.filter(queryText);
                    }


                    return true;
                } else {
                    return false;
                }

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }

    }

    @OnClick(R.id.aiv_clear)
    public void clearSearch() {
        if (aet_search.getText().toString().length() != 0) {
            aet_search.setText("");
        }
    }

    @OnClick(R.id.img_view_event_pic)
    public void groupImage() {
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        dialogBuilder.setTitle("Select image");
            /*dialogBuilder.setMessage("Select ");*/
        dialogBuilder.setCancelable(true);
        dialogBuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dispatchTakePictureIntent();


            }
        });
        dialogBuilder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dispatchSelectGalleryPicIntent();


            }
        });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
        //Create alert dialog object via builder
        android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    @AfterPermissionGranted(RC_CAMERA_PERM_AND_STORAGE)
    private void dispatchTakePictureIntent() {
        if (EasyPermissions.hasPermissions(CreateGroupActivity.this, perms)) {


            dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE);

           /* Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }*/
        } else {
            EasyPermissions.requestPermissions(CreateGroupActivity.this, getString(R.string.access_camera_storage), RC_CAMERA_PERM_AND_STORAGE, perms);
        }

    }

    @AfterPermissionGranted(RC_GALLERY_PERM_AND_STORAGE)
    private void dispatchSelectGalleryPicIntent() {

        if (EasyPermissions.hasPermissions(CreateGroupActivity.this, galleryPerms)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_);
        } else {
            EasyPermissions.requestPermissions(CreateGroupActivity.this, getString(R.string.access_camera_storage),
                    RC_CAMERA_PERM_AND_STORAGE, perms);
        }

    }


    private String getAddedGroupFriends() {
        String userIds = "";
        try {

            ArrayList<MyFriendsModal.ListBean.UserInfoBean> taggedFriendsIds = createGroupAddFriendsAdapter.selectedListIds();


            if (taggedFriendsIds != null && taggedFriendsIds.size() >= 0) {
                for (int i = 0; i < taggedFriendsIds.size(); i++) {
                    if (i == 0) {
                        userIds = String.valueOf(taggedFriendsIds.get(i).getId());
                    } else {
                        userIds += "," + String.valueOf(taggedFriendsIds.get(i).getId());
                    }
                }
            }

            return userIds;

        } catch (Exception e) {
            e.printStackTrace();
            return userIds = "";
        }
    }

    private String getAddedGroupMembersWhileUpdating() {
        String userIds = "";

        try {
            ArrayList<UpdateGroupParcebleModal> taggedFriendsIds = updateGroupAdapter.selectedListIds();

            Log.i("isChecked Tagged", "" + taggedFriendsIds.size());


            if (taggedFriendsIds != null && taggedFriendsIds.size() >= 0) {
                for (int i = 0; i < taggedFriendsIds.size(); i++) {
                    if (i == 0) {
                        userIds = String.valueOf(taggedFriendsIds.get(i).getIds());
                    } else {
                        userIds += "," + String.valueOf(taggedFriendsIds.get(i).getIds());
                    }
                }
            }

            Log.i("isChecked userId", userIds);
            return userIds;

        } catch (Exception e) {
            e.printStackTrace();
            return userIds = "";
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyboard();
        CreateGroupActivity.this.finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (groupFunction.equalsIgnoreCase(Constants.GROUP_UPDATE)) {
            getMenuInflater().inflate(R.menu.menu_update, menu);
            return super.onCreateOptionsMenu(menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_create, menu);
            return super.onCreateOptionsMenu(menu);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_create_group) {
            //Call Create Group Api

            try {

                //Log.i("getAddedGroupFriends", getAddedGroupFriends());

                if (validation()) {
                    if (isConnectedToInternet()) {
                        showLoading();
                        hideKeyboard();
                        ServerAPI.getInstance().createGroup(APIServerResponse.CREATE_GROUP, getUserSessionId(), edt_txt_group_name.getText().toString().trim(), edt_txt_group_description.getText().toString().trim(), "1", getAddedGroupFriends(), mCurrentPhotoPath, this);
                        //create group api
                    } else {
                        showToast("Please check your internet connection.", Toast.LENGTH_LONG);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (item.getItemId() == R.id.menu_update_group) {
            try {

                //Log.i("getAddedGroupFriends", getAddedGroupFriends());

                if (validationGroupUpdate()) {
                    if (isConnectedToInternet()) {
                        showLoading();
                        hideKeyboard();
                        ServerAPI.getInstance().updateGroup(APIServerResponse.GROUP_UPDATE, getUserSessionId(), groupId, edt_txt_group_name.getText().toString().trim(), edt_txt_group_description.getText().toString().trim(), "1", getAddedGroupMembersWhileUpdating(), mCurrentPhotoPath, this);
                        //create group api
                    } else {
                        showToast("Please check your internet connection.", Toast.LENGTH_LONG);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            hideLoading();
            if (response.isSuccessful()) {
                CreateGroupModal createGroupModal;
                MyFriendsModal userListingModal;
                UpdateGroupModal updateGroupModal;

                switch (tag) {
                    case APIServerResponse.CREATE_GROUP:

                        createGroupModal = (CreateGroupModal) response.body();

                        if (createGroupModal.getStatus().equals("1")) {
                            EventBus.getDefault().post(new EventBusIsGroupCreatedModal("1"));
                            showToast(createGroupModal.getMessage(), Toast.LENGTH_LONG);
                            CreateGroupActivity.this.finish();
                        } else {
                            showToast(createGroupModal.getMessage(), Toast.LENGTH_LONG);
                        }
                        break;

                    case APIServerResponse.GROUP_UPDATE:

                        updateGroupModal = (UpdateGroupModal) response.body();
                        if (updateGroupModal.getStatus().equals("1")) {
                            EventBus.getDefault().post(new EventBusUpdateGroup("1"));
                            EventBus.getDefault().post(new EventBusIsGroupCreatedModal("1"));
                            showToast(updateGroupModal.getMessage(), Toast.LENGTH_LONG);
                            CreateGroupActivity.this.finish();
                        } else {
                            showToast(updateGroupModal.getMessage(), Toast.LENGTH_LONG);
                        }


                        break;

                    case APIServerResponse.MYFRIENDLIST:

                        userListingModal = (MyFriendsModal) response.body();

                        if (userListingModal.getStatus().equalsIgnoreCase("1")) {

                            if (userListingModal.getList().size() != 0) {


                                friendsList = userListingModal.getList();

                                if (groupFunction.equalsIgnoreCase(Constants.GROUP_UPDATE)) {

                                   /* for (int i = 0; i < userListingModal.getList().size(); i++) {
                                        UpdateGroupParcebleModal parcebleModal = new UpdateGroupParcebleModal();
                                        ArrayList<UpdateGroupParcebleModal> allFriends = new ArrayList<>();
                                        parcebleModal.setIds(userListingModal.getList().get(i).getUser_info().getId());
                                        parcebleModal.setName(userListingModal.getList().get(i).getUser_info().getProfile_details().getFirstname() + " " + userListingModal.getList().get(i).getUser_info().getProfile_details().getLastname());
                                        allFriends.add(parcebleModal);
                                    }*/
                                    for (int i = 0; i < userListingModal.getList().size(); i++) {
                                        UpdateGroupParcebleModal updateGroupParcebleModal = new UpdateGroupParcebleModal();
                                        /*updateGroupParcebleModal.setIds(userListingModal.getList().get(i).getUser_info().getId());
                                        updateGroupParcebleModal.setName(userListingModal.getList().get(i).getUser_info().getProfile_details().getFirstname() + " " + userListingModal.getList().get(i).getUser_info().getProfile_details().getLastname());*/
                                        //updateGroupParcebleModal.setGroupMember(true);

                                        for (int j = 0; j < groupMembers.size(); j++) {
                                            if (userListingModal.getList().get(i).getUser_info().getId() == groupMembers.get(j).getIds()) {
                                                updateGroupParcebleModal.setIds(userListingModal.getList().get(i).getUser_info().getId());
                                                updateGroupParcebleModal.setName(userListingModal.getList().get(i).getUser_info().getProfile_details().getFirstname() + " " + userListingModal.getList().get(i).getUser_info().getProfile_details().getLastname());
                                                updateGroupParcebleModal.setGroupMember(true);
                                                break;
                                            } else {
                                                updateGroupParcebleModal.setIds(userListingModal.getList().get(i).getUser_info().getId());
                                                updateGroupParcebleModal.setName(userListingModal.getList().get(i).getUser_info().getProfile_details().getFirstname() + " " + userListingModal.getList().get(i).getUser_info().getProfile_details().getLastname());
                                                updateGroupParcebleModal.setGroupMember(false);
                                            }
                                        }


                                        /*if (groupMembers.contains(updateGroupParcebleModal)) {
                                            updateGroupParcebleModal.setGroupMember(true);
                                        } else {
                                            updateGroupParcebleModal.setGroupMember(false);
                                        }*/


                                        allFriendsMembers.add(updateGroupParcebleModal);

                                        /*for (int j = 0; j < groupMembers.size(); j++){
                                            if (groupMembers.get(j).getIds() == updateGroupParcebleModal.getIds()){

                                            }
                                        }*/

                                      /*  if (groupMembers.indexOf(updateGroupParcebleModal) == 1) {
                                            Log.i("IndexOf", "Equal");

                                        } else if (groupMembers.indexOf(updateGroupParcebleModal) == -1) {
                                            Log.i("IndexOf", "NotEqual");
                                        }


                                        if (groupMembers.contains(updateGroupParcebleModal)) {
                                            updateGroupParcebleModal.setGroupMember(true);
                                        } else {
                                            updateGroupParcebleModal.setGroupMember(false);
                                        }*/


                                    }


                                    updateGroupAdapter = new UpdateGroupAdapter(CreateGroupActivity.this, allFriendsMembers);
                                    rv_tagging_friends_recycler_view.setAdapter(updateGroupAdapter);

                                } else {
                                    createGroupAddFriendsAdapter = new CreateGroupAddFriendsAdapter(CreateGroupActivity.this, friendsList);
                                    rv_tagging_friends_recycler_view.setAdapter(createGroupAddFriendsAdapter);
                                }


                            } else {
                                txtvw_no_friends.setVisibility(View.VISIBLE);
                                rv_tagging_friends_recycler_view.setVisibility(View.GONE);
                            }


                        } else {
                            txtvw_no_friends.setVisibility(View.VISIBLE);
                            rv_tagging_friends_recycler_view.setVisibility(View.GONE);
                        }


                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            hideLoading();
            switch (tag) {
                case APIServerResponse.CREATE_GROUP:
                    throwable.printStackTrace();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean validation() {
        if (edt_txt_group_name.getText().toString().isEmpty()) {
            showToast(Constants.GROUP_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (getAddedGroupFriends() == null || getAddedGroupFriends().isEmpty()) {
            showToast("Please select group members.", Toast.LENGTH_SHORT);
            return false;
        } else {
            return true;
        }
    }

    public boolean validationGroupUpdate() {
        if (edt_txt_group_name.getText().toString().isEmpty()) {
            showToast(Constants.GROUP_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (getAddedGroupMembersWhileUpdating() == null || getAddedGroupMembersWhileUpdating().isEmpty()) {
            showToast("Please select group members.", Toast.LENGTH_SHORT);
            return false;
        } else {
            return true;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == PICK_IMAGE_) {
                if (data != null) {

                    mCurrentPhotoPath = null;
                    mCurrentPhotoPath = FileUtils.getPath(CreateGroupActivity.this, data.getData());
                    Glide.with(CreateGroupActivity.this).load(mCurrentPhotoPath).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).override(96, 96).fitCenter().crossFade().into(img_view_event_pic);
                    mCurrentPhotoPath = Utilities.compressImage(mCurrentPhotoPath);

                }
            } else if (requestCode == REQUEST_IMAGE_CAPTURE) {

                if (mCurrentPhotoPath != null && !mCurrentPhotoPath.equalsIgnoreCase("")) {

                    handleBigCameraPhoto();

                    Log.i("Photo Path", mCurrentPhotoPath);
                    ///Previous Method
                   /* Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");

                    //imgvw_upload_book_cover_image.setImageBitmap(imageBitmap);

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
                    groupImagePath = getRealPathFromURI(tempUri);

                    Glide.with(CreateGroupActivity.this).load(groupImagePath).placeholder(R.drawable.placeholder).thumbnail(0.1f).override(96, 96).fitCenter().crossFade().into(img_view_event_pic);

*/
                }

            }

        }
    }

  /*  public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Image" + String.valueOf(System.currentTimeMillis()), null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        int idx = 0;
        if (cursor != null) {
            idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        }
        if (cursor != null) {
            String path = cursor.getString(idx);
            cursor.close();
            return path;
        } else {
            return "";
        }
    }*/


    private void dispatchTakePictureIntent(int actionCode) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        switch (actionCode) {
            case REQUEST_IMAGE_CAPTURE:
                File f = null;
                mCurrentPhotoPath = null;
                try {
                    f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                } catch (IOException e) {
                    e.printStackTrace();
                    f = null;
                    mCurrentPhotoPath = null;
                }
                break;

            default:
                break;
        } // switch

        startActivityForResult(takePictureIntent, actionCode);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
    }

    /* Photo album for this application */
    private String getAlbumName() {

        return getString(R.string.call4blessing_pictures);
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    private void handleBigCameraPhoto() {

        if (mCurrentPhotoPath != null) {
            setPic();
            galleryAddPic();
            mCurrentPhotoPath = Utilities.compressImage(mCurrentPhotoPath);
            //mCurrentPhotoPath = null;
        }

    }

    private void setPic() {

		/* There isn't enough memory to open up more than a couple camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */

        int targetW = img_view_event_pic.getWidth();
        int targetH = img_view_event_pic.getHeight();

		/* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		/* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		/* Associate the Bitmap to the ImageView */
        img_view_event_pic.setImageBitmap(bitmap);
        img_view_event_pic.setVisibility(View.VISIBLE);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }
}
