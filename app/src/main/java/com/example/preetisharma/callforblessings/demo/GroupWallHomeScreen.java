package com.example.preetisharma.callforblessings.demo;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Fragment.GroupWallHomeScreenFragment;
import com.example.preetisharma.callforblessings.HomeActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusIsGroupCreatedModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusUpdateGroup;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallModal;
import com.example.preetisharma.callforblessings.Server.Modal.UpdateGroupParcebleModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/6/2017.
 */

public class GroupWallHomeScreen extends BaseActivity implements APIServerResponse {


    @BindView(R.id.iv_group_cover_pic)
    AppCompatImageView iv_group_cover_pic;
    @BindView(R.id.iv_change_group_cover_pic)
    ImageView iv_change_group_cover_pic;
    @BindView(R.id.iv_add_group_friend)
    ImageView iv_add_group_friend;
    @BindView(R.id.iv_group_info)
    ImageView iv_group_info;
    @BindView(R.id.iv_delete_group)
    ImageView iv_delete_group;

    @BindView(R.id.atv_group_name)
    AppCompatTextView atv_group_name;
    @BindView(R.id.atv_group_member_count)
    AppCompatTextView atv_group_member_count;
    @BindView(R.id.txtvw_new_post)
    AppCompatTextView txtvw_new_post;
    @BindView(R.id.group_image)
    CircleImageView group_image;
    @BindView(R.id.viewpager)
    ViewPager viewpager;

    String groupId;

    String groupDescription = "", groupOwner = "", groupCreatedOn = "", groupModifiedOn = "", groupImage = "", notificationFlag = "";
    boolean isCreatedByMe;
    ArrayList<UpdateGroupParcebleModal> groupMembersIds = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_home_wall_activity_layout);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            groupId = extras.getString(Constants.GROUP_ID, "0");
            if (getIntent().getExtras().getString("Notification_Flag") != null && !getIntent().getExtras().getString("Notification_Flag").equalsIgnoreCase("")) {
                notificationFlag = getIntent().getExtras().getString("Notification_Flag");
            }
            Log.i("GroupID", groupId);
        }


        if (isConnectedToInternet()) {
            ServerAPI.getInstance().getGroupHomeWallPosts(APIServerResponse.GROUP_HOME_WALL_POSTS, getUserSessionId(), groupId, "1", this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        viewpager.setAdapter(new TabsAdapter(getSupportFragmentManager()));

    }

    @OnClick(R.id.iv_group_cover_pic)
    public void changeGroupPic() {
        Intent intent = new Intent(GroupWallHomeScreen.this, CreateGroupActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.GROUP_FUNCTION, Constants.GROUP_UPDATE);
        b.putString(Constants.GROUP_NAME, atv_group_name.getText().toString().trim());
        b.putString(Constants.GROUP_DESC, groupDescription);
        b.putString(Constants.GROUP_ID, groupId);
        b.putString(Constants.GROUP_PIC, groupImage);


        if (groupMembersIds != null && !groupMembersIds.isEmpty()) {
            b.putParcelableArrayList(Constants.GROUP_MEMBERS, groupMembersIds);
        }

        intent.putExtras(b);
        startActivity(intent);
    }

    @OnClick(R.id.iv_change_group_cover_pic)
    public void changeGroupPicture() {
        Intent intent = new Intent(GroupWallHomeScreen.this, CreateGroupActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.GROUP_FUNCTION, Constants.GROUP_UPDATE);
        b.putString(Constants.GROUP_NAME, atv_group_name.getText().toString().trim());
        b.putString(Constants.GROUP_DESC, groupDescription);
        b.putString(Constants.GROUP_ID, groupId);
        b.putString(Constants.GROUP_PIC, groupImage);


        if (groupMembersIds != null && !groupMembersIds.isEmpty()) {
            b.putParcelableArrayList(Constants.GROUP_MEMBERS, groupMembersIds);
        }

        intent.putExtras(b);
        startActivity(intent);
    }


    @OnClick(R.id.txtvw_new_post)
    public void createPost() {
        try {

            Intent intentCreatePost = new Intent(GroupWallHomeScreen.this, CreateGroupPostActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.GROUP_ID, groupId);
            intentCreatePost.putExtras(b);
            startActivity(intentCreatePost);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.iv_add_group_friend)
    public void addGroupMembers() {

        Intent intent = new Intent(GroupWallHomeScreen.this, CreateGroupActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.GROUP_FUNCTION, Constants.GROUP_UPDATE);
        b.putString(Constants.GROUP_NAME, atv_group_name.getText().toString().trim());
        b.putString(Constants.GROUP_DESC, groupDescription);
        b.putString(Constants.GROUP_ID, groupId);
        b.putString(Constants.GROUP_PIC, groupImage);


        if (groupMembersIds != null && !groupMembersIds.isEmpty()) {
            b.putParcelableArrayList(Constants.GROUP_MEMBERS, groupMembersIds);
        }

        intent.putExtras(b);
        startActivity(intent);
    }

    @OnClick(R.id.iv_group_info)
    public void groupInfo() {
        iv_group_info.setEnabled(false);
        groupInfoDialog(atv_group_name.getText().toString().trim(), groupDescription, groupOwner, groupCreatedOn, groupModifiedOn, isCreatedByMe);
        iv_group_info.setEnabled(true);

    }

    @OnClick(R.id.iv_delete_group)
    public void deleteGroup() {


        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        dialogBuilder.setTitle("Delete this group?");
            /*dialogBuilder.setMessage("Select ");*/
        dialogBuilder.setCancelable(true);
        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (isConnectedToInternet()) {

                    showLoading();
                    ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, getUserSessionId(), groupId, GroupWallHomeScreen.this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }


            }
        });
        dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();


            }
        });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
        //Create alert dialog object via builder
        android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {

            if (response.isSuccessful()) {
                GroupHomeWallModal groupHomeWallModal;
                GroupDeleteModal groupDeleteModal;

                switch (tag) {

                    case APIServerResponse.GROUP_HOME_WALL_POSTS:

                        groupHomeWallModal = (GroupHomeWallModal) response.body();

                        if (groupHomeWallModal.getStatus().equalsIgnoreCase("1")) {
                            atv_group_name.setText(groupHomeWallModal.getDetail().getGroup_name());
                            atv_group_member_count.setText(groupHomeWallModal.getDetail().getMembers_count() + " Members");


                            if (!groupHomeWallModal.getDetail().getGroup_icon().equalsIgnoreCase("")) {
                                Glide.with(GroupWallHomeScreen.this).load(groupHomeWallModal.getDetail().getGroup_icon())/*.placeholder(R.drawable.placeholder).thumbnail(0.1f)*/.into(group_image);
                            } else {
                                Glide.with(GroupWallHomeScreen.this).load(groupHomeWallModal.getDetail().getGroup_icon()).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(group_image);
                            }

                            groupOwner = String.format(getResources().getString(R.string.no_of_like), groupHomeWallModal.getDetail().getUser_info().getProfile_details().getFirstname(), groupHomeWallModal.getDetail().getUser_info().getProfile_details().getLastname());
                            groupDescription = groupHomeWallModal.getDetail().getGroup_description();
                            groupCreatedOn = groupHomeWallModal.getDetail().getCreated_on();
                            groupModifiedOn = groupHomeWallModal.getDetail().getModified_on();

                            Glide.with(GroupWallHomeScreen.this).load(groupHomeWallModal.getDetail().getGroup_icon()).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(iv_group_cover_pic);

                            /*setPic(groupHomeWallModal.getDetail().getGroup_icon());*/
                            groupId = String.valueOf(groupHomeWallModal.getDetail().getId());
                            isCreatedByMe = groupHomeWallModal.getDetail().isIs_created_by_me();

                            if (groupHomeWallModal.getDetail().isIs_created_by_me()) {
                                iv_add_group_friend.setVisibility(View.VISIBLE);
                                iv_delete_group.setVisibility(View.VISIBLE);
                                iv_group_info.setVisibility(View.VISIBLE);
                                groupImage = groupHomeWallModal.getDetail().getGroup_icon();
                                for (int i = 0; i < groupHomeWallModal.getDetail().getMembers().size(); i++) {
                                    UpdateGroupParcebleModal updateGroupParcebleModal = new UpdateGroupParcebleModal();
                                    updateGroupParcebleModal.setIds(groupHomeWallModal.getDetail().getMembers().get(i).getUser_info().getId());
                                    updateGroupParcebleModal.setName(groupHomeWallModal.getDetail().getMembers().get(i).getUser_info().getProfile_details().getFirstname() + " " + groupHomeWallModal.getDetail().getMembers().get(i).getUser_info().getProfile_details().getLastname());
                                    updateGroupParcebleModal.setGroupMember(true);
                                    groupMembersIds.add(updateGroupParcebleModal);

                                }
                            } else {
                                iv_add_group_friend.setVisibility(View.GONE);
                                iv_delete_group.setVisibility(View.GONE);
                                iv_group_info.setVisibility(View.VISIBLE);
                            }

                        }

                        break;
                    case APIServerResponse.GROUP_DELETE:

                        groupDeleteModal = (GroupDeleteModal) response.body();

                        if (groupDeleteModal.getStatus().equalsIgnoreCase("1")) {
                            EventBus.getDefault().post(new EventBusIsGroupCreatedModal("1"));
                            showToast(groupDeleteModal.getMessage(), Toast.LENGTH_SHORT);
                            GroupWallHomeScreen.this.finish();
                        }


                        hideLoading();
                        break;

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();

    }


    private class TabsAdapter extends FragmentPagerAdapter {
        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0: {
                    fragment = new GroupWallHomeScreenFragment();
                    Bundle b = new Bundle();

                    b.putString(Constants.GROUP_ID, groupId);
                    fragment.setArguments(b);
                    return fragment;
                }
               /* case 1: {
                    fragment = new NotificationsFragment();

                    return fragment;
                }*/

            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Wall";
               /* case 1:
                    return "Friends";
                case 2:
                    return "Gallery";*/
            }
            return "";
        }
    }

    private void groupInfoDialog(final String groupName, final String groupDesc, String groupOwner, String groupCreatedOn, String groupModifiedOn, boolean isCreatedByMe) {


        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setTitle(String.format(res.getString(R.string.about_brand), brandName));
        dialog.setContentView(R.layout.dialog_group_info_layout);


        //LinearLayout ll_brand_info = (LinearLayout) dialog.findViewById(R.id.ll_brand_info);

        TextInputEditText tet_group_name = (TextInputEditText) dialog.findViewById(R.id.tet_group_name);

        TextInputEditText tet_group_desc = (TextInputEditText) dialog.findViewById(R.id.tet_group_desc);
        TextInputEditText tet_group_owner = (TextInputEditText) dialog.findViewById(R.id.tet_group_owner);
        TextInputEditText tet_group_created_on = (TextInputEditText) dialog.findViewById(R.id.tet_group_created_on);
        TextInputEditText tet_group_modified_on = (TextInputEditText) dialog.findViewById(R.id.tet_group_modified_on);
        //AppCompatTextView atv_group_member_count = (AppCompatTextView) dialog.findViewById(R.id.atv_group_member_count);

        tet_group_name.setText(groupName);
        tet_group_desc.setText(groupDesc);
        tet_group_owner.setText(groupOwner);
        tet_group_created_on.setText(groupCreatedOn);
        tet_group_modified_on.setText(groupModifiedOn);
        //atv_group_member_count.setText(groupMemberCount);

        FloatingActionButton fab_edit_group = (FloatingActionButton) dialog.findViewById(R.id.fab_edit_group);
        if (isCreatedByMe) {
            fab_edit_group.setVisibility(View.VISIBLE);
        } else {
            fab_edit_group.setVisibility(View.GONE);
        }

        fab_edit_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(GroupWallHomeScreen.this, CreateGroupActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.GROUP_FUNCTION, Constants.GROUP_UPDATE);
                b.putString(Constants.GROUP_NAME, groupName);
                b.putString(Constants.GROUP_DESC, groupDesc);
                b.putString(Constants.GROUP_ID, groupId);
                b.putString(Constants.GROUP_PIC, groupImage);


                if (groupMembersIds != null && !groupMembersIds.isEmpty()) {
                    b.putParcelableArrayList(Constants.GROUP_MEMBERS, groupMembersIds);
                }

                intent.putExtras(b);
                startActivity(intent);
            }
        });


        dialog.show();
    }


    @Subscribe
    public void isGroupCreatedPost(EventBusUpdateGroup eventBusUpdateGroup) {

        if (isConnectedToInternet()) {
            try {
                if (eventBusUpdateGroup.getIsGroupUpdated().equalsIgnoreCase("1")) {
                    ServerAPI.getInstance().getGroupHomeWallPosts(APIServerResponse.GROUP_HOME_WALL_POSTS, getUserSessionId(), groupId, "1", this);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }


    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();


    }

    @Override
    public void onBackPressed() {
        if (notificationFlag != null && !notificationFlag.equalsIgnoreCase("")) {
            Intent i = new Intent(GroupWallHomeScreen.this, HomeActivity.class);
            startActivity(i);
            finish();
        } else {
            super.onBackPressed();
        }
    }


    /*private void setPic(String imgPath) {



      *//*   There isn't enough memory to open up more than a couple camera photos
         So pre-scale the target bitmap into which the file is decoded

		 Get the size of the ImageView *//*

        int targetW = group_image.getWidth();
        int targetH = group_image.getHeight();

		 *//*Get the size of the image *//*
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		 *//*Figure out which way needs to be reduced less*//*
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		 *//*Set bitmap options to scale the image decode target *//*
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

		 *//*Decode the JPEG file into a Bitmap *//*
        Bitmap bitmap = BitmapFactory.decodeFile(imgPath, bmOptions);

		 *//*Associate the Bitmap to the ImageView *//*
        group_image.setImageBitmap(bitmap);
    }*/

}
