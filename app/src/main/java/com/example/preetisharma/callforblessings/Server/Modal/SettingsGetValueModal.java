package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/27/2017.
 */

public class SettingsGetValueModal {


    /**
     * status : 1
     * settings : PUBLIC
     */

    private String status;
    private String settings;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }
}
