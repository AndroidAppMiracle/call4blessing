package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/15/2017.
 */

public class EventBusHomeWallCommented {
    private final int status;
    private final int itemPosition;

    public EventBusHomeWallCommented(int status, int itemPosition) {
        this.status = status;
        this.itemPosition = itemPosition;
    }

    public int getStatus() {
        return status;
    }

    public int getItemPosition() {

        return itemPosition;
    }
}
