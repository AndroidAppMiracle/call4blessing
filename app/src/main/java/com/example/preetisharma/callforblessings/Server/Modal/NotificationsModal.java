package com.example.preetisharma.callforblessings.Server.Modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kshitiz Bali on 5/4/2017.
 */

public class NotificationsModal {

    /**
     * status : 1
     * list : [{"object_class":"FRIEND_REQUEST","object_id":6,"userInfo":{"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492509042image.jpg"},"is_friend":1,"request_respond":"NO"},"created_time":"3  minutes   ago","text":"Mukesh Kumar has sent you friend request."},{"object_class":"EVENT_INVITE","object_id":6,"userInfo":{"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492509042image.jpg"},"is_friend":1,"request_respond":"NO"},"created_time":"4  minutes   ago","text":"You have an invitation for the event Joy Event"},{"object_class":"POST_COMMENT","object_id":54,"userInfo":{"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492509042image.jpg"},"is_friend":1,"request_respond":"NO"},"created_time":"7  minutes   ago","text":"Mukesh Kumar commented on Palash Jindal post."},{"object_class":"POST_LIKE","object_id":54,"userInfo":{"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492509042image.jpg"},"is_friend":1,"request_respond":"NO"},"created_time":"7  minutes   ago","text":"Mukesh Kumar liked Palash Jindal post."}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * object_class : FRIEND_REQUEST
         * object_id : 6
         * userInfo : {"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492509042image.jpg"},"is_friend":1,"request_respond":"NO"}
         * created_time : 3  minutes   ago
         * text : Mukesh Kumar has sent you friend request.
         */

        private String object_class;
        private int object_id;
        private UserInfoBean userInfo;
        private String created_time;
        private String text;

        public String getObject_class() {
            return object_class;
        }

        public void setObject_class(String object_class) {
            this.object_class = object_class;
        }

        public int getObject_id() {
            return object_id;
        }

        public void setObject_id(int object_id) {
            this.object_id = object_id;
        }

        public UserInfoBean getUserInfo() {
            return userInfo;
        }

        public void setUserInfo(UserInfoBean userInfo) {
            this.userInfo = userInfo;
        }

        public String getCreated_time() {
            return created_time;
        }

        public void setCreated_time(String created_time) {
            this.created_time = created_time;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public static class UserInfoBean {
            /**
             * id : 223
             * username : Mukesh.singh
             * email : Mukesh.singh@emptask.com
             * updated : 1
             * profile_details : {"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492509042image.jpg"}
             * is_friend : 1
             * request_respond : NO
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private String WALL;
            @SerializedName("ABOUT INFO")
            private String about_info;
            @SerializedName("FRIEND REQUEST")
            private String friend_request;
            private String MESSAGE;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public String getWALL() {
                return WALL;
            }

            public void setWALL(String WALL) {
                this.WALL = WALL;
            }

            public String getabout_info() {
                return about_info;
            }

            public void setabout_info(String about_info) {
                this.about_info = about_info;
            }

            public String getfriend_request() {
                return friend_request;
            }

            public void setfriend_request(String friend_request) {
                this.friend_request = friend_request;
            }

            public String getMESSAGE() {
                return MESSAGE;
            }

            public void setMESSAGE(String MESSAGE) {
                this.MESSAGE = MESSAGE;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : Mukesh
                 * lastname : Kumar
                 * d_o_b : 1991/09/08
                 * phone : 9988549399
                 * gender : MALE
                 * location :
                 * country :
                 * state :
                 * cover_pic : http://call4blessing.com/uploads/coverphoto/1492602899image.jpg
                 * profile_pic : http://call4blessing.com/uploads/profilepic/1492509042image.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }
    }
}
