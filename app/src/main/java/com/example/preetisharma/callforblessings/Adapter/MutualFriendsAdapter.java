package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.UnfriendAFriendModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/3/2017.
 */

public class MutualFriendsAdapter extends RecyclerView.Adapter<MutualFriendsAdapter.DataViewHolder> {
    public List<MyFriendsModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;

    public MutualFriendsAdapter(Activity mContext, List<MyFriendsModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<MyFriendsModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public MutualFriendsAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_adapter_friends_list, parent, false);
        MutualFriendsAdapter.DataViewHolder dataView = new MutualFriendsAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final MutualFriendsAdapter.DataViewHolder holder, int position) {

        holder.layout_response_buttons.setVisibility(View.GONE);
        holder.txtvw_friends_request_name.setText(list.get(position).getUser_info().getProfile_details().getFirstname() + " " + list.get(position).getUser_info().getProfile_details().getLastname());
        Glide.with(mContext).load(list.get(position).getUser_info().getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).into(holder.iv_friend_request_image);
        holder.ll_my_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("user id", String.valueOf(list.get(holder.getAdapterPosition()).getId()));

                Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUser_info().getId()));
                b.putBoolean(Constants.TIMELINE_ENABLED, false);
                b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                b.putString(Constants.USER_IMAGE, list.get(holder.getAdapterPosition()).getUser_info().getProfile_details().getProfile_pic());
                b.putString(Constants.COVER_PIC, list.get(holder.getAdapterPosition()).getUser_info().getProfile_details().getCover_pic());
                b.putString(Constants.USER_NAME, list.get(holder.getAdapterPosition()).getUser_info().getProfile_details().getFirstname() + " " + list.get(holder.getAdapterPosition()).getUser_info().getProfile_details().getLastname());
                b.putString(Constants.PRIVACY_WALL, list.get(holder.getAdapterPosition()).getUser_info().getWALL());
                b.putString(Constants.PRIVACY_ABOUT_INFO, list.get(holder.getAdapterPosition()).getUser_info().getabout_info());
                b.putString(Constants.PRIVACY_FRIEND_REQUEST, list.get(holder.getAdapterPosition()).getUser_info().getfriend_request());
                b.putString(Constants.PRIVACY_MESSAGE, list.get(holder.getAdapterPosition()).getUser_info().getMESSAGE());
                i.putExtras(b);
                mContext.startActivity(i);
            }
        });


    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;
        @BindView(R.id.layout_response_buttons)
        LinearLayout layout_response_buttons;
        @BindView(R.id.ll_my_friend)
        LinearLayout ll_my_friend;
        @BindView(R.id.txtvw_ignore_friend_request)
        AppCompatTextView txtvw_ignore_friend_request;
        @BindView(R.id.txtvw_accept_friend_request)
        AppCompatTextView txtvw_accept_friend_request;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            txtvw_ignore_friend_request.setVisibility(View.GONE);
            txtvw_accept_friend_request.setText("Unfriend");

            txtvw_accept_friend_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new AlertDialog.Builder(mContext)
                            .setTitle("Do you want to Unfriend " + list.get(getAdapterPosition()).getUser_info().getProfile_details().getFirstname() + "?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                                        ServerAPI.getInstance().unfriend(APIServerResponse.UNFRIEND, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getUser_info().getId()), MutualFriendsAdapter.DataViewHolder.this);
                                    } else {
                                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setIcon(R.mipmap.ic_app_icon)
                            .show();


                }
            });

        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                if (response.isSuccessful()) {
                    UnfriendAFriendModal unfriendAFriendModal;

                    switch (tag) {
                        case APIServerResponse.UNFRIEND:
                            unfriendAFriendModal = (UnfriendAFriendModal) response.body();
                            if (unfriendAFriendModal.getStatus().equals("1")) {
                                refresh(getAdapterPosition());
                            }
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int tag, Throwable throwable) {

        }
    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        list.remove(position);
        notifyItemRemoved(position);
    }
}
