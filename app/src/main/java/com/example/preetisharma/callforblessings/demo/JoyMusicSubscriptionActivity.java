package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.JoyMusicSubscriptionPlansAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionPlansModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.List;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/10/2017.
 */

public class JoyMusicSubscriptionActivity extends BaseActivity implements APIServerResponse {

    RecyclerView rv_viewSubscriptionPlan;
    AppCompatImageView aiv_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joy_music_subcription);

        rv_viewSubscriptionPlan = (RecyclerView) findViewById(R.id.rv_viewSubscriptionPlan);
        aiv_close = (AppCompatImageView) findViewById(R.id.aiv_close);

        aiv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_viewSubscriptionPlan.setLayoutManager(gridLayoutManager);
        rv_viewSubscriptionPlan.setItemAnimator(new DefaultItemAnimator());


        try {

            showLoading();
            ServerAPI.getInstance().getJoyMusicSubscriptionPlans(APIServerResponse.JOY_MUSIC_SUBSCRIPTION_PLAN, getUserSessionId(), this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {


        try {

            if (response.isSuccessful()) {

                JoyMusicSubscriptionPlansModal joyMusicSubscriptionPlansModal;
                switch (tag) {

                    case APIServerResponse.JOY_MUSIC_SUBSCRIPTION_PLAN:

                        joyMusicSubscriptionPlansModal = (JoyMusicSubscriptionPlansModal) response.body();

                        if (joyMusicSubscriptionPlansModal.getStatus().equalsIgnoreCase("1")) {

                            List<JoyMusicSubscriptionPlansModal.ListBean> list = joyMusicSubscriptionPlansModal.getList();


                            JoyMusicSubscriptionPlansAdapter adapter = new JoyMusicSubscriptionPlansAdapter(this, list);
                            rv_viewSubscriptionPlan.setAdapter(adapter);

                        }

                        hideLoading();
                        break;
                }


            } else {
                hideLoading();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.getMessage();
    }
}
