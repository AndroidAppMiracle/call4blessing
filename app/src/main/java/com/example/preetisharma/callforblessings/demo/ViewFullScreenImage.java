package com.example.preetisharma.callforblessings.demo;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.TouchImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 6/15/2017.
 */

public class ViewFullScreenImage extends BaseActivity {


    @BindView(R.id.tiv_fullscreen_img)
    TouchImageView imgView;

    @BindView(R.id.image_progress)
    ProgressBar image_progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image_full_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);

        //TouchImageView imgView = (TouchImageView) findViewById(R.id.tiv_fullscreen_img);


        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            String img = extras.getString(Constants.IMAGE, "0");
            Log.i("img", img);


            //Glide.with(this).load(img).placeholder(R.drawable.placeholder).into(imgView);


            Glide.with(this).load(img)
                    .asBitmap()
                    .placeholder(R.drawable.placeholder_callforblessings)
                    .into(new BitmapImageViewTarget(imgView) {
                        @Override
                        public void onResourceReady(Bitmap drawable, GlideAnimation anim) {
                            super.onResourceReady(drawable, anim);
                            image_progress.setVisibility(View.GONE);
                        }
                    });

            /*Glide.with(this).load(img).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    image_progress.setVisibility(View.GONE);
                    imgView.setImageResource(R.drawable.placeholder);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    image_progress.setVisibility(View.GONE);
                    return false;
                }
            }).into(imgView);*/
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ViewFullScreenImage.this.finish();
    }
}
