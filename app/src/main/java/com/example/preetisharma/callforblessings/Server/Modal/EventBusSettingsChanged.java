package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/27/2017.
 */

public class EventBusSettingsChanged {

    private final boolean isSettingChanged;

    public EventBusSettingsChanged(boolean isSettingChanged) {
        this.isSettingChanged = isSettingChanged;
    }

    public boolean getIsSettingChanged() {
        return isSettingChanged;
    }

}
