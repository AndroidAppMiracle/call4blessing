package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.HomeActivity;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.delight.android.webview.AdvancedWebView;

/**
 * Created by satoti.garg on 6/30/2017.
 */

public class DirectMessageWebActivity extends BaseActivity implements AdvancedWebView.Listener {
    @BindView(R.id.webview_chat)
    AdvancedWebView webview_chat;
    String notificationFlag = "";
    String friendId = "", messageUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        updateStatusBar();
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {

            if (getIntent().getExtras().getString("Notification_Flag") != null && !getIntent().getExtras().getString("Notification_Flag", "").equalsIgnoreCase("")) {
                notificationFlag = getIntent().getExtras().getString("Notification_Flag");
            }
            Log.i("notificationFlag", notificationFlag);

            if (getIntent().getExtras().getString(Constants.FRIEND_ID, "") != null && !getIntent().getExtras().getString(Constants.FRIEND_ID, "").equalsIgnoreCase("")) {

                friendId = getIntent().getExtras().getString(Constants.FRIEND_ID, "");
            }

        }


        try {
            Uri.Builder builder = new Uri.Builder();
            builder.scheme("http").encodedAuthority("www.call4blessing.com:8890").appendPath("chat").appendPath(getUserID()).appendPath(friendId).appendPath("okk");
            messageUrl = builder.build().toString();
            Log.i("messageUrl", messageUrl);
        } catch (Exception e) {
            e.printStackTrace();
            messageUrl = "http://www.call4blessing.com:8890/chat/" + getUserID() + "/" + friendId + "/okk";
        }


        webview_chat.getSettings().setJavaScriptEnabled(true);
        Map<String, String> params = new HashMap<>();
        params.put("sessionkey", getUserSessionId());
        webview_chat.loadUrl(messageUrl);
        webview_chat.setWebViewClient(new webviewClient());
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        webview_chat.onActivityResult(requestCode, resultCode, intent);
        // ...
    }


    public class webviewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (request.getRequestHeaders().equals(getUserSessionId())) {

                    Map<String, String> params = new HashMap<>();
                    params.put("sessionkey", getUserSessionId());
                    webview_chat.loadUrl("http://dev.miracleglobal.com:8890/friendList", params);
                } else {
                    onBackPressed();
                }

            }
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (event.getAction()) {
                    case KeyEvent.KEYCODE_BACK:
                        if (view.canGoBack()) {
                            view.goBack();
                        } else {
                            finish();
                        }
                        return true;
                }

            }
            return super.shouldOverrideKeyEvent(view, event);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showLoading();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideLoading();
        }


    }

    @Override
    public void onBackPressed() {

        if (webview_chat.canGoBack()) {
            webview_chat.goBack();
        } else {
            if (notificationFlag != null && !notificationFlag.equalsIgnoreCase("")) {
                Intent i = new Intent(DirectMessageWebActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        return super.onKeyDown(keyCode, event);
    }
}

