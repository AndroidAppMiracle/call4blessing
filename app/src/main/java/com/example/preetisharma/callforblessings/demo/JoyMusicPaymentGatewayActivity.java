package com.example.preetisharma.callforblessings.demo;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BookPaymentModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.payu.india.Extras.PayUSdkDetails;
import com.payu.india.Interfaces.OneClickPaymentListener;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.payuui.Activity.PayUBaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/11/2017.
 */

public class JoyMusicPaymentGatewayActivity extends BaseActivity implements OneClickPaymentListener, APIServerResponse {

    PayuHashes payuHashes = new PayuHashes();
    private PaymentParams mPaymentParams;
    private PayuConfig payuConfig;
    private String merchantKey, userCredentials, buyItemFlag = "", itemID = "", itemAmount = "", itemName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getExtras() != null) {
            //buyItemFlag = getIntent().getExtras().getString(Constants.BUY_MUSIC_ITEM_FLAG);
            itemID = getIntent().getExtras().getString(Constants.BUY_JOY_MUSIC_SUBSCRIPTION_ID);
            itemAmount = getIntent().getExtras().getString(Constants.BUY_JOY_MUSIC_SUBSCRIPTION_AMOUNT);
            itemName = getIntent().getExtras().getString(Constants.BUY_JOY_MUSIC_SUBSCRIPTION_NAME);
        }
        Payu.setInstance(this);
        //PayUSdkDetails payUSdkDetails = new PayUSdkDetails();
        //Toast.makeText(this, "Build No: " + payUSdkDetails.getSdkBuildNumber() + "\n Build Type: " + payUSdkDetails.getSdkBuildType() + " \n Build Flavor: " + payUSdkDetails.getSdkFlavor() + "\n Application Id: " + payUSdkDetails.getSdkApplicationId() + "\n Version Code: " + payUSdkDetails.getSdkVersionCode() + "\n Version Name: " + payUSdkDetails.getSdkVersionName(), Toast.LENGTH_LONG).show();
        //brandDetailsDialog("1 Month Plan", R.drawable.ic_success_transaction, "DONE");
        sendPaymentDeatils();

    }

    @Override
    public HashMap<String, String> getAllOneClickHash(String userCredentials) {
        return null;
    }

    @Override
    public void getOneClickHash(String cardToken, String merchantKey, String userCredentials) {

    }

    @Override
    public void saveOneClickHash(String cardToken, String oneClickHash) {

    }

    @Override
    public void deleteOneClickHash(String cardToken, String userCredentials) {
        final String postParams = "card_token=" + cardToken;

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    //TODO Replace below url with your server side file url.
                    URL url = new URL("https://payu.herokuapp.com/delete_merchant_hash");

                    byte[] postParamsByte = postParams.getBytes("UTF-8");

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                this.cancel(true);
            }
        }.execute();

    }


    public void sendPaymentDeatils() {

        merchantKey = getResources().getString(R.string.merchant_key);
        userCredentials = merchantKey + ":" + getUserEmailID();
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().sendPaymentDeatilstoServer(APIServerResponse.BOOKPAYMENTHASH, getUserSessionId(), itemID, "MUSIC", getFirstName(), getLastName(), getUserEmailID(), "", "", getUserPhoneNumber(), getUserAddress(), userCredentials, this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {

        hideLoading();
        BookPaymentModal bookPayment;
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.BOOKPAYMENTHASH:

                        bookPayment = (BookPaymentModal) response.body();


                        if (bookPayment.getStatus().equals("1")) {
                            payuHashes.setPaymentHash(bookPayment.getHashes().getPayment_hash());
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(bookPayment.getHashes().getPayment_related_details_for_mobile_sdk_hash());
                            payuHashes.setVasForMobileSdkHash(bookPayment.getHashes().getVas_for_mobile_sdk_hash());
                            navigateToBaseActivity(payuHashes, bookPayment.getTxnid());
                        } else {
                            showToast(bookPayment.getError(), Toast.LENGTH_SHORT);
                        }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }


    public void navigateToBaseActivity(PayuHashes payhash, String transactionId) {

        String amount = itemAmount;
        //String email = edt_txt_email.getText().toString();


        //TODO Below are mandatory params for hash generation
        mPaymentParams = new PaymentParams();
        /**
         * For Test Environment, merchantKey = "gtKFFx"
         * For Production Environment, merchantKey should be your live key or for testing in live you can use "0MQaQP"
         */
        mPaymentParams.setKey(merchantKey);
        mPaymentParams.setAmount(amount);
        mPaymentParams.setProductInfo(itemName);
        mPaymentParams.setFirstName(getFirstName());
        //mPaymentParams.setLastName(getLastName());
        mPaymentParams.setEmail(getUserEmailID());
        // mPaymentParams.setCity("");
        // mPaymentParams.setZipCode("");
        mPaymentParams.setPhone(getUserPhoneNumber());
        //mPaymentParams.setAddress1(getUserAddress());
        mPaymentParams.setHash(payhash.getPaymentHash());

        /*
        * Transaction Id should be kept unique for each transaction.
        * */
        mPaymentParams.setTxnId(transactionId);

        /**
         * Surl --> Success url is where the transaction response is posted by PayU on successful transaction
         * Furl --> Failre url is where the transaction response is posted by PayU on failed transaction
         */
        mPaymentParams.setSurl(Constants.PAYU_URL);
        mPaymentParams.setFurl(Constants.PAYU_URL);

        /*mPaymentParams.setSurl("http://dev.miracleglobal.com/cal-php/web/payment/callback");
        mPaymentParams.setFurl("http://dev.miracleglobal.com/cal-php/web/payment/callback");*/

         /*
         * udf1 to udf5 are options params where you can pass additional information related to transaction.
         * If you don't want to use it, then send them as empty string like, udf1=""
         * */
        mPaymentParams.setUdf1("");
        mPaymentParams.setUdf2("");
        mPaymentParams.setUdf3("");
        mPaymentParams.setUdf4("");
        mPaymentParams.setUdf5("");

        /**
         * These are used for store card feature. If you are not using it then user_credentials = "default"
         * user_credentials takes of the form like user_credentials = "merchant_key : user_id"
         * here merchant_key = your merchant key,
         * user_id = unique id related to user like, email, phone number, etc.
         * */
        mPaymentParams.setUserCredentials(userCredentials);

        //TODO Pass this param only if using offer key
        //mPaymentParams.setOfferKey("cardnumber@8370");

        //TODO Sets the payment environment in PayuConfig object
        payuConfig = new PayuConfig();
        payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);
        launchSdkUI(payhash);


    }


    public void launchSdkUI(PayuHashes payuHashes) {

        Intent intent = new Intent(this, PayUBaseActivity.class);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        //fetchMerchantHashes(intent);

    }


    private void fetchMerchantHashes(final Intent intent) {
        // now make the api call.
        final String postParams = "merchant_key=" + merchantKey + "&user_credentials=" + userCredentials;
        final Intent baseActivityIntent = intent;
        new AsyncTask<Void, Void, HashMap<String, String>>() {

            @Override
            protected HashMap<String, String> doInBackground(Void... params) {
                try {
                    //TODO Replace below url with your server side file url.
                    URL url = new URL("http://dev.miracleglobal.com/cal-php/web/payment/callback");

                    byte[] postParamsByte = postParams.getBytes("UTF-8");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);
                    Log.e("conn.getInputStream()", "Response" + conn.getInputStream());
                    InputStream responseInputStream = conn.getInputStream();
                    StringBuffer responseStringBuffer = new StringBuffer();
                    byte[] byteContainer = new byte[1024];
                    for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                        responseStringBuffer.append(new String(byteContainer, 0, i));
                    }
                    Log.e("Response", "Response" + responseStringBuffer.toString());
                    // JSONObject response = new JSONObject(responseStringBuffer.toString());

                   /* HashMap<String, String> cardTokens = new HashMap<String, String>();
                    JSONArray oneClickCardsArray = response.getJSONArray("data");
                    int arrayLength;
                    if ((arrayLength = oneClickCardsArray.length()) >= 1) {
                        for (int i = 0; i < arrayLength; i++) {
                            cardTokens.put(oneClickCardsArray.getJSONArray(i).getString(0), oneClickCardsArray.getJSONArray(i).getString(1));
                        }
                        return cardTokens;
                   */
                    // pass these to next activity

                } catch (Exception e) {
                    e.printStackTrace();
                } /*catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                return null;
            }

            @Override
            protected void onPostExecute(HashMap<String, String> oneClickTokens) {
                super.onPostExecute(oneClickTokens);
                baseActivityIntent.putExtra(PayuConstants.ONE_CLICK_CARD_TOKENS, oneClickTokens);

                startActivityForResult(baseActivityIntent, PayuConstants.PAYU_REQUEST_CODE);
            }
        }.execute();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {

                    /**
                     * Here, data.getStringExtra("payu_response") ---> Implicit response sent by PayU
                     * data.getStringExtra("result") ---> Response received from merchant's Surl/Furl
                     *
                     * PayU sends the same response to merchant server and in app. In response check the value of key "status"
                     * for identifying status of transaction. There are two possible status like, success or failure
                     * */


                    try {
                        JSONObject jsonObject = new JSONObject(data.getStringExtra("payu_response"));
                        if (jsonObject.getString("status").equalsIgnoreCase("success")) {

                            confirmationDialog(itemName, R.drawable.ic_success_transaction, "Your subscription for Joy Music has been activiated.");
                            /*Intent sendData = new Intent(this, OrderConfirmationActivity.class);
                            Bundle b = new Bundle();
                            b.putString(Constants.TRANSACTIONSTATUS, Constants.SUCCESS);
                            b.putString(Constants.TRANSACTIONID, jsonObject.getString("txnid"));
                            sendData.putExtras(b);
                            startActivity(sendData);*/
                        } else {

                            confirmationDialog(itemName, R.drawable.ic_cancel_transaction, "Your subscription for Joy Music cannot be activiated.");
                            /*Intent sendData = new Intent(this, OrderConfirmationActivity.class);
                            Bundle b = new Bundle();
                            b.putString(Constants.TRANSACTIONSTATUS, Constants.FAILURE);
                            b.putString(Constants.TRANSACTIONID, jsonObject.getString("txnid"));
                            sendData.putExtras(b);
                            startActivity(sendData);*/
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(this, getString(R.string.could_not_receive_data), Toast.LENGTH_LONG).show();
                }
            } else {
                JoyMusicPaymentGatewayActivity.this.finish();
            }
        }
    }


    private void confirmationDialog(String planName, int drawableImage, String message) {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setTitle(String.format(res.getString(R.string.about_brand), brandName));
        dialog.setContentView(R.layout.dialog_joy_music_subscription_layout);


        //LinearLayout ll_brand_info = (LinearLayout) dialog.findViewById(R.id.ll_brand_info);

        TextView tv_plan = (TextView) dialog.findViewById(R.id.tv_plan);
        ImageView iv_order_confirmation = (ImageView) dialog.findViewById(R.id.iv_order_confirmation);
        TextView tv_orderPaymentStatus = (TextView) dialog.findViewById(R.id.tv_orderPaymentStatus);

        TextView tv_ok = (TextView) dialog.findViewById(R.id.tv_ok);


        tv_plan.setText(planName);
        Glide.with(this)
                .load(drawableImage).placeholder(R.drawable.placeholder_callforblessings)
                .thumbnail(0.1f).override(150, 150).crossFade()
                .into(iv_order_confirmation);

        tv_orderPaymentStatus.setText(message);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/

        //TextView text_Cancel = (TextView) dialog.findViewById(R.id.text_Cancel);


        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //sendPaymentDeatils();
                finish();
            }
        });


        dialog.show();
    }


}
