package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 2/14/2017.
 */

public class MyInvitationRequestAcceptReject {

    /**
     * status : 1
     * message : Invitation Accepted.
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
