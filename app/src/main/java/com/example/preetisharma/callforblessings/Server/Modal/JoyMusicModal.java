package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by preeti.sharma on 2/27/2017.
 */

public class JoyMusicModal {


    /**
     * status : 1
     * list : [{"id":1,"language":"English","albums":[{"id":2,"name":"free","description":"Calvin Harris n Disciples","language":"English","payment_type":"FREE","amount":"","created_at":"2 months ago","is_featured":"NO","album_cover_image":"http://call4blessing.com/uploads/album/149577685201 Tu Meri Jaan.mp3","is_downloaded":"false"}]},{"id":2,"language":"Hindi","albums":[{"id":1,"name":"Tu Meri Jaan","description":"Tu Meri Jaan","language":"Hindi","payment_type":"PAID","amount":"100","created_at":"2 months ago","is_featured":"YES","album_cover_image":"http://call4blessing.com/uploads/album/1492508820album-cover.png","is_downloaded":"false"},{"id":6,"name":"Rock Music","description":"Rock music is best music.","language":"Hindi","payment_type":"PAID","amount":"1500","created_at":"1 month ago","is_featured":"YES","album_cover_image":"http://call4blessing.com/uploads/album/1495449313Jellyfish.jpg","is_downloaded":"false"},{"id":7,"name":"sam","description":"created for testing","language":"Hindi","payment_type":"FREE","amount":"","created_at":"7 days ago","is_featured":"YES","album_cover_image":"http://call4blessing.com/uploads/album/1498132902reach2.jpg","is_downloaded":"false"}]}]
     * trending_albums : [{"id":1,"name":"Tu Meri Jaan","description":"Tu Meri Jaan","language":"Hindi","payment_type":"PAID","amount":"100","created_at":"2 months ago","is_featured":"YES","album_cover_image":"http://call4blessing.com/uploads/album/1492508820album-cover.png","is_downloaded":"false"},{"id":6,"name":"Rock Music","description":"Rock music is best music.","language":"Hindi","payment_type":"PAID","amount":"1500","created_at":"1 month ago","is_featured":"YES","album_cover_image":"http://call4blessing.com/uploads/album/1495449313Jellyfish.jpg","is_downloaded":"false"},{"id":7,"name":"sam","description":"created for testing","language":"Hindi","payment_type":"FREE","amount":"","created_at":"7 days ago","is_featured":"YES","album_cover_image":"http://call4blessing.com/uploads/album/1498132902reach2.jpg","is_downloaded":"false"}]
     */

    private String status;
    private List<ListBean> list;
    private List<TrendingAlbumsBean> trending_albums;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public List<TrendingAlbumsBean> getTrending_albums() {
        return trending_albums;
    }

    public void setTrending_albums(List<TrendingAlbumsBean> trending_albums) {
        this.trending_albums = trending_albums;
    }

    public static class ListBean {
        /**
         * id : 1
         * language : English
         * albums : [{"id":2,"name":"free","description":"Calvin Harris n Disciples","language":"English","payment_type":"FREE","amount":"","created_at":"2 months ago","is_featured":"NO","album_cover_image":"http://call4blessing.com/uploads/album/149577685201 Tu Meri Jaan.mp3","is_downloaded":"false"}]
         */

        private int id;
        private String language;
        private List<AlbumsBean> albums;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public List<AlbumsBean> getAlbums() {
            return albums;
        }

        public void setAlbums(List<AlbumsBean> albums) {
            this.albums = albums;
        }

        public static class AlbumsBean {
            /**
             * id : 2
             * name : free
             * description : Calvin Harris n Disciples
             * language : English
             * payment_type : FREE
             * amount :
             * created_at : 2 months ago
             * is_featured : NO
             * album_cover_image : http://call4blessing.com/uploads/album/149577685201 Tu Meri Jaan.mp3
             * is_downloaded : false
             */

            private int id;
            private String name;
            private String description;
            private String language;
            private String payment_type;
            private String amount;
            private String created_at;
            private String is_featured;
            private String album_cover_image;
            private String is_downloaded;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public String getPayment_type() {
                return payment_type;
            }

            public void setPayment_type(String payment_type) {
                this.payment_type = payment_type;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getIs_featured() {
                return is_featured;
            }

            public void setIs_featured(String is_featured) {
                this.is_featured = is_featured;
            }

            public String getAlbum_cover_image() {
                return album_cover_image;
            }

            public void setAlbum_cover_image(String album_cover_image) {
                this.album_cover_image = album_cover_image;
            }

            public String getIs_downloaded() {
                return is_downloaded;
            }

            public void setIs_downloaded(String is_downloaded) {
                this.is_downloaded = is_downloaded;
            }
        }
    }

    public static class TrendingAlbumsBean {
        /**
         * id : 1
         * name : Tu Meri Jaan
         * description : Tu Meri Jaan
         * language : Hindi
         * payment_type : PAID
         * amount : 100
         * created_at : 2 months ago
         * is_featured : YES
         * album_cover_image : http://call4blessing.com/uploads/album/1492508820album-cover.png
         * is_downloaded : false
         */

        private int id;
        private String name;
        private String description;
        private String language;
        private String payment_type;
        private String amount;
        private String created_at;
        private String is_featured;
        private String album_cover_image;
        private String is_downloaded;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getIs_featured() {
            return is_featured;
        }

        public void setIs_featured(String is_featured) {
            this.is_featured = is_featured;
        }

        public String getAlbum_cover_image() {
            return album_cover_image;
        }

        public void setAlbum_cover_image(String album_cover_image) {
            this.album_cover_image = album_cover_image;
        }

        public String getIs_downloaded() {
            return is_downloaded;
        }

        public void setIs_downloaded(String is_downloaded) {
            this.is_downloaded = is_downloaded;
        }
    }
}
