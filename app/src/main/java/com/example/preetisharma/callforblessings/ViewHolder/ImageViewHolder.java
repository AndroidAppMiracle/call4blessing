package com.example.preetisharma.callforblessings.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.callforblessings.R;


/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class ImageViewHolder extends RecyclerView.ViewHolder {

    private ImageView ivExample;

    public ImageViewHolder(View v) {
        super(v);
        ivExample = (ImageView) v.findViewById(R.id.viewholder_image);
    }

    public ImageView getImageView() {
        return ivExample;
    }

    public void setImageView(ImageView ivExample) {
        this.ivExample = ivExample;
    }
}