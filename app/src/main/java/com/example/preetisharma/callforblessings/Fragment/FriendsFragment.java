package com.example.preetisharma.callforblessings.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.UsersArrayAdapter;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSendFriendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.UserFilterModal;
import com.example.preetisharma.callforblessings.Server.Modal.UserListingModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/12/2017.
 */

public class FriendsFragment extends Fragment implements APIServerResponse, TabLayout.OnTabSelectedListener {
    @BindView(R.id.search_text)
    AutoCompleteTextView search_text;
    @BindView(R.id.iv_close_icon)
    AppCompatImageView iv_close_icon;

    @BindView(R.id.viewPager_friendsTab)
    ViewPager viewPager_friendsTab;
    FriendsAdapter _adapter;
    HashMap<String, String> users;
    ArrayList<UserFilterModal> Users = new ArrayList<>();
    // String[] Users;
    private List<UserListingModal.ListBean> mList;
    ViewPager viewPager;
    UsersArrayAdapter usersAdapter;

    public static FriendsFragment newInstance() {

        FriendsFragment fragment = new FriendsFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.friends_fragment_tab, container, false);
        ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        ((BaseActivity) getActivity()).getmPrefs();
        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabs_friends);
        tabLayout.addTab(tabLayout.newTab().setText("My Friends"));
        tabLayout.addTab(tabLayout.newTab().setText("Request"));
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager_friendsTab);

        viewPager.setAdapter(new FriendsAdapter(getChildFragmentManager(), tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(0);
        tabLayout.setOnTabSelectedListener(this);
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().userList(APIServerResponse.USER_LIST, ((BaseActivity) getActivity()).getUserSessionId(), FriendsFragment.this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        iv_close_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text.setText("");
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().userList(APIServerResponse.USER_LIST, ((BaseActivity) getActivity()).getUserSessionId(), FriendsFragment.this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }*/
    }

   /* @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseActivity) getActivity()).dismissDialog();
    }*/

    @Override
    public void onSuccess(int tag, Response response) {
        mList = new ArrayList<UserListingModal.ListBean>();
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideLoading();
        }

        UserListingModal userListingModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.USER_LIST: {
                        if (response.isSuccessful()) {
                            userListingModal = (UserListingModal) response.body();

                            if (userListingModal.getStatus().equals("1")) {
                                mList = userListingModal.getList();
                                for (int i = 0; i < mList.size(); i++) {

                                    if (!((BaseActivity) getActivity()).getUserID().equals(String.valueOf(mList.get(i).getId()))) {
                                        UserFilterModal user = new UserFilterModal();
                                        user.setId(String.valueOf(mList.get(i).getId()));
                                        user.setUserName(mList.get(i).getProfile_details().getFirstname() + " " + mList.get(i).getProfile_details().getLastname());
                                        Users.add(user);
                                    }
                                }
                            } else {
                            }
                        }
                    }


                    Log.e("List data", "List data size array" + Users.size());
                    usersAdapter = new UsersArrayAdapter(getActivity(), mList);
                  /*  ArrayAdapter<UserFilterModal> adapter = new ArrayAdapter<UserFilterModal>
                            (getActivity(), android.R.layout.simple_list_item_1, Users);*/
                    search_text.setAdapter(usersAdapter);
                    search_text.setThreshold(1);

                    //friends_recycler_view.setAdapter(_adapter);
                    // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    // friends_recycler_view.setLayoutManager(mLayoutManager);

                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        search_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent timeLine = new Intent(getActivity(), MaterialUpConceptActivity.class);
                Bundle b = new Bundle();
                UserListingModal.ListBean mobj= usersAdapter.suggestions.get(i);
                b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(usersAdapter.getItemId(i)));
                b.putInt(Constants.IS_FRIEND, mobj.getIs_friend());
                b.putBoolean(Constants.TIMELINE_ENABLED, false);
                b.putString(Constants.USER_IMAGE, usersAdapter.getUserImage(i));
                b.putString(Constants.COVER_PIC, usersAdapter.getUserCoverImage(i));
                b.putString(Constants.USER_NAME, usersAdapter.getUserFullName(i));
                b.putString(Constants.PRIVACY_WALL, usersAdapter.getUserPrivacyWall(i));
                b.putString(Constants.PRIVACY_ABOUT_INFO, usersAdapter.getUserPrivacyAboutInfo(i));
                b.putString(Constants.PRIVACY_FRIEND_REQUEST, usersAdapter.getUserPrivacyFriendRequest(i));
                b.putString(Constants.PRIVACY_MESSAGE, usersAdapter.getUserPrivacyMessage(i));
                timeLine.putExtras(b);
                startActivity(timeLine);
                search_text.setText("");
            }
        });


    }


    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());
    }


    public class FriendsAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public FriendsAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {

                case 0:
                    fragment = new MyFriendsFragment();
                    return fragment;
                case 1:
                    fragment = new FriendsRequestFragment();
                    return fragment;
                default:
                    fragment = new MyFriendsFragment();
                    return fragment;

            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Subscribe
    public void onFriendRequestSent(EventBusSendFriendRequestModal eventBusSendFriendRequestModal) {
        if (eventBusSendFriendRequestModal.getMessage().equals("1")) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().userList(APIServerResponse.USER_LIST, ((BaseActivity) getActivity()).getUserSessionId(), FriendsFragment.this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        ((BaseActivity) getActivity()).dismissDialog();
        super.onDestroy();

    }
}
