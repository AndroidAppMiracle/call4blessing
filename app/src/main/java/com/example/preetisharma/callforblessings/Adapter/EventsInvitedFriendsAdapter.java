package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.MyEventsCreatedModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/9/2017.
 */

public class EventsInvitedFriendsAdapter extends RecyclerView.Adapter<EventsInvitedFriendsAdapter.DataViewHolder> {


    private Context mContext;
    //private List<MyEventsCreatedModal.ListBean> myEventInvitesList = new ArrayList<>();
    private Resources res;
    private List<MyEventsCreatedModal.ListBean.InvitesBean> usersInvitedList = new ArrayList<>();

    public EventsInvitedFriendsAdapter(Context mContext/*, List<MyEventsCreatedModal.ListBean> list*/, List<MyEventsCreatedModal.ListBean.InvitesBean> invitedList) {
        //this.myEventInvitesList = list;
        this.mContext = mContext;
        this.usersInvitedList = invitedList;
        res = mContext.getResources();
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_event_friends_invited, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {

        try {
            String text = String.format(res.getString(R.string.two_parameters), usersInvitedList.get(position).getUser_to().getProfile_details().getFirstname(), usersInvitedList.get(position).getUser_to().getProfile_details().getLastname());
            holder.txtvw_friends_request_name.setText(text);

            holder.txtvw_event_request_status.setText(usersInvitedList.get(position).getInvite_status());
            Glide.with(mContext).load(usersInvitedList.get(position).getUser_to().getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).centerCrop().into(holder.iv_friend_request_image);
            /*holder.txtvw_event_request_status.setText(myEventInvitesList.get(position).getInvites().get(position).getInvite_status());
            Glide.with(mContext).load(myEventInvitesList.get(position).getInvites().get(position).getUser_to().getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder).centerCrop().into(holder.iv_friend_request_image);*/


            holder.ll_my_invited_friends.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(usersInvitedList.get(position).getUser_to().getId()));
                    b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                    b.putString(Constants.USER_IMAGE, usersInvitedList.get(position).getUser_to().getProfile_details().getProfile_pic());
                    b.putString(Constants.COVER_PIC, usersInvitedList.get(position).getUser_to().getProfile_details().getCover_pic());
                    b.putString(Constants.USER_NAME, usersInvitedList.get(position).getUser_to().getProfile_details().getFirstname() + " " + usersInvitedList.get(position).getUser_from().getProfile_details().getLastname());
                    b.putString(Constants.PRIVACY_WALL, usersInvitedList.get(holder.getAdapterPosition()).getUser_to().getWALL());
                    b.putString(Constants.PRIVACY_ABOUT_INFO, usersInvitedList.get(holder.getAdapterPosition()).getUser_to().getabout_info());
                    b.putString(Constants.PRIVACY_FRIEND_REQUEST, usersInvitedList.get(holder.getAdapterPosition()).getUser_to().getfriend_request());
                    b.putString(Constants.PRIVACY_MESSAGE, usersInvitedList.get(holder.getAdapterPosition()).getUser_to().getMESSAGE());
                    i.putExtras(b);
                    mContext.startActivity(i);
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (usersInvitedList.size() != 0) {
            return usersInvitedList.size();
        } else {
            return 0;
        }

    }


    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {


        @BindView(R.id.txtvw_event_request_status)
        AppCompatTextView txtvw_event_request_status;

        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;

        /*@BindView(R.id.txtvw_my_event_edit)
        AppCompatTextView txtvw_my_event_edit;*/

        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;

        @BindView(R.id.ll_my_invited_friends)
        LinearLayout ll_my_invited_friends;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/
        }

        @Override
        public void onSuccess(int tag, Response response) {

            /*try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    DeleteEventModal deleteEvent;

                    switch (tag) {
                        case APIServerResponse.DELETEEVENT:
                            deleteEvent = (DeleteEventModal) response.body();
                            if (deleteEvent.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast("Event Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }

        @Override
        public void onError(int tag, Throwable throwable) {
           /* switch (tag) {
                case APIServerResponse.DELETEEVENT:
                    ((BaseActivity) mContext).showToast(throwable.getMessage().toString(), Toast.LENGTH_SHORT);

            }*/
        }
    }

    /*public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        myEventsCreatedModalList.remove(position);
        notifyItemRemoved(position);
    }*/
}