package com.example.preetisharma.callforblessings.photos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.DemoDataMyPhotosAdapter;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 7/17/2017.
 */

public class PhotosOfYouFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    ArrayList<DemoDataModal> list = new ArrayList<>();


    String[] images = {"https://i.pinimg.com/originals/6e/ab/f6/6eabf6c1fda5194f076c402192b5858b.jpg",
            "http://t.wallpaperweb.org/wallpaper/3d_landscape/1024x768/fantasy_art_scenery_wallpaper_sarel_theron_03_1024x768.jpg",
            "http://2.bp.blogspot.com/-bQp0j6m1O7M/UEyE743yh6I/AAAAAAAAAdI/4zPjg4UNpCQ/s1600/beautiful-autumn-scenery-723-2.jpg",
            "https://s-media-cache-ak0.pinimg.com/originals/03/2c/2b/032c2b83402721bf65db22c597594fdd.jpg",
            "http://cdn.pcwallart.com/images/arizona-scenery-sunset-wallpaper-4.jpg",
            "https://lh6.ggpht.com/YKKQHs9gzT9OWOrr6dwXakjB30Rb33IgXolZpNY_pwXxZMngNPRT1PpV1kS6bo_s7w=h900",
            "http://www.wallmaya.com/wp-content/uploads/2016/03/scenery-photography-wallpaper-latest-hd-collections.jpg",
            "https://southeastgreenway.org/wp-content/uploads/2016/08/Scenery-33.jpg",
            "http://wallpapercave.com/wp/cJ5iQB1.jpg",
            "http://www.desktop-background.com/download/1024x768/2015/10/29/1033760_wallpapers-onepiece-nature-beautiful-natural-scenery-1366x768_1366x768_h.jpg"};


    //int[] images = {R.drawable.ic_album_placeholder};

    @BindView(R.id.rv_photos)
    RecyclerView rv_photos;

    DemoDataMyPhotosAdapter adapter;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photos_of_you, container, false);
        ButterKnife.bind(this, rootView);

        swipe_refresh_layout.setOnRefreshListener(PhotosOfYouFragment.this);
        rv_photos.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_photos.setItemAnimator(new DefaultItemAnimator());

        swipe_refresh_layout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        addDummyImages();
                    }
                }
        );

        ItemClickSupport.addTo(rv_photos).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                Intent intentViewImages = new Intent(getActivity(), PhotosSlideView.class);
                intentViewImages.putExtra(Constants.CURRENT_POSITION, position);
                Bundle b = new Bundle();
                b.putParcelableArrayList(Constants.IMAGE_SLIDESHOW, list);
                intentViewImages.putExtra(Constants.IMAGE_BUNDLE, b);
                startActivity(intentViewImages);

            }
        });


        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            //Load Data
            //addDummyImages();
        } else {
            //No Loading of Data
        }
    }


    public void addDummyImages() {

        if (!list.isEmpty()) {
            list.clear();
        }

        for (int i = 0; i < 10; i++) {

            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setId(i);
            demoDataModal.setImage(images[i]);
            demoDataModal.setName("Image " + (i + 1));

            list.add(demoDataModal);
        }

        if (swipe_refresh_layout != null) {
            swipe_refresh_layout.setRefreshing(false);
        }

        adapter = new DemoDataMyPhotosAdapter(getActivity(), list);
        rv_photos.setAdapter(adapter);


    }

    @Override
    public void onRefresh() {

        addDummyImages();

    }
}
