package com.example.preetisharma.callforblessings;

import android.os.Bundle;
import android.view.WindowManager;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

/**
 * Created by preeti.sharma on 2/2/2017.
 */

public class ThankyouActivity extends BaseActivity {


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thankyou_layout);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

}
