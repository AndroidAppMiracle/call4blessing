package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.example.preetisharma.callforblessings.Fragment.FriendsFragment;
import com.example.preetisharma.callforblessings.Fragment.HomeFragment;
import com.example.preetisharma.callforblessings.Fragment.MoreFragment;
import com.example.preetisharma.callforblessings.Fragment.NotificationsFragment;
import com.example.preetisharma.callforblessings.demo.DemoHomeFragment;
import com.example.preetisharma.callforblessings.demo.DemoHomeFragmentTest;

/**
 * Created by preeti.sharma on 1/12/2017.
 */

public class HomeTabAdapter extends FragmentPagerAdapter {
    //integer to count number of tabs
    int tabCount;
    Context mContext;

    //Constructor to the class
    public HomeTabAdapter(FragmentManager fm, int tabCount, Context mContext) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
        this.mContext = mContext;

    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                //fragment = new HomeFragment();

                /*fragment = new DemoHomeFragmentTest();*/
                fragment = new DemoHomeFragment();

               /* if (fragmentHome.isVisible()) {
                    Log.i("isVisible", "Yes");
                    fragmentHome.repositionRecyclerViewOnTabClick();
                } else {
                    Log.i("isVisible", "No");
                }*/
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                return fragment;


            case 1:

                fragment = new FriendsFragment();

                return fragment;
            case 2:
                fragment = new NotificationsFragment();

                return fragment;
            case 3:
                fragment = new MoreFragment();
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                return fragment;

            default:
                fragment = new HomeFragment();
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                //notifyDataSetChanged();
                return fragment;
        }

    }

    @Override
    public int getCount() {
        return tabCount;
    }

    public Fragment getFragment(ViewPager container, int position, FragmentManager fm) {
        String name = makeFragmentName(container.getId(), position);
        return fm.findFragmentByTag(name);
    }

    private String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }
}
