package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/15/2017.
 */

public class EventBusHomeWallPostCreated  {

    private final String isNewPostCreated;

    public EventBusHomeWallPostCreated(String isNewPostCreated) {
        this.isNewPostCreated = isNewPostCreated;
    }

    public String getIsNewPostCreated() {
        return isNewPostCreated;
    }
}
