package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 3/2/2017.
 */

public class EventBusSendFriendRequestModal {

    private final String comment_count;

    public EventBusSendFriendRequestModal(String count) {
        this.comment_count = count;
    }

    public String getMessage() {
        return comment_count;
    }
}
