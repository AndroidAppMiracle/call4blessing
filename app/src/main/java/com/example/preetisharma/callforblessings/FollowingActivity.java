package com.example.preetisharma.callforblessings;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.FollowingAdapter;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FollowingActivity extends BaseActivity {


    @BindView(R.id.rv_following_friends)
    RecyclerView rv_following_friends;
    FollowingAdapter followingAdapter;
    private LinearLayoutManager mLayoutManager;


    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);
        ButterKnife.bind(this);

        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText(
                "Followers ");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        followingAdapter=new FollowingAdapter(this);

        mLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };


        rv_following_friends.setLayoutManager(mLayoutManager);
        rv_following_friends.setNestedScrollingEnabled(false);
        rv_following_friends.setItemAnimator(new DefaultItemAnimator());
        rv_following_friends.setAdapter(followingAdapter);
    }
}
