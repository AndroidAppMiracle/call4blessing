package com.example.preetisharma.callforblessings.demo;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatTextView;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumListingModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.Utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by preeti.sharma on 3/1/2017.
 */

public class AlbumPlayerActivity extends BaseActivity implements SeekBar.OnSeekBarChangeListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnSeekCompleteListener {
    @BindView(R.id.album_cover_pic)
    AppCompatImageView album_cover_pic;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_play)
    AppCompatImageView txtvw_play;
    @BindView(R.id.seekbar_layout)
    AppCompatSeekBar seekBar;
    @BindView(R.id.txtvw_start_duration)
    AppCompatTextView txtvw_start_duration;
    @BindView(R.id.txtvw_end_duration)
    AppCompatTextView txtvw_end_duration;
    @BindView(R.id.txtvw_next)
    AppCompatImageView txtvw_next;
    @BindView(R.id.txtvw_previous)
    AppCompatImageView txtvw_previous;
    @BindView(R.id.txtvw_album_name)
    AppCompatTextView txtvw_album_name;
    @BindView(R.id.txtvw_song_name)
    AppCompatTextView txtvw_song_name;

    @BindView(R.id.txtvw_song_name_below_album_cover)
    AppCompatTextView txtvw_song_name_below_album_cover;

    MediaPlayer player;


    private Handler mHandler = new Handler();
    AudioManager audioManager;
    private boolean ongoingCall = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;
    List<AlbumListingModal.DetailBean.FilesBean> musicFiles = new ArrayList<>();
    Uri uri;
    private int SongPostion;
    private Utilities utils;
    int progress;
    private int mediaFileLengthInMilliseconds;
    List<AlbumListingModal.DetailBean.FilesBean> list;
    String albumCoverPic, albumName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_album_player);
        updateStatusBar();
        ButterKnife.bind(this);
        txtvw_start_duration = (AppCompatTextView) findViewById(R.id.txtvw_start_duration);
        utils = new Utilities();

        txtvw_header_title.setText("Player");
        img_view_change_password.setVisibility(View.GONE);
        audioManager = (AudioManager) this.getSystemService(this.AUDIO_SERVICE);

        player = new MediaPlayer();
        player.setOnBufferingUpdateListener(this);
        player.setOnCompletionListener(this);
        player.setOnSeekCompleteListener(this);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        if (getIntent().getExtras() != null) {

            SongPostion = getIntent().getExtras().getInt(Constants.POSITION);
            albumCoverPic = getIntent().getExtras().getString(Constants.ALBUM_COVER);
            albumName = getIntent().getExtras().getString(Constants.ALBUM_NAME);

            Glide.with(this).load(albumCoverPic).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).crossFade().into(album_cover_pic);

        }

        //COMMENTED 18/5/2017
        //list = AlbumSpecificListingActivity.songslist;
        playercheck();
        txtvw_album_name.setText(albumName);
        txtvw_song_name.setText(list.get(SongPostion).getSong_name());
        txtvw_song_name_below_album_cover.setText(list.get(SongPostion).getSong_name());
        showLoading();
        Thread th = new Thread(new Runnable() {

            @Override
            public void run() {
// TODO Auto-generated method stub
                try {
                    Thread.sleep(100);
                    playSong(SongPostion);
                } catch (InterruptedException e) {
// TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        th.start();

        callStateListener();
    }

    int resumePosition;

    private void callStateListener() {
        // Get the telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //Starting listening for PhoneState changes
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    //if at least one call exists or the phone is ringing
                    //pause the MediaPlayer
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (player != null) {
                            player.pause();
                            resumePosition = player.getCurrentPosition();
                            ongoingCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        // Phone idle. Start playing.
                        if (player != null) {
                            if (ongoingCall) {
                                ongoingCall = false;
                                player.seekTo(resumePosition);
                                player.start();
                            }
                        }
                        break;
                }
            }
        };
        // Register the listener with the telephony manager
        // Listen for changes to the device call state.
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    @OnClick(R.id.txtvw_next)
    public void onClickNext(View arg0) {
        txtvw_next.setClickable(false);

        if (SongPostion < (list.size() - 1)) {
            showLoading();
            SongPostion = SongPostion + 1;
            playSong(SongPostion);
            showToast(list.get(SongPostion).getSong_name(), Toast.LENGTH_SHORT);
        } else {
            hideLoading();
            showToast("No more songs found", Toast.LENGTH_SHORT);
        }


    }


    /**
     * Back button click event
     * Plays previous song by currentSongIndex - 1
     */

    @OnClick(R.id.txtvw_previous)
    public void previous_play() {
        txtvw_previous.setClickable(false);
        if (SongPostion > 0) {
            showLoading();
            SongPostion = SongPostion - 1;
            showToast(list.get(SongPostion).getSong_name(), Toast.LENGTH_SHORT);
            playSong(SongPostion);

        } else {
            hideLoading();
            showToast("No more songs found", Toast.LENGTH_SHORT);
        }
    }

    public void playSong(int songIndex) {

        seekBar.setProgress(0);
        // Play song
        try {
            showLoading();
            player.reset();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(list.get(songIndex).getMusic_file());

            try {
                player.prepare();
                txtvw_next.setClickable(true);
                txtvw_previous.setClickable(true);

            } catch (Exception ex) {
                Log.e("Music", ex.getMessage(), ex);
            }

            if (!player.isPlaying()) {
                hideLoading();
                player.start();

            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtvw_album_name.setText(albumName);
                    /*txtvw_album_name.setText(list.get(SongPostion).getSong_name());*/
                    if (player.isPlaying()) {
                        txtvw_play.setImageResource(R.drawable.ic_pause_light);
                    } else {
                        txtvw_play.setImageResource(R.drawable.ic_play);

                    }

                }
            });
            // Displaying Song title

            updateProgressBar();

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update timer on seekbar
     */


    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            if (player != null) {
                long totalDuration = player.getDuration();
                long currentDuration = player.getCurrentPosition();
                if (currentDuration <= totalDuration) {
                    // Displaying Total Duration time
                    txtvw_end_duration.setText("" + Utilities.milliSecondsToTimer(totalDuration));
                    // Displaying time completed playing
                    txtvw_start_duration.setText("" + Utilities.milliSecondsToTimer(currentDuration));

                    // Updating progress bar
                    int progress = (int) (Utilities.getProgressPercentage(currentDuration, totalDuration));


                    // Running this thread after 100 milliseconds
                    mHandler.postDelayed(this, 100);
                    Log.d("Progress", "" + progress);
                    seekBar.setProgress(progress);
                }
            }


        }
    };

    @OnClick(R.id.txtvw_play)
    public void playAlbum() {


        if (player.isPlaying()) {
            if (player != null) {
                player.pause();
                // Changing button image to play button
                txtvw_play.setImageResource(R.drawable.ic_play);
                updateProgressBar();
            }
        } else {
            // Resume song
            if (player != null) {
                playSong(SongPostion);
                // Changing button image to pause button
                txtvw_play.setImageResource(R.drawable.ic_pause_light);
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUpdateTimeTask != null) {
            mHandler.removeCallbacks(mUpdateTimeTask);
        }
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.release();
            player = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mUpdateTimeTask != null) {
            mHandler.removeCallbacks(mUpdateTimeTask);
        }
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.release();
            player = null;
        }
    }

    public void isplaying() {
        if (player != null) {
            player.stop();
            player = null;
//           player .release();
        }
    }


    private int result;

    // check the another system  player is running the stop it
    public void playercheck() {
        if (player != null) {
            result = audioManager.requestAudioFocus(new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {

                    if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                        player.pause();
                        // Pause
                    } else if (focusChange == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                        player.start();
                        // Resume
                    } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS)

                    {
                        if (player != null) {
                            player.pause();
                        } // Stop or pause depending on your need
                    }
                }
            }, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }
    }


    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, final int percent) {

        if (percent < seekBar.getMax()) {
            seekBar.setSecondaryProgress(percent / 100);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

        if (SongPostion < (list.size() - 1)) {
            playSong(SongPostion + 1);
            SongPostion = SongPostion + 1;
        } else {
            hideLoading();
            showToast("No more Song", Toast.LENGTH_SHORT);
        }
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            if (player != null) {
                if (progress <= seekBar.getSecondaryProgress()) {
                } else {
                    seekBar.setProgress(progress);
                }

            }
        }
    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * When user starts moving the progress handler
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = player.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        player.seekTo(currentPosition);

        // update timer progress again

    }

    @Override
    public void onSeekComplete(MediaPlayer mediaPlayer) {
        if (player != null) {
            hideLoading();
            player.stop();
        }
    }
}
