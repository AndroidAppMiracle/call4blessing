package com.example.preetisharma.callforblessings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.HomeTabAdapter;
import com.example.preetisharma.callforblessings.Fragment.NotificationsFragment;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.demo.ChatActivity;
import com.example.preetisharma.callforblessings.demo.DemoHomeFragment;
import com.example.preetisharma.callforblessings.demo.DemoHomeFragmentTest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by preeti.sharma on 1/11/2017.
 */

public class HomeActivity extends BaseActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {
    private TabLayout tabLayout;
    private boolean doubleBackToExitPressedOnce;
    //This is our viewPager
    private ViewPager viewPager;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.appCompatTextView_htp)
    AppCompatTextView appCompatTextView_htp;
    @BindView(R.id.txtvw_chat)
    AppCompatTextView txtvw_chat;
    /*@BindView(R.id.img_vw_joy)
    AppCompatImageView img_vw_joy;*/
    /*@BindView(R.id.img_vw_books)
    AppCompatImageView img_vw_books;*/
    /*@BindView(R.id.imgvw_matrimonial)
    AppCompatImageView imgvw_matrimonial;
    @BindView(R.id.imgvw_video)
    AppCompatImageView imgvw_video;*/
    //@BindView(R.id.lnr_layout_top_view)
    //LinearLayout lnr_layout_top_view;
    HomeTabAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.demodemo_material_home_activity);*/
        /*setContentView(R.layout.demodemo_home_layout);*/
        setContentView(R.layout.home_activity);
        updateStatusBar();
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        ButterKnife.bind(this);

        //Adding toolbar to the activity
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        img_view_back.setImageResource(R.drawable.ic_video);
        img_view_change_password.setImageResource(R.drawable.ic_bible);
        txtvw_header_title.setText("Home");


        //This is our tablayout
        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);
        //Creating our pager adapter
        createTabWithIcons();
        tabWidth(tabLayout.getTabCount());
        adapter = new HomeTabAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(HomeActivity.this);
        viewPager.setOnPageChangeListener(this);
        /*img_vw_books.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, BookTabs.class);
                *//*Intent intent = new Intent(HomeActivity.this, BooksListingActivity.class);*//*
                startActivity(intent);
            }
        });*/

    }

    public void createTabWithIcons() {
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        TextView tabHome = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHome.setText("Home");
        tabHome.setTextSize(11);
        tabHome.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home, 0, 0);
        tabLayout.addTab((tabLayout.newTab().setCustomView(tabHome)));
        TextView tabFriends = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFriends.setText("Friends");
        tabFriends.setTextSize(11);
        tabFriends.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_friends, 0, 0);
        tabLayout.addTab((tabLayout.newTab().setCustomView(tabFriends)));
        TextView tabNotifications = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabNotifications.setText("Notifications");
        tabNotifications.setTextSize(11);
        tabNotifications.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_notification, 0, 0);
        tabLayout.addTab((tabLayout.newTab().setCustomView(tabNotifications)));
        TextView tabMore = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabMore.setText("More");
        tabMore.setTextSize(11);
        tabMore.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_more, 0, 0);
        tabLayout.addTab((tabLayout.newTab().setCustomView(tabMore)));

    }

    public int tabWidth(int count) {
        return getWindowManager().getDefaultDisplay().getWidth() / count;

    }

    public void changeHeader(String title) {
        txtvw_header_title.setText(title);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //if (viewPager.getCurrentItem() == 0) {

        Log.i("isVisible", "Yes");
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = adapter.getFragment(viewPager, tab.getPosition(), fm);

        if (f != null) {
            if (tab.getPosition() == 0) {
               /* DemoHomeFragmentTest fragment = (DemoHomeFragmentTest) f;
                fragment.repositionRecyclerViewOnTabClick();
*/
                DemoHomeFragment fragment = (DemoHomeFragment) f;
                fragment.repositionRecyclerViewOnTabClick();
            } else if (tab.getPosition() == 2) {
                NotificationsFragment notificationsFragment = (NotificationsFragment) f;
                notificationsFragment.repositionRecyclerViewOnTabClick();
            }
        }
        // }
    }


    /*try {
        Fragment f = adapter.getItem(tab.getPosition());
        if (f != null) {
            View fragmentView = f.getView();
            RecyclerView mRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.home_listing);//mine one is RecyclerView
            if (mRecyclerView != null) {
                if (mRecyclerView.getAdapter().getItemCount() > 0) {
                    mRecyclerView.smoothScrollToPosition(0);
                    mRecyclerView.scrollToPosition(0);
                }

            }

        }
    } catch (NullPointerException npe) {
        npe.printStackTrace();
    }*/


    /*Log.i("isVisible", "Yes");
    FragmentManager fm = getSupportFragmentManager();
    Fragment f = adapter.getFragment(viewPager, tab.getPosition(), fm);

            if (f != null) {
        if (tab.getPosition() == 0) {
            DemoHomeFragment fragment = (DemoHomeFragment) f;
            fragment.repositionRecyclerViewOnTabClick();
        }
    }*/
    @Override
    public void onBackPressed() {
       /* if (!txtvw_header_title.getText().toString().equalsIgnoreCase("Home")){
            adapter.getItem(0);
        }*/
        backPress();

    }

    private void backPress() {
        if (doubleBackToExitPressedOnce) {

            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    @OnClick(R.id.appCompatTextView_htp)
    public void htpRequestActivity() {
        Intent i = new Intent(this, HTPLayoutActivity.class);
        startActivity(i);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        switch (position) {
            case 0:
                //lnr_layout_top_view.setVisibility(View.VISIBLE);
                img_view_back.setClickable(false);
                img_view_change_password.setClickable(false);
                img_view_back.setVisibility(View.GONE);
                img_view_change_password.setVisibility(View.GONE);
                appCompatTextView_htp.setVisibility(View.VISIBLE);
                txtvw_chat.setVisibility(View.VISIBLE);
                txtvw_header_title.setText("Home");
                break;
            case 1:
                //lnr_layout_top_view.setVisibility(View.GONE);
                txtvw_header_title.setText("Friends");
                img_view_change_password.setVisibility(View.GONE);
                txtvw_chat.setVisibility(View.GONE);

                img_view_back.setImageResource(R.drawable.ic_back);
                img_view_back.setVisibility(View.GONE);
                img_view_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        backPress();
                    }
                });
                appCompatTextView_htp.setVisibility(View.GONE);
                img_view_change_password.setClickable(false);
                break;
            case 2:
                txtvw_chat.setVisibility(View.GONE);
                img_view_back.setVisibility(View.GONE);
                img_view_back.setImageResource(R.drawable.ic_back);
                img_view_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        backPress();
                    }
                });
                appCompatTextView_htp.setVisibility(View.GONE);
                //lnr_layout_top_view.setVisibility(View.GONE);
                txtvw_header_title.setText("Notifications");
                break;
            case 3:
                txtvw_chat.setVisibility(View.GONE);
                appCompatTextView_htp.setVisibility(View.GONE);
                img_view_back.setVisibility(View.GONE);
                //lnr_layout_top_view.setVisibility(View.GONE);
                img_view_back.setImageResource(R.drawable.ic_back);
                img_view_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        backPress();
                    }
                });
                img_view_change_password.setVisibility(View.GONE);
                img_view_change_password.setClickable(false);

                txtvw_header_title.setText("More");
                break;
            default:
                //lnr_layout_top_view.setVisibility(View.VISIBLE);
                img_view_back.setClickable(false);
                img_view_change_password.setClickable(false);
                img_view_back.setVisibility(View.GONE);
                img_view_change_password.setVisibility(View.GONE);
                appCompatTextView_htp.setVisibility(View.VISIBLE);
                txtvw_chat.setVisibility(View.VISIBLE);
                txtvw_header_title.setText("Home");
                break;
        }
    }

    public void backButton() {
        img_view_back.setImageResource(R.drawable.ic_back);
        img_view_back.setVisibility(View.VISIBLE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backPress();
            }
        });
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

  /*  @OnClick(R.id.img_vw_joy)
    public void joy_musicDialog() {
        Intent intent = new Intent(this, JoyMusicActivity.class);
        startActivity(intent);

    }*/

   /* @OnClick(R.id.imgvw_matrimonial)
    public void matrimonialDialog() {
        comingSoonDialog("Matrimonial");
    }

    @OnClick(R.id.imgvw_video)
    public void videoDialog() {
        comingSoonDialog("Videos");
    }*/

    @OnClick(R.id.txtvw_chat)
    public void chat() {

        Intent chat = new Intent(this, ChatActivity.class);
        startActivity(chat);
    }

    public void comingSoonDialog(String title) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage("Coming Soon")

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .setIcon(R.mipmap.ic_app_icon)
                .show();

    }

}
