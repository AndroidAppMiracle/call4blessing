package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/6/2017.
 */

public class EventBusIsGroupCreatedModal {


    private final String isGroupCreated;

    public EventBusIsGroupCreatedModal(String isGroupCreated) {
        this.isGroupCreated = isGroupCreated;
    }

    public String getIsGroupCreated() {
        return isGroupCreated;
    }
}
