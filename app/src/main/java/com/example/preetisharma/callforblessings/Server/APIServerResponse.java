package com.example.preetisharma.callforblessings.Server;

import retrofit2.Response;


public interface APIServerResponse {


    int REGISTER = 1;
    int QUESTIONS = 2;
    int VALIDREGISTERATION = 3;
    int LOGIN = 4;
    int SAVEPROFILE = 5;
    int HOME = 6;

    int Prayer_request = 7;
    int LIKE = 8;

    int SHARE = 9;
    int CHANGE_PASSWORD = 10;
    int FORGOT_PASSWORD = 11;
    int CREATE_POST = 12;
    int PRAYER_INDEX = 13;
    int TESTIMONY_INDEX = 14;
    int AMEN_PRAYER = 15;
    int HALE_TESTIMONY = 16;
    int LOGOUT = 17;
    int FETCHPROFILE = 18;
    int CREATE_EVENT = 19;
    int ADD_BOOK = 20;
    int LIST_BOOK = 21;
    int CREATE_HELP_REQUEST = 22;
    int GET_HELP_REQUEST = 23;
    int EVENTS_LIST = 24;
    int USER_LIST = 25;
    int COMMENT = 26;
    int COMMENTLIST = 27;
    int TIMELINE = 28;
    int UNLIKE = 29;
    int GENERATEOTP = 30;
    int VERIFY_CODE = 31;
    int FRIENDREQUEST_LIST = 32;
    int MYFRIENDLIST = 33;
    int REQUESTACCEPTED = 34;
    int REQUESTIGNORED = 35;
    int DELETEEVENT = 36;
    int SEND_REQUEST = 37;
    int MY_CREATED_EVENTS = 38;
    int EVENT_INVITES = 39;
    int CHANGE_PROFILE_COVER_PIC = 40;
    int CHANGE_PROFILE_PIC = 49;
    int EVENT_DETAILS = 41;
    int SEND_INVITES = 42;
    int EDIT_EVENT = 43;
    int EVENT_INVITE_REQUEST_RESPONSE = 44;
    int GALLERYIMAGES = 45;
    int GALLERYVIDEO = 46;
    int ALARM_REQUEST_RESPONSE = 47;
    int ALARM_REQUEST_COUNT = 48;
    int UNAMEN = 49;
    int UNHALE = 50;

    int CREATE_GROUP = 51;
    int GROUP_LISTING = 52;
    int GROUP_DELETE = 53;
    int JOY_MUSIC_LISTING = 54;
    int ALBUM_LISTING = 55;
    int DELETE_POST = 56;
    int UNFRIEND = 57;
    int BOOK_DETAILS = 58;
    int EDIT_POST = 59;
    int BOOK_LANGUAGES = 60;
    int REMOVEPROFILEPIC = 61;
    int BOOKPAYMENTHASH = 62;
    int MY_BOOKS = 63;
    int MY_MESSAGES = 64;
    int MY_NOTIFICATIONS = 65;
    int MY_POST = 66;
    int SEARCH_JOY_MUSIC = 67;
    int JOY_MUSIC_SONGS = 68;
    int JOY_MUSIC_SONGS_VIEW_ALL = 69;
    int JOY_MUSIC_ALBUMS_VIEW_ALL = 70;
    int JOY_MUSIC_VIEW_ALL_SONGS_PAGINATION = 71;
    int JOY_MUSIC_VIEW_ALL_ALBUMS_PAGINATION = 72;
    int JOY_MUSIC_SUBSCRIPTION_PLAN = 73;
    int JOY_MUSIC_CHECK_SUBSCRIPTION = 74;
    int JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY = 75;
    int BUY_DOWNLOAD_MUSIC = 76;
    int MY_DOWNLOADS = 77;
    int HOME_PAGINATION = 78;
    int BOOK_BUY_FREE_BOOK = 79;
    int DEMO_HOME_POST = 80;
    int DEMO_HOME_POST_PAGINATION = 81;
    int DONATION_PAYMENT_HASH = 82;
    int GROUP_HOME_WALL_POSTS = 83;
    int GROUP_HOME_WALL_POSTS_PAGINATION = 84;
    int GROUP_CREATE_POST = 85;
    int GROUP_POST_LIKE = 86;
    int GROUP_POST_DETAIL = 87;
    int GROUP_POST_COMMENT = 88;
    int GROUP_UPDATE = 89;
    int VIDEO_ON_WALL = 90;
    int GROUP_POST_VIEW = 91;
    int GROUP_POST_COMMENT_LIST = 92;
    int TIMELINE_PAGINATION = 93;
    int SETTINGS_MAIN = 94;
    int SETTINGS_SET = 95;
    int SETTING_VALUE = 96;
    int SETTING_VALUE_UPDATED = 97;


    /*

    int SET_LOCATION = 4;
    int GET_CATEGORIES = 5;
    int GET_SHOP_LIST = 6;
    int GET_SHOP_DETAILS = 7;
    int GET_PRODUCT_DETAILS = 8;
    int ADD_TO_CART = 9;
    int GET_CART = 10;
    int LIKE_PRODUCT = 11;
    int ADD_WISHLIST = 12;
    int WISHLIST = 13;
    int ORDER_LIST = 14;
    int CANCEL_ORDER = 15;
    int GET_USER_PROFILE = 16;
    int FORGET_PASSWORD = 17;
    int UPDATE_REFERRAL = 18;
    int UPDATE_PROFILE = 19;
    int ORDER_PRODUCT = 20;
    int REPORT_ORDER_ISSUE = 21;
    int GET_BILL_LIST = 22;
    int GET_MY_SHOP_LIST = 23;
    int SELLER_DELETE_PRODUCT = 24;
    int GET_BEST_SELLING = 25;
    int EDIT_PRODUCT = 26;
    int LOCK_PRODUCT = 27;
    int SELLER_MY_ORDER = 28;
    int NEW_ORDER_LIST = 29;
    int EDIT_SHOP = 30;
    int FILE_UPLOAD = 31;
    int UPDATE_ORDER = 32;
    int CANCEL_ORDER_LIST = 33;
    int DELIVER_ORDER_LIST = 34;
    int RETURNED_ORDER_LIST = 35;
    int GET_FEEDBACK = 36;
    int FORWARD_BILL = 37;
    int FINANCE_FILE_UPLOAD = 38;
    int FINANCE_APPLY = 39;
    int PAY_TOBECOME_POPULAR = 40;
    int GET_POPULAR_DATES = 41;
    int SELLER_TRANSACTION_DETAILS = 42;


    */

    /**
     * Finance Strings
     *//*

    String ORDER_ID = "ORDER_ID";
    String PAYMENT_AMOUNT = "PAYMENT_AMOUNT";
    //User Name
    String USER_IMAGE = "USER_IMAGE";
    String FIRST_NAME = "FIRST_NAME";
    String LAST_NAME = "LAST_NAME";
    String DOCUMENT_ONE = "DOCUMENT_ONE";
    String DOCUMENT_TWO = "DOCUMENT_TWO";
    String DOB = "DOB";

    //User Residence
    String TOTAL_CREDIT_SCORE = "TOTAL_CREDIT_SCORE";
    String ADDRESS_PROOF = "ADDRESS_PROOF";
    String LICENCE = "LICENCE";
    String RESIDENCE = "RESIDENCE";
    String GPA = "GPA";

    //College
    String COLLEGE = "COLLEGE";

    //SSN
    String SSN = "SSN";
    String SSN_IMAGE = "SSN_IMAGE";

    //JOB
    String ANNUAL_INCOME = "ANNUAL_INCOME";
    String COMPANY = "COMPANY";
    String TITLE = "TITLE";
    String CADDRESSPROOF = "CADDRESSPROOF";
    String BANKSTATEMENT = "BANKSTATEMENT";
*/

    void onSuccess(int tag, Response response);

    void onError(int tag, Throwable throwable);
}
