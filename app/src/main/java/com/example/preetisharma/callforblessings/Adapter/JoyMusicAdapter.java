package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.JoyMusicViewAllAcitivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class JoyMusicAdapter extends RecyclerView.Adapter<JoyMusicAdapter.DataViewHolder> {
    private List<JoyMusicModal.ListBean> mList;


    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;


    public JoyMusicAdapter(Activity mContext, List<JoyMusicModal.ListBean> list) {
        this.mList = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        mList.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(ArrayList<JoyMusicModal.ListBean> list) {
        this.mList.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public JoyMusicAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.joy_music_item, parent, false);
        JoyMusicAdapter.DataViewHolder dataView = new JoyMusicAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    int i = 0;

    @Override
    public void onBindViewHolder(final JoyMusicAdapter.DataViewHolder holder, final int position) {
        holder.album_title.setText(mList.get(holder.getAdapterPosition()).getLanguage());
        JoyMusicModal.ListBean.AlbumsBean albumData = null;
        List<JoyMusicModal.ListBean.AlbumsBean> albumList = new ArrayList<>();
        Album_list_adapter albumAdapter = null;
        for (int i = 0; i < mList.get(holder.getAdapterPosition()).getAlbums().size(); i++) {
            albumData = new JoyMusicModal.ListBean.AlbumsBean();
            albumData.setName(mList.get(holder.getAdapterPosition()).getAlbums().get(i).getName());
            albumData.setId(mList.get(holder.getAdapterPosition()).getAlbums().get(i).getId());
            albumData.setAlbum_cover_image(mList.get(holder.getAdapterPosition()).getAlbums().get(i).getAlbum_cover_image());
            albumList.add(albumData);

        }
        albumAdapter = new Album_list_adapter(mContext, albumList);
        holder.joy_music_album_recycler_view.setAdapter(albumAdapter);
        holder.joy_music_album_recycler_view.scrollToPosition(0);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.joy_music_album_recycler_view.setLayoutManager(mLayoutManager);


    }

    public int getcount() {
        return mList.size();

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.album_title)
        AppCompatTextView album_title;
        @BindView(R.id.joy_music_album_recycler_view)
        RecyclerView joy_music_album_recycler_view;

        @BindView(R.id.atv_viewAll)
        AppCompatTextView atv_viewAll;

       /* @BindView(R.id.horizontal_scroll)
        HorizontalScrollView horizontal_scroll;
        @BindView(R.id.linear_layout_scroll)
        LinearLayout linear_layout_scroll;
        @BindView(R.id.albums_preview)
        LinearLayout albums_preview;
*/

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            atv_viewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ViewAll API

                    Intent listSongs = new Intent(mContext, JoyMusicViewAllAcitivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.LANGUAGE_ID, String.valueOf(mList.get(getAdapterPosition()).getId()));
                    bundle.putString(Constants.VIEW_ALL_FLAG, Constants.ALBUM);
                    bundle.putString(Constants.LANGUAGE_NAME, mList.get(getAdapterPosition()).getLanguage().trim());


                    //bundle.putString(Constants.ALBUM_NAME, mList.get(holder.getAdapterPosition()).getAlbum_name());
                    listSongs.putExtras(bundle);
                    mContext.startActivity(listSongs);
                }
            });


        }
    }
}
