package com.example.preetisharma.callforblessings.downloadjoymusic;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.AlbumListAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.BuyMusicDownloadedModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSongDownloadComplete;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Response;

import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class DownloadMainActivity extends BaseActivity implements Downloader.Listener, APIServerResponse {
    private static final String URI_STRING = "http://www.cbu.edu.zm/downloads/pdf-sample.pdf";
    private static final String[] PERMISSIONS = new String[]{
            WRITE_EXTERNAL_STORAGE,
            READ_EXTERNAL_STORAGE,
            INTERNET
    };

    //private Button download;

    private Downloader downloader;
    private String type = "", url = "", songName = "";
    private int id = 0;
    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermissions();
        //checkPermissions();
        setContentView(R.layout.demo_trans);

        //download = (Button) findViewById(R.id.download);
       /* download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadOrCancel();
            }
        });*/

        Bundle extras = getIntent().getExtras();
        id = extras.getInt(Constants.SONG_ID, 0);
        type = extras.getString(Constants.TYPE, Constants.SONG_CAPS);
        position = extras.getInt(Constants.POSITION, 0);
        url = extras.getString(Constants.SONG_DOWNLOAD_URL, "");
        songName = extras.getString(Constants.SONG_NAME, "");


        downloader = Downloader.newInstance(this, songName);

        if (isConnectedToInternet()) {
            ServerAPI.getInstance().buyDownloadMusic(APIServerResponse.BUY_DOWNLOAD_MUSIC, getUserSessionId(), String.valueOf(id), type, this);
        }

        downloadOrCancel(url, String.valueOf(id), type);


    }

    private void checkPermissions() {

        for (String permission : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
                return;
            }
        }
    }


    public void downloadOrCancel(String Url, String id, String type) {
        try {

            if (downloader.isDownloading()) {
                cancel();
            } else {
                download(Url);
            }
            updateUi();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void cancel() {
        downloader.cancel();
    }

    private void download(String URI_STRING) {
        Uri uri = Uri.parse(URI_STRING);
        downloader.download(uri);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (!areAllPermissionsGranted(requestCode, grantResults)) {
            finish();
        }
    }

    private boolean areAllPermissionsGranted(int requestCode, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void fileDownloaded(Uri uri, String mimeType) {
        /*Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, mimeType);
        startActivity(intent);*/
       /* if (isConnectedToInternet()) {
            ServerAPI.getInstance().buyDownloadMusic(APIServerResponse.BUY_DOWNLOAD_MUSIC, getUserSessionId(), String.valueOf(id), type, this);
        }*/

    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    private void updateUi() {
        if (downloader.isDownloading()) {
            //download.setText(R.string.cancel);
            showToast("Downloading", Toast.LENGTH_SHORT);
            finish();

        } else {
            showToast("Done", Toast.LENGTH_SHORT);
            //download.setText(R.string.download);
        }
    }

    @Override
    protected void onDestroy() {
        downloader.unregister();
        super.onDestroy();
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {

                BuyMusicDownloadedModal buyMusicDownloadedModal;

                switch (tag) {
                    case APIServerResponse.BUY_DOWNLOAD_MUSIC:

                        buyMusicDownloadedModal = (BuyMusicDownloadedModal) response.body();

                        if (buyMusicDownloadedModal.getStatus().equalsIgnoreCase("1")) {
                            //EventBus.getDefault().post(new EventBusSongDownloadComplete(position));
                            //updateUi();
                            //AlbumListAdapter.isDownloaded = false;
                            //finish();
                        }


                        break;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }
}
