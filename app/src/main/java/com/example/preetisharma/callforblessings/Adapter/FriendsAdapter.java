package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestAcceptedModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.DataViewHolder> {
    public List<FriendRequestModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;

    public FriendsAdapter(Activity mContext, List<FriendRequestModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<FriendRequestModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_adapter_friends_list, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        holder.txtvw_friends_request_name.setText(list.get(position).getUser_info().getProfile_details().getFirstname() + " " + list.get(position).getUser_info().getProfile_details().getLastname());
        Glide.with(mContext).load(list.get(position).getUser_info().getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).centerCrop().into(holder.iv_friend_request_image);


        holder.ll_my_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUser_info().getId()));
                b.putBoolean(Constants.TIMELINE_ENABLED, false);
                b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                b.putString(Constants.PRIVACY_WALL, list.get(holder.getAdapterPosition()).getUser_info().getWALL());
                b.putString(Constants.PRIVACY_ABOUT_INFO, list.get(holder.getAdapterPosition()).getUser_info().getabout_info());
                b.putString(Constants.PRIVACY_FRIEND_REQUEST, list.get(holder.getAdapterPosition()).getUser_info().getfriend_request());
                b.putString(Constants.PRIVACY_MESSAGE, list.get(holder.getAdapterPosition()).getUser_info().getMESSAGE());
                i.putExtras(b);
                mContext.startActivity(i);
            }
        });

    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {
        @BindView(R.id.txtvw_accept_friend_request)
        AppCompatTextView txtvw_accept_friend_request;
        @BindView(R.id.txtvw_ignore_friend_request)
        AppCompatTextView txtvw_ignore_friend_request;
        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;
        @BindView(R.id.layout_response_buttons)
        LinearLayout layout_response_buttons;
        @BindView(R.id.ll_my_friend)
        LinearLayout ll_my_friend;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            txtvw_accept_friend_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ServerAPI.getInstance().request_accepted(APIServerResponse.REQUESTACCEPTED, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), "ACCEPT", DataViewHolder.this);
                }
            });
            txtvw_ignore_friend_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ServerAPI.getInstance().request_accepted(APIServerResponse.REQUESTIGNORED, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), "REJECT", DataViewHolder.this);
                }
            });

        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    FriendRequestAcceptedModal acceptedModal;

                    switch (tag) {
                        case APIServerResponse.REQUESTACCEPTED:
                            acceptedModal = (FriendRequestAcceptedModal) response.body();
                            if (acceptedModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                                EventBus.getDefault().post(new DemoFriendsModal(acceptedModal.getStatus()));
                            } else {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                            }
                            break;
                        case APIServerResponse.REQUESTIGNORED:
                            acceptedModal = (FriendRequestAcceptedModal) response.body();
                            if (acceptedModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            } else {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                            }
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {

        }
    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        list.remove(position);
        notifyItemRemoved(position);
    }
}
