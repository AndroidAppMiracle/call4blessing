package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.BooksTabsPagerAdapter;
import com.example.preetisharma.callforblessings.Fragment.BooksFragment;
import com.example.preetisharma.callforblessings.Fragment.MyBooksFragment;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

/**
 * Created by Kshitiz Bali on 5/1/2017.
 */

public class BookTabs extends BaseActivity implements TabLayout.OnTabSelectedListener {

    ViewPager viewPager;
    AppCompatTextView txtvw_header_title;
    AppCompatImageView img_view_back;
    AppCompatImageView img_view_change_password;
    BooksTabsPagerAdapter booksTabsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);
        updateStatusBar();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*getSupportActionBar().setDisplayShowTitleEnabled(false);*/
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_friends);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        txtvw_header_title.setText(getString(R.string.books));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.my_books));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.books));
        viewPager = (ViewPager) findViewById(R.id.viewPager_friendsTab);

        img_view_change_password.setVisibility(View.VISIBLE);
        img_view_back.setVisibility(View.VISIBLE);
        img_view_back.setImageResource(R.drawable.ic_back);
        img_view_change_password.setImageResource(R.drawable.ic_add);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        img_view_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addEvent = new Intent(BookTabs.this, AddBookActivity.class);
                startActivity(addEvent);
            }
        });
        booksTabsPagerAdapter = new BooksTabsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(booksTabsPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(0);
        tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {


        Log.i("isVisible", "Yes");
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = booksTabsPagerAdapter.getFragment(viewPager, tab.getPosition(), fm);

        if (f != null) {
            if (tab.getPosition() == 0) {
               /* DemoHomeFragmentTest fragment = (DemoHomeFragmentTest) f;
                fragment.repositionRecyclerViewOnTabClick();
*/
                MyBooksFragment fragment = (MyBooksFragment) f;
                fragment.repositionRecyclerViewOnTabClick();
            } else if (tab.getPosition() == 1) {
                BooksFragment booksFragment = (BooksFragment) f;
                booksFragment.repositionRecyclerViewOnTabClick();
            }
        }


    }
}
