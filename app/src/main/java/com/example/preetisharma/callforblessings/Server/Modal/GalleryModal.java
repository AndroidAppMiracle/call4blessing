package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.ArrayList;

/**
 * Created by preeti.sharma on 2/9/2017.
 */

public class GalleryModal {
    /**
     * status : 1
     * list : [{"id":1,"name":"Desert","video_link":"","file":"http://dev.miracleglobal.com/cal-php/web/uploads/image-video/20170209030200Desert.jpg","type":"IMAGE"}]
     */

    private String status;
    private ArrayList<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ListBean> getList() {
        return list;
    }

    public void setList(ArrayList<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 1
         * name : Desert
         * video_link :
         * file : http://dev.miracleglobal.com/cal-php/web/uploads/image-video/20170209030200Desert.jpg
         * type : IMAGE
         */

        private int id;
        private String name;
        private String video_link;
        private String file;
        private String type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVideo_link() {
            return video_link;
        }

        public void setVideo_link(String video_link) {
            this.video_link = video_link;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
