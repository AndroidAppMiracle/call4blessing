package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.HelpRequestAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.HelpRequestListModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/31/2017.
 */

public class HelpRequestFragment extends Fragment implements APIServerResponse {
    @BindView(R.id.help_list)
    RecyclerView help_list;
    HelpRequestAdapter _adapter;


    List<HelpRequestListModal.ListBean> listAllPrayers;

    private String prayerId;

    HelpRequestListModal helpRequestListModal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.activity_help_request_list, container, false);

        ButterKnife.bind(this, rootView);
        help_list = (RecyclerView) rootView.findViewById(R.id.help_list);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        try {

            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getHelpRequest(APIServerResponse.GET_HELP_REQUEST, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {

            listAllPrayers = new ArrayList<>();

            ((BaseActivity) getActivity()).hideLoading();
            if (response.isSuccessful()) {


                switch (tag) {

                    case APIServerResponse.GET_HELP_REQUEST:
                        helpRequestListModal = (HelpRequestListModal) response.body();
                        if (helpRequestListModal.getStatus().equals("1")) {

                            listAllPrayers = helpRequestListModal.getList();
                            _adapter = new HelpRequestAdapter(getActivity(), listAllPrayers);
                            help_list.setAdapter(_adapter);
                            help_list.setItemAnimator(new DefaultItemAnimator());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            help_list.setLayoutManager(mLayoutManager);

                        } else {
                        }


                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Subscribe
    public void onEvent(DemoCommentModal event) {
        try {
            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getHelpRequest(APIServerResponse.GET_HELP_REQUEST, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            ((BaseActivity) getActivity()).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.GET_HELP_REQUEST:
                    System.out.println("Error");
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {


            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getHelpRequest(APIServerResponse.GET_HELP_REQUEST, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }
}
