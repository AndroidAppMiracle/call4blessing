package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.BooksAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BooksListingModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/1/2017.
 */

public class BooksFragment extends Fragment implements APIServerResponse {
    RecyclerView books_recycler_view;
    private List<BooksListingModal.ListBean> mList;
    BooksAdapter _adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_book_listing, container, false);
        books_recycler_view = (RecyclerView) rootView.findViewById(R.id.books_recycler_view);

        //RecyclerView.booksFragment mLayoutManager = new GridLayoutManager(this, 2);
        books_recycler_view.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().booksList(APIServerResponse.LIST_BOOK, ((BaseActivity) getActivity()).getUserSessionId(), this);
            ((BaseActivity) getActivity()).showLoading();
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().booksList(APIServerResponse.LIST_BOOK, ((BaseActivity) getActivity()).getUserSessionId(), this);
            ((BaseActivity) getActivity()).showLoading();
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {


        BooksListingModal booksModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.LIST_BOOK:

                    {
                        booksModal = (BooksListingModal) response.body();


                        if (booksModal.getStatus().equals("1")) {
                            if (booksModal.getList().size() > 0) {
                                mList = booksModal.getList();
                                _adapter = new BooksAdapter(getActivity(), mList);
                                books_recycler_view.setAdapter(_adapter);

                            }
                            break;
                        } else {
                            ((BaseActivity) getActivity()).showToast("No Books Found", Toast.LENGTH_SHORT);
                        }

                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ((BaseActivity) getActivity()).hideLoading();
        } else {
            ((BaseActivity) getActivity()).hideLoading();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        ((BaseActivity) getActivity()).hideLoading();
    }


    public void repositionRecyclerViewOnTabClick() {
        try {

            if (_adapter != null && _adapter.getItemCount() > 0) {
                //home_listing.scrollToPosition(0);
                books_recycler_view.smoothScrollToPosition(0);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
