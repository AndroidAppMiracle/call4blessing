package com.example.preetisharma.callforblessings.photos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.DemoDataAlbumsAdapter;
import com.example.preetisharma.callforblessings.Adapter.DemoDataMyPhotosAdapter;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 7/17/2017.
 */

public class UploadedByYouFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    ArrayList<DemoDataModal> list = new ArrayList<>();


    String[] images = {"https://s-media-cache-ak0.pinimg.com/originals/75/39/b4/7539b44290c0408c9e3346ea7ea07df7.jpg",
            "http://wallpapercave.com/wp/KcJ5HSD.jpg",
            "https://c1.staticflickr.com/3/2120/5814061558_7a129d91c7_b.jpg",
            "http://2.bp.blogspot.com/-gbNIsIhfmVk/UTdXGuLWKyI/AAAAAAAAUJQ/EeK2AC_djfk/s1600/Night+Scenery+Wallpapers+1.jpg",
            "http://freewallpapersbase.com/1024x768/nature-wallpapers/picture1/Italy04-scenery-wallpapers.jpg",
            "http://www.hdwallpaperup.com/wp-content/uploads/2014/12/December-Scenery-1024x768.jpg",
            "https://atozimages.files.wordpress.com/2011/07/snow-scenery-2.jpg",
            "https://c1.staticflickr.com/6/5227/5592652511_94cb1a2dc8_b.jpg",
            "http://images.summitpost.org/large/763459.jpg",
            "http://freewallpapersbase.com/1024x768/nature-wallpapers/picture1/Italy31-scenery-wallpapers.jpg",
    };


    //int[] images = {R.drawable.ic_album_placeholder};

    @BindView(R.id.rv_photos)
    RecyclerView rv_photos;

    DemoDataMyPhotosAdapter adapter;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_uploaded_by_you, container, false);
        ButterKnife.bind(this, rootView);

        swipe_refresh_layout.setOnRefreshListener(UploadedByYouFragment.this);
        rv_photos.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_photos.setItemAnimator(new DefaultItemAnimator());

        swipe_refresh_layout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        addDummyImages();
                    }
                }
        );

        ItemClickSupport.addTo(rv_photos).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                Intent intentViewImages = new Intent(getActivity(), PhotosSlideView.class);
                intentViewImages.putExtra(Constants.CURRENT_POSITION, position);
                Bundle b = new Bundle();
                b.putParcelableArrayList(Constants.IMAGE_SLIDESHOW, list);
                intentViewImages.putExtra(Constants.IMAGE_BUNDLE, b);
                startActivity(intentViewImages);

            }
        });


        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            //Load Data
            //addDummyImages();
        } else {
            //No Loading of Data
        }
    }

    public void addDummyImages() {

        if (!list.isEmpty()) {
            list.clear();
        }

        for (int i = 0; i < 10; i++) {

            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setId(i);
            demoDataModal.setImage(images[i]);
            demoDataModal.setName("Image " + (i + 1));

            list.add(demoDataModal);
        }

        if (swipe_refresh_layout != null) {
            swipe_refresh_layout.setRefreshing(false);
        }

        adapter = new DemoDataMyPhotosAdapter(getActivity(), list);
        rv_photos.setAdapter(adapter);


    }

    @Override
    public void onRefresh() {

        addDummyImages();

    }
}
