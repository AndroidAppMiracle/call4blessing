package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 2/21/2017.
 */

public class GroupListingModal {


    /**
     * status : 1
     * list : [{"id":4,"group_name":"check","group_description":"for testing","is_active":1,"group_icon":"","user_info":{"id":215,"username":"babitanayal1226","email":"babitanayal1226@gmail.com","updated":1,"profile_details":{"firstname":"Pushpeen","lastname":"Nayal","d_o_b":"1992-7-26","phone":"8800845355","gender":"FEMALE","location":"","country":"","state":"Andaman and Nicobar Islands","cover_pic":"http://call4blessing.com/uploads/coverphoto/1495437303WhatsApp Image 2017-05-22 at 07.57.36.jpeg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492345123user_profile.jpg"},"is_friend":0,"request_respond":"NO"},"is_created_by_me":false,"members":[{"group_id":4,"user_info":{"id":216,"username":"rajneeshjesus21","email":"rajneeshjesus21@gmail.com","updated":1,"profile_details":{"firstname":"Rajneesh ","lastname":"Kumar","d_o_b":"1988-08-21","phone":"9045171793","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1496250098user_profile.jpg"},"is_friend":0,"request_respond":"NO"}}]},{"id":1,"group_name":"First group","group_description":" ","is_active":1,"group_icon":"http://call4blessing.com/uploads/group/1493104347Desert.jpg","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"},"is_friend":0,"request_respond":"NO"},"is_created_by_me":true,"members":[]},{"id":2,"group_name":"joy group","group_description":" ","is_active":1,"group_icon":"http://call4blessing.com/uploads/group/20170419100415image.jpg","user_info":{"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495439742user_profile.jpg"},"is_friend":2,"request_respond":"NO"},"is_created_by_me":false,"members":[{"group_id":2,"user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"},"is_friend":0,"request_respond":"NO"}}]},{"id":3,"group_name":"new group","group_description":" ","is_active":1,"group_icon":"http://call4blessing.com/uploads/group/20170501160520image.jpg","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"},"is_friend":0,"request_respond":"NO"},"is_created_by_me":true,"members":[{"group_id":3,"user_info":{"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495439742user_profile.jpg"},"is_friend":2,"request_respond":"NO"}}]}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 4
         * group_name : check
         * group_description : for testing
         * is_active : 1
         * group_icon :
         * user_info : {"id":215,"username":"babitanayal1226","email":"babitanayal1226@gmail.com","updated":1,"profile_details":{"firstname":"Pushpeen","lastname":"Nayal","d_o_b":"1992-7-26","phone":"8800845355","gender":"FEMALE","location":"","country":"","state":"Andaman and Nicobar Islands","cover_pic":"http://call4blessing.com/uploads/coverphoto/1495437303WhatsApp Image 2017-05-22 at 07.57.36.jpeg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492345123user_profile.jpg"},"is_friend":0,"request_respond":"NO"}
         * is_created_by_me : false
         * members : [{"group_id":4,"user_info":{"id":216,"username":"rajneeshjesus21","email":"rajneeshjesus21@gmail.com","updated":1,"profile_details":{"firstname":"Rajneesh ","lastname":"Kumar","d_o_b":"1988-08-21","phone":"9045171793","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1496250098user_profile.jpg"},"is_friend":0,"request_respond":"NO"}}]
         */

        private int id;
        private String group_name;
        private String group_description;
        private int is_active;
        private String group_icon;
        private UserInfoBean user_info;
        private boolean is_created_by_me;
        private List<MembersBean> members;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getGroup_description() {
            return group_description;
        }

        public void setGroup_description(String group_description) {
            this.group_description = group_description;
        }

        public int getIs_active() {
            return is_active;
        }

        public void setIs_active(int is_active) {
            this.is_active = is_active;
        }

        public String getGroup_icon() {
            return group_icon;
        }

        public void setGroup_icon(String group_icon) {
            this.group_icon = group_icon;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public boolean isIs_created_by_me() {
            return is_created_by_me;
        }

        public void setIs_created_by_me(boolean is_created_by_me) {
            this.is_created_by_me = is_created_by_me;
        }

        public List<MembersBean> getMembers() {
            return members;
        }

        public void setMembers(List<MembersBean> members) {
            this.members = members;
        }

        public static class UserInfoBean {
            /**
             * id : 215
             * username : babitanayal1226
             * email : babitanayal1226@gmail.com
             * updated : 1
             * profile_details : {"firstname":"Pushpeen","lastname":"Nayal","d_o_b":"1992-7-26","phone":"8800845355","gender":"FEMALE","location":"","country":"","state":"Andaman and Nicobar Islands","cover_pic":"http://call4blessing.com/uploads/coverphoto/1495437303WhatsApp Image 2017-05-22 at 07.57.36.jpeg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492345123user_profile.jpg"}
             * is_friend : 0
             * request_respond : NO
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : Pushpeen
                 * lastname : Nayal
                 * d_o_b : 1992-7-26
                 * phone : 8800845355
                 * gender : FEMALE
                 * location :
                 * country :
                 * state : Andaman and Nicobar Islands
                 * cover_pic : http://call4blessing.com/uploads/coverphoto/1495437303WhatsApp Image 2017-05-22 at 07.57.36.jpeg
                 * profile_pic : http://call4blessing.com/uploads/profilepic/1492345123user_profile.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }

        public static class MembersBean {
            /**
             * group_id : 4
             * user_info : {"id":216,"username":"rajneeshjesus21","email":"rajneeshjesus21@gmail.com","updated":1,"profile_details":{"firstname":"Rajneesh ","lastname":"Kumar","d_o_b":"1988-08-21","phone":"9045171793","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1496250098user_profile.jpg"},"is_friend":0,"request_respond":"NO"}
             */

            private int group_id;
            private UserInfoBeanX user_info;

            public int getGroup_id() {
                return group_id;
            }

            public void setGroup_id(int group_id) {
                this.group_id = group_id;
            }

            public UserInfoBeanX getUser_info() {
                return user_info;
            }

            public void setUser_info(UserInfoBeanX user_info) {
                this.user_info = user_info;
            }

            public static class UserInfoBeanX {
                /**
                 * id : 216
                 * username : rajneeshjesus21
                 * email : rajneeshjesus21@gmail.com
                 * updated : 1
                 * profile_details : {"firstname":"Rajneesh ","lastname":"Kumar","d_o_b":"1988-08-21","phone":"9045171793","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1496250098user_profile.jpg"}
                 * is_friend : 0
                 * request_respond : NO
                 */

                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanX {
                    /**
                     * firstname : Rajneesh
                     * lastname : Kumar
                     * d_o_b : 1988-08-21
                     * phone : 9045171793
                     * gender : MALE
                     * location :
                     * country :
                     * state :
                     * cover_pic : http://call4blessing.com/theme/images/cover-picture.jpg
                     * profile_pic : http://call4blessing.com/uploads/profilepic/1496250098user_profile.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }
        }
    }
}
