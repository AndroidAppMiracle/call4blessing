package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by preeti.sharma on 1/4/2017.
 */

public class RegisterModal {


    /**
     * status : 1
     * message : Registration completed!
     * session_key : PIlnw_kBR7cBSzXvAi4CYCiG9UyxwsD2
     * detail : {"id":75,"username":"plash75","email":"plash@atc.com","updated":0,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"321631456","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/profilepic.jpg"},"post":[]}
     */

    private String status;
    private String message;
    private String session_key;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSession_key() {
        return session_key;
    }

    public void setSession_key(String session_key) {
        this.session_key = session_key;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * id : 75
         * username : plash75
         * email : plash@atc.com
         * updated : 0
         * profile_details : {"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"321631456","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/profilepic.jpg"}
         * post : []
         */

        private int id;
        private String username;
        private String email;
        private int updated;
        private ProfileDetailsBean profile_details;
        private List<?> post;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getUpdated() {
            return updated;
        }

        public void setUpdated(int updated) {
            this.updated = updated;
        }

        public ProfileDetailsBean getProfile_details() {
            return profile_details;
        }

        public void setProfile_details(ProfileDetailsBean profile_details) {
            this.profile_details = profile_details;
        }

        public List<?> getPost() {
            return post;
        }

        public void setPost(List<?> post) {
            this.post = post;
        }

        public static class ProfileDetailsBean {
            /**
             * firstname : Plash
             * lastname : Jindal
             * d_o_b : 1992-08-29
             * phone : 321631456
             * gender : MALE
             * location :
             * country :
             * state :
             * cover_pic : http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg
             * profile_pic : http://dev.miracleglobal.com/cal-php/web/theme/images/profilepic.jpg
             */

            private String firstname;
            private String lastname;
            private String d_o_b;
            private String phone;
            private String gender;
            private String location;
            private String country;
            private String state;
            private String cover_pic;
            private String profile_pic;

            public String getFirstname() {
                return firstname;
            }

            public void setFirstname(String firstname) {
                this.firstname = firstname;
            }

            public String getLastname() {
                return lastname;
            }

            public void setLastname(String lastname) {
                this.lastname = lastname;
            }

            public String getD_o_b() {
                return d_o_b;
            }

            public void setD_o_b(String d_o_b) {
                this.d_o_b = d_o_b;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCover_pic() {
                return cover_pic;
            }

            public void setCover_pic(String cover_pic) {
                this.cover_pic = cover_pic;
            }

            public String getProfile_pic() {
                return profile_pic;
            }

            public void setProfile_pic(String profile_pic) {
                this.profile_pic = profile_pic;
            }
        }
    }
}
