package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CreateGroupPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusGroupCreatePostModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.FileUtils;
import com.example.preetisharma.callforblessings.Utils.ImageUtils;
import com.example.preetisharma.callforblessings.Utils.Utilities;
import com.example.preetisharma.callforblessings.video.MediaController;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/8/2017.
 */

public class CreateGroupPostActivity extends BaseActivity implements APIServerResponse {


    @BindView(R.id.img_vw_user_profile)
    AppCompatImageView img_vw_user_profile;
    @BindView(R.id.txtvw_user_name)
    AppCompatTextView txtvw_user_name;
    @BindView(R.id.aet_post_text)
    AppCompatEditText aet_post_text;
    @BindView(R.id.card_img_video_upload_preview)
    CardView card_img_video_upload_preview;
    @BindView(R.id.iv_grou_post_image)
    AppCompatImageView iv_grou_post_image;
    @BindView(R.id.img_vw_upload_image)
    AppCompatImageView img_vw_upload_image;
    @BindView(R.id.img_vw_upload_video)
    AppCompatImageView img_vw_upload_video;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.txtvw_chat)
    AppCompatTextView txtvw_chat;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int PICK_IMAGE = 2;
    private static final int RECORD_VIDEO = 3;
    private static final int PICK_VIDEO = 4;

    private static final int RC_GALLERY_PERM = 10;
    static final int RC_CAMERA_PERM_AND_STORAGE = 11;

    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

    private String videoIntentData = "", compressVideoFilePath = "", videoFileSavedThumbnail = "", mCurrentPhotoPath = "", groupId = "", postTypeFlag = Constants.TEXT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_post);
        ButterKnife.bind(this);


        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            groupId = extras.getString(Constants.GROUP_ID, "0");
            Log.i("GroupID", groupId);
        }

        setSupportActionBar(toolbar);


        Glide.with(CreateGroupPostActivity.this).load(getUserImage()).placeholder(R.drawable.ic_me).thumbnail(0.1f).crossFade().into(img_vw_user_profile);
        txtvw_user_name.setText(getFullName());

        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Create Post");
        txtvw_chat.setVisibility(View.VISIBLE);
        txtvw_chat.setText(" Post");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                CreateGroupPostActivity.this.finish();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }


    }


    @OnClick(R.id.txtvw_chat)
    public void createPost() {

        try {

            hideKeyboard();
            if (postTypeFlag.equalsIgnoreCase(Constants.VIDEO)) {

                showLoading();
                ServerAPI.getInstance().createGroupPostWithImageOrVideo(APIServerResponse.GROUP_CREATE_POST, getUserSessionId(), groupId, aet_post_text.getText().toString().trim(), compressVideoFilePath, Constants.VIDEO, videoFileSavedThumbnail, this);

            } else if (postTypeFlag.equalsIgnoreCase(Constants.IMAGE)) {

                showLoading();
                ServerAPI.getInstance().createGroupPostWithImageOrVideo(APIServerResponse.GROUP_CREATE_POST, getUserSessionId(), groupId, aet_post_text.getText().toString().trim(), mCurrentPhotoPath, Constants.IMAGE, "", this);

            } else {
                showLoading();
                ServerAPI.getInstance().createGroupPostWithText(APIServerResponse.GROUP_CREATE_POST, getUserSessionId(), groupId, aet_post_text.getText().toString().trim(), this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyboard();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {

                CreateGroupPostModal createGroupPostModal;

                switch (tag) {

                    case APIServerResponse.GROUP_CREATE_POST:

                        createGroupPostModal = (CreateGroupPostModal) response.body();

                        if (createGroupPostModal.getStatus().equalsIgnoreCase("1")) {
                            EventBus.getDefault().post(new EventBusGroupCreatePostModal("1"));
                            showToast("Post created successfully", Toast.LENGTH_SHORT);
                            CreateGroupPostActivity.this.finish();

                        }

                        hideLoading();
                        break;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();

    }

    @OnClick(R.id.img_vw_upload_image)
    public void selectImage() {
        try {
            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            dialogBuilder.setTitle("Select image");
            /*dialogBuilder.setMessage("Select ");*/
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dispatchTakePictureIntent();


                }
            });
            dialogBuilder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dispatchSelectGalleryPicIntent();


                }
            });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.img_vw_upload_video)
    public void selectVideo() {
        try {
            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            dialogBuilder.setTitle("Select video");
            /*dialogBuilder.setMessage("Select ");*/
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    createVideo();


                }
            });
            dialogBuilder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    pickVideo();


                }
            });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {

                if (requestCode == RECORD_VIDEO) {

                    if (data.getData() != null) {
                        Log.i("data", "" + data.getData());
                        Log.i("dataGetPath", "" + data.getData().getPath());


                        Bitmap mThumbnailVideo = ThumbnailUtils.createVideoThumbnail(data.getData().getPath(), MediaStore.Images.Thumbnails.MINI_KIND);

                        if (card_img_video_upload_preview.getVisibility() == View.GONE) {
                            card_img_video_upload_preview.setVisibility(View.VISIBLE);
                        }
                        iv_grou_post_image.setImageBitmap(mThumbnailVideo);
                        videoIntentData = data.getData().getPath();
                        videoFileSavedThumbnail = ImageUtils.saveImageOnSDCard(this, ImageUtils.getBytesFromBitmap(mThumbnailVideo));
                        videoFileSavedThumbnail = Utilities.compressImage(videoFileSavedThumbnail);
                        new DemoVideoCompressor().execute();
                        postTypeFlag = Constants.VIDEO;
                    }
                } else if (requestCode == PICK_VIDEO) {
                    if (data.getData() != null) {
                        Log.i("data", "" + data.getData());
                        Log.i("dataGetPath", "" + data.getData().getPath());
                        Bitmap mThumbnailVideo = ThumbnailUtils.createVideoThumbnail(FileUtils.getPath(CreateGroupPostActivity.this, data.getData()), MediaStore.Images.Thumbnails.MINI_KIND);

                        if (card_img_video_upload_preview.getVisibility() == View.GONE) {
                            card_img_video_upload_preview.setVisibility(View.VISIBLE);
                        }
                        iv_grou_post_image.setImageBitmap(mThumbnailVideo);
                        videoIntentData = FileUtils.getPath(CreateGroupPostActivity.this, data.getData());
                        videoFileSavedThumbnail = ImageUtils.saveImageOnSDCard(CreateGroupPostActivity.this, ImageUtils.getBytesFromBitmap(mThumbnailVideo));
                        videoFileSavedThumbnail = Utilities.compressImage(videoFileSavedThumbnail);
                        new DemoVideoCompressor().execute();
                        postTypeFlag = Constants.VIDEO;
                    }
                } else if (requestCode == REQUEST_IMAGE_CAPTURE) {

                    if (data != null) {

                        handleBigCameraPhoto();
                        postTypeFlag = Constants.IMAGE;
                        Log.i("Photo Path", mCurrentPhotoPath);

                    }
                } else if (requestCode == PICK_IMAGE) {
                    if (data != null) {


                        mCurrentPhotoPath = null;
                        String fullImgPath = FileUtils.getPath(CreateGroupPostActivity.this, data.getData());

                        if (card_img_video_upload_preview.getVisibility() == View.GONE) {
                            card_img_video_upload_preview.setVisibility(View.VISIBLE);
                        }
                        mCurrentPhotoPath = Utilities.compressImage(fullImgPath);
                        Glide.with(CreateGroupPostActivity.this).load(mCurrentPhotoPath).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).crossFade().into(iv_grou_post_image);
                        postTypeFlag = Constants.IMAGE;


                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    ///////////////////////////////


    @AfterPermissionGranted(RC_CAMERA_PERM_AND_STORAGE)
    private void dispatchTakePictureIntent() {
        if (EasyPermissions.hasPermissions(CreateGroupPostActivity.this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {


            dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE);

           /* Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }*/
        } else {
            EasyPermissions.requestPermissions(CreateGroupPostActivity.this, getString(R.string.access_camera_storage),
                    RC_CAMERA_PERM_AND_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    private void dispatchSelectGalleryPicIntent() {

        if (EasyPermissions.hasPermissions(CreateGroupPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        } else {
            EasyPermissions.requestPermissions(CreateGroupPostActivity.this, getString(R.string.access_camera_storage),
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

    }


    @AfterPermissionGranted(RC_CAMERA_PERM_AND_STORAGE)
    public void createVideo() {
        if (EasyPermissions.hasPermissions(CreateGroupPostActivity.this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {


            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            startActivityForResult(takeVideoIntent, RECORD_VIDEO);
        } else {
            EasyPermissions.requestPermissions(CreateGroupPostActivity.this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM_AND_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickVideo() {
        if (EasyPermissions.hasPermissions(CreateGroupPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("video/*");
            startActivityForResult(intent, PICK_VIDEO);
        } else {
            EasyPermissions.requestPermissions(CreateGroupPostActivity.this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    private void dispatchTakePictureIntent(int actionCode) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        switch (actionCode) {
            case REQUEST_IMAGE_CAPTURE:
                File f = null;
                mCurrentPhotoPath = null;
                try {
                    f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                } catch (IOException e) {
                    e.printStackTrace();
                    f = null;
                    mCurrentPhotoPath = null;
                }
                break;

            default:
                break;
        } // switch

        startActivityForResult(takePictureIntent, actionCode);
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        Log.i("Name", f.getName());
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private String getAlbumName() {

        return getString(R.string.call4blessing_pictures);
    }


    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
    }

    private void handleBigCameraPhoto() {

        if (mCurrentPhotoPath != null) {
            setPic();
            galleryAddPic();
            mCurrentPhotoPath = Utilities.compressImage(mCurrentPhotoPath);
            //mCurrentPhotoPath = null;
        }

    }

    private void setPic() {


        if (mCurrentPhotoPath != null && !mCurrentPhotoPath.equalsIgnoreCase("")) {
            if (card_img_video_upload_preview.getVisibility() == View.GONE) {
                card_img_video_upload_preview.setVisibility(View.VISIBLE);
            }

            Glide.with(CreateGroupPostActivity.this).load(mCurrentPhotoPath).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).crossFade().into(iv_grou_post_image);

        }

        /* There isn't enough memory to open up more than a couple camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */

        /*int targetW = img_view_event_pic.getWidth();
        int targetH = img_view_event_pic.getHeight();

		*//* Get the size of the image *//*
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		*//* Figure out which way needs to be reduced less *//*
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		*//* Set bitmap options to scale the image decode target *//*
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

		*//* Decode the JPEG file into a Bitmap *//*
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		*//* Associate the Bitmap to the ImageView *//*
        img_view_event_pic.setImageBitmap(bitmap);
        img_view_event_pic.setVisibility(View.VISIBLE);*/
    }


    private void resizePic() {

         /*There isn't enough memory to open up more than a couple camera photos
         So pre-scale the target bitmap into which the file is decoded

		 Get the size of the ImageView */

        int targetW = iv_grou_post_image.getWidth();
        int targetH = iv_grou_post_image.getWidth();

		/* Get the size of the image*/
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		 /*Figure out which way needs to be reduced less*/
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		 /*Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

		 /*Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		 /*Associate the Bitmap to the ImageView */
        iv_grou_post_image.setImageBitmap(bitmap);
        iv_grou_post_image.setVisibility(View.VISIBLE);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }


    private class DemoVideoCompressor extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (MediaController.getInstance().convertVideo(videoIntentData)) {
                return MediaController.getInstance().compressedFilePath();
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String compressedPath) {
            super.onPostExecute(compressedPath);
            hideLoading();
            if (compressedPath != null) {
                compressVideoFilePath = compressedPath;
                Log.e("File path", compressedPath);

            }


            //img_vw_play_video.setVisibility(View.VISIBLE);

        }
    }


    //////////////////////////////////////Compression Methods


    /*public String compressImage(String filePath) {

        *//*String filePath = getRealPathFromURI(imageUri);*//*
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        //String filename = sada;
        try {
            out = new FileOutputStream(filePath);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        Log.i("Compressed Image", filePath);

        return filePath;

    }*/

  /*  public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }*/

   /* private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }*/
}
