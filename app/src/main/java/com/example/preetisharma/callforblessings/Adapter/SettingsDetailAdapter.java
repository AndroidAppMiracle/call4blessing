package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSettingsChanged;
import com.example.preetisharma.callforblessings.Server.Modal.SettingDetailModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsMainModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsSetModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.SettingsDetailActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/27/2017.
 */

public class SettingsDetailAdapter extends RecyclerView.Adapter<SettingsDetailAdapter.DataViewHolder> implements APIServerResponse {
    public List<SettingDetailModal> list;
    Activity mContext;
    private String settingId = "", selectedSettingName = "";
    private int lastSelectedPosition = 0;
    /*private int[] settingsImages = {R.drawable.ic_action_setting_wall,
            R.drawable.ic_action_setting_messages,
            R.drawable.ic_action_setting_friend_req,
            R.drawable.ic_action_setting_about_info};*/

    public SettingsDetailAdapter(Activity mContext, List<SettingDetailModal> list, String settingId) {
        this.list = list;
        this.mContext = mContext;
        this.settingId = settingId;
    }


 /*   public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }*/

  /*  public void addAllItems(List<MyBooksModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }*/


    @Override
    public SettingsDetailAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_settings_detail_item, parent, false);
        SettingsDetailAdapter.DataViewHolder dataView = new SettingsDetailAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final SettingsDetailAdapter.DataViewHolder holder, int position) {


        holder.atv_setting_name.setText(list.get(holder.getAdapterPosition()).getName());

        holder.aiv_setting_image.setImageResource(list.get(holder.getAdapterPosition()).getSettingImage());

        //Glide.with(mContext).load(list.get(holder.getAdapterPosition()).getSettingImage()).placeholder(R.drawable.placeholder)/*.thumbnail(0.1f)*/.crossFade()/*.override(64, 64)*/.into(holder.aiv_setting_image);


        holder.radioButtonSelected.setChecked(list.get(holder.getAdapterPosition()).isChecked());
        if (list.get(holder.getAdapterPosition()).isChecked()) {
            selectedSettingName = list.get(holder.getAdapterPosition()).getName();
            lastSelectedPosition = holder.getAdapterPosition();
        }

        holder.radioButtonSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    Log.i("isChecked", "Checked");

                    list.get(lastSelectedPosition).setChecked(false);
                    list.get(holder.getAdapterPosition()).setChecked(true);
                    Log.i("Previous Pos", "" + lastSelectedPosition);
                    notifyItemChanged(lastSelectedPosition);
                    notifyItemChanged(holder.getAdapterPosition());
                    lastSelectedPosition = holder.getAdapterPosition();
                    selectedSettingName = list.get(holder.getAdapterPosition()).getName();
                    Log.i("Selected Pos", "" + holder.getAdapterPosition());
                    Log.i("Selected Value", "" + selectedSettingName);

                    if (((BaseActivity) mContext).isConnectedToInternet()) {

                        if (selectedSettingName.equalsIgnoreCase(Constants.FRIENDS)) {
                            selectedSettingName = Constants.FRIEND;
                        } else if (selectedSettingName.equalsIgnoreCase(Constants.ONLY_ME)) {
                            selectedSettingName = Constants.ONLY_ME_SETTING;
                        }


                        ServerAPI.getInstance().setSettings(APIServerResponse.SETTINGS_SET, ((BaseActivity) mContext).getUserSessionId(), settingId, selectedSettingName.toUpperCase(), SettingsDetailAdapter.this);

                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }

                   /* list.get(lastSelectedPosition).setChecked(false);
                    list.get(holder.getAdapterPosition()).setChecked(true);
                    lastSelectedPosition = holder.getAdapterPosition();
                    selectedSettingName = list.get(holder.getAdapterPosition()).getName();
                    notifyDataSetChanged();

                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        if (!selectedSettingName.equalsIgnoreCase("")) {

                            ServerAPI.getInstance().setSettings(APIServerResponse.SETTINGS_SET, ((BaseActivity) mContext).getUserSessionId(), settingId, selectedSettingName, SettingsDetailAdapter.this);
                        }
                        *//*else {

                            ((BaseActivity) mContext).showToast("Please select an option", Toast.LENGTH_SHORT);
                        }*//*
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*/


                } else {


                  /*  lastSelectedPosition = holder.getAdapterPosition();
                    list.get(holder.getAdapterPosition()).setChecked(true);
                    selectedSettingName = list.get(holder.getAdapterPosition()).getName();
                    notifyDataSetChanged();*/


                }


            }
        });


    }


    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {

                SettingsSetModal settingsSetModal;

                switch (tag) {

                    case APIServerResponse.SETTINGS_SET:
                        settingsSetModal = (SettingsSetModal) response.body();
                        if (settingsSetModal.getStatus().equalsIgnoreCase("1")) {
                            EventBus.getDefault().post(new EventBusSettingsChanged(true));

                        }

                        break;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();

    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_setting_name)
        AppCompatTextView atv_setting_name;

        @BindView(R.id.aiv_setting_image)
        AppCompatImageView aiv_setting_image;

        @BindView(R.id.cv_item)
        CardView cv_item;

        @BindView(R.id.radioButtonSelected)
        RadioButton radioButtonSelected;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

         /*   cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        if (!selectedSettingName.equalsIgnoreCase("")) {
                            ServerAPI.getInstance().setSettings(APIServerResponse.SETTINGS_SET, ((BaseActivity) mContext).getUserSessionId(), settingId, selectedSettingName, DataViewHolder.this);
                        } else {

                            ((BaseActivity) mContext).showToast("Please select an option", Toast.LENGTH_SHORT);
                        }
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }


                }
            });*/

        }



        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }
}
