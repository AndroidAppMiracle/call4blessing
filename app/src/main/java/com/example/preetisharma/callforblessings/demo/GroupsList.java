package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GroupsListAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusIsGroupCreatedModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupListingModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/21/2017.
 */

public class GroupsList extends BaseActivity implements APIServerResponse {

    RecyclerView rv_groups;

    List<GroupListingModal.ListBean> groupList = new ArrayList<>();
    GroupsListAdapter groupsListAdapter;
    Toolbar toolbar;
    AppCompatImageView img_view_back;
    AppCompatImageView img_view_change_password;
    AppCompatTextView txtvw_header_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups_list);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        rv_groups = (RecyclerView) findViewById(R.id.rv_groups);
        rv_groups.setLayoutManager(new GridLayoutManager(this, 2));
        rv_groups.setItemAnimator(new DefaultItemAnimator());
        //rv_groups.setAdapter(groupsListAdapter);


        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setImageResource(R.drawable.ic_add);
        txtvw_header_title.setText("Groups");

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getGroupsList(APIServerResponse.GROUP_LISTING, getUserSessionId(), this);
        } else {
            showToast("Please check your internet connection.", Toast.LENGTH_LONG);
        }

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img_view_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupsList.this, CreateGroupActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.GROUP_FUNCTION, Constants.GROUP_CREATE);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {

            GroupListingModal groupListingModal;
            if (response.isSuccessful()) {

                switch (tag) {

                    case APIServerResponse.GROUP_LISTING:
                        groupListingModal = (GroupListingModal) response.body();
                        if (groupListingModal.getStatus().equals("1")) {
                            groupList = groupListingModal.getList();
                            groupsListAdapter = new GroupsListAdapter(GroupsList.this, groupList);
                            rv_groups.setAdapter(groupsListAdapter);


                        } else {
                            showToast("No groups created.", Toast.LENGTH_LONG);
                        }

                        hideLoading();
                        break;
                }

            } else {
                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {

        try {
            hideLoading();
            if (tag == APIServerResponse.GROUP_LISTING) {
                throwable.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();


    }


    @Subscribe
    public void isGroupCreatedPost(EventBusIsGroupCreatedModal eventBusIsGroupCreatedModal) {

        if (isConnectedToInternet()) {
            try {
                if (eventBusIsGroupCreatedModal.getIsGroupCreated().equalsIgnoreCase("1")) {
                    showLoading();
                    ServerAPI.getInstance().getGroupsList(APIServerResponse.GROUP_LISTING, getUserSessionId(), this);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

}
