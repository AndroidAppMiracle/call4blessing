package com.example.preetisharma.callforblessings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.OtpModal;
import com.example.preetisharma.callforblessings.Server.Modal.RegisterModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.DataHolder;
import com.example.preetisharma.callforblessings.Utils.IncomingSms;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/13/2017.
 */

public class OtpRequestActivity extends BaseActivity implements APIServerResponse, EasyPermissions.PermissionCallbacks {
    @BindView(R.id.edt_txt_otp)
    AppCompatEditText edt_txt_otp;
    @BindView(R.id.click_again)
    AppCompatTextView click_again;
    @BindView(R.id.txtvw_submit)
    AppCompatTextView txtvw_submit;
    String device_id;
    TimeZone tz;
    final SmsManager sms = SmsManager.getDefault();
    static OtpRequestActivity otpinstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_request);
        otpinstance = this;
        updateStatusBar();
        ButterKnife.bind(this);
        // device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        device_id = getUserFcmToken();
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().generateotp(APIServerResponse.GENERATEOTP, device_id, getUserPhoneNumber(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        click_again.setTextColor(getResources().getColorStateList(R.color.textview_selector));
        Calendar cal = Calendar.getInstance();
        tz = cal.getTimeZone();
    }

    @OnClick(R.id.click_again)
    public void againSend() {
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().generateotp(APIServerResponse.GENERATEOTP, device_id, getUserPhoneNumber(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void checkSim() {
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                showToast("No Sim Card", Toast.LENGTH_SHORT);
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                // do something
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                // do something
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                // do something
                break;
            case TelephonyManager.SIM_STATE_READY:
                IncomingSms incomingsms = new IncomingSms();

                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                // do something
                break;
        }
    }

    public static OtpRequestActivity getInstance() {
        return otpinstance;
    }

    public void setOtp(String otp)

    {
        edt_txt_otp.setText(otp);
    }

    @OnClick(R.id.txtvw_submit)
    public void submitOtp() {

        if (edt_txt_otp.getText().toString().isEmpty()) {
            showToast("Enter OTP to proceed.", Toast.LENGTH_SHORT);
        } else if (edt_txt_otp.getText().length() < 5) {
            showToast("Please check your OTP.", Toast.LENGTH_SHORT);
        } else {

            if (isConnectedToInternet()) {
                ServerAPI.getInstance().verifyOtp(APIServerResponse.VERIFY_CODE, edt_txt_otp.getText().toString(), this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }

    }


    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        RegisterModal reg;
        OtpModal generateOtp;
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.GENERATEOTP:
                        generateOtp = (OtpModal) response.body();
                        if (generateOtp.getStatus().equals("1")) {

                            checkSim();
                        }
                        break;
                    case APIServerResponse.VERIFY_CODE:
                        generateOtp = (OtpModal) response.body();
                        if (generateOtp.getStatus().equals("1")) {
                            if (isConnectedToInternet()) {
                                showLoading();

                                ServerAPI.getInstance().register(APIServerResponse.REGISTER, DataHolder.getEmail(), DataHolder.getPassword(), device_id, "ANDROID", DataHolder.getFirst_name(), DataHolder.getLast_name(), DataHolder.getGender(), DataHolder.getDate_of_birth(), DataHolder.getPhone_number(), tz.getID(), this);
                            } else {
                                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                            }
                        } else {
                            showToast(generateOtp.getMessage(), Toast.LENGTH_SHORT);
                        }
                        break;
                    case APIServerResponse.REGISTER:

                        reg = (RegisterModal) response.body();


                        if (reg.getStatus().equals("1")) {
                            getmPrefs();
                            setUserLoggedIn(true);
                            setUserID(String.valueOf(reg.getDetail().getId()));
                            setUserSessionId(reg.getSession_key());
                            setFirstName(reg.getDetail().getUsername());
                            setFullName(reg.getDetail().getProfile_details().getFirstname() + reg.getDetail().getProfile_details().getLastname());

                            showToast(reg.getMessage(), Toast.LENGTH_SHORT);
                            Intent intent = new Intent(this, HomeActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (reg.getStatus().equals("0")) {
                            hideLoading();
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        switch (tag) {
            case APIServerResponse.REGISTER:
                System.out.println("Error");
                break;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().register(APIServerResponse.REGISTER, DataHolder.getEmail(), DataHolder.getPassword(), device_id, "ANDROID", DataHolder.getFirst_name(), DataHolder.getLast_name(), DataHolder.getGender(), DataHolder.getDate_of_birth(), DataHolder.getPhone_number(), tz.getID(), this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onBackPressed() {

    }


}
