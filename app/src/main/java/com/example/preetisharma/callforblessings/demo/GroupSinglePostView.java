package com.example.preetisharma.callforblessings.demo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GroupCommentsAdapter;
import com.example.preetisharma.callforblessings.Adapter.GroupSinglePostViewAdapter;
import com.example.preetisharma.callforblessings.Adapter.GroupSinglePostViewCommentsAdapter;
import com.example.preetisharma.callforblessings.HomeActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSingPostCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupPostDetailCommentsModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupPostViewModal;
import com.example.preetisharma.callforblessings.Server.Modal.SinglePostViewModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/21/2017.
 */

public class GroupSinglePostView extends BaseActivity implements APIServerResponse {

    @BindView(R.id.nestedscroll)
    NestedScrollView nestedscroll;

    @BindView(R.id.post_listing)
    RecyclerView post_listing;

    @BindView(R.id.rv_comments)
    RecyclerView rv_comments;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    String postID;

    @BindView(R.id.edt_txt_comment)
    AppCompatEditText edt_txt_comment;
    @BindView(R.id.txtvw_submit_comment)
    AppCompatTextView txtvw_submit_comment;
    boolean keyboardOpen;
    String notificationFlag = "";

    GroupSinglePostViewAdapter groupSinglePostViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_single_post_view_layout);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        txtvw_header_title.setText("Post");
        img_view_change_password.setVisibility(View.GONE);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (notificationFlag != null && !notificationFlag.equalsIgnoreCase("")) {
                    Intent i = new Intent(GroupSinglePostView.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    GroupSinglePostView.this.finish();
                }

            }
        });

        if (getIntent().getExtras() != null) {
            postID = getIntent().getExtras().getString(Constants.GROUP_ID);
            if (getIntent().getExtras().getString("Notification_Flag") != null && !getIntent().getExtras().getString("Notification_Flag").equalsIgnoreCase("")) {
                notificationFlag = getIntent().getExtras().getString("Notification_Flag");
            }
        }

        showLoading();
        ServerAPI.getInstance().groupPostView(APIServerResponse.GROUP_POST_VIEW, getUserSessionId(), postID, this);


    }

    @OnClick(R.id.txtvw_submit_comment)
    public void postComment() {
        hideKeyboard();
        Log.e("data", "Comment" + edt_txt_comment.getText().toString());
        if (edt_txt_comment.getText().toString().trim().length() != 0 || !edt_txt_comment.getText().toString().isEmpty() || !edt_txt_comment.getText().toString().equals("")) {

            if (isConnectedToInternet()) {

                ServerAPI.getInstance().commentOnGroupPost(APIServerResponse.GROUP_POST_COMMENT, getUserSessionId(), postID, edt_txt_comment.getText().toString().trim(), this);
                //ServerAPI.getInstance().comment(APIServerResponse.COMMENT, getUserSessionId(), postID, Constants.POST, edt_txt_comment.getText().toString(), GroupSinglePostView.this);

            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        } else {
            showToast("Enter comment", Toast.LENGTH_SHORT);
        }
    }


    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {

                SinglePostViewModal singlePostViewModal;
                CommentModal comment;
                GroupCommentModal groupCommentModal;
                GroupPostDetailCommentsModal groupPostDetailCommentsModal;
                GroupPostViewModal groupPostViewModal;

                switch (tag) {
                    /*case APIServerResponse.MY_POST:

                        singlePostViewModal = (SinglePostViewModal) response.body();

                        if (singlePostViewModal.getStatus().equalsIgnoreCase("1")) {


                            singlePostViewAdapter = new SinglePostViewAdapter(GroupSinglePostView.this, singlePostViewModal.getDetail());
                            post_listing.setLayoutManager(new LinearLayoutManager(this) {
                                @Override
                                public boolean canScrollVertically() {
                                    return false;
                                }
                            });
                            post_listing.setItemAnimator(new DefaultItemAnimator());
                            post_listing.setAdapter(singlePostViewAdapter);


                            List<SinglePostViewModal.DetailBean.CommentsBean> commentsList = singlePostViewModal.getDetail().getComments();

                            SinglePostViewCommentsAdapter _commentsAdapter = new SinglePostViewCommentsAdapter(this, commentsList);


                            rv_comments.setLayoutManager(new LinearLayoutManager(this) {
                                @Override
                                public boolean canScrollVertically() {
                                    return false;
                                }
                            });
                            rv_comments.setItemAnimator(new DefaultItemAnimator());
                            rv_comments.setAdapter(_commentsAdapter);


                        }


                        hideLoading();
                        break;

                    case APIServerResponse.COMMENT:

                        comment = (CommentModal) response.body();
                        if (comment.getStatus().equals("1")) {
                            showToast("Commented", Toast.LENGTH_SHORT);
                            edt_txt_comment.setText("");
                            ServerAPI.getInstance().getPost(APIServerResponse.COMMENTLIST, getUserSessionId(), postID, Constants.POST, this);
                        }


                        break;

                    case APIServerResponse.COMMENTLIST:


                        singlePostViewModal = (SinglePostViewModal) response.body();

                        singlePostViewAdapter.updateASingleItem(0);

                        List<SinglePostViewModal.DetailBean.CommentsBean> commentsList = singlePostViewModal.getDetail().getComments();

                        SinglePostViewCommentsAdapter _commentsAdapter = new SinglePostViewCommentsAdapter(this, commentsList);


                        rv_comments.setItemAnimator(new DefaultItemAnimator());
                        rv_comments.setAdapter(_commentsAdapter);
                        if (commentsList.size() > 2) {

                            if (!nestedscroll.isSmoothScrollingEnabled()) {
                                nestedscroll.setSmoothScrollingEnabled(true);
                            }

                            nestedscroll.smoothScrollTo(nestedscroll.computeVerticalScrollRange(), nestedscroll.computeVerticalScrollRange());
                            //rv_comments.smoothScrollToPosition(commentsList.size() - 1);
                        }
                        rv_comments.setLayoutManager(new LinearLayoutManager(this) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });

                        break;*/

                    case APIServerResponse.GROUP_POST_COMMENT:

                        groupCommentModal = (GroupCommentModal) response.body();

                        if (groupCommentModal.getStatus().equalsIgnoreCase("1")) {

                            //EventBus.getDefault().post(new EventBusGroupCommentModal(1, groupPostPosition));
                            showToast("Commented", Toast.LENGTH_SHORT);
                            edt_txt_comment.setText("");
                            if (isConnectedToInternet()) {
                                ServerAPI.getInstance().getGroupPostDetailsAndComments(APIServerResponse.GROUP_POST_COMMENT_LIST, getUserSessionId(), postID, this);
                            } else {
                                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                            }


                        }


                        break;

                    case APIServerResponse.GROUP_POST_VIEW:

                        groupPostViewModal = (GroupPostViewModal) response.body();

                        if (groupPostViewModal.getStatus().equalsIgnoreCase("1")) {
                            groupSinglePostViewAdapter = new GroupSinglePostViewAdapter(GroupSinglePostView.this, groupPostViewModal.getDetail());
                            post_listing.setLayoutManager(new LinearLayoutManager(this) {
                                @Override
                                public boolean canScrollVertically() {
                                    return false;
                                }
                            });
                            post_listing.setItemAnimator(new DefaultItemAnimator());
                            post_listing.setAdapter(groupSinglePostViewAdapter);


                            List<GroupPostViewModal.DetailBean.CommentsBean> groupCommentsList = groupPostViewModal.getDetail().getComments();

                            GroupSinglePostViewCommentsAdapter _groupCommentsList = new GroupSinglePostViewCommentsAdapter(this, groupCommentsList);


                            rv_comments.setLayoutManager(new LinearLayoutManager(this) {
                                @Override
                                public boolean canScrollVertically() {
                                    return false;
                                }
                            });
                            rv_comments.setItemAnimator(new DefaultItemAnimator());
                            rv_comments.setAdapter(_groupCommentsList);
                        }


                        hideLoading();
                        break;

                    case APIServerResponse.GROUP_POST_COMMENT_LIST:

                        groupPostDetailCommentsModal = (GroupPostDetailCommentsModal) response.body();
                        if (groupPostDetailCommentsModal.getStatus().equalsIgnoreCase("1")) {

                            if (groupPostDetailCommentsModal.getDetail().getComments().size() > 0) {


                                groupSinglePostViewAdapter.updateASingleItem(0);

                                List<GroupPostDetailCommentsModal.DetailBean.CommentsBean> commentsBeanList = groupPostDetailCommentsModal.getDetail().getComments();
                                GroupCommentsAdapter adapter = new GroupCommentsAdapter(GroupSinglePostView.this, commentsBeanList);
                                rv_comments.setAdapter(adapter);
                                rv_comments.smoothScrollToPosition(commentsBeanList.size());
                                rv_comments.setLayoutManager(new LinearLayoutManager(this) {
                                    @Override
                                    public boolean canScrollVertically() {
                                        return false;
                                    }
                                });
                                rv_comments.setItemAnimator(new DefaultItemAnimator());

                                if (commentsBeanList.size() > 2) {

                                    if (!nestedscroll.isSmoothScrollingEnabled()) {
                                        nestedscroll.setSmoothScrollingEnabled(true);
                                    }

                                    nestedscroll.smoothScrollTo(nestedscroll.computeVerticalScrollRange(), nestedscroll.computeVerticalScrollRange());
                                    //rv_comments.smoothScrollToPosition(commentsList.size() - 1);
                                }

                            }


                        }

                        break;


                }

            }


        } catch (Exception e) {
            hideLoading();
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }


    @Subscribe
    public void onCommentOnPost(EventBusSingPostCommentModal eventBusSingPostCommentModal) {

        if (eventBusSingPostCommentModal.getMessage() == 1) {

            edt_txt_comment.requestFocusFromTouch();
            edt_txt_comment.requestFocus();

            if (keyboardOpen) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_txt_comment.getWindowToken(), 0);
                keyboardOpen = false;
            } else {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(edt_txt_comment, InputMethodManager.SHOW_IMPLICIT);
                keyboardOpen = true;
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (notificationFlag != null && !notificationFlag.equalsIgnoreCase("")) {
            Intent i = new Intent(GroupSinglePostView.this, HomeActivity.class);
            startActivity(i);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
