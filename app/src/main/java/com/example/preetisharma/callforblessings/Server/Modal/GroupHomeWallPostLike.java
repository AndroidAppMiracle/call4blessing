package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/9/2017.
 */

public class GroupHomeWallPostLike {

    /**
     * status : 1
     * liked : NO
     */

    private String status;
    private String liked;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLiked() {
        return liked;
    }

    public void setLiked(String liked) {
        this.liked = liked;
    }
}
