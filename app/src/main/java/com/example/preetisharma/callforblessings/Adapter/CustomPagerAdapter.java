package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicModal;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class CustomPagerAdapter extends PagerAdapter

{
    Context mContext;
    LayoutInflater mLayoutInflater;
    List<JoyMusicModal.TrendingAlbumsBean> joymusic_trending_albums = new ArrayList<>();

    public CustomPagerAdapter(Context context, List<JoyMusicModal.TrendingAlbumsBean> joymusic_trending_albums) {
        mContext = context;
        if (mContext != null) {
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (joymusic_trending_albums != null) {

            this.joymusic_trending_albums = joymusic_trending_albums;
        }
    }

    @Override
    public int getCount() {
        if (joymusic_trending_albums != null) {
            return joymusic_trending_albums.size();
        } else {
            return 0;
        }

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.layout_pager_imageview, container, false);

        final AppCompatImageView imageView = (AppCompatImageView) itemView.findViewById(R.id.imageViewPagerItem);


        if (joymusic_trending_albums != null) {

            Glide.with(mContext).load(joymusic_trending_albums.get(position).getAlbum_cover_image()).placeholder(R.drawable.placeholder_callforblessings).into(imageView);
        }
        container.addView(itemView);
        return itemView;
    }


    private void getDropboxIMGSize(Uri uri) {
        DisplayMetrics metrics = new DisplayMetrics();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
        metrics.widthPixels = options.outWidth;
        metrics.heightPixels = options.outHeight;

        Log.e("height", "height" + metrics.widthPixels + "Width" + metrics.heightPixels);


    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

