package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.TimeLineAdapter;
import com.example.preetisharma.callforblessings.Adapter.TimeLinePaginationAdapterNew;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusHomeWallCommented;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusHomeWallPostCreated;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSubmitPostTimlineModal;
import com.example.preetisharma.callforblessings.Server.Modal.TimeLineModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.listener.PaginationScrollListener;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/30/2017.
 */

public class UserTimelineFragment extends Fragment implements APIServerResponse {
    private RecyclerView mRootView;
    private String userId;
    List<TimeLineModal.ListBean.PostBean> postList;
    TimeLineAdapter _adapter;
    int is_friend;

    private static final int PAGE_NUMBER = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int currentPage = PAGE_NUMBER;
    private int TOTAL_PAGES = 1;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.progressBar)
    CircleProgressBar progressBar;

    private LinearLayoutManager mLayoutManager;
    List<TimeLineModal.ListBean.PostBean> tempList = new ArrayList<>();
    TimeLinePaginationAdapterNew timeLinePaginationAdapterNew;
    SwipeRefreshLayout swipe_refresh;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        mRootView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        swipe_refresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);

        ButterKnife.bind(this, view);
        if (getArguments().getString(Constants.SHARED_PREF_USER_ID) != null) {
            userId = getArguments().getString(Constants.SHARED_PREF_USER_ID);
            is_friend = getArguments().getInt(Constants.IS_FRIEND);
        }

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        timeLinePaginationAdapterNew = new TimeLinePaginationAdapterNew(getActivity());
        mLayoutManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };


        mRootView.setLayoutManager(mLayoutManager);
        mRootView.setNestedScrollingEnabled(false);
        mRootView.setItemAnimator(new DefaultItemAnimator());
        mRootView.setAdapter(timeLinePaginationAdapterNew);

        //mLayoutManager = new LinearLayoutManager(getActivity());


        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().userTimeLine(APIServerResponse.TIMELINE, ((BaseActivity) getActivity()).getUserSessionId(), userId, currentPage, this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    timeLinePaginationAdapterNew.removeAll();
                    currentPage = 1;
                    TOTAL_PAGES = 1;
                    isLastPage = false;
                    ServerAPI.getInstance().userTimeLine(APIServerResponse.TIMELINE, ((BaseActivity) getActivity()).getUserSessionId(), userId, currentPage, UserTimelineFragment.this);
                } else {
                    ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        });


        mRootView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPages();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        return view;
    }

    private void loadNextPages() {

        Log.d("loadNextPage: ", "" + currentPage);

        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {

                //showLoading();
                currentApi(currentPage);


            }
        }

    }

    public void currentApi(int currentPage) {
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().userTimeLine(APIServerResponse.TIMELINE_PAGINATION, ((BaseActivity) getActivity()).getUserSessionId(), userId, currentPage, this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public static Fragment newInstance() {

        return new UserTimelineFragment();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            TimeLineModal timeLine;

            switch (tag) {


                /*case APIServerResponse.TIMELINE:

                    timeLine = (TimeLineModal) response.body();
                    if (timeLine.getStatus().equals("1")) {

                        postList = timeLine.getList().getPost();
                        if (postList.size() > 0) {
                            tempList.addAll(postList);
                            if (is_friend > 4) {
                                ((BaseActivity) getActivity()).setUserImage(timeLine.getList().getProfile_details().getProfile_pic());
                            }


                            scrollView.setOnScrollChangeListener(new EndlessParentScrollListener(mLayoutManager) {
                                @Override
                                public boolean onLoadMore(int page, int totalItemsCount) {

                                    progressBar.setVisibility(View.VISIBLE);
                                    currentPage++;
                                    currentApi(currentPage);
                                    return true;
                                }
                            });
                            mRootView.setNestedScrollingEnabled(false);
                            mRootView.setHasFixedSize(false);
                            mLayoutManager.setAutoMeasureEnabled(true);
                            mRootView.setItemAnimator(new DefaultItemAnimator());
                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRootView.getContext(),
                                    mLayoutManager.getOrientation());
                            mRootView.addItemDecoration(dividerItemDecoration);
                            _adapter = new TimeLineAdapter(getActivity(), postList);
                            mRootView.setAdapter(_adapter);
                            _adapter.notifyDataSetChanged();
                            ((BaseActivity) getActivity()).hideLoading();
                        } else {
                            ((BaseActivity) getActivity()).hideLoading();
                            ((BaseActivity) getActivity()).showToast("No posts found", Toast.LENGTH_SHORT);
                        }
                    }
                    break;*/

                case APIServerResponse.TIMELINE:


                    timeLine = (TimeLineModal) response.body();
                    if (swipe_refresh.isRefreshing()) {
                        swipe_refresh.setRefreshing(false);
                    }
                    if (timeLine.getStatus().equalsIgnoreCase("1")) {


                        //TOTAL_PAGES = joyMusicViewAllSongsModal.getTotalPages();
                          /*  if (adapter != null && adapter.getItemCount() < demoHomeModal.getPosts().size() && isLastPage) {

                                adapter.add(demoHomeModal.getPosts().get(0));
                                adapter.notifyItemInserted(0);

                            } else {*/
                        if (timeLine.getList().getPost().size() != 0) {

                            if (is_friend > 4) {
                                ((BaseActivity) getActivity()).setUserImage(timeLine.getList().getProfile_details().getProfile_pic());
                            }

                            TOTAL_PAGES = Integer.parseInt(timeLine.getList().getTotalPages());
                            //currentPage = Integer.parseInt(demoHomeModal.getCurrentPage());
                            List<TimeLineModal.ListBean.PostBean> listBeen = timeLine.getList().getPost();
                            timeLinePaginationAdapterNew.addAll(listBeen);
                            if (currentPage < TOTAL_PAGES)
                                timeLinePaginationAdapterNew.addLoadingFooter();
                            else isLastPage = true;


                        } else {
                            //TOTAL_PAGES = currentPage;
                            ((BaseActivity) getActivity()).hideLoading();
                            ((BaseActivity) getActivity()).showToast("No posts found", Toast.LENGTH_SHORT);
                        }
                          /*  }*/


                        //rv_viewAllList.setAdapter(adapter);


                    }

                    ((BaseActivity) getActivity()).hideLoading();
                    break;

                case TIMELINE_PAGINATION:

                    timeLine = (TimeLineModal) response.body();
                    if (timeLine.getStatus().equalsIgnoreCase("1")) {

                        //TOTAL_PAGES = joyMusicViewAllAlbumModal.getTotalPages();

                           /* if (adapter != null && adapter.getItemCount() < demoHomeModal.getPosts().size() && isLastPage) {

                                adapter.add(demoHomeModal.getPosts().get(0));
                                adapter.notifyItemInserted(0);

                            } else {*/
                        if (timeLine.getList().getPost().size() != 0) {

                            TOTAL_PAGES = Integer.parseInt(timeLine.getList().getTotalPages());
                            //currentPage = Integer.parseInt(demoHomeModal.getCurrentPage());
                            timeLinePaginationAdapterNew.removeLoadingFooter();
                            isLoading = false;
                            List<TimeLineModal.ListBean.PostBean> list = timeLine.getList().getPost();
                            timeLinePaginationAdapterNew.addAll(list);
                            if (currentPage != TOTAL_PAGES)
                                timeLinePaginationAdapterNew.addLoadingFooter();
                            else isLastPage = true;
                        } else {
                            //TOTAL_PAGES = currentPage;
                            ((BaseActivity) getActivity()).hideLoading();
                            ((BaseActivity) getActivity()).showToast("No more posts.", Toast.LENGTH_SHORT);
                        }
                           /* }*/


                    }


                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
        ((BaseActivity) getActivity()).hideLoading();

    }

    @Override
    public void onResume() {
        super.onResume();
        currentApi(currentPage);

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();

    }

    @Subscribe
    public void onSharePost(DemoShareModal share) {
        currentApi(currentPage);
    }

    @Subscribe
    public void onUserPost(EventBusSubmitPostTimlineModal post) {
        currentApi(currentPage);
    }

    @Subscribe
    public void onHomeWallPostCreated(EventBusHomeWallPostCreated eventBusHomeWallPostCreated) {

        try {

            Log.i("Post", "Before IF");

            if (eventBusHomeWallPostCreated.getIsNewPostCreated().equalsIgnoreCase("1")) {

                Log.i("Post", "After IF");

                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    timeLinePaginationAdapterNew.removeAll();
                    currentPage = 1;
                    TOTAL_PAGES = 1;
                    isLastPage = false;
                    ServerAPI.getInstance().userTimeLine(APIServerResponse.TIMELINE, ((BaseActivity) getActivity()).getUserSessionId(), userId, currentPage, UserTimelineFragment.this);
                } else {
                    ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Subscribe
    public void onCommentPost(EventBusHomeWallCommented eventBusHomeWallCommented) {
        try {

            if (eventBusHomeWallCommented.getStatus() == 1) {
                timeLinePaginationAdapterNew.updateASingleItem(eventBusHomeWallCommented.getItemPosition());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}