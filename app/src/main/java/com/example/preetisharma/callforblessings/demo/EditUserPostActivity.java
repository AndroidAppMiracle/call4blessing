package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EditUserPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusEditPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.FilterModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/1/2017.
 */

public class EditUserPostActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks, ImagePickerCallback, APIServerResponse {

    AppCompatImageView img_vw_user_profile, viewholder_image, img_view_back, img_view_change_password;
    AppCompatTextView txtvw_user_name;
    AppCompatEditText edt_txt_post_details;
    AppCompatTextView txtvw_add_to_your_post, txtvw_header_title;
    @BindView(R.id.img_vw_tag_friends)
    AppCompatImageView img_vw_tag_friends;
    private static final int RC_CAMERA_PERM = 342;
    private static final int RC_GALLERY_PERM = 545;
    public int responseCode = 101;
    String username, userImage, postText, postImage, postID;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private String pickerPath = "";
    Toolbar toolbar;
    private String picChange;
    @BindView(R.id.lnr_add_to_your_post)
    LinearLayout lnr_add_to_your_post;
    ArrayList<FilterModal> arrayListTagFriends = new ArrayList<>();
    @BindView(R.id.txtvw_tagged_friends)
    AppCompatTextView txtvw_tagged_friends;
    String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_post_edit);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        img_vw_user_profile = (AppCompatImageView) findViewById(R.id.img_vw_user_profile);
        txtvw_user_name = (AppCompatTextView) findViewById(R.id.txtvw_user_name);
        edt_txt_post_details = (AppCompatEditText) findViewById(R.id.edt_txt_post_details);
        txtvw_add_to_your_post = (AppCompatTextView) findViewById(R.id.txtvw_add_to_your_post);
        viewholder_image = (AppCompatImageView) findViewById(R.id.viewholder_image);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setVisibility(View.GONE);
        lnr_add_to_your_post = (LinearLayout) findViewById(R.id.lnr_add_to_your_post);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        txtvw_header_title.setText("Edit Post");
        showLoading();

        if (getIntent().getExtras() != null) {
            username = getIntent().getExtras().getString(Constants.USER_NAME);
            userImage = getIntent().getExtras().getString(Constants.USER_IMAGE);
            postText = getIntent().getExtras().getString(Constants.POST_TEXT);
            postID = getIntent().getExtras().getString(Constants.POSTID);
            // postImage = getIntent().getExtras().getString(Constants.POSTIMAGE);
            picChange = getIntent().getExtras().getString(Constants.PIC_CHANGE);
        }

        if (getIntent().getExtras().getString(Constants.POST_IMAGE) != null) {
            postImage = getIntent().getExtras().getString(Constants.POST_IMAGE);
        } else {
            viewholder_image.setVisibility(View.GONE);
        }


        //Glide.with(EditUserPostActivity.this).load(R.drawable.ic_send_post).thumbnail(0.1f).into(img_view_change_password);

        try {
            setSupportActionBar(toolbar);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            img_view_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditUserPostActivity.this.finish();
                }
            });


           /* img_view_change_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (isConnectedToInternet()) {
                            if (!pickerPath.equals("") && !txtvw_post_details.getText().toString().equals("")) {
                                if (!pickerPath.equals("")) {
                                    ServerAPI.getInstance().editUserPost(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), txtvw_post_details.getText().toString(), pickerPath, "IMAGE", "WALL", EditUserPostActivity.this);
                                } else {
                                    ServerAPI.getInstance().editUserPostWithoutImage(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), txtvw_post_details.getText().toString(), "WALL", EditUserPostActivity.this);

                                }
                            } else {
                                showToast("Hello", Toast.LENGTH_SHORT);
                            }


                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        txtvw_user_name.setText(username);
        if (postImage != null && postImage != "") {
            viewholder_image.setVisibility(View.VISIBLE);
            Glide.with(EditUserPostActivity.this).load(postImage).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(viewholder_image);
            if (picChange.equalsIgnoreCase(Constants.COVER_PIC) || picChange.equalsIgnoreCase(Constants.PROFILE_PIC)) {
                //viewholder_image.setVisibility(View.VISIBLE);
                lnr_add_to_your_post.setVisibility(View.GONE);
            } else {
                lnr_add_to_your_post.setVisibility(View.VISIBLE);
            }
        } else if (pickerPath != null && pickerPath != "") {
            viewholder_image.setVisibility(View.VISIBLE);
            Glide.with(EditUserPostActivity.this).load(pickerPath).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(viewholder_image);

        } else {
            viewholder_image.setVisibility(View.GONE);
        }

        Glide.with(EditUserPostActivity.this).load(userImage).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(img_vw_user_profile);

        edt_txt_post_details.setText(postText);


        txtvw_add_to_your_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(EditUserPostActivity.this)

                        .setTitle("Select Image")
                        .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                takePicture();
                            }
                        })
                        .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                pickImageSingle();
                            }
                        })
                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            }
        });

        hideLoading();
    }

    @OnClick(R.id.img_vw_upload_image)
    public void uploadImage() {
        new AlertDialog.Builder(this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    @OnClick(R.id.img_vw_tag_friends)
    public void TagFriend() {
        Intent tagIntent = new Intent(this, TagFriendActivity.class);
        startActivityForResult(tagIntent, responseCode, new Bundle());
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(EditUserPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(EditUserPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public void taggedFriendsArrayResult() {
        arrayListTagFriends = FilterModal.getFriendsArray();
        if (arrayListTagFriends != null && arrayListTagFriends.size() > 0) {

            txtvw_tagged_friends.setVisibility(View.VISIBLE);
            if (arrayListTagFriends.size() == 1) {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getLastname());
            } else if (arrayListTagFriends.size() == 2) {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname() + " and " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname());

            } else {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + (arrayListTagFriends.size() - 1) + " others");

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            arrayListTagFriends = FilterModal.getFriendsArray();
            if (arrayListTagFriends != null && arrayListTagFriends.size() > 0) {

                txtvw_tagged_friends.setVisibility(View.VISIBLE);
                if (arrayListTagFriends.size() == 1) {
                    txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getLastname());
                } else if (arrayListTagFriends.size() == 2) {
                    txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname() + " and " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname());

                } else {
                    txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + (arrayListTagFriends.size() - 1) + " others");
                }
            }
            if (resultCode == Activity.RESULT_OK) {
                taggedFriendsArrayResult();

                if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(this);
                    }
                    imagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                }
            } else

            {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            viewholder_image.setVisibility(View.VISIBLE);
            viewholder_image.setImageURI(Uri.parse(pickerPath));
            viewholder_image.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
            Intent intent = intentBuilder.build(EditUserPostActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {
                EditUserPostModal editUserPostModal;
                hideLoading();
                switch (tag) {
                    case APIServerResponse.EDIT_POST:
                        editUserPostModal = (EditUserPostModal) response.body();
                        EventBus.getDefault().post(new EventBusEditPostModal(editUserPostModal.getStatus()));
                        if (editUserPostModal.getStatus().equals("1")) {
                            EventBus.getDefault().post(new EventBusEditPostModal(editUserPostModal.getStatus()));

                            showToast(editUserPostModal.getMessage(), Toast.LENGTH_SHORT);
                            finish();
                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_send, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.menu_send_item) {
            try {
                if (isConnectedToInternet()) {
                    hideKeyboard();
                    if (arrayListTagFriends != null && arrayListTagFriends.size() >= 0) {
                        for (int i = 0; i < arrayListTagFriends.size(); i++) {
                            if (i == 0) {
                                userId = String.valueOf(arrayListTagFriends.get(i).getList().getUser_info().getId());
                            } else {
                                userId += "," + String.valueOf(arrayListTagFriends.get(i).getList().getUser_info().getId());
                            }
                        }
                    }
                    Log.e("User id for tagging", "Tagged users" + userId);
                    if (!pickerPath.equals("") || !edt_txt_post_details.getText().toString().equals("")) {
                        if (!pickerPath.equalsIgnoreCase("")) {
                            showLoading();
                            ServerAPI.getInstance().editUserPost(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), edt_txt_post_details.getText().toString(), pickerPath, "IMAGE", "WALL", userId, EditUserPostActivity.this);
                        } else {
                            showLoading();
                            ServerAPI.getInstance().editUserPostWithoutImage(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), edt_txt_post_details.getText().toString(), "WALL", userId, EditUserPostActivity.this);
                        }
                    }

                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
