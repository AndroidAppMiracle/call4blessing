package com.example.preetisharma.callforblessings;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.callforblessings.R;
import com.crashlytics.android.Crashlytics;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import io.fabric.sdk.android.Fabric;

import java.util.List;

/**
 * Created by preeti.sharma on 12/29/2016.
 */

public class SplashActivity extends BaseActivity {
    boolean backpress = false;
    private final Handler handler = new Handler();
    private Runnable myRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        updateStatusBar();
        getmPrefs();
        /**
         * Sending User to the Main Page so that he/she can get Navigated
         */
        if (isAppOnForeground(getApplicationContext(), "com.callforblessings")) {
            if (getIntent().getExtras() != null) {
                Log.e("notification data", "notification data" + getIntent().getExtras().getString("data"));
            }
        }


        /*Thread th = new Thread(new Runnable() {

            @Override
            public void run() {
// TODO Auto-generated method stub
                try {
                    *//*Thread.sleep(2000);
                    if (!backpress) {
                        if (getUserLoggedIn()) {

                            Intent gotoMain = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(gotoMain);
                        } else {
                            Intent gotoMain = new Intent(SplashActivity.this, LandingScreenActivity.class);
                            startActivity(gotoMain);
                        }
                    }*//*


                    myRunnable = new Runnable() {
                        @Override
                        public void run() {

                            if (isConnectedToInternet()) {

                                if (!backpress) {
                                    if (getUserLoggedIn()) {

                                        Intent gotoMain = new Intent(SplashActivity.this, HomeActivity.class);
                                        startActivity(gotoMain);
                                    } else {
                                        Intent gotoMain = new Intent(SplashActivity.this, LandingScreenActivity.class);
                                        startActivity(gotoMain);
                                    }
                                }

                            }

                        }
                    };
                    handler.postDelayed(myRunnable, 3000);


                } catch (Exception e) {
// TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        th.start();*/


        if (isConnectedToInternet()) {

            if (!backpress) {
                if (getUserLoggedIn()) {

                    Intent gotoMain = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(gotoMain);
                } else {
                    Intent gotoMain = new Intent(SplashActivity.this, LandingScreenActivity.class);
                    startActivity(gotoMain);
                }
            }

        }

       /* myRunnable = new Runnable() {
            @Override
            public void run() {

                if (isConnectedToInternet()) {

                    if (!backpress) {
                        if (getUserLoggedIn()) {

                            Intent gotoMain = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(gotoMain);
                        } else {
                            Intent gotoMain = new Intent(SplashActivity.this, LandingScreenActivity.class);
                            startActivity(gotoMain);
                        }
                    }

                }

            }
        };
        handler.postDelayed(myRunnable, 3000);*/


    }

    @Override
    public void onBackPressed() {
// TODO Auto-generated method stub
        super.onBackPressed();
        handler.removeCallbacks(myRunnable);
        backpress = true;
    }


    public void updateStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorStatusBar));
        }
    }

    private boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                //                Log.e("app",appPackageName);
                return true;
            }
        }
        return false;
    }


}
