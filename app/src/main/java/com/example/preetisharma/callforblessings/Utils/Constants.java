package com.example.preetisharma.callforblessings.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by preeti.sharma on 12/30/2016.
 */

public class Constants {
    public static final String BOOK_TITLE_NOT_BLANK = "Book title should not be blank.";
    public static final String BOOK_AUTHOR_NOT_BLANK = "Book title should not be blank.";
    public static final String DEVICE_ID = "Device_ID";
    public static final String FIRST_NAMENOT_BLANK = "First name should not be blank.";
    public static final String GROUP_NAME_START = "Name should not starts with numeric character.";
    public static final String GROUP_NAMENOT_BLANK = "Group name should not be blank.";
    public static final String FIRST_NAME_START = "Name should not starts with numeric character.";
    public static final String EMAIL_START = "Email should not starts with numeric character.";
    public static final String LAST_NAMENOT_BLANK = "Last name should not be blank.";
    public static final String PHONE_NUMBER_NOT_BLANK = "Phone Number should not be blank.";
    public static final String PHONE_NUMBER_LENGTH = "Phone number Length greater than 10.";
    public static final String EMAIL_NOT_BLANK = "Email Id should not be blank.";
    public static final String ALBUM_DESC_NOT_BLANK = "Album description should not be blank.";
    public static final String ALBUM_NAME_NOT_BLENK = "Album name should not be blank.";
    public static final String OTP_NOT_BLANK = "OTP should not be blank.";
    public static final String ALBUM_NAME_NOT_VALID = "Please enter alteat 4 charaters";
    public static final String ALBUM_DESC_NOT_VALID = "Please enter alteat 4 charaters";
    public static final String EMAIL_NOT_VALID = "Enter Valid Email Id.";
    public static final String PASSWORD_NOT_BLANK = "Password should not be blank.";
    public static final String PASSWORD_LENGTH = "Password Length should be equals to or greater than 6.";
    public static final String SHARED_PREF_NAME = "CallForBlessingsShared";
    public static final String SHARED_PREF_USER_ID = "CallForBlessingsSharedUserID";
    public static final String IS_FRIEND = "is_friend";
    public static final String SHARED_PREF_USER_SESSION_ID = "CallForBlessingsSharedUserSessionID";
    public static final String FCM_TOKEN = "token";
    public static final String INTERNET_CONNECTION = "Please check your internet connection.";
    public static final String SHARED_PREF_USER_TYPE = "CallForBlessingsUserType";
    public static final String COMMENTS = "COMMENTS";
    public static final String POSTTYPE = "postType";
    public static final String WALL = "WALL";
    public static final String USERWALL = "USERWALL";
    public static final String SIMSTATUS = "Sim not found";
    public static final String USER_TYPE_SELLER = "seller";
    public static final String SHARED_PREF_USER_ADDRESS = "userSetAddress";
    public static final String SHARED_PREF_USER_LONG = "userSetLong";
    public static final String SHARED_PREF_USER_LAT = "userSetLat";
    public static final String SHARED_PREF_FIRST_NAME = "userfirstname";
    public static final String SHARED_PREF_LAST_NAME = "userlastname";
    public static final String SHARED_PREF_FULL_NAME = "userfullname";
    public static final String SHARED_PREF_USER_COVER_IMAGE = "usercoverimage";
    public static final String SHARED_PREF_USER_IMAGE = "userimage";
    public static final String SHARED_PREF_USER_PHONE_NUMBER = "userPhoneNumber";
    public static final String SHARED_PREF_USER_EMAIL_ID = "userEmailID";
    public static final String PRAYER_NAMENOT_BLANK = "Please enter name";
    public static final String PRAYER_TITLE = "Please enter title";
    public static final String DOWNLOAD_BOOK = "Book will be available shortly.";
    public static final String PRAYER = "PRAYER";
    public static final String BOOK = "book";
    public static final String TESTIMONY = "TESTIMONY";
    public static final String HELP_REQUEST = "HELP_REQUEST";
    public static final String POST = "POST";
    public static final String EVENT_ID = "event_id";
    public static final String EVENT_TYPE_FLAG = "event_type";
    public static final String EVENT_INVITE_STATUS = "event_invite_status";
    public static final String TIMELINE_ENABLED = "timeline_enable";
    public static final String POSTIMAGE = "postImage";
    public static final String POSTVIDEO = "postVideo";
    public static final String POSTTHUMBNAIL = "thumbnail";
    public static final String POSTID = "postId";
    public static final String ALBUMID = "albumId";
    public static final String SONG_ID = "song_id";
    public static final String SONG_NAME = "song_name";
    public static final String POSTTEXT = "postText";
    public static final String TEXT = "TEXT";
    public static final String POSTTIMESTAMP = "postTimeStamp";
    public static final String POSITION = "position";
    public static final String BOOK_ID = "book_id";
    public static final String BOOK_DESC = "book_desc";
    public static final String BOOK_DELIVER = "Book will be delievered to you within next 7 working days.";
    public static final String BOOK_BY_YOU = "Book uploaded by you.";
    public static final String USER_NAME = "username";
    public static final String GENDER = "gender";
    public static final String USER_IMAGE = "user_image";
    public static final String POST_TEXT = "post_text";
    public static final String POST_IMAGE = "post_image";
    public static final String FRAGMENT_NAME = "fragment_name";
    public static final String PROFILE_PIC = "PROFILE_PIC";
    public static final String COVER_PIC = "COVER_PIC";
    public static final String ALBUM_COVER = "album_cover";
    public static final String ALBUM_NAME = "album_name";
    public static final String PIC_CHANGE = "pic_change";
    public static final String PROFILE_PREVIEW = "profile_preview";
    public static final String COVER_PREVIEW = "profile_preview";
    public static final String TYPE = "type";
    public static final String SONG_DOWNLOAD_URL = "song_download_url";

    public static final String CHANGED_PROFILE_PIC = "changed profile picture.";
    public static final String CHANGED_COVER_PIC = "changed cover picture.";
    public static final String IMAGE = "IMAGE";
    public static final String IMAGE_SLIDESHOW = "image_slideshow";
    public static final String CURRENT_POSITION = "current_position";
    public static final String IMAGE_BUNDLE = "image_bundle";

    public static final String MALE = "MALE";

    public static final String FEMALE = "FEMALE";
    public static final String SHARE = "SHARE";
    public static final String SHARED = "shared";
    public static final String TAGGEDFRIENDS = "taggedfriend";
    public static final String TRANSACTIONSTATUS = "status";
    public static final String TRANSACTIONID = "transactionId";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "fail";
    public static int USER_NOT_FRIEND = 1;
    public static int USER_REQUEST_COME = 2;
    public static int USER_REQUEST_SEND = 3;
    public static int USER_REQUEST_ACCEPT = 4;
    public static int USER_SELFTIMELINE = 5;
    public static final String AMOUNT = "book_amount";
    public static final String VIDEO = "VIDEO";
    public static final String POST_COMMENT = "POST_COMMENT";
    public static final String POST_LIKE = "POST_LIKE";
    public static final String POST_SHARE = "POST_SHARE";
    public static final String EVENT_INVITE = "EVENT_INVITE";
    public static final String FRIEND_REQUEST = "FRIEND_REQUEST";
    public static final String LANGUAGE_ID = "language_id";
    public static final String LANGUAGE_NAME = "language_name";
    public static final String PAGE_NUMBER = "page_number";
    public static final String VIEW_ALL_FLAG = "view_all_flag";
    public static final String SONG = "song";
    public static final String ALBUM = "album";
    public static final String MUSIC = "music";
    public static final String SONGS_LIST = "songs_list";
    public static final String ALBUM_SONGS_LIST = "album_songs_list";

    public static final String LIKED = "Liked";
    public static final String NOT_LIKED = "Not Liked";

    public static final String IS_DOWNLOADED = "is_downloaded";

    public static final String PLAYLIST_ID_VIEW_ALL = "playlist_id_view_all";

    public static final String SONG_CAPS = "SONG";

    public static final String BUY_MUSIC_ITEM_FLAG = "item_flag";
    public static final String BUY_MUSIC_ITEM_ID = "item_id";
    public static final String BUY_MUSIC_ITEM_NAME = "item_name";
    public static final String BUY_MUSIC_ITEM_AMOUNT = "item_amount";

    public static final String BUY_JOY_MUSIC_SUBSCRIPTION_ID = "subscription_id";
    public static final String BUY_JOY_MUSIC_SUBSCRIPTION_NAME = "subscription_name";
    public static final String BUY_JOY_MUSIC_SUBSCRIPTION_AMOUNT = "subscription_amount";


    public static final String DONATION = "donation";

    public static final String PAID = "PAID";
    public static final String FREE = "FREE";
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    public static final String GROUP_ID = "group_id";
    public static final String GROUP_POST_ID = "group_post_id";
    public static final String GROUP_FUNCTION = "group_function";
    public static final String GROUP_CREATE = "group_create";
    public static final String GROUP_UPDATE = "group_update";
    public static final String GROUP_NAME = "group_name";
    public static final String GROUP_DESC = "group_desc";
    public static final String GROUP_MEMBERS = "group_members";
    public static final String GROUP_PIC = "group_pic";

    public static final String SETTING_ID = "setting_id";
    public static final String SETTING_VALUE = "setting_value";

    public static final String PAYU_URL = "http://www.call4blessing.com/payment/callback";

    public static final String NO_ALBUM_COVER_URL = "http://www.sylviacuenca.com/images/no_album_art.jpg";

    public static final String PUBLIC = "Public";
    public static final String FRIENDS = "Friends";
    public static final String FRIEND = "FRIEND";
    public static final String ONLY_ME = "Only me";
    public static final String ONLY_ME_SETTING = "ONLYME";
    public static final String FRIEND_REQUEST_SETTING = "FRIEND REQUEST";
    public static final String MESSAGE = "MESSAGE";
    public static final String ABOUT_INFO = "ABOUT INFO";

    public static final String FRIEND_ID = "friend_id";

    public static final String PRIVACY_WALL = "privacy_wall";
    public static final String PRIVACY_ABOUT_INFO = "privacy_about_info";
    public static final String PRIVACY_FRIEND_REQUEST = "privacy_friend_request";
    public static final String PRIVACY_MESSAGE = "privacy_message";

    public static final String PLAYLIST_ID = "playlist_id";


    public static final void setAppFont(ViewGroup mContainer, Context context) {
        final Typeface mFont = Typeface.createFromAsset(context.getAssets(),
                "font_titillium.ttf");
        if (mContainer == null || mFont == null)
            return;
        final int mCount = mContainer.getChildCount();
        // Loop through all of the children.
        for (int i = 0; i < mCount; ++i) {
            final View mChild = mContainer.getChildAt(i);
            if (mChild instanceof TextView) {
                // Set the font if it is a TextView.
                ((TextView) mChild).setTypeface(mFont);
            } else if (mChild instanceof ViewGroup) {
                // Recursively attempt another ViewGroup.
                setAppFont((ViewGroup) mChild, context);
            }
        }
    }

    public static final void setTypeFont(TextView textView, Context context) {
        Typeface mFont = Typeface.createFromAsset(context.getAssets(),
                "font_titillium.TTF");
        textView.setTypeface(mFont);
    }

}
