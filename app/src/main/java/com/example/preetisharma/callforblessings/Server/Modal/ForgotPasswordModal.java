package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 1/17/2017.
 */

public class ForgotPasswordModal {

    /**
     * status : 1
     * message : Recovery message sent
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
