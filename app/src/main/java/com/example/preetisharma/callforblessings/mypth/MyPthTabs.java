package com.example.preetisharma.callforblessings.mypth;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.MyPthTabsAdapter;
import com.example.preetisharma.callforblessings.HelpRequest;
import com.example.preetisharma.callforblessings.LandingScreenActivity;
import com.example.preetisharma.callforblessings.RequestActivity;
import com.example.preetisharma.callforblessings.TestimonyRequestActivity;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.OnClick;

/**
 * Created by satoti.garg on 8/1/2017.
 */

public class MyPthTabs extends BaseActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    ViewPager viewPagerPhotos;
    AppCompatTextView txtvw_header_title;
    AppCompatImageView img_view_back;
    AppCompatImageView img_view_change_password;
    MyPthTabsAdapter myPthTabsAdapter;

    FloatingActionButton floatingActionButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pth);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_mypth);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setVisibility(View.GONE);
        img_view_change_password.setImageResource(R.drawable.ic_add);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        floatingActionButton=(FloatingActionButton)findViewById(R.id.fab);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPthTabs.this.finish();
            }
        });
        txtvw_header_title.setText("My PTH");

        tabLayout.addTab(tabLayout.newTab().setText("Prayers"));
        tabLayout.addTab(tabLayout.newTab().setText("Testimony"));
        tabLayout.addTab(tabLayout.newTab().setText("Help Request"));

        viewPagerPhotos = (ViewPager) findViewById(R.id.viewPagerPth);

        myPthTabsAdapter = new MyPthTabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPagerPhotos.setAdapter(myPthTabsAdapter);
        viewPagerPhotos.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPagerPhotos.setOffscreenPageLimit(0);
        tabLayout.addOnTabSelectedListener(this);
        viewPagerPhotos.addOnPageChangeListener(this);

        if (tabLayout.getSelectedTabPosition() == 0) {
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent signUpIntent = new Intent(MyPthTabs.this, RequestActivity.class);
                    startActivity(signUpIntent);

                }
            });
        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPagerPhotos.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {


        if (position == 0) {

            img_view_change_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent signUpIntent = new Intent(MyPthTabs.this, RequestActivity.class);
                    startActivity(signUpIntent);

                }
            });


        } else if (position == 1) {

            img_view_change_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent signUpIntent = new Intent(MyPthTabs.this, TestimonyRequestActivity.class);
                    startActivity(signUpIntent);
                }
            });


        } else if (position == 2) {

            img_view_change_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent signUpIntent = new Intent(MyPthTabs.this, HelpRequest.class);
                    startActivity(signUpIntent);
                }
            });


        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
