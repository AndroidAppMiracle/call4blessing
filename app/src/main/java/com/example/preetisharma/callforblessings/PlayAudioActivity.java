package com.example.preetisharma.callforblessings;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by preeti.sharma on 1/9/2017.
 */

public class PlayAudioActivity extends BaseActivity {

    @BindView(R.id.txtvw_play)
    AppCompatImageView txtvw_play;
    @BindView(R.id.seekbar_layout)
    AppCompatSeekBar seekBar;
    MediaPlayer player;
    Handler seekHandler = new Handler();
    boolean playEnabled = false;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    private Handler mHandler = new Handler();
    @BindView(R.id.txtvw_start_duration)
    AppCompatTextView txtvw_start_duration;
    @BindView(R.id.txtvw_end_duration)
    AppCompatTextView txtvw_end_duration;
    @BindView(R.id.txtvw_next_play)
    AppCompatTextView txtvw_next_play;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_audio);
        updateStatusBar();


        ButterKnife.bind(this);
        txtvw_header_title.setText("I Commit");
     /*   img_view_change_password.setImageResource(R.drawable.ic_add);*/
        img_view_change_password.setVisibility(View.GONE);
        player = MediaPlayer.create(this, R.raw.callaudio);
        txtvw_next_play.setEnabled(true);
        txtvw_next_play.setClickable(true);

        // showLoading();
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            if (player != null) {
                long totalDuration = player.getDuration();
                long currentDuration = player.getCurrentPosition();
                if (currentDuration <= totalDuration) {
                    // Displaying Total Duration time
                    txtvw_end_duration.setText("" + Utilities.milliSecondsToTimer(totalDuration));
                    // Displaying time completed playing
                    txtvw_start_duration.setText("" + Utilities.milliSecondsToTimer(currentDuration));

                    // Updating progress bar
                    int progress = (int) (Utilities.getProgressPercentage(currentDuration, totalDuration));
                    //Log.d("Progress", ""+progress);
                    seekBar.setProgress(progress);

                    // Running this thread after 100 milliseconds
                    mHandler.postDelayed(this, 100);
                }
            }
        }
    };

    Runnable run = new Runnable() {

        @Override
        public void run() {
            seekUpdation();
        }
    };

    public void seekUpdation() {

        seekBar.setProgress(player.getCurrentPosition());
        seekHandler.postDelayed(run, 1000);
    }

    @OnClick(R.id.txtvw_next_play)
    void moveNext() {
        if (!playEnabled) {
            showToast("Please wait till the audio ends.", Toast.LENGTH_SHORT);
        } else {
            player.stop();
            openConfirmationDialog();
        }


    }

    @OnClick(R.id.txtvw_play)
    void playAudio() {
        //.setText("Playing...");
        if (playEnabled) {
            AlertDialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage("You have already listened once, do you want to listen again? ");
            // Add the buttons
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    playEnabled = false;
                    updateProgressBar();
                    if (player.isPlaying()) {
                        if (player != null) {
                            player.pause();
                            // Changing button image to play button
                            txtvw_play.setImageResource(R.drawable.ic_play);

                        }
                    } else {
                        // Resume song
                        if (player != null) {
                            player.start();
                            // Changing button image to pause button
                            txtvw_play.setImageResource(R.drawable.pause_icon);
                        }
                    }
                    dialog.dismiss();
                    /*finish();*/
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                    dialog.dismiss();
                }
            });
            // Create the AlertDialog
            dialog = builder.create();
            dialog.show();
        } else {

            if (player.isPlaying()) {
                if (player != null) {
                    player.pause();
                    // Changing button image to play button
                    txtvw_play.setImageResource(R.drawable.ic_play);

                }
            } else {
                // Resume song
                if (player != null) {
                    player.start();
                    // Changing button image to pause button
                    txtvw_play.setImageResource(R.drawable.pause_icon);
                }
            }


        }
        updateProgressBar();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                playEnabled = true;
                txtvw_play.setImageResource(R.drawable.ic_play);
                txtvw_next_play.setEnabled(true);
                txtvw_next_play.setClickable(true);

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.release();
            player = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.release();
            player = null;
        }
    }


    public void openConfirmationDialog() {
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_box_terms);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        AppCompatTextView txtvw_yes = (AppCompatTextView) dialog.findViewById(R.id.txtvw_yes);
        AppCompatTextView txtvw_no = (AppCompatTextView) dialog.findViewById(R.id.txtvw_no);
        txtvw_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PlayAudioActivity.this, OtpRequestActivity.class);
                startActivity(intent);
                finish();

            }
        });
        txtvw_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}
