package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/23/2017.
 */

public class ShareDialogActivity extends BaseActivity implements APIServerResponse {
    @BindView(R.id.img_vw_profile_share)
    AppCompatImageView img_vw_profile_share;
    @BindView(R.id.txtvw_user_name_share)
    AppCompatTextView txtvw_user_name_share;
    @BindView(R.id.txtvw_timestamp_share)
    AppCompatTextView txtvw_timestamp_share;
    @BindView(R.id.txtvw_shared_post)
    AppCompatTextView txtvw_shared_post;
    @BindView(R.id.share_image)
    AppCompatImageView share_image;
    @BindView(R.id.edt_txt_shared)
    AppCompatEditText edt_txt_shared;
    @BindView(R.id.txtvw_cancel)
    AppCompatTextView txtvw_cancel;
    @BindView(R.id.txtvw_share)
    AppCompatTextView txtvw_share;
    private String user_post_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(Window.FEATURE_NO_TITLE, Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_box_share);
        updateStatusBar();
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            share_image.setVisibility(View.GONE);
            txtvw_user_name_share.setText(getFullName());
            user_post_id = getIntent().getExtras().getString(Constants.POSTID);
            if (getIntent().getExtras().getString(Constants.POSTTIMESTAMP) != null) {
                txtvw_timestamp_share.setText(getIntent().getExtras().getString(Constants.POSTTIMESTAMP));
            } else {
                txtvw_timestamp_share.setVisibility(View.GONE);
            }
            Glide.with(this).load(getUserImage()).placeholder(R.drawable.ic_me).into(img_vw_profile_share);
            if (getIntent().getExtras().getString(Constants.POSTTEXT) != null) {
                txtvw_shared_post.setText(stripText(getIntent().getExtras().getString(Constants.POSTTEXT)));
            }
            if (getIntent().getExtras().getString(Constants.POSTIMAGE) != null && getIntent().getExtras().getString(Constants.POSTIMAGE) != "") {
                share_image.setVisibility(View.VISIBLE);
                Glide.with(this).load(getIntent().getExtras().getString(Constants.POSTIMAGE)).placeholder(R.drawable.placeholder_callforblessings).into(share_image);
            } else {
                share_image.setVisibility(View.GONE);
            }

        }

        txtvw_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                if (edt_txt_shared.getText().toString().length() == 0) {
                    showToast("Please input something to share", Toast.LENGTH_SHORT);
                } else if (isConnectedToInternet()) {

                    ServerAPI.getInstance().share(APIServerResponse.SHARE, getUserSessionId(), user_post_id, getIntent().getExtras().getString(Constants.POSTTYPE), edt_txt_shared.getText().toString(), ShareDialogActivity.this);

                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);

                }
            }
        });
        txtvw_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();

                finish();
            }
        });

    }

    public String stripText(String text) {

        String regexp = "<p>.*?</p>";
        String replace = "";
        return text.replaceAll(regexp, replace);
        //  return finalText.replaceAll("<img.+?>", "");

    }

    @Override
    public void onSuccess(int tag, Response response) {
        ShareModal shareModal;
        hideLoading();
        if (response.isSuccessful()) {
            switch (tag) {
                case APIServerResponse.SHARE:
                    hideKeyboard();
                    shareModal = (ShareModal) response.body();
                    if (shareModal.getStatus().equals("1")) {

                        showToast("Shared successfully", Toast.LENGTH_SHORT);
                        EventBus.getDefault().post(new DemoShareModal("YES"));
                        finish();
                    }


                    break;
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
