package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.AlbumSpecificListingActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class Album_list_adapter extends RecyclerView.Adapter<Album_list_adapter.DataViewHolder> {
    List<JoyMusicModal.ListBean.AlbumsBean> mList;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;

    public Album_list_adapter(Activity mContext, List<JoyMusicModal.ListBean.AlbumsBean> list) {
        this.mList = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        mList.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(ArrayList<JoyMusicModal.ListBean.AlbumsBean> list) {
        this.mList.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public Album_list_adapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.joy_music_scroll_view_item, parent, false);
        Album_list_adapter.DataViewHolder dataView = new Album_list_adapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    int i = 0;

    @Override
    public void onBindViewHolder(final Album_list_adapter.DataViewHolder holder, final int position) {
        holder.album_name.setText(mList.get(holder.getAdapterPosition()).getName());

        Glide.with(mContext).load(mList.get(holder.getAdapterPosition()).getAlbum_cover_image()).placeholder(R.drawable.placeholder_callforblessings).override(100, 100).into(holder.albumImage);

        holder.albumImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listAlbum = new Intent(mContext, AlbumSpecificListingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ALBUMID, mList.get(holder.getAdapterPosition()).getId());
                listAlbum.putExtras(bundle);
                mContext.startActivity(listAlbum);
            }
        });

        /*holder.albumImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listAlbum = new Intent(mContext, AlbumSpecificListingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ALBUMID, mList.get(holder.getAdapterPosition()).getId());
                listAlbum.putExtras(bundle);
                mContext.startActivity(listAlbum);
            }
        });
*/

    }

    public int getcount() {
        return mList.size();

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.album_name)
        AppCompatTextView album_name;
        @BindView(R.id.albumImage)
        AppCompatImageView albumImage;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }
    }
}
