package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.Modal.MessagesModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.ChatActivity;
import com.example.preetisharma.callforblessings.demo.DirectMessageWebActivity;
import com.example.preetisharma.callforblessings.demo.EventsDetailsActivityNew;
import com.example.preetisharma.callforblessings.demo.SinglePostView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 6/14/2017.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.DataViewHolder> {
    public List<MessagesModal.ListBean> list;
    private Context mContext;
    private Resources resources;
    private int userIDInt;

    public MessagesAdapter(Activity mContext, List<MessagesModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
        this.resources = this.mContext.getResources();
        String userID = ((BaseActivity) this.mContext).getUserID();
        userIDInt = Integer.parseInt(userID);
    }


 /*   public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }*/

  /*  public void addAllItems(List<MyBooksModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }*/


    @Override
    public MessagesAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_messages_layout, parent, false);
        MessagesAdapter.DataViewHolder dataView = new MessagesAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final MessagesAdapter.DataViewHolder holder, int position) {


        holder.tv_messagin_with_name.setText(String.format(resources.getString(R.string.no_of_like), list.get(position).getUserInfo().getProfile_details().getFirstname(), list.get(position).getUserInfo().getProfile_details().getLastname()));
        holder.tv_msg_date.setText(list.get(position).getLastMessage().getDate());

        Glide.with(mContext).load(list.get(position).getUserInfo().getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).crossFade()/*.override(64, 64)*/.into(holder.aiv_message_with_image);


        if (list.get(position).getLastMessage().getSenderId() == userIDInt) {

            int imgResource = R.drawable.ic_action_reply;
            holder.tv_last_mesg.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);

            if (list.get(position).getLastMessage().getText().contains(".mp4")) {
                holder.tv_last_mesg.setText("sent a video");
            } else if (list.get(position).getLastMessage().getText().contains(".jpg") || list.get(position).getLastMessage().getText().contains(".jpeg") ||
                    list.get(position).getLastMessage().getText().contains(".png")) {
                holder.tv_last_mesg.setText("sent a picture");
            } else {
                holder.tv_last_mesg.setText(list.get(position).getLastMessage().getText());
            }


        } else {

            if (list.get(position).getLastMessage().getText().contains(".mp4")) {
                holder.tv_last_mesg.setText("received a video");
            } else if (list.get(position).getLastMessage().getText().contains(".jpg") || list.get(position).getLastMessage().getText().contains(".jpeg") ||
                    list.get(position).getLastMessage().getText().contains(".png")) {
                holder.tv_last_mesg.setText("received a picture");
            } else {
                holder.tv_last_mesg.setText(list.get(position).getLastMessage().getText());
            }


        }

        /*holder.aiv_message_with_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(holder.getAdapterPosition()).getUserInfo().getIs_friend() == 1) {
                    Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUserInfo().getId()));
                    b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                    i.putExtras(b);
                    mContext.startActivity(i);
                } else if (list.get(holder.getAdapterPosition()).getUserInfo().getIs_friend() == 0) {
                    Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUserInfo().getId()));
                    b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    b.putInt(Constants.IS_FRIEND, Constants.USER_NOT_FRIEND);
                    i.putExtras(b);
                    mContext.startActivity(i);
                }
            }
        });*/

        /*holder.ll_notification_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.FRIEND_REQUEST)) {


                    if (list.get(holder.getAdapterPosition()).getUserInfo().getRequest_respond().equalsIgnoreCase("No") && list.get(holder.getAdapterPosition()).getUserInfo().getIs_friend() == 1) {
                        Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUserInfo().getId()));
                        b.putBoolean(Constants.TIMELINE_ENABLED, false);
                        b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                        i.putExtras(b);
                        mContext.startActivity(i);
                    } else {

                        if (list.get(holder.getAdapterPosition()).getUserInfo().getRequest_respond().equalsIgnoreCase("No") && list.get(holder.getAdapterPosition()).getUserInfo().getIs_friend() == 0) {

                        } else {

                        }
                        Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getUserInfo().getId()));
                        b.putBoolean(Constants.TIMELINE_ENABLED, false);
                        b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                        i.putExtras(b);
                        mContext.startActivity(i);
                    }


                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.POST_COMMENT)) {


                    Intent i = new Intent(mContext, SinglePostView.class);
                    Bundle b = new Bundle();
                    //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                    //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                    b.putString(Constants.POSTID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                    i.putExtras(b);
                    mContext.startActivity(i);


                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.POST_LIKE)) {

                    Intent i = new Intent(mContext, SinglePostView.class);
                    Bundle b = new Bundle();
                    //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                    //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                    b.putString(Constants.POSTID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                    i.putExtras(b);
                    mContext.startActivity(i);

                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.POST_SHARE)) {

                    Intent i = new Intent(mContext, SinglePostView.class);
                    Bundle b = new Bundle();
                    //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                    //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                    b.putString(Constants.POSTID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                    i.putExtras(b);
                    mContext.startActivity(i);

                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.EVENT_INVITE)) {


                    if (list.get(holder.getAdapterPosition()).getUserInfo().getRequest_respond().equalsIgnoreCase("NO")) {
                        Intent i = new Intent(mContext, EventsDetailsActivityNew.class);
                        Bundle b = new Bundle();
                        //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                        //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                        //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                        b.putString(Constants.EVENT_ID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                        b.putString(Constants.EVENT_TYPE_FLAG, "Event Invites");
                        b.putString(Constants.EVENT_INVITE_STATUS, "PENDING");
                        i.putExtras(b);
                        mContext.startActivity(i);
                    } else {
                        Intent i = new Intent(mContext, EventsDetailsActivityNew.class);
                        Bundle b = new Bundle();
                        //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                        //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                        //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                        b.putString(Constants.EVENT_ID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                        b.putString(Constants.EVENT_TYPE_FLAG, "Event Invites");
                        b.putString(Constants.EVENT_INVITE_STATUS, "ACCEPTED");
                        i.putExtras(b);
                        mContext.startActivity(i);
                    }


                } else if (list.get(holder.getAdapterPosition()).getObject_class().equalsIgnoreCase(Constants.POST)) {
                    Intent i = new Intent(mContext, SinglePostView.class);
                    Bundle b = new Bundle();
                    //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(position).getUserInfo().getId()));
                    //b.putBoolean(Constants.TIMELINE_ENABLED, false);
                    //b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_COME);
                    b.putString(Constants.POSTID, String.valueOf(list.get(holder.getAdapterPosition()).getObject_id()));
                    i.putExtras(b);
                    mContext.startActivity(i);
                }
            }
        });*/


    }


    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_last_mesg)
        AppCompatTextView tv_last_mesg;

        @BindView(R.id.aiv_message_with_image)
        AppCompatImageView aiv_message_with_image;

        @BindView(R.id.tv_messagin_with_name)
        AppCompatTextView tv_messagin_with_name;

        @BindView(R.id.tv_msg_date)
        AppCompatTextView tv_msg_date;


        @BindView(R.id.ll_message_item)
        LinearLayout ll_message_item;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            ll_message_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*Intent chat = new Intent(mContext, ChatActivity.class);
                    mContext.startActivity(chat);*/

                    try {
                        Intent chat = new Intent(mContext, DirectMessageWebActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.FRIEND_ID, String.valueOf(list.get(getAdapterPosition()).getUserInfo().getId()));
                        chat.putExtras(b);
                        mContext.startActivity(chat);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


        }

        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }
}
