package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.FriendsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/1/2017.
 */

public class FriendsRequestFragment extends Fragment implements APIServerResponse {
    @BindView(R.id.friends_recycler_view)
    RecyclerView friends_recycler_view;
    private List<FriendRequestModal.ListBean> mList;
    FriendsAdapter _adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_friends_list_search_request, container, false);
        ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().friendsRequestList(APIServerResponse.FRIENDREQUEST_LIST, ((BaseActivity) getActivity()).getUserSessionId(), this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        return rootView;
    }

    @Override
    public void onSuccess(int tag, Response response) {
        mList = new ArrayList<FriendRequestModal.ListBean>();
        ((BaseActivity) getActivity()).hideLoading();

        FriendRequestModal friendRequestModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.FRIENDREQUEST_LIST:


                    {
                        friendRequestModal = (FriendRequestModal) response.body();


                        if (friendRequestModal.getStatus().equals("1")) {
                            Log.e("List data", "List data" + friendRequestModal.getList().size());
                            mList = friendRequestModal.getList();

                        }
                       /* Log.e("List data", "List data" + mList.size());
                        Users = new String[mList.size()];
                        for (int i = 0; i < mList.size(); i++)
                            Users[i] = mList.get(i).getUsername();*/

                    }
                   /* UsersArrayAdapter usersArrayAdapter = new UsersArrayAdapter(getActivity(), mList);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (getActivity(), android.R.layout.simple_list_item_1, Users);
*/
                   /* search_text.setAdapter(adapter);
                    search_text.setThreshold(2);*/
                    _adapter = new FriendsAdapter(getActivity(), mList);
                    friends_recycler_view.setAdapter(_adapter);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    friends_recycler_view.setLayoutManager(mLayoutManager);
                  /*  search_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.e("Clicked here", "Clicked here" + mList.get(i).getId());
                            Intent timeLine = new Intent(getActivity(), MaterialUpConceptActivity.class);
                            Bundle b = new Bundle();
                            b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(mList.get(i).getId()));
                            timeLine.putExtras(b);
                            startActivity(timeLine);
                        }
                    });*/
                   /* search_text.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                              @Override
                                                              public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                                  Log.e("Clicked here", "Clicked here");
                                                                  Intent timeLine = new Intent(getActivity(), MaterialUpConceptActivity.class);
                                                                  Bundle b = new Bundle();
                                                                  b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(mList.get(i).getId()));
                                                                  timeLine.putExtras(b);
                                                                  startActivity(timeLine);
                                                              }

                                                              @Override
                                                              public void onNothingSelected(AdapterView<?> adapterView) {

                                                              }


                                                          }

                    );*/
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Subscribe
    public void friend_request_accepted(DemoFriendsModal friendsModal) {
        if (friendsModal.getMessage().equals("1")) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

        super.onDestroy();

    }
}
