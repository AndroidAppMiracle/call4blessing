package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.MyFriendsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSendFriendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/1/2017.
 */

public class MyFriendsFragment extends Fragment implements APIServerResponse {
    @BindView(R.id.friends_recycler_view)
    RecyclerView friends_recycler_view;
    private List<MyFriendsModal.ListBean> mList;
    MyFriendsAdapter _adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_friends_list_search_request, container, false);
        ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        return rootView;
    }

    @Override
    public void onSuccess(int tag, Response response) {
        mList = new ArrayList<MyFriendsModal.ListBean>();
        ((BaseActivity) getActivity()).hideLoading();

        MyFriendsModal userListingModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.MYFRIENDLIST:


                        userListingModal = (MyFriendsModal) response.body();


                        if (userListingModal.getStatus().equals("1")) {
                            Log.e("List data", "List data" + userListingModal.getList().size());
                            mList = userListingModal.getList();

                        } else {
                        }

                        _adapter = new MyFriendsAdapter(getActivity(), mList);
                        friends_recycler_view.setAdapter(_adapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        friends_recycler_view.setLayoutManager(mLayoutManager);


                        break;


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Subscribe
    public void friend_request_accepted(DemoFriendsModal friendsModal) {
        if (friendsModal.getMessage().equals("1")) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void onFriendRequestSent(EventBusSendFriendRequestModal eventBusSendFriendRequestModal) {
        if (eventBusSendFriendRequestModal.getMessage().equals("1")) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }
    }
}