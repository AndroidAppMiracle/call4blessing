package com.example.preetisharma.callforblessings.ViewHolder;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.callforblessings.R;


/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class TextViewHolder  extends RecyclerView.ViewHolder {

    public AppCompatTextView getTxtpost() {
        return txtpost;
    }


    public void setTxtpost(AppCompatTextView txtpost) {
        this.txtpost = txtpost;
    }

    private AppCompatTextView txtpost;

    public TextViewHolder(View v) {
        super(v);
        txtpost = (AppCompatTextView) v.findViewById(R.id.viewholder_text);
    }


}