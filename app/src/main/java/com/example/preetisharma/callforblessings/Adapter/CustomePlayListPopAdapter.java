package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.downloadjoymusic.DownloadMainActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.playlist.PlaylistSongsList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 9/20/2017.
 */

public class CustomePlayListPopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int ADD_ALBUM = 0;
    public final int ALBUM = 1;

    Activity mContext;
    Dialog dialogAlert;

    public CustomePlayListPopAdapter(Activity mContext, Dialog dialog) {
        this.mContext = mContext;
        this.dialogAlert = dialog;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            //Return "Create New Album" Item
            return ADD_ALBUM;
        } else {
            //Return Albums Item
            return ALBUM;
        }
        //return super.getItemViewType(position);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;

        switch (viewType) {
            case ADD_ALBUM:
                v = inflater.inflate(R.layout.playlist_pop_items, parent, false);
                viewHolder = new DataViewHolderAddAlbum(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;

            case ALBUM:
                v = inflater.inflate(R.layout.playlist_pop_items, parent, false);
                viewHolder = new DataViewHolderAlbum(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class DataViewHolderAlbum extends RecyclerView.ViewHolder {

      /*  @BindView(R.id.atv_album_name)
        AppCompatTextView atv_album_name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

       *//* @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*//*

        @BindView(R.id.album_progress)
        ProgressBar mProgress;

        @BindView(R.id.cv_album_item)
        CardView cv_album_item;
*/

        @BindView(R.id.cv_playList_item)
        CardView cv_playList_item;

        public DataViewHolderAlbum(View v) {
            super(v);
            ButterKnife.bind(this, v);
            cv_playList_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Item Click", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, PlaylistSongsList.class);
                    mContext.startActivity(intent);
                }
            });
        }

    }


    public class DataViewHolderAddAlbum extends RecyclerView.ViewHolder {

        /*@BindView(R.id.atv_Name)
        AppCompatTextView atv_Name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.movie_progress)
        ProgressBar mProgress;*/

     /*   @BindView(R.id.cv_playList_item)
        CardView cv_playList_item;
*/

        @BindView(R.id.cv_playList_item)
        CardView cv_playList_item;

        public DataViewHolderAddAlbum(View v) {
            super(v);
            ButterKnife.bind(this, v);


          /*  cv_playList_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Item Click", Toast.LENGTH_SHORT).show();
                }
            });*/


            cv_playList_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.custom_create_playlist_layout);

                    Button cancleBtn = (Button) dialog.findViewById(R.id.cancle_playlist_btn);
                    Button createPlayList = (Button) dialog.findViewById(R.id.create_playlist_btn);

                    cancleBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    createPlayList.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(mContext, "New playlist is created..", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(mContext, PlaylistSongsList.class);
                            mContext.startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    dialogAlert.dismiss();
                    dialog.show();
                }
            });
        }

    }
}
