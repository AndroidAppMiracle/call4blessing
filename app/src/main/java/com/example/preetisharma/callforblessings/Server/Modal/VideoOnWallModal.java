package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/16/2017.
 */

public class VideoOnWallModal {


    /**
     * status : 1
     * detail : {"id":2,"title":"xcxvcxvc","youtube_link":"","file":"http://call4blessing.com/uploads/image-video/1497519213SampleVideo_360x240_2mb.mp4"}
     */

    private String status;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * id : 2
         * title : xcxvcxvc
         * youtube_link :
         * file : http://call4blessing.com/uploads/image-video/1497519213SampleVideo_360x240_2mb.mp4
         */

        private int id;
        private String title;
        private String youtube_link;
        private String file;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getYoutube_link() {
            return youtube_link;
        }

        public void setYoutube_link(String youtube_link) {
            this.youtube_link = youtube_link;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }
    }
}
