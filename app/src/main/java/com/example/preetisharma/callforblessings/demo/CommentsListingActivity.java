package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.CommentsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.CommentsListModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusHomeWallCommented;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/27/2017.
 */

public class CommentsListingActivity extends BaseActivity implements APIServerResponse {
    AppCompatTextView txtvw_header_title;
    // @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    //@BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.comments_recycler_view)
    RecyclerView comments_recycler_view;
    CommentsAdapter _commentsAdapter;


    @BindView(R.id.edt_txt_comment)
    AppCompatEditText edt_txt_comment;
    @BindView(R.id.txtvw_no_comments)
    AppCompatTextView txtvw_no_comments;
    @BindView(R.id.txtvw_submit_comment)
    AppCompatTextView txtvw_submit_comment;
    ArrayList<CommentsListModal.DetailBean.CommentsBean> mList;
    String post_id, post_type;
    int postPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments_layout);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            post_id = getIntent().getExtras().getString(Constants.COMMENTS);
            post_type = getIntent().getExtras().getString(Constants.POSTTYPE);
            postPosition = getIntent().getExtras().getInt(Constants.POSITION, 0);
        }
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);

        txtvw_header_title.setText("Comments");
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentsListingActivity.this.finish();
            }
        });

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().commentListing(APIServerResponse.COMMENTLIST, getUserSessionId(), post_id, post_type, this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


        txtvw_submit_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                Log.e("data", "Comment" + edt_txt_comment.getText().toString());
                if (edt_txt_comment.getText().toString().trim().length() != 0 || !edt_txt_comment.getText().toString().isEmpty() || !edt_txt_comment.getText().toString().equals("")) {

                    if (isConnectedToInternet()) {

                        ServerAPI.getInstance().comment(APIServerResponse.COMMENT, getUserSessionId(), post_id, post_type, edt_txt_comment.getText().toString(), CommentsListingActivity.this);

                    } else {
                        showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                } else {
                    showToast("Enter comment", Toast.LENGTH_SHORT);
                }
            }
        });
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            CommentModal comment;
            CommentsListModal commentModal;
            hideLoading();
            if (response.isSuccessful()) {
                switch (tag) {

                    case APIServerResponse.COMMENTLIST: {
                        commentModal = (CommentsListModal) response.body();
                        if (commentModal.getStatus().equals("1")) {

                            mList = commentModal.getDetail().getComments();

                            if (mList.size() > 0) {
                                txtvw_no_comments.setVisibility(View.GONE);
                                comments_recycler_view.setVisibility(View.VISIBLE);

                                _commentsAdapter = new CommentsAdapter(this, mList);
                                comments_recycler_view.setItemAnimator(new DefaultItemAnimator());
                                comments_recycler_view.setAdapter(_commentsAdapter);
                                comments_recycler_view.smoothScrollToPosition(mList.size());
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                                comments_recycler_view.setLayoutManager(mLayoutManager);
                            } else {
                                txtvw_no_comments.setVisibility(View.VISIBLE);
                                comments_recycler_view.setVisibility(View.GONE);
                            }
                        }
                    }
                    break;
                    case APIServerResponse.COMMENT: {
                        comment = (CommentModal) response.body();
                        if (comment.getStatus().equals("1")) {
                            EventBus.getDefault().post(new DemoCommentModal(mList.size()));
                            EventBus.getDefault().post(new EventBusHomeWallCommented(1, postPosition));
                            showToast("Commented", Toast.LENGTH_SHORT);
                            edt_txt_comment.setText("");
                            ServerAPI.getInstance().commentListing(APIServerResponse.COMMENTLIST, getUserSessionId(), post_id, post_type, this);
                        }
                    }

                    break;

                    default:
                        break;

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        switch (tag) {
            case APIServerResponse.LIKE:
                System.out.println("Error");
                break;
            case APIServerResponse.SHARE:
                System.out.println("Error");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            hideKeyboard();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
