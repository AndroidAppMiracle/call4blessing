package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.TaggedFriendsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.FilterModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 3/24/2017.
 */

public class TagFriendActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.rv_tagging_friends_recycler_view)
    RecyclerView rv_tagging_friends_recycler_view;
    private List<MyFriendsModal.ListBean> mList = new ArrayList<>();
    TaggedFriendsAdapter _tagFriendsAdapter;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.txtvw_chat)
    AppCompatTextView txtvw_chat;
    @BindView(R.id.txtvw_no_friends)
    AppCompatTextView txtvw_no_friends;
    @BindView(R.id.remove_selection)
    AppCompatTextView remove_selection;
    @BindView(R.id.search_text)
    AppCompatEditText search_text;
    public static String[] userId;
    public int responseCode = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taggingfriend_activity);
        ButterKnife.bind(this);
        txtvw_header_title.setText("Tag Friends");
        txtvw_chat.setText("Done");
        txtvw_chat.setVisibility(View.VISIBLE);
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> taggedFriends = _tagFriendsAdapter.selectedList();
                ArrayList<MyFriendsModal.ListBean.UserInfoBean> taggedFriendsIds = _tagFriendsAdapter.selectedListIds();
       *//* FilterModal._filterInstance.setFriendsArray(taggedFriendsArray);*//*
                Intent intent = getIntent();
                Bundle b = new Bundle();
                b.putParcelableArrayList("tagged_list", taggedFriends);
                b.putParcelableArrayList("tagged_list_ids", taggedFriendsIds);
                intent.putExtras(b);
                setResult(RESULT_OK, intent);*/
                hideKeyboard();
                finish();
            }
        });
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, getUserSessionId(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);

        }
        userId = new String[mList.size()];


    }

    @OnClick(R.id.txtvw_chat)
    public void tagFriends() {
        //ArrayList<FilterModal> taggedFriendsArray = _tagFriendsAdapter.unselectList();
        ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> taggedFriends = _tagFriendsAdapter.selectedList();
        ArrayList<MyFriendsModal.ListBean.UserInfoBean> taggedFriendsIds = _tagFriendsAdapter.selectedListIds();
       /* FilterModal._filterInstance.setFriendsArray(taggedFriendsArray);*/
        Intent intent = getIntent();
        Bundle b = new Bundle();
        b.putParcelableArrayList("tagged_list", taggedFriends);
        b.putParcelableArrayList("tagged_list_ids", taggedFriendsIds);
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {

            hideLoading();

            if (response.isSuccessful()) {

                final MyFriendsModal userListingModal;


                switch (tag) {
                    case APIServerResponse.MYFRIENDLIST:
                        userListingModal = (MyFriendsModal) response.body();
                        if (userListingModal.getStatus().equals("1")) {
                            mList = userListingModal.getList();
                        }
                        if (mList.size() <= 0) {

                            txtvw_no_friends.setVisibility(View.VISIBLE);
                            rv_tagging_friends_recycler_view.setVisibility(View.GONE);
                        } else {
                            txtvw_no_friends.setVisibility(View.GONE);
                            rv_tagging_friends_recycler_view.setVisibility(View.VISIBLE);
                            _tagFriendsAdapter = new TaggedFriendsAdapter(this, mList);
                            rv_tagging_friends_recycler_view.setAdapter(_tagFriendsAdapter);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                            rv_tagging_friends_recycler_view.setLayoutManager(mLayoutManager);
                            search_text.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                    charSequence = charSequence.toString().toLowerCase();
                                    final List<MyFriendsModal.ListBean> filteredList = new ArrayList<MyFriendsModal.ListBean>();
                                    for (int p = 0; p < mList.size(); p++) {
                                        final String text = mList.get(p).getUser_info().getProfile_details().getFirstname().toLowerCase();
                                        if (text.contains(charSequence)) {
                                            filteredList.add(mList.get(p));
                                        }
                                        _tagFriendsAdapter = new TaggedFriendsAdapter(TagFriendActivity.this, filteredList);
                                        rv_tagging_friends_recycler_view.setAdapter(_tagFriendsAdapter);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(TagFriendActivity.this);
                                        rv_tagging_friends_recycler_view.setLayoutManager(mLayoutManager);
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable editable) {

                                }
                            });

                        }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyboard();
    }
}

