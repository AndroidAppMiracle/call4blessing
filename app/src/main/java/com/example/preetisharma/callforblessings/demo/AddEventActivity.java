package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AddEventModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 1/19/2017.
 */

public class AddEventActivity extends BaseActivity implements APIServerResponse, EasyPermissions.PermissionCallbacks, ImagePickerCallback {

    @BindView(R.id.img_view_event_pic)
    AppCompatImageView img_view_event_pic;

    @BindView(R.id.edt_txt_event_name)
    AppCompatEditText edt_txt_event_name;

    @BindView(R.id.edt_txt_event_date_time)
    AppCompatTextView edt_txt_event_date_time;

    @BindView(R.id.edt_txt_event_description)
    AppCompatEditText edt_txt_event_description;

    @BindView(R.id.edt_txt_event_location)
    AppCompatEditText edt_txt_event_location;

    @BindView(R.id.txtvw_update)
    AppCompatTextView txtvw_update;

    @BindView(R.id.edt_txt_event_time)
    AppCompatTextView edt_txt_event_time;


    //Toolbar items
    // @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    // @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    //@BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    private static final int PLACE_PICKER_REQUEST = 1;

    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private static final int RC_CAMERA_PERM = 342;
    private static final int RC_GALLERY_PERM = 545;
    private String pickerPath = "";
    private int mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        ButterKnife.bind(this);

        edt_txt_event_description.setMaxHeight(edt_txt_event_description.getHeight());
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);

        txtvw_header_title.setText("Add Event");
        img_view_change_password.setVisibility(View.GONE);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddEventActivity.this.finish();
            }
        });

    }

    @OnClick(R.id.img_view_event_pic)
    public void takeImage() {
        new AlertDialog.Builder(AddEventActivity.this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }


    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        try {
            if (response.isSuccessful()) {
                hideLoading();
                AddEventModal addEventModal;
                switch (tag) {
                    case APIServerResponse.CREATE_EVENT:
                        addEventModal = (AddEventModal) response.body();
                        if (addEventModal.getStatus().equals("1")) {
                            showToast(addEventModal.getMessage(), Toast.LENGTH_LONG);
                            finish();
                        } else {
                            showToast(addEventModal.getMessage(), Toast.LENGTH_LONG);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.edt_txt_event_date_time)
    void date_picker_dialog() {
        hideKeyboard();
        openDateDia();
    }


    @OnClick(R.id.edt_txt_event_time)
    public void timePickerDialog() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();

        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        mHour = hourOfDay;
                        mMinute = minute;
                        String timeSet = "";
                        if (mHour > 12) {
                            mHour -= 12;
                            timeSet = "PM";
                        } else if (mHour == 0) {
                            mHour += 12;
                            timeSet = "AM";
                        } else if (mHour == 12) {
                            timeSet = "PM";
                        } else {
                            timeSet = "AM";
                        }

                        String min = "";
                        if (mMinute < 10)
                            min = "0" + mMinute;
                        else
                            min = String.valueOf(mMinute);

                        // Append in a StringBuilder
                        String aTime = new StringBuilder().append(mHour).append(':')
                                .append(min).append(" ").append(timeSet).toString();
                        edt_txt_event_time.setText(aTime);


                        //edt_txt_event_time.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();

    }

    Calendar calendar = Calendar.getInstance();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void openDateDia() {
        final Calendar cal = Calendar.getInstance();

        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        Log.e("Date is", "Date is" + format);
                        try {
                            if (isValidDate(format)) {
                                edt_txt_event_date_time.setText(format);
                            } else {
                                showToast("Select Valid Date of Event", Toast.LENGTH_SHORT);
                                edt_txt_event_date_time.requestFocus();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


//                        }else{
//                            Utils.showToast(BookAppointmentActivity.this, "Please select future date");
//                        }

//                        dateTime = year + "-" + (int)(monthOfYear+1) + "-" + dayOfMonth;

                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);

        datePicker.setCancelable(true);

        datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }

    /*Check valid date of birth is valid*/
    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(pDateString);
        return new Date().before(date);
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.CREATE_EVENT:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            img_view_event_pic.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
            Intent intent = intentBuilder.build(AddEventActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
           /* if (requestCode == PLACE_PICKER_REQUEST) {

                final Place place = PlacePicker.getPlace(this, data);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();


                edt_txt_location.setText(name);
                txtvw_state.setText(place.getLocale().getCountry());
                txtvw_country.setText(place.getAddress());


            }*/
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(AddEventActivity.this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(AddEventActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    public static final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";

    @OnClick(R.id.txtvw_update)
    public void submitNewEvent() {

        try {

            if (Validation(edt_txt_event_name.getText().toString(), edt_txt_event_date_time.getText().toString(), edt_txt_event_location.getText().toString(), edt_txt_event_description.getText().toString(), edt_txt_event_time.getText().toString().trim())) {
                String dateTime = edt_txt_event_date_time.getText().toString().trim() + " " + edt_txt_event_time.getText().toString().trim();
                if (pickerPath.equals("")) {
                    showLoading();
                    hideKeyboard();

                    ServerAPI.getInstance().createEvent(APIServerResponse.CREATE_EVENT, getUserSessionId(), edt_txt_event_name.getText().toString(), dateTime, edt_txt_event_location.getText().toString(), edt_txt_event_description.getText().toString(), this);
                } else {
                    showLoading();
                    hideKeyboard();
                    Glide.with(AddEventActivity.this).load(pickerPath).into(img_view_event_pic);
                    ServerAPI.getInstance().createEventWithImage(APIServerResponse.CREATE_EVENT, getUserSessionId(), edt_txt_event_name.getText().toString(), dateTime, edt_txt_event_location.getText().toString(), edt_txt_event_description.getText().toString(), pickerPath, this);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public boolean Validation(String eventName, String eventDateTime, String eventLocation, String eventDescription, String eventTime) {
        if (eventName.isEmpty()) {
            showToast("Enter event name.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventName.equals("")) {
            showToast("Enter event name.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventDateTime.isEmpty()) {
            showToast("Enter event date & time.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventDateTime.equals("")) {
            showToast("Enter event date & time.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventLocation.isEmpty()) {
            showToast("Enter event location.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventLocation.equals("")) {
            showToast("Enter event location.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventDescription.isEmpty()) {
            showToast("Enter event description.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventDescription.equals("")) {
            showToast("Enter event description.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventTime.equalsIgnoreCase("")) {
            showToast("Enter event time.", Toast.LENGTH_SHORT);
            return false;
        }
        return true;


    }

}


