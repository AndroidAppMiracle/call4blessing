package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/27/2017.
 */

public class SettingDetailModal {


    private String name;
    private boolean isChecked;
    private int settingImage;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getSettingImage() {
        return settingImage;
    }

    public void setSettingImage(int settingImage) {
        this.settingImage = settingImage;
    }
}
