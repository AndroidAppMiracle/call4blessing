package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllAlbumModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllSongsModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.AlbumSpecificListingActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kshitiz Bali on 5/10/2017.
 */

public class PaginationAlbumsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM = 0;
    public static final int LOADING = 1;
    //private static final String BASE_URL_IMG = "https://image.tmdb.org/t/p/w150";

    private List<JoyMusicViewAllAlbumModal.ListBean> albumResults;
    private Context context;

    private boolean isLoadingAdded = false;

    public PaginationAlbumsAdapter(Context context) {
        this.context = context;
        albumResults = new ArrayList<>();
    }

    public List<JoyMusicViewAllAlbumModal.ListBean> getMovies() {
        return albumResults;
    }

    public void setMovies(List<JoyMusicViewAllAlbumModal.ListBean> albumResults) {
        this.albumResults = albumResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new PaginationAlbumsAdapter.LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.adapter_joy_music_view_all, parent, false);
        viewHolder = new PaginationAlbumsAdapter.MovieVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        JoyMusicViewAllAlbumModal.ListBean result = albumResults.get(position); // Movie

        switch (getItemViewType(position)) {
            case ITEM:
                final PaginationAlbumsAdapter.MovieVH movieVH = (PaginationAlbumsAdapter.MovieVH) holder;

                movieVH.atv_Name.setText(result.getName());


                Glide
                        .with(context)
                        .load(result.getAlbum_cover_image())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                // TODO: 08/11/16 handle failure
                                movieVH.mProgress.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                // image ready, hide progress now
                                movieVH.mProgress.setVisibility(View.GONE);
                                return false;   // return false if you want Glide to handle everything else.
                            }
                        })
                        .diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image
                        .centerCrop()
                        .crossFade()
                        .into(movieVH.atv_AlbumCover);

                break;

            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return albumResults == null ? 0 : albumResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == albumResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(JoyMusicViewAllAlbumModal.ListBean r) {
        albumResults.add(r);
        notifyItemInserted(albumResults.size() - 1);
    }

    public void addAll(List<JoyMusicViewAllAlbumModal.ListBean> moveResults) {
        for (JoyMusicViewAllAlbumModal.ListBean result : moveResults) {
            add(result);
        }
    }

    public void remove(JoyMusicViewAllAlbumModal.ListBean r) {
        int position = albumResults.indexOf(r);
        if (position > -1) {
            albumResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new JoyMusicViewAllAlbumModal.ListBean());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = albumResults.size() - 1;
        JoyMusicViewAllAlbumModal.ListBean result = getItem(position);

        if (result != null) {
            albumResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public JoyMusicViewAllAlbumModal.ListBean getItem(int position) {
        return albumResults.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class MovieVH extends RecyclerView.ViewHolder {
        //private TextView mMovieTitle;
        // private TextView mMovieDesc;
        // private TextView mYear; // displays "year | language"
        // private ImageView mPosterImg;
        private ProgressBar mProgress;

        AppCompatTextView atv_Name;

        AppCompatImageView atv_AlbumCover;

        LinearLayout ll_songItem;

        public MovieVH(View itemView) {
            super(itemView);

            atv_Name = (AppCompatTextView) itemView.findViewById(R.id.atv_Name);
            /*mMovieDesc = (TextView) itemView.findViewById(R.id.movie_desc);
            mYear = (TextView) itemView.findViewById(R.id.movie_year);*/
            ll_songItem = (LinearLayout) itemView.findViewById(R.id.ll_songItem);
            atv_AlbumCover = (AppCompatImageView) itemView.findViewById(R.id.atv_AlbumCover);
            mProgress = (ProgressBar) itemView.findViewById(R.id.movie_progress);


            ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent listAlbum = new Intent(context, AlbumSpecificListingActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ALBUMID, albumResults.get(getAdapterPosition()).getId());
                    listAlbum.putExtras(bundle);
                    context.startActivity(listAlbum);
                }
            });
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
