package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by preeti.sharma on 1/27/2017.
 */

public class CommentsListModal {

    /**
     * status : 1
     * detail : {"id":179,"post_by":{"id":57,"username":"user5","email":"user5@gmail.com","updated":1,"profile_details":{"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}},"post_content":"check this out","posted_at":"6  hours   ago","post_type":"SHARE","comments":[{"id":13,"post_id":179,"post_by":{"id":57,"username":"user5","email":"user5@gmail.com","updated":1,"profile_details":{"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}},"comment":"gud","created_at":"5  hours   ago","post_type":"POST"}],"like_count":"1","comment_count":"1","like_flag":"Not Liked","media":[]}
     */

    private String status;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * id : 179
         * post_by : {"id":57,"username":"user5","email":"user5@gmail.com","updated":1,"profile_details":{"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}}
         * post_content : check this out
         * posted_at : 6  hours   ago
         * post_type : SHARE
         * comments : [{"id":13,"post_id":179,"post_by":{"id":57,"username":"user5","email":"user5@gmail.com","updated":1,"profile_details":{"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}},"comment":"gud","created_at":"5  hours   ago","post_type":"POST"}]
         * like_count : 1
         * comment_count : 1
         * like_flag : Not Liked
         * media : []
         */

        private int id;
        private PostByBean post_by;
        private String post_content;
        private String posted_at;
        private String post_type;
        private String like_count;
        private String comment_count;
        private String like_flag;
        private ArrayList<CommentsBean> comments;
        private List<?> media;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public PostByBean getPost_by() {
            return post_by;
        }

        public void setPost_by(PostByBean post_by) {
            this.post_by = post_by;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPosted_at() {
            return posted_at;
        }

        public void setPosted_at(String posted_at) {
            this.posted_at = posted_at;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getLike_count() {
            return like_count;
        }

        public void setLike_count(String like_count) {
            this.like_count = like_count;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getLike_flag() {
            return like_flag;
        }

        public void setLike_flag(String like_flag) {
            this.like_flag = like_flag;
        }

        public ArrayList<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(ArrayList<CommentsBean> comments) {
            this.comments = comments;
        }

        public List<?> getMedia() {
            return media;
        }

        public void setMedia(List<?> media) {
            this.media = media;
        }

        public static class PostByBean {
            /**
             * id : 57
             * username : user5
             * email : user5@gmail.com
             * updated : 1
             * profile_details : {"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : new user
                 * lastname : user
                 * d_o_b : 2017-01-09
                 * phone : 12345678900
                 * gender : MALE
                 * location :
                 * country :
                 * state :
                 * cover_pic : http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg
                 * profile_pic : http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }

        public static class CommentsBean {
            /**
             * id : 13
             * post_id : 179
             * post_by : {"id":57,"username":"user5","email":"user5@gmail.com","updated":1,"profile_details":{"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}}
             * comment : gud
             * created_at : 5  hours   ago
             * post_type : POST
             */

            private int id;
            private int post_id;
            private PostByBeanX post_by;
            private String comment;
            private String created_at;
            private String post_type;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getPost_id() {
                return post_id;
            }

            public void setPost_id(int post_id) {
                this.post_id = post_id;
            }

            public PostByBeanX getPost_by() {
                return post_by;
            }

            public void setPost_by(PostByBeanX post_by) {
                this.post_by = post_by;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getPost_type() {
                return post_type;
            }

            public void setPost_type(String post_type) {
                this.post_type = post_type;
            }

            public static class PostByBeanX {
                /**
                 * id : 57
                 * username : user5
                 * email : user5@gmail.com
                 * updated : 1
                 * profile_details : {"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}
                 */

                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanX profile_details;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanX profile_details) {
                    this.profile_details = profile_details;
                }

                public static class ProfileDetailsBeanX {
                    /**
                     * firstname : new user
                     * lastname : user
                     * d_o_b : 2017-01-09
                     * phone : 12345678900
                     * gender : MALE
                     * location :
                     * country :
                     * state :
                     * cover_pic : http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg
                     * profile_pic : http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }
        }
    }
}
