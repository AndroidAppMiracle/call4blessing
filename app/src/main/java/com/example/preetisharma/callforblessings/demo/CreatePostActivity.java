package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialcamera.MaterialCamera;
import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.HomeActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusHomeWallPostCreated;
import com.example.preetisharma.callforblessings.Server.Modal.FilterModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.UploadPostModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ImageUtils;
import com.example.preetisharma.callforblessings.Utils.Utilities;
import com.example.preetisharma.callforblessings.file.FileUtils;
import com.example.preetisharma.callforblessings.video.MediaController;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 3/22/2017.
 */

public class CreatePostActivity extends BaseActivity implements ImagePickerCallback, EasyPermissions.PermissionCallbacks, APIServerResponse {
    @BindView(R.id.img_vw_user_profile)
    AppCompatImageView img_vw_user_profile;
    @BindView(R.id.txtvw_user_name)
    AppCompatTextView txtvw_user_name;
    @BindView(R.id.txtvw_time_stamp)
    AppCompatTextView txtvw_time_stamp;
    @BindView(R.id.edt_txt_post_details)
    AppCompatEditText edt_txt_post_details;
    @BindView(R.id.viewholder_image)
    AppCompatImageView viewholder_image;
    @BindView(R.id.txtvw_add_to_your_post)
    AppCompatTextView txtvw_add_to_your_post;
    @BindView(R.id.img_vw_upload_image)
    AppCompatImageView img_vw_upload_image;
    @BindView(R.id.img_vw_upload_video)
    AppCompatImageView img_vw_upload_video;
    @BindView(R.id.img_vw_tag_friends)
    AppCompatImageView img_vw_tag_friends;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private Uri fileUri;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.lnr_add_to_your_post)
    LinearLayout lnr_add_to_your_post;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.txtvw_chat)
    AppCompatTextView txtvw_chat;
    @BindView(R.id.img_vw_play_video)
    AppCompatImageView img_vw_play_video;
    private static final int RC_CAMERA_PERM = 342;
    private static final int RC_GALLERY_PERM = 545;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private final static int CAMERA_RQ = 6969;
    String username, userImage, postText, postImage, postID;
    private static final String TAG = "CreatePostActivity";
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private String pickerPath = "";
    public int responseCode = 101;
    String userId = "";
    ArrayList<FilterModal> arrayListTagFriends = new ArrayList<>();
    @BindView(R.id.txtvw_tagged_friends)
    AppCompatTextView txtvw_tagged_friends;
    private static final int RESULT_CODE_COMPRESS_VIDEO = 3;
    private static final int DEMO_RECORD_VIDEO_COMPRESSED = 4;
    private File tempFile;
    String CompressedFileName;
    private int chooserType;
    private String thumbnailString;
    String text, videoIntentData;
    public static String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA};
    public static String[] mediaColumns = {MediaStore.Video.Media._ID};
    private ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> myTaggedList = new ArrayList<>();
    private ArrayList<MyFriendsModal.ListBean.UserInfoBean> myTaggedListIds = new ArrayList<>();
    private String postType = "", friendId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_post_edit);
        ButterKnife.bind(this);

        if (getUserGender().equalsIgnoreCase("Male")) {
            Glide.with(this).load(getUserImage()).placeholder(R.mipmap.profile_holder).into(img_vw_user_profile);
        } else {
            Glide.with(this).load(getUserImage()).placeholder(R.drawable.ic_female).into(img_vw_user_profile);
        }

        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            postType = extras.getString(Constants.POSTTYPE, Constants.WALL);
            Log.i("postType", postType);

            if (getIntent().getExtras().getString(Constants.FRIEND_ID) != null && !getIntent().getExtras().getString(Constants.FRIEND_ID).equalsIgnoreCase("")) {
                friendId = getIntent().getExtras().getString(Constants.FRIEND_ID);
            }
        }


        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Create Post");
        txtvw_chat.setVisibility(View.VISIBLE);
        txtvw_chat.setText(" Post");
        txtvw_user_name.setText(getFullName());
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreatePostActivity.this.finish();
            }
        });


    }

    @OnClick(R.id.txtvw_chat)
    public void createPost() {

        hideKeyboard();
        createPostApi();
        txtvw_chat.setEnabled(false);
    }

    @OnClick(R.id.img_vw_tag_friends)
    public void TagFriend() {
        Intent tagIntent = new Intent(this, TagFriendActivity.class);
        startActivityForResult(tagIntent, responseCode, new Bundle());
    }

    @OnClick(R.id.img_vw_upload_video)
    public void uploadVideo() {
        hasPermissionInManifest(CreatePostActivity.this, Manifest.permission.CAMERA);
        new AlertDialog.Builder(this)

                .setTitle("Select Video")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        createVideo();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickVideoSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickVideoSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("video/*");
            startActivityForResult(intent, RESULT_CODE_COMPRESS_VIDEO);
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    class VideoCompressor extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
            Log.d(TAG, "Start video compression");
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (MediaController.getInstance().convertVideo(tempFile.getPath())) {
                return MediaController.getInstance().compressedFilePath();
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String compressedPath) {
            super.onPostExecute(compressedPath);
            hideLoading();
            if (compressedPath != null) {
                CompressedFileName = compressedPath;
                Log.e("File path", compressedPath);
            }

            img_vw_play_video.setVisibility(View.VISIBLE);

        }
    }

    private class DemoVideoCompressor extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
            Log.d(TAG, "Start video compression");
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (MediaController.getInstance().convertVideo(videoIntentData)) {
                return MediaController.getInstance().compressedFilePath();
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String compressedPath) {
            super.onPostExecute(compressedPath);
            hideLoading();
            if (compressedPath != null) {
                CompressedFileName = compressedPath;
                Log.e("File path", compressedPath);

            }


            //img_vw_play_video.setVisibility(View.VISIBLE);

        }
    }


    @OnClick(R.id.img_vw_upload_image)
    public void uploadImage() {
        hasPermissionInManifest(CreatePostActivity.this, Manifest.permission.CAMERA);
        new AlertDialog.Builder(this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {

        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();

        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

 /*   public void taggedFriendsArrayResult() {
        arrayListTagFriends = FilterModal.getFriendsArray();
        if (arrayListTagFriends != null && arrayListTagFriends.size() > 0) {

            txtvw_tagged_friends.setVisibility(View.VISIBLE);
            if (arrayListTagFriends.size() == 1) {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getLastname());
            } else if (arrayListTagFriends.size() == 2) {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getLastname() + " and " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname());

            } else {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " and" + " " + (arrayListTagFriends.size() - 1) + " others");

            }
        }
    }*/

    private void setTaggedFriends(ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> list) {

        if (list != null && list.size() > 0) {

            txtvw_tagged_friends.setVisibility(View.VISIBLE);
            if (list.size() == 1) {
                txtvw_tagged_friends.setText(" with " + list.get(0).getFirstname() + " " + list.get(0).getLastname());
            } else if (list.size() == 2) {
                txtvw_tagged_friends.setText(" with " + list.get(0).getFirstname() + " " + list.get(0).getLastname() + " and " + list.get(1).getFirstname() + " " + list.get(1).getLastname());

            } else {
                txtvw_tagged_friends.setText(" with " + list.get(0).getFirstname() + " and" + " " + (list.size() - 1) + " others");

            }
        } else {
            txtvw_tagged_friends.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {

            if (resultCode == RESULT_OK && requestCode == responseCode) {

                Bundle extras = data.getExtras();

                //myTaggedList = extras.getParcelableArrayList("tagged_list");
                myTaggedList = extras.getParcelableArrayList("tagged_list");
                myTaggedListIds = extras.getParcelableArrayList("tagged_list_ids");
                setTaggedFriends(myTaggedList);

                //taggedFriendsArrayResult();

            } else if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
            if (requestCode == CAMERA_RQ) {

                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "Saved to: " + data.getDataString(), Toast.LENGTH_LONG).show();
                } else if (data != null) {
                    Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                    e.printStackTrace();
                }
            } else if (requestCode == DEMO_RECORD_VIDEO_COMPRESSED) {

                if (data.getData() != null) {
                    Log.i("data", "" + data.getData());
                    Log.i("dataGetPath", "" + data.getData().getPath());

                    Bitmap mThumbnailVideo = ThumbnailUtils.createVideoThumbnail(data.getData().getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
                    img_vw_play_video.setVisibility(View.VISIBLE);
                    viewholder_image.setVisibility(View.VISIBLE);
                    viewholder_image.setImageBitmap(mThumbnailVideo);
                    videoIntentData = data.getData().getPath();
                    text = ImageUtils.saveImageOnSDCard(this, ImageUtils.getBytesFromBitmap(mThumbnailVideo));
                    text = Utilities.compressImage(text);
                    new DemoVideoCompressor().execute();
                }


                //mVideoUri = data.getData();
                //videoView.setVideoURI(mVideoUri);
            } else {
                super.onActivityResult(requestCode, resultCode, data);
                Uri uri = data.getData();

                if (requestCode == RESULT_CODE_COMPRESS_VIDEO) {
                    if (uri != null) {
                        Cursor cursor = getContentResolver().query(uri, null, null, null, null, null);

                        try {
                            if (cursor != null && cursor.moveToFirst()) {

                                String displayName = cursor.getString(
                                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                CompressedFileName = displayName;
                                int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                                String size = null;
                                if (!cursor.isNull(sizeIndex)) {
                                    size = cursor.getString(sizeIndex);
                                } else {
                                    size = "Unknown";
                                }
                                tempFile = FileUtils.saveTempFile(displayName, this, uri);
                                viewholder_image.setVisibility(View.VISIBLE);
                                ContentResolver crThumb = getContentResolver();
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inSampleSize = 1;

                                Bitmap mThumbnailVideo = ThumbnailUtils.createVideoThumbnail(tempFile.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
                                img_vw_play_video.setVisibility(View.VISIBLE);
                                viewholder_image.setImageBitmap(mThumbnailVideo);

                                text = ImageUtils.saveImageOnSDCard(this, ImageUtils.getBytesFromBitmap(mThumbnailVideo));
                                text = Utilities.compressImage(text);

                                //viewholder_image.setImageBitmap(ThumbnailUtils.createVideoThumbnail(tempFile.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));


                                new VideoCompressor().execute();
                                //text= getThumbnailPathForLocalFile(this, uri);
                                Log.e("Path of file", "thumbnail" + getThumbnailPathForLocalFile(this, uri));

                                /*text = getThumbnailPathForLocalFile(this, uri);
                               *//* thumbnailString = BitMapToString(ThumbnailUtils.createVideoThumbnail(tempFile.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));
                                decodeBase64(thumbnailString);*//*
                                //create uri for thumbnail bitmap
                               *//* Uri tempUri = ImageUtils.getImageUri(CreatePostActivity.this, mThumbnailVideo);

                                // CALL THIS METHOD TO GET THE ACTUAL PATH
                                //File mUriThumbnail = new File(ImageUtils.getRealPathFromURI(CreatePostActivity.this, tempUri));
                                String thumbnailpath = ImageUtils.getFilePath(CreatePostActivity.this, tempUri);

                                thumbnailString = thumbnailpath;*//*
                               // BitMapToString(ThumbnailUtils.createVideoThumbnail(tempFile.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));


                               *//* viewholder_image.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent videoIntent = new Intent(CreatePostActivity.this, VideoViewActivity.class);
                                        Bundle b = new Bundle();
                                        b.putString(Constants.FRAGMENT_NAME, "CREATEPOST");
                                        b.putString(Constants.VIDEO, tempFile.getPath());

                                        videoIntent.putExtras(b);
                                        startActivity(videoIntent);
                                    }
                                });*/
                            }
                        } finally {
                            if (cursor != null) {
                                cursor.close();
                            }
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getThumbnailPathForLocalFile(Activity context,
                                               Uri fileUri) {

        long fileId = getFileId(context, fileUri);

        MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(),
                fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null);

        Cursor thumbCursor = null;
        try {

            thumbCursor = context.managedQuery(
                    MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                    thumbColumns, MediaStore.Video.Thumbnails.VIDEO_ID + " = "
                            + fileId, null, null);

            if (thumbCursor.moveToFirst()) {
                String thumbPath = thumbCursor.getString(thumbCursor
                        .getColumnIndex(MediaStore.Video.Thumbnails.DATA));

                return thumbPath;
            }

        } finally {
        }

        return null;
    }

    public long getFileId(Activity context, Uri fileUri) {
        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            return cursor.getInt(columnIndex);
        }
        return 0;
    }


    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            ChosenImage image = list.get(0);
            if (image != null) {
                if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                    pickerPath = image.getThumbnailPath();
                else
                    pickerPath = image.getOriginalPath();
                viewholder_image.setVisibility(View.VISIBLE);
                viewholder_image.setImageURI(Uri.fromFile(new File(pickerPath)));
            } else
                showSnack("Invalid Image");
        }
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
            Intent intent = intentBuilder.build(this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    public void createPostApi() {


        if (postType.equalsIgnoreCase(Constants.WALL)) {
            if (isConnectedToInternet()) {
                if (myTaggedListIds != null && myTaggedListIds.size() >= 0) {
                    for (int i = 0; i < myTaggedListIds.size(); i++) {
                        if (i == 0) {
                            userId = String.valueOf(myTaggedListIds.get(i).getId());
                        } else {
                            userId += "," + String.valueOf(myTaggedListIds.get(i).getId());
                        }
                    }
                }

            /*if (arrayListTagFriends != null && arrayListTagFriends.size() >= 0) {
                for (int i = 0; i < arrayListTagFriends.size(); i++) {
                    if (i == 0) {
                        userId = String.valueOf(arrayListTagFriends.get(i).getList().getUser_info().getId());
                    } else {
                        userId += "," + String.valueOf(arrayListTagFriends.get(i).getList().getUser_info().getId());
                    }
                }
            }*/
                Log.e("User id for tagging", "Tagged users" + userId);
                showLoading();
                if (edt_txt_post_details.getText().length() < 0 || edt_txt_post_details.getText().toString().isEmpty()) {

                    if (pickerPath.equals("") || pickerPath.isEmpty() || pickerPath == null) {
                        if (CompressedFileName == null || CompressedFileName.equals("") || CompressedFileName.isEmpty()) {
                            hideLoading();
                            showToast("Empty post cannot be created", Toast.LENGTH_SHORT);
                        } else {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().createPostWithVideo(APIServerResponse.CREATE_POST, getUserSessionId(), getUserID(), "", CompressedFileName, text, "VIDEO", "WALL", userId, this);

                            }
                        }
                    } else {
                        if (isConnectedToInternet()) {
                            showLoading();
                            ServerAPI.getInstance().createPostWithImage(APIServerResponse.CREATE_POST, getUserSessionId(), getUserID(), "", pickerPath, "IMAGE", "WALL", userId, this);
                        } else {
                            hideLoading();

                        }
                    }

                } else {

                    if (pickerPath.equals("")) {
                        if (CompressedFileName == null || CompressedFileName.equals("")) {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().createPostWithoutImage(APIServerResponse.CREATE_POST, getUserID(), getUserSessionId(), edt_txt_post_details.getText().toString(), "WALL", userId, CreatePostActivity.this);
                            } else {
                                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                            }
                        } else {
                            showLoading();
                            ServerAPI.getInstance().createPostWithVideo(APIServerResponse.CREATE_POST, getUserSessionId(), getUserID(), edt_txt_post_details.getText().toString(), CompressedFileName, text, "VIDEO", "WALL", userId, CreatePostActivity.this);
                        }

                    } else if (!pickerPath.equals("")) {

                        Glide.with(this).load(pickerPath).centerCrop().into(viewholder_image);
                        showLoading();
                        ServerAPI.getInstance().createPostWithImage(APIServerResponse.CREATE_POST, getUserSessionId(), getUserID(), edt_txt_post_details.getText().toString(), pickerPath, "IMAGE", "WALL", userId, this);

                    } else if (CompressedFileName.equals("")) {
                        showLoading();
                        ServerAPI.getInstance().createPostWithoutImage(APIServerResponse.CREATE_POST, getUserID(), getUserSessionId(), edt_txt_post_details.getText().toString(), "WALL", userId, CreatePostActivity.this);

                    }
                }
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        } else if (postType.equalsIgnoreCase(Constants.USERWALL)) {


            if (isConnectedToInternet()) {
                if (myTaggedListIds != null && myTaggedListIds.size() >= 0) {
                    for (int i = 0; i < myTaggedListIds.size(); i++) {
                        if (i == 0) {
                            userId = String.valueOf(myTaggedListIds.get(i).getId());
                        } else {
                            userId += "," + String.valueOf(myTaggedListIds.get(i).getId());
                        }
                    }
                }

            /*if (arrayListTagFriends != null && arrayListTagFriends.size() >= 0) {
                for (int i = 0; i < arrayListTagFriends.size(); i++) {
                    if (i == 0) {
                        userId = String.valueOf(arrayListTagFriends.get(i).getList().getUser_info().getId());
                    } else {
                        userId += "," + String.valueOf(arrayListTagFriends.get(i).getList().getUser_info().getId());
                    }
                }
            }*/
                Log.e("User id for tagging", "Tagged users" + userId);
                showLoading();
                if (edt_txt_post_details.getText().length() < 0 || edt_txt_post_details.getText().toString().isEmpty()) {

                    if (pickerPath.equals("") || pickerPath.isEmpty() || pickerPath == null) {
                        if (CompressedFileName == null || CompressedFileName.equals("") || CompressedFileName.isEmpty()) {
                            hideLoading();
                            showToast("Empty post cannot be created", Toast.LENGTH_SHORT);
                        } else {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().createPostWithVideo(APIServerResponse.CREATE_POST, getUserSessionId(), friendId, "", CompressedFileName, text, "VIDEO", Constants.USERWALL, userId, this);

                            }
                        }
                    } else {
                        if (isConnectedToInternet()) {
                            showLoading();
                            ServerAPI.getInstance().createPostWithImage(APIServerResponse.CREATE_POST, getUserSessionId(), friendId, "", pickerPath, "IMAGE", Constants.USERWALL, userId, this);
                        } else {
                            hideLoading();

                        }
                    }

                } else {

                    if (pickerPath.equals("")) {
                        if (CompressedFileName == null || CompressedFileName.equals("")) {
                            if (isConnectedToInternet()) {
                                showLoading();
                                ServerAPI.getInstance().createPostWithoutImage(APIServerResponse.CREATE_POST, friendId, getUserSessionId(), edt_txt_post_details.getText().toString(), Constants.USERWALL, userId, CreatePostActivity.this);
                            } else {
                                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                            }
                        } else {
                            showLoading();
                            ServerAPI.getInstance().createPostWithVideo(APIServerResponse.CREATE_POST, getUserSessionId(), friendId, edt_txt_post_details.getText().toString(), CompressedFileName, text, "VIDEO", Constants.USERWALL, userId, CreatePostActivity.this);
                        }

                    } else if (!pickerPath.equals("")) {

                        Glide.with(this).load(pickerPath).centerCrop().into(viewholder_image);
                        showLoading();
                        ServerAPI.getInstance().createPostWithImage(APIServerResponse.CREATE_POST, getUserSessionId(), friendId, edt_txt_post_details.getText().toString(), pickerPath, "IMAGE", Constants.USERWALL, userId, this);

                    } else if (CompressedFileName.equals("")) {
                        showLoading();
                        ServerAPI.getInstance().createPostWithoutImage(APIServerResponse.CREATE_POST, friendId, getUserSessionId(), edt_txt_post_details.getText().toString(), Constants.USERWALL, userId, CreatePostActivity.this);

                    }
                }
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }


    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {
                UploadPostModal postModal;
                hideLoading();
                switch (tag) {
                    case APIServerResponse.CREATE_POST:
                        postModal = (UploadPostModal) response.body();
                        if (postModal.getStatus().equals("1")) {

                            EventBus.getDefault().post(new EventBusHomeWallPostCreated("1"));
                            showToast("Post created successfully", Toast.LENGTH_SHORT);
                            pickerPath = "";
                            finish();

                            /*Intent home = new Intent(this, HomeActivity.class);

                            startActivity(home);*/

                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    private String readableFileSize(long size) {
        if (size <= 0) return size + " B";
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private String fileSize(File file) {
        return readableFileSize(file.length());
    }


    public void createVideo() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {


            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            startActivityForResult(takeVideoIntent, DEMO_RECORD_VIDEO_COMPRESSED);


          /*  Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            // create a file to save the video
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);


            // set the video image quality to high
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

            // start the Video Capture Intent
            startActivityForResult(intent, RESULT_CODE_COMPRESS_VIDEO);*/
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    private static Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraVideo");


        // Create the storage directory(MyCameraVideo) if it does not exist
        if (!mediaStorageDir.exists()) {

            if (!mediaStorageDir.mkdirs()) {

                Log.e("", "Failed to create directory MyCameraVideo.");


                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }
        java.util.Date date = new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());

        File mediaFile;

        if (type == MEDIA_TYPE_VIDEO) {

            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");

        } else {
            return null;
        }

        return mediaFile;
    }
}
