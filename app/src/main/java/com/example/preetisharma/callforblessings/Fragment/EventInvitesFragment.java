package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.EventsInvitesAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventsInvitesModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/7/2017.
 */

public class EventInvitesFragment extends Fragment implements APIServerResponse {


    RecyclerView rv_events_invitation_recycler_view;
    List<EventsInvitesModal.ListBean> list = new ArrayList<>();
    EventsInvitesAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_events_invitations, container, false);
        ButterKnife.bind(this, rootView);

        rv_events_invitation_recycler_view = (RecyclerView) rootView.findViewById(R.id.rv_events_invitation_recycler_view);


        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().getEventInvites(APIServerResponse.EVENT_INVITES, ((BaseActivity) getActivity()).getUserSessionId(), EventInvitesFragment.this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().getEventInvites(APIServerResponse.EVENT_INVITES, ((BaseActivity) getActivity()).getUserSessionId(), EventInvitesFragment.this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            ((BaseActivity) getActivity()).hideLoading();
            EventsInvitesModal eventsInvitesModal;
            if (response.isSuccessful()) {

                switch (tag) {
                    case APIServerResponse.EVENT_INVITES:
                        eventsInvitesModal = (EventsInvitesModal) response.body();
                        if (eventsInvitesModal.getStatus().equals("1")) {

                            list = eventsInvitesModal.getList();
                        } else {
                            ((BaseActivity) getActivity()).showToast("No Event Invitations", Toast.LENGTH_LONG);
                        }

                        adapter = new EventsInvitesAdapter(getActivity(), list);

                        rv_events_invitation_recycler_view.setAdapter(adapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        rv_events_invitation_recycler_view.setLayoutManager(mLayoutManager);
                        break;

                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            switch (tag) {
                case APIServerResponse.EVENT_INVITES:
                    throwable.printStackTrace();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
