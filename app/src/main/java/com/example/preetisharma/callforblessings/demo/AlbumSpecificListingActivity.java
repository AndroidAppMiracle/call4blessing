package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.AlbumListAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSongDownloadComplete;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/27/2017.
 */

public class AlbumSpecificListingActivity extends BaseActivity implements APIServerResponse {

    String albumId;
    AlbumListAdapter _adapter;
    List<AlbumListingModal.DetailBean> mList = new ArrayList<>();
    @BindView(R.id.album_recycler_view)
    RecyclerView album_recycler_view;
    @BindView(R.id.album_image)
    AppCompatImageView album_image;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    List<AlbumListingModal.DetailBean.FilesBean> songslist = new ArrayList<>();
    AlbumListingModal albumModal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_album_listing);
        updateStatusBar();
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        txtvw_header_title.setText("");
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getIntent().getExtras() != null) {
            albumId = String.valueOf(getIntent().getExtras().getInt(Constants.ALBUMID));
        }
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().albumListing(APIServerResponse.ALBUM_LISTING, getUserSessionId(), albumId, this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        hideLoading();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        AlbumListingModal albumModal;
        if (response.isSuccessful()) {
            switch (tag) {
                case APIServerResponse.ALBUM_LISTING: {
                    albumModal = (AlbumListingModal) response.body();
                    if (albumModal.getStatus().equals("1")) {

                        txtvw_header_title.setText(albumModal.getDetail().getName());
                        if (!albumModal.getDetail().getAlbum_cover_image().equalsIgnoreCase("")) {
                            Glide.with(this).load(albumModal.getDetail().getAlbum_cover_image()).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).crossFade().into(album_image);
                        } else {
                            Glide.with(this).load(albumModal.getDetail().getAlbum_cover_image()).placeholder(R.drawable.placeholder_callforblessings).crossFade().into(album_image);

                        }
                        songslist = albumModal.getDetail().getFiles();

                        _adapter = new AlbumListAdapter(this, songslist, albumModal.getDetail(), false);
                        album_recycler_view.setAdapter(_adapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                        album_recycler_view.setLayoutManager(mLayoutManager);
                        /*for (int i = 0; i < albumModal.getDetail().getFiles().size(); i++) {
                            PlayMusicModal playMusicModal = new PlayMusicModal();
                            playMusicModal.setMediaUrl(albumModal.getDetail().getFiles().get(i).getMusic_file());
                            playMusicModal.setThumbnailUrl(albumModal.getDetail().getAlbum_cover_image());
                            playMusicModal.setArtworkUrl(albumModal.getDetail().getAlbum_cover_image());
                            playMusicModal.setTitle(albumModal.getDetail().getFiles().get(i).getSong_name());
                            playMusicModal.setAlbum(albumModal.getDetail().getName());
                            playMusicModal.setArtist(albumModal.getDetail().getName());
                            playAlbum.add(playMusicModal);

                        }*/
                    }


                }
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }


    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }


    @Subscribe
    public void onDownloadComplete(EventBusSongDownloadComplete eventBusSongDownloadComplete) {
        //int pos = eventBusSongDownloadComplete.getMessage();
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().albumListing(APIServerResponse.ALBUM_LISTING, getUserSessionId(), albumId, this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        /*albumModal.getDetail().getFiles().get(eventBusSongDownloadComplete.getMessage()).setIs_downloaded(Constants.TRUE);
        _adapter.notifyDataSetChanged();*/
    }
}
