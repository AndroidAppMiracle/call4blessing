package com.example.preetisharma.callforblessings;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.HTPTabAdapter;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.mypth.MyPthTabs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by preeti.sharma on 1/30/2017.
 */

public class HTPLayoutActivity extends BaseActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    private TabLayout tabLayout;
    private boolean doubleBackToExitPressedOnce;
    //This is our viewPager
    private ViewPager viewPager;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.appCompatTextView_htp)
    AppCompatTextView appCompatTextView_htp;
    @BindView(R.id.txtvw_chat)
    AppCompatTextView txtvw_chat;
    HTPTabAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.htp_layout_activity);
        updateStatusBar();

        ButterKnife.bind(this);
        img_view_change_password.setImageResource(R.drawable.folder);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        //Adding toolbar to the activity
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        txtvw_header_title.setText("PTH");


        //This is our tablayout
        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout_htp);

        //Adding the tabs using addTab() method

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager_htp);


        //Creating our pager adapter
        createTabWithIcons();
        tabWidth(tabLayout.getTabCount());
        adapter = new HTPTabAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(0);

        //Adding onTabSelectedListener to swipe views
        tabLayout.addOnTabSelectedListener(HTPLayoutActivity.this);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                //Page is scrolling
            }

            @Override
            public void onPageSelected(int position) {

                //page is selected
            }

            @Override
            public void onPageScrollStateChanged(int state) {

                //state of the page while scrolling

            }
        });
        tabLayout.setupWithViewPager(viewPager);


    }

    public void createTabWithIcons() {

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        TextView tabHome = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHome.setText("Prayer");

        tabLayout.addTab((tabLayout.newTab().setCustomView(tabHome)));
        TextView tabFriends = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFriends.setText("Testimony");
        tabLayout.addTab((tabLayout.newTab().setCustomView(tabFriends)));
        TextView tabNotifications = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabNotifications.setText("Help Request");
        tabLayout.addTab((tabLayout.newTab().setCustomView(tabNotifications)));
    }

    public int tabWidth(int count) {
        return getWindowManager().getDefaultDisplay().getWidth() / count;
    }


    public void changeHeader(String title) {
        txtvw_header_title.setText(title);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment f = adapter.getItem(tab.getPosition());

        if (f != null) {

            Fragment fragment = (Fragment) f;
            ft.detach(fragment);
            ft.attach(fragment);

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    public void backPress() {

        super.onBackPressed();
        finish();


    }


    @OnClick(R.id.img_view_change_password)
    public void myPth() {

        Intent myPth = new Intent(HTPLayoutActivity.this, MyPthTabs.class);
        startActivity(myPth);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        viewPager.requestFocus();
        switch (position) {
            case 0:
                img_view_back.setClickable(false);
                img_view_change_password.setClickable(true);
                img_view_back.setVisibility(View.VISIBLE);
                img_view_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        backPress();
                    }
                });
                appCompatTextView_htp.setVisibility(View.GONE);
                txtvw_chat.setVisibility(View.GONE);
                img_view_change_password.setImageResource(R.drawable.ic_bible);
                img_view_change_password.setVisibility(View.GONE);
                txtvw_header_title.setText("PRAYER");
                viewPager.requestFocus();

                break;
            case 1:
                txtvw_header_title.setText("TESTIMONY");
                img_view_back.setClickable(false);
                img_view_change_password.setClickable(false);
                img_view_back.setVisibility(View.VISIBLE);
                img_view_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        backPress();
                    }
                });
                img_view_back.setImageResource(R.drawable.ic_back);
                appCompatTextView_htp.setVisibility(View.GONE);
                txtvw_chat.setVisibility(View.GONE);
                img_view_change_password.setImageResource(R.drawable.ic_bible);
                img_view_change_password.setVisibility(View.GONE);
                img_view_back.setImageResource(R.drawable.ic_back);
                img_view_change_password.setVisibility(View.GONE);
                viewPager.requestFocus();
                break;
            case 2:
                txtvw_header_title.setText("HELP REQUEST");
                img_view_back.setClickable(false);
                img_view_change_password.setClickable(false);
                img_view_back.setVisibility(View.VISIBLE);
                img_view_back.setImageResource(R.drawable.ic_back);
                img_view_change_password.setVisibility(View.GONE);
                appCompatTextView_htp.setVisibility(View.GONE);
                txtvw_chat.setVisibility(View.GONE);
                img_view_change_password.setVisibility(View.GONE);
                img_view_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        backPress();
                    }
                });
                img_view_change_password.setImageResource(R.drawable.ic_bible);
                viewPager.requestFocus();
                break;

            default:
                txtvw_header_title.setText("PRAYER");
                img_view_back.setClickable(false);
                img_view_change_password.setClickable(false);
                img_view_back.setVisibility(View.VISIBLE);
                img_view_back.setImageResource(R.drawable.ic_video);
                img_view_change_password.setVisibility(View.VISIBLE);
                appCompatTextView_htp.setVisibility(View.GONE);
                txtvw_chat.setVisibility(View.GONE);
                img_view_change_password.setImageResource(R.drawable.ic_bible);
                img_view_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        backPress();
                    }
                });
                viewPager.requestFocus();
                break;
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }
}
