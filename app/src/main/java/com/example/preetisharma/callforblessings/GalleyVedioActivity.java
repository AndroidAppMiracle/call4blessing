package com.example.preetisharma.callforblessings;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GalleryVideosAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GalleryModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.GalleryActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class GalleyVedioActivity extends BaseActivity  implements APIServerResponse{

    @BindView(R.id.gallery_images_recycler_view)
    RecyclerView gallery_images_recycler_view;
    ArrayList<GalleryModal.ListBean> mList;
    GalleryVideosAdapter _adapter;
    AppCompatImageView img_view_back;

    private static final int RC_CAMERA_PERM = 342;
    private static final int RC_GALLERY_PERM = 545;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private final static int CAMERA_RQ = 6969;
    private static final int RESULT_CODE_COMPRESS_VIDEO = 3;
    private static final int DEMO_RECORD_VIDEO_COMPRESSED = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galley_vedio);
        ButterKnife.bind(this);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GalleyVedioActivity.this.finish();
            }
        });

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().galleryListing(APIServerResponse.GALLERYVIDEO,getUserSessionId(), "VIDEO",this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


    }
    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }
    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickVideoSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("video/*");
            startActivityForResult(intent, RESULT_CODE_COMPRESS_VIDEO);
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }
    public void createVideo() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
           startActivityForResult(takeVideoIntent, DEMO_RECORD_VIDEO_COMPRESSED);
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @OnClick(R.id.img_view_change_password)
    public void uploadVedio()
    {
        hasPermissionInManifest(this, Manifest.permission.CAMERA);
        new AlertDialog.Builder(this)

                .setTitle("Select Video")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        createVideo();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickVideoSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        if (this != null) {
            hideLoading();
        }
        mList = new ArrayList<>();
        GalleryModal galleryData = null;
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.GALLERYVIDEO:

                    {
                        galleryData = (GalleryModal) response.body();


                        if (galleryData.getStatus().equals("1")) {
                            mList = galleryData.getList();


                        } else {
                            showToast(response.message(), Toast.LENGTH_SHORT);
                        }

                    }
                    _adapter = new GalleryVideosAdapter(this, mList);
                    gallery_images_recycler_view.setAdapter(_adapter);
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(gallery_images_recycler_view.getContext(), DividerItemDecoration.VERTICAL);
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
                    gallery_images_recycler_view.setLayoutManager(mLayoutManager);

                    gallery_images_recycler_view.addItemDecoration(dividerItemDecoration);
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
