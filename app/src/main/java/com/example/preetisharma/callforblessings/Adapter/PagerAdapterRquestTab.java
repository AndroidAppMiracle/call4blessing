package com.example.preetisharma.callforblessings.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.preetisharma.callforblessings.Fragment.PrayerFragment;
import com.example.preetisharma.callforblessings.Fragment.TestimonyFragment;

/**
 * Created by preeti.sharma on 1/3/2017.
 */

public class PagerAdapterRquestTab extends FragmentStatePagerAdapter {
    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public PagerAdapterRquestTab(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PrayerFragment prayer = new PrayerFragment();
                return prayer;
            case 1:
                TestimonyFragment testimonyFragment = new TestimonyFragment();
                return testimonyFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
