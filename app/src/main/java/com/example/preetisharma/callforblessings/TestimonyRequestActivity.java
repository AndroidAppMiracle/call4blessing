package com.example.preetisharma.callforblessings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerRequestModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/2/2017.
 */

public class TestimonyRequestActivity extends BaseActivity implements ImagePickerCallback, EasyPermissions.PermissionCallbacks, APIServerResponse {


    @BindView(R.id.edt_txt_name)
    AppCompatEditText edt_txt_name;
    @BindView(R.id.edt_txt_title)
    AppCompatEditText edt_txt_title;
    @BindView(R.id.edt_txt_description)
    AppCompatEditText edt_txt_description;
    @BindView(R.id.imgvw_upload_image)
    AppCompatImageView imgvw_upload_image;

    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private String pickerPath = "";
    private static final int PLACE_PICKER_REQUEST = 1;
    String[] perms = {Manifest.permission.CAMERA, Manifest.permission.INTERNET};
    private int RC_LOCATION_INTERNET = 102;
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;
    @BindView(R.id.edt_txt_video_url)
    AppCompatEditText edt_txt_video_url;
    @BindView(R.id.txtvw_send)
    AppCompatTextView txtvw_send;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_testimony_request_activity);
        updateStatusBar();
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                onBackPressed();
            }
        });
        txtvw_header_title.setText("Share Testimony");
        txtvw_send.setText("Share");
    }

    @OnClick(R.id.imgvw_upload_image)
    public void UploadImage() {
        imagePickerDialog();
    }

    public void validation() {
        if (edt_txt_name.getText().toString().isEmpty()) {
            showToast(Constants.PRAYER_NAMENOT_BLANK, Toast.LENGTH_SHORT);
        } else if (edt_txt_title.getText().toString().isEmpty()) {
            showToast(Constants.PRAYER_TITLE, Toast.LENGTH_SHORT);
        } else {
            if (!isConnectedToInternet()) {
                showToast("Check your internet connection.", Toast.LENGTH_SHORT);
            } else {
                showLoading();
                if (pickerPath.equalsIgnoreCase("")) {
                    showLoading();
                    ServerAPI.getInstance().requestTestimonyWithoutImage(APIServerResponse.Prayer_request, edt_txt_name.getText().toString(), edt_txt_title.getText().toString(), edt_txt_description.getText().toString(), edt_txt_video_url.getText().toString(), this);
                } else {
                    showLoading();
                    ServerAPI.getInstance().requestTestimonyWithImage(APIServerResponse.Prayer_request, edt_txt_name.getText().toString(), edt_txt_title.getText().toString(), edt_txt_description.getText().toString(), edt_txt_video_url.getText().toString(), pickerPath, this);
                }
            }
        }

    }

    @OnClick(R.id.txtvw_send)
    void request() {

        validation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
           /* if (requestCode == PLACE_PICKER_REQUEST) {

                final Place place = PlacePicker.getPlace(this, data);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();


                edt_txt_location.setText(name);
                txtvw_state.setText(place.getLocale().getCountry());
                txtvw_country.setText(place.getAddress());


            }*/
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void imagePickerDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_launcher)
                .show();
    }


    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            imgvw_upload_image.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);
    }


    @Override
    public void onSuccess(int tag, Response response) {

        PrayerRequestModal requestModal;
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.Prayer_request:

                        Log.e("message", "Message" + response.toString());
                        requestModal = (PrayerRequestModal) response.body();


                        if (requestModal.getStatus().equals("1")) {
                            showToast("Testimony Uploaded successfully", Toast.LENGTH_SHORT);
                            onBackPressed();
                        } else {
                            showToast(requestModal.getMessage(), Toast.LENGTH_SHORT);
                        }

                        hideLoading();
                        break;
                }

            } catch (Exception e) {
                hideLoading();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        switch (tag) {
            case APIServerResponse.Prayer_request:
                System.out.println("Error");
                break;
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        try {
            hideKeyboard();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
