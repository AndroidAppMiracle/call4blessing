package com.example.preetisharma.callforblessings.fcm;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AlarmRequestAcceptModal;
import com.example.preetisharma.callforblessings.Server.Modal.AlarmRequestCountModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Response;


public class NotifyAlertDialog extends BaseActivity implements View.OnClickListener, APIServerResponse {

    Button mBtnAccept, mBtnReject, mBtnPrayerRequestAccepted;
    TextView mTxtVwCountdown, tv_accept_reject_title/*, mtxtVwDialogText*/;
    /*RelativeLayout mRltvButtons;*/
    boolean isRequestAccepted = false, isIntervalFinished;
    int count = 0;
    int numberOfPeopleAccepted = 0;
    static final long REQUIRED_INTERVAL = 1800000;//half an hour in milliseconds : 1800000   60000
    long REFRESH_INTERVAL = 3000;
    long intervalCounter = 0;

    //Handler hand = new Handler();
    Timer timer = new Timer();
    public Handler mHandler;
    MediaPlayer player;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_alert_dialog);
        mBtnAccept = (Button) findViewById(R.id.btnAccept);
        mBtnAccept.setOnClickListener(this);
        mBtnReject = (Button) findViewById(R.id.btnReject);
        mBtnReject.setOnClickListener(this);
        mBtnPrayerRequestAccepted = (Button) findViewById(R.id.btnRequestAccepted);
        mBtnPrayerRequestAccepted.setOnClickListener(this);
        mTxtVwCountdown = (TextView) findViewById(R.id.txtVwCountdown);
        tv_accept_reject_title = (TextView) findViewById(R.id.tv_accept_reject_title);
        //mtxtVwDialogText = (TextView)findViewById(R.id.txtVwDialogInfo);
        /*mRltvButtons = (RelativeLayout)findViewById(R.id.rltvBtns);*/

        try {
            if (getIntent().hasExtra("NOTIFICATION_MESSAGE")) {
            /*eventID = getIntent().getExtras().getString(Constants.EVENT_ID);*/
                tv_accept_reject_title.setText(getIntent().getExtras().getString("NOTIFICATION_MESSAGE"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            player = MediaPlayer.create(this,
                    R.raw.call4blessing_alarm);
            player.start();

            /*player = MediaPlayer.create(this,
                    Settings.System.DEFAULT_NOTIFICATION_URI);
            player.start();*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        updateUI();


        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        if (!isIntervalFinished) {
                            // Toast.makeText(getBaseContext(), "updateUI.. ", Toast.LENGTH_SHORT).show();
                            updateUI();
                        } else {
                            finish();
                        }
                        break;
                }
            }
        };


        //Declare the timer

//Set the schedule function and rate
        timer.scheduleAtFixedRate(new TimerTask() {

                                      @Override
                                      public void run() {


                                          if (intervalCounter == REQUIRED_INTERVAL) {
                                              Log.d("++++++++++", "Interval counter:" + intervalCounter);
                                              isIntervalFinished = true;
                                          } else {
                                              isIntervalFinished = false;

                                              ServerAPI.getInstance().alarmRequestCount(APIServerResponse.ALARM_REQUEST_COUNT, getUserSessionId(), NotifyAlertDialog.this);
                                             /* int incrementCount = 0;
                                              if (numberOfPeopleAccepted != incrementCount) {

                                                  count = count + numberOfPeopleAccepted;
                                                  incrementCount = numberOfPeopleAccepted;
                                              }*/

                                              Log.i("Count", "" + count);
                                              /*count = count + 1;*/
                                              //hit api to fetch updated data

                                          }
                                          intervalCounter = intervalCounter + REFRESH_INTERVAL;

                                      }

                                  },
//Set how long before to start calling the TimerTask (in milliseconds)
                REFRESH_INTERVAL,
//Set the amount of time between each execution (in milliseconds)
                REFRESH_INTERVAL);



        /*//We must use this function in order to change the text view text
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if(!isIntervalFinished)
                updateUI();
                else
                    finish();
            }

        });*/

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAccept:
                //Call Api
                /*Toast.makeText(getBaseContext(), "Accepted request", Toast.LENGTH_SHORT).show();*/
                mBtnAccept.setEnabled(false);
                mBtnAccept.setAlpha(0.3f);
                if (player != null) {
                    if (player.isPlaying()) {
                        player.stop();
                    }
                    player.release();
                    player = null;
                }
                ServerAPI.getInstance().acceptAlarmRequest(APIServerResponse.ALARM_REQUEST_RESPONSE, getUserSessionId(), NotifyAlertDialog.this);


                break;
            case R.id.btnReject:

                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
                if (player != null) {
                    if (player.isPlaying()) {
                        player.stop();
                    }
                    player.release();
                    player = null;
                }
               /* if (mHandler != null) {
                    mHandler.removeCallbacks(run);
                }*/
                showToast("Prayer request is rejected by you.", Toast.LENGTH_SHORT);
                //Toast.makeText(NotifyAlertDialog.this, "Prayer request is rejected by you.", Toast.LENGTH_SHORT).show();


                this.finish();
                //Toast.makeText(NotifyAlertDialog.this, " "+isDestroyed(), Toast.LENGTH_SHORT).show();

                break;

            case R.id.btnRequestAccepted:

                extraDetailsDialog();
               /* AlertDialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(NotifyAlertDialog.this);
                builder.setMessage("You are watching countdown for people accepting prayer, Do you want to stop watching?")
                        .setIcon(R.mipmap.ic_app_icon).setCancelable(false);
                // Add the buttons
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        NotifyAlertDialog.this.finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.dismiss();
                    }
                });
                // Create the AlertDialog
                dialog = builder.create();
                dialog.show();*/
                break;
        }
    }


  /*  public Runnable run = new Runnable() {
        @Override
        public void run() {
            updateUI();
        }
    };*/


    private void updateUI() {
        //Toast.makeText(getBaseContext(), "updateUI.. " + count, Toast.LENGTH_SHORT).show();
        mTxtVwCountdown.setText("Prayer Request accepted by " + count + " people.");
    }


    @Override
    public void onBackPressed() {
        try {

            if (player != null) {
                if (player.isPlaying()) {
                    player.stop();
                }
                player.release();
                player = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                AlarmRequestAcceptModal alarmRequestAcceptModal;
                AlarmRequestCountModal alarmRequestCountModal;
                switch (tag) {
                    case APIServerResponse.ALARM_REQUEST_RESPONSE:
                        alarmRequestAcceptModal = (AlarmRequestAcceptModal) response.body();
                        if (alarmRequestAcceptModal.getStatus().equals("1")) {
                            //numberOfPeopleAccepted = alarmRequestAcceptModal.get

                            Log.i("getStatus()", "" + alarmRequestAcceptModal.getStatus());
                            Log.i("getMessage()", "" + alarmRequestAcceptModal.getMessage());
                            //Toast.makeText(getBaseContext(), alarmRequestAcceptModal.getMessage(), Toast.LENGTH_SHORT).show();
                            isRequestAccepted = true;

                            /*if (isRequestAccepted) {*/
                            //mRltvButtons.setVisibility(View.GONE);
                            mBtnAccept.setVisibility(View.GONE);
                            mBtnReject.setVisibility(View.GONE);
                            mBtnPrayerRequestAccepted.setVisibility(View.VISIBLE);


                            /*}*/
                        } else {
                            isRequestAccepted = false;
                            mBtnAccept.setVisibility(View.VISIBLE);
                            mBtnReject.setVisibility(View.VISIBLE);
                            mBtnPrayerRequestAccepted.setVisibility(View.GONE);
                        }
                        break;

                    case APIServerResponse.ALARM_REQUEST_COUNT:
                        alarmRequestCountModal = (AlarmRequestCountModal) response.body();
                        if (alarmRequestCountModal.getStatus().equals("1")) {
                            count = Integer.parseInt(alarmRequestCountModal.getCount());
                            Log.i("numberOfPeopleAccepted", "" + count);
                            //mHandler.obtainMessage(1).sendToTarget();
                            mHandler.sendEmptyMessage(1);
                        }
                        break;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

   /* @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (player.isPlaying()) {
                player.stop();
            }
            if (player != null) {
                player.release();
                player = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    private void extraDetailsDialog() {
        try {
            final Dialog dialog = new Dialog(NotifyAlertDialog.this);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            //dialog.setTitle(R.string.title_activity_product_detail);
            dialog.setContentView(R.layout.custom_notification_dialog_layout);

            AppCompatTextView txtvw_accept = (AppCompatTextView) dialog.findViewById(R.id.txtvw_accept);

            AppCompatTextView txtvw_cancel = (AppCompatTextView) dialog.findViewById(R.id.txtvw_cancel);

        /*View view = (View) dialog.findViewById(R.id.view1);
        view.setVisibility(View.GONE);*/


            txtvw_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                    if (player != null) {
                        if (player.isPlaying()) {
                            player.stop();
                        }
                        player.release();
                        player = null;
                    }
                    if (timer != null) {
                        timer.cancel();
                        timer = null;
                    }

                }
            });

            txtvw_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //accept_event or Edit  event
                        dialog.dismiss();

                        if (player != null) {
                            if (player.isPlaying()) {
                                player.stop();
                            }
                            player.release();
                            player = null;
                        }
                        if (timer != null) {
                            timer.cancel();
                            timer = null;
                        }
                        NotifyAlertDialog.this.finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
