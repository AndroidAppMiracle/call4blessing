package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionCheckModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllSongsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.JoyMusicSubscriptionActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by Suleiman on 19/10/16.
 */

public class PaginationSongsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM = 0;
    public static final int LOADING = 1;
    //private static final String BASE_URL_IMG = "https://image.tmdb.org/t/p/w150";

    private List<JoyMusicViewAllSongsModal.ListBean> movieResults;
    private ArrayList<PlayMusicModal> arrayListMusic = new ArrayList<>();
    private Context context;

    private boolean isLoadingAdded = false;
    private int songsPlaylistID = 0;

    public PaginationSongsAdapter(Context context, int songsPlaylistID) {
        this.context = context;
        movieResults = new ArrayList<>();
        this.songsPlaylistID = songsPlaylistID;
    }

    public List<JoyMusicViewAllSongsModal.ListBean> getMovies() {
        return movieResults;
    }

    public void setMovies(List<JoyMusicViewAllSongsModal.ListBean> movieResults) {
        this.movieResults = movieResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.adapter_joy_music_view_all, parent, false);
        viewHolder = new MovieVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        JoyMusicViewAllSongsModal.ListBean result = movieResults.get(position); // Movie

        switch (getItemViewType(position)) {
            case ITEM:
                final MovieVH movieVH = (MovieVH) holder;

                movieVH.atv_Name.setText(result.getSong_name());


               /* movieVH.mYear.setText(
                        result.getReleaseDate().substring(0, 4)  // we want the year only
                                + " | "
                                + result.getOriginalLanguage().toUpperCase()
                );
                movieVH.mMovieDesc.setText(result.getOverview());*/

                /**
                 * Using Glide to handle image loading.
                 * Learn more about Glide here:
                 * <a href="http://blog.grafixartist.com/image-gallery-app-android-studio-1-4-glide/" />
                 */
                Glide
                        .with(context)
                        .load(result.getAlbum_cover_image())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                // TODO: 08/11/16 handle failure
                                movieVH.mProgress.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                // image ready, hide progress now
                                movieVH.mProgress.setVisibility(View.GONE);
                                return false;   // return false if you want Glide to handle everything else.
                            }
                        })
                        .diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image
                        .centerCrop()
                        .crossFade()
                        .into(movieVH.atv_AlbumCover);

                break;

            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return movieResults == null ? 0 : movieResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == movieResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(JoyMusicViewAllSongsModal.ListBean r) {
        movieResults.add(r);
        notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<JoyMusicViewAllSongsModal.ListBean> moveResults) {
        for (JoyMusicViewAllSongsModal.ListBean result : moveResults) {
            add(result);
        }
    }

    public void remove(JoyMusicViewAllSongsModal.ListBean r) {
        int position = movieResults.indexOf(r);
        if (position > -1) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new JoyMusicViewAllSongsModal.ListBean());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = movieResults.size() - 1;
        JoyMusicViewAllSongsModal.ListBean result = getItem(position);

        if (result != null) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public JoyMusicViewAllSongsModal.ListBean getItem(int position) {
        return movieResults.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class MovieVH extends RecyclerView.ViewHolder implements APIServerResponse {
        //private TextView mMovieTitle;
        // private TextView mMovieDesc;
        // private TextView mYear; // displays "year | language"
        // private ImageView mPosterImg;
        private ProgressBar mProgress;

        AppCompatTextView atv_Name;

        AppCompatImageView atv_AlbumCover;

        LinearLayout ll_songItem;

        public MovieVH(View itemView) {
            super(itemView);

            atv_Name = (AppCompatTextView) itemView.findViewById(R.id.atv_Name);
            /*mMovieDesc = (TextView) itemView.findViewById(R.id.movie_desc);
            mYear = (TextView) itemView.findViewById(R.id.movie_year);*/
            ll_songItem = (LinearLayout) itemView.findViewById(R.id.ll_songItem);
            atv_AlbumCover = (AppCompatImageView) itemView.findViewById(R.id.atv_AlbumCover);
            mProgress = (ProgressBar) itemView.findViewById(R.id.movie_progress);

            ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    playAudio(getAdapterPosition());

                  /*  if (movieResults.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase(Constants.PAID)) {
                        if (((BaseActivity) context).isConnectedToInternet()) {
                            ((BaseActivity) context).showLoading();
                            ServerAPI.getInstance().checkjoyMusicSubscription(APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY, ((BaseActivity) context).getUserSessionId(), PaginationSongsAdapter.MovieVH.this);
                        }
                    } else {
                        playAudio(getAdapterPosition());
                    }*/
                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                            joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                                playAudio(getAdapterPosition());
                            } else {
                                Intent player = new Intent(context, JoyMusicSubscriptionActivity.class);
                                context.startActivity(player);
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) context).hideLoading();
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


    private void playAudio(int position) {
        if (!arrayListMusic.isEmpty()) {
            arrayListMusic.clear();
        }

        if (movieResults.size() < 10) {
            for (int i = 0; i < movieResults.size(); i++) {
                PlayMusicModal playMusicModal = new PlayMusicModal();
                playMusicModal.setMediaUrl(movieResults.get(i).getMusic_file());
                playMusicModal.setThumbnailUrl(movieResults.get(i).getAlbum_cover_image());
                playMusicModal.setArtworkUrl(movieResults.get(i).getAlbum_cover_image());
                playMusicModal.setTitle(movieResults.get(i).getSong_name());
                playMusicModal.setAlbum(movieResults.get(i).getAlbum_name());
                playMusicModal.setArtist(movieResults.get(i).getAlbum_name());
                playMusicModal.setPaymentType(movieResults.get(i).getPayment_type());
                playMusicModal.setDownloaded(movieResults.get(i).getIs_downloaded());
                arrayListMusic.add(playMusicModal);
            }
        } else {
            for (int i = 0; i < 10; i++) {
                PlayMusicModal playMusicModal = new PlayMusicModal();
                playMusicModal.setMediaUrl(movieResults.get(i).getMusic_file());
                playMusicModal.setThumbnailUrl(movieResults.get(i).getAlbum_cover_image());
                playMusicModal.setArtworkUrl(movieResults.get(i).getAlbum_cover_image());
                playMusicModal.setTitle(movieResults.get(i).getSong_name());
                playMusicModal.setAlbum(movieResults.get(i).getAlbum_name());
                playMusicModal.setArtist(movieResults.get(i).getAlbum_name());
                playMusicModal.setPaymentType(movieResults.get(i).getPayment_type());
                playMusicModal.setDownloaded(movieResults.get(i).getIs_downloaded());
                arrayListMusic.add(playMusicModal);
            }
        }


        Intent intent = new Intent(context, AudioPlayerActivity.class);
        intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, position);
        intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, songsPlaylistID);
        intent.putExtra(Constants.SONG_NAME, movieResults.get(position).getSong_name());
        intent.putExtra(Constants.ALBUM_COVER, movieResults.get(position).getAlbum_cover_image());
        intent.putExtra(Constants.MUSIC, Constants.SONG);

        intent.putExtra(Constants.ALBUM_NAME, movieResults.get(position).getAlbum_name());
        intent.putExtra(Constants.TYPE, Constants.SONG_CAPS);
        intent.putExtra(Constants.SONG_ID, movieResults.get(position).getId());
        intent.putExtra(Constants.SONG_DOWNLOAD_URL, movieResults.get(position).getMusic_file());
        intent.putExtra(Constants.POSITION, position);
        intent.putExtra(Constants.IS_DOWNLOADED, movieResults.get(position).getIs_downloaded());

        Bundle b = new Bundle();
        b.putParcelableArrayList(Constants.SONGS_LIST, arrayListMusic);
        intent.putExtra("songs_bundle", b);
        //intent.putParcelableArrayListExtra(Constants.SONGS_LIST, songsBeanArrayList);
        context.startActivity(intent);
    }


}
