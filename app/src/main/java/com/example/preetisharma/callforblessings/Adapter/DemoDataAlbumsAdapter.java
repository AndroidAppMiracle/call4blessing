package com.example.preetisharma.callforblessings.Adapter;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.CreateNewAlbumActivity;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by satoti.garg on 8/3/2017.
 */

public class DemoDataAlbumsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ImagePickerCallback, EasyPermissions.PermissionCallbacks{

    public static final int ADD_ALBUM = 0;
    public final int ALBUM = 1;

    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private String pickerPath = "";
    private static final int PLACE_PICKER_REQUEST = 1;
    String[] perms = {android.Manifest.permission.CAMERA, Manifest.permission.INTERNET};
    private int RC_LOCATION_INTERNET = 102;
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;

    private Context mContext;
    private List<DemoDataModal> myGroupsList = new ArrayList<>();
    private SparseBooleanArray selectedItems;

    private int x, y;

    public DemoDataAlbumsAdapter(Context mContext, List<DemoDataModal> list) {
        this.myGroupsList = list;
        this.mContext = mContext;
        selectedItems = new SparseBooleanArray();
        x = (int) mContext.getResources().getDimension(R.dimen.height_video_item);
        y = (int) mContext.getResources().getDimension(R.dimen.height_video_item);
    }


    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            //Return "Create New Album" Item
            return ADD_ALBUM;
        } else {
            //Return Albums Item
            return ALBUM;
        }

        //return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;

        switch (viewType) {
            case ADD_ALBUM:
                v = inflater.inflate(R.layout.adapter_album_add_new_layout, parent, false);
                viewHolder = new DemoDataAlbumsAdapter.DataViewHolderAddAlbum(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;

            case ALBUM:
                v = inflater.inflate(R.layout.adapter_albums_layout, parent, false);
                viewHolder = new DemoDataAlbumsAdapter.DataViewHolderAlbum(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        try {

            switch (getItemViewType(position)) {
                case ADD_ALBUM:

                    //Do Nothing

                    break;

                case ALBUM:


                    final DemoDataAlbumsAdapter.DataViewHolderAlbum dataViewHolderAlbum = (DemoDataAlbumsAdapter.DataViewHolderAlbum) holder;

                    dataViewHolderAlbum.atv_album_name.setText(myGroupsList.get(position).getName());


                    Glide.with(mContext)
                            .load(myGroupsList.get(position).getImage())
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    dataViewHolderAlbum.mProgress.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    // image ready, hide progress now
                                    dataViewHolderAlbum.mProgress.setVisibility(View.GONE);
                                    return false;   // return false if you want Glide to handle everything else.
                                }
                            })
                    /*.diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image*/
                            .centerCrop()
                            .override(x, y)
                            .thumbnail(0.1f)
                            .crossFade()
                            .into(dataViewHolderAlbum.atv_AlbumCover);

                    holder.itemView.setActivated(selectedItems.get(position, false));
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (myGroupsList.size() != 0) {
            return myGroupsList.size();
        } else {
            return 0;
        }

    }

    public DemoDataModal getItem(int position) {
        return myGroupsList.get(position);
    }

    public void toggleSelection(int pos) {
        /*currentSelectedIndex = pos;*/
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            /*animationItemsIndex.delete(pos);*/
        } else {
            selectedItems.put(pos, true);
            /*animationItemsIndex.put(pos, true);*/
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        /*reverseAllAnimations = true;*/
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        myGroupsList.remove(position);
        //resetCurrentIndex();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {

        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            //imgvw_upload_image.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            ((BaseActivity)mContext).showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    public class DataViewHolderAlbum extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_album_name)
        AppCompatTextView atv_album_name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

       /* @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.album_progress)
        ProgressBar mProgress;

        @BindView(R.id.cv_album_item)
        CardView cv_album_item;


        public DataViewHolderAlbum(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/


            /*ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, PlaylistSongsList.class);
                    *//*Bundle b = new Bundle();
                    b.putString(Constants.GROUP_ID, String.valueOf(myGroupsList.get(getAdapterPosition()).getId()));
                    i.putExtras(b);*//*
                    mContext.startActivity(i);

                   *//* if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myGroupsList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*//*
                }
            });*/

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

    }


    public class DataViewHolderAddAlbum extends RecyclerView.ViewHolder {

        /*@BindView(R.id.atv_Name)
        AppCompatTextView atv_Name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.movie_progress)
        ProgressBar mProgress;*/

        @BindView(R.id.cv_album_item)
        CardView cv_album_item;


        public DataViewHolderAddAlbum(View v) {
            super(v);
            ButterKnife.bind(this, v);


            cv_album_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, CreateNewAlbumActivity.class);
                    mContext.startActivity(intent);
                }
            });


            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/


            /*ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, PlaylistSongsList.class);
                    *//*Bundle b = new Bundle();
                    b.putString(Constants.GROUP_ID, String.valueOf(myGroupsList.get(getAdapterPosition()).getId()));
                    i.putExtras(b);*//*
                    mContext.startActivity(i);

                   *//* if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myGroupsList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*//*
                }
            });*/

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

    }

    public void imagePickerDialog() {
        new AlertDialog.Builder(mContext)
                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }


    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        try {
            if (EasyPermissions.hasPermissions(mContext, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)) {
                cameraPicker = new CameraImagePicker(((BaseActivity)mContext));
                cameraPicker.shouldGenerateMetadata(true);
                cameraPicker.shouldGenerateThumbnails(true);
                cameraPicker.setImagePickerCallback(this);
                pickerPath = cameraPicker.pickImage();

            } else {
                EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                        RC_CAMERA_PERM, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(mContext, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)) {
            imagePicker = new ImagePicker(((BaseActivity)mContext));
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
        }
    }



}