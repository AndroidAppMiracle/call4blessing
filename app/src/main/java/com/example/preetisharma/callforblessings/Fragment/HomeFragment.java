package com.example.preetisharma.callforblessings.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.HomeWallAdapter;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusEditPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.HomeTempModal;
import com.example.preetisharma.callforblessings.Server.Modal.HomeWallModal;
import com.example.preetisharma.callforblessings.Server.Modal.UploadPostModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.listener.EndlessParentScrollListener;
import com.example.preetisharma.callforblessings.demo.BookTabs;
import com.example.preetisharma.callforblessings.demo.CreatePostActivity;
import com.example.preetisharma.callforblessings.demo.JoyMusicActivity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/12/2017.
 */

public class HomeFragment extends Fragment implements APIServerResponse, EasyPermissions.PermissionCallbacks {
    @BindView(R.id.home_listing)
    RecyclerView home_listing;
    @BindView(R.id.img_vw_profile)
    AppCompatImageView img_vw_profile;
    @BindView(R.id.edt_txt_new_post)
    AppCompatTextView edt_txt_new_post;
    @BindView(R.id.imgvw_post_image)
    AppCompatImageView imgvw_post_image;
    @BindView(R.id.imgvw_post_video)
    AppCompatImageView imgvw_post_video;
    @BindView(R.id.txtvw_no_posts)
    AppCompatTextView txtvw_no_posts;
    @BindView(R.id.imgvw_preview_post)
    AppCompatImageView imgvw_preview_post;
    @BindView(R.id.txtvw_preview_post)
    AppCompatTextView txtvw_preview_post;
    HomeWallAdapter _adapter;
    /*@BindView(R.id.lnr_layout_post_preview)
    LinearLayout lnr_layout_post_preview;*/
    @BindView(R.id.title_container)
    LinearLayout title_container;
    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private String pickerPath = "";
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int PLACE_PICKER_REQUEST = 1;
    String[] perms = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET};
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;
    private List<HomeWallModal.PostsBean> mList = new ArrayList<>();
    private int currentPage = 1;
    private LinearLayoutManager mLayoutManager;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.progressBar)
    CircleProgressBar progressBar;
    EndlessParentScrollListener scrollListener;
    private List<HomeWallModal.PostsBean> tempList = new ArrayList<>();

    @BindView(R.id.img_vw_joy)
    AppCompatImageView img_vw_joy;
    @BindView(R.id.img_vw_books)
    AppCompatImageView img_vw_books;
    @BindView(R.id.imgvw_matrimonial)
    AppCompatImageView imgvw_matrimonial;
    @BindView(R.id.imgvw_video)
    AppCompatImageView imgvw_video;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.home_fragment_layout, container, false);
        ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        ((BaseActivity) getActivity()).getmPrefs();

        callApi(currentPage);
        mLayoutManager = new LinearLayoutManager(getActivity());
        home_listing.setLayoutManager(mLayoutManager);


        try {
            img_vw_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), MaterialUpConceptActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.SHARED_PREF_USER_ID, (((BaseActivity) getActivity()).getUserID()));
                    b.putBoolean(Constants.TIMELINE_ENABLED, true);
                    b.putString(Constants.USER_IMAGE, (((BaseActivity) getActivity()).getUserImage()));
                    Log.e("Cover", "Cover Image" + (((BaseActivity) getActivity()).getUserCoverImage()));
                    b.putString(Constants.COVER_PIC, (((BaseActivity) getActivity()).getUserCoverImage()));
                    b.putString(Constants.USER_NAME, (((BaseActivity) getActivity()).getFullName()));
                    b.putInt(Constants.IS_FRIEND, Constants.USER_SELFTIMELINE);
                    i.putExtras(b);
                    startActivity(i);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        img_vw_books.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), BookTabs.class);
                /*Intent intent = new Intent(HomeActivity.this, BooksListingActivity.class);*/
                startActivity(intent);
            }
        });
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        currentPage = 1;
    }

    @OnClick(R.id.img_vw_joy)
    public void joy_musicDialog() {
        Intent intent = new Intent(getActivity(), JoyMusicActivity.class);
        startActivity(intent);

    }

    @OnClick(R.id.imgvw_matrimonial)
    public void matrimonialDialog() {
        comingSoonDialog("Matrimonial");
    }

    @OnClick(R.id.imgvw_video)
    public void videoDialog() {
        comingSoonDialog("Videos");
    }


    public void callApi(int pageNumber) {


        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                ServerAPI.getInstance().Home(APIServerResponse.HOME, ((BaseActivity) getActivity()).getUserSessionId(), pageNumber, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.title_container)
    public void createPost() {
        Intent createPostIntent = new Intent(getActivity(), CreatePostActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.POSTTYPE, Constants.WALL);
        createPostIntent.putExtras(b);
        startActivity(createPostIntent);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
            Intent intent = intentBuilder.build(getActivity());
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.imgvw_post_video)
    public void uploadVideo() {
        Intent createPostIntent = new Intent(getActivity(), CreatePostActivity.class);
        startActivity(createPostIntent);
    }

    @OnClick(R.id.imgvw_post_image)
    public void uploadImage() {

        Intent createPostIntent = new Intent(getActivity(), CreatePostActivity.class);
        startActivity(createPostIntent);

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        ((BaseActivity) getActivity()).dismissDialog();
        super.onDestroy();
    }


    public static final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";


    @OnClick(R.id.edt_txt_new_post)
    public void createPostText() {
        Intent createPostIntent = new Intent(getActivity(), CreatePostActivity.class);
        startActivity(createPostIntent);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        HomeWallModal homeModal = null;
        UploadPostModal postModal;
        HomeTempModal tempData = null;


        if (response.isSuccessful()) {

            try {
                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).hideLoading();
                }
                switch (tag) {
                    case APIServerResponse.HOME: {
                        progressBar.setVisibility(View.GONE);
                        mList.clear();
                        homeModal = (HomeWallModal) response.body();
                        if (homeModal.getStatus().equals("1")) {

                            mList = homeModal.getPosts();

                            if (mList.size() > 0 && tempList.size() != mList.size()) {
                                tempList.addAll(mList);
                                txtvw_no_posts.setVisibility(View.GONE);
                                home_listing.setVisibility(View.VISIBLE);
                                if (((BaseActivity) getActivity()).getUserGender().equalsIgnoreCase("male")) {
                                    Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.mipmap.ic_placeholder_male_avatar).centerCrop().into(img_vw_profile);
                                } else {
                                    Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.mipmap.ic_placeholder_female_avatar).centerCrop().into(img_vw_profile);

                                }
                                _adapter = new HomeWallAdapter(getActivity(), tempList);
                                home_listing.setNestedScrollingEnabled(false);
                                home_listing.setHasFixedSize(false);
                                mLayoutManager.setAutoMeasureEnabled(true);
                                home_listing.setItemAnimator(new DefaultItemAnimator());
                                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(home_listing.getContext(),
                                        mLayoutManager.getOrientation());
                                home_listing.addItemDecoration(dividerItemDecoration);
                                home_listing.setAdapter(_adapter);
                                scrollListener = new EndlessParentScrollListener(mLayoutManager) {
                                    @Override
                                    public boolean onLoadMore(int page, int totalItemsCount) {

                                        progressBar.setVisibility(View.VISIBLE);
                                        currentPage++;
                                        callApi(currentPage);
                                        return true;
                                    }
                                };
                                home_listing.addOnScrollListener(scrollListener);
                            }
                        } else if (mList.size() == 0) {
                            txtvw_no_posts.setVisibility(View.VISIBLE);
                        }

                    }
                    break;

                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            if (getActivity() != null) {
                ((BaseActivity) getActivity()).hideLoading();
            }
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


    }


    @Override
    public void onError(int tag, Throwable throwable) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideLoading();
        }
        switch (tag) {
            case APIServerResponse.HOME:
                System.out.println("Error");
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) getActivity()).hideKeyboard();
        ((BaseActivity) getActivity()).showLoading();
        tempList.clear();
        mList.clear();
        callApi(currentPage);
        if (((BaseActivity) getActivity()).getUserGender().equalsIgnoreCase("male")) {
            Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.mipmap.ic_placeholder_male_avatar).centerCrop().into(img_vw_profile);
        } else {
            Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.mipmap.ic_placeholder_female_avatar).centerCrop().into(img_vw_profile);

        }
    }

    @Subscribe
    public void onCommentPost(DemoCommentModal share) {
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                currentPage = 1;
                callApi(currentPage);
            /*    ServerAPI.getInstance().Home(APIServerResponse.HOME, ((BaseActivity) getActivity()).getUserSessionId(), this);
         */
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void onSharePost(DemoShareModal share) {
        currentPage = 1;
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                callApi(currentPage);
             /*   ServerAPI.getInstance().Home(APIServerResponse.HOME, ((BaseActivity) getActivity()).getUserSessionId(), this);
            */
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void onEditPost(EventBusEditPostModal eventBusEditPostModal) {
        if (eventBusEditPostModal.getMessage().equals("1")) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                try {
                    tempList.clear();
                    currentPage = 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ((BaseActivity) getActivity()).hideLoading();
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }
    }

    public void comingSoonDialog(String title) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage("Coming Soon")

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

}
