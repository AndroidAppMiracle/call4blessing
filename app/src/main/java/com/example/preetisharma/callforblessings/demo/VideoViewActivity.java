package com.example.preetisharma.callforblessings.demo;

import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.widget.MediaController;
import android.widget.VideoView;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 4/4/2017.
 */

public class VideoViewActivity extends BaseActivity{

    @BindView(R.id.video_view)
    VideoView video_view;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString(Constants.FRAGMENT_NAME).equalsIgnoreCase("HOMEWALLADAPTER")) {
                PlayVideoUsingServerUrl(getIntent().getExtras().getString(Constants.VIDEO));
            } else if (getIntent().getExtras().getString(Constants.FRAGMENT_NAME).equalsIgnoreCase("CREATEPOST")) {
                PlayVideoUsingUri(getIntent().getExtras().getString(Constants.VIDEO));
            }
        }

    }

    public void PlayVideoUsingUri(String uri) {
        video_view.setMediaController(new MediaController(this));
        video_view.setVideoURI(Uri.parse(Environment.getExternalStorageDirectory() + uri));
        video_view.requestFocus();
        video_view.setFitsSystemWindows(true);
        video_view.start();
       /* MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(video_view);
        video_view.setMediaController(mediaController);
        mediaPlayer = new MediaPlayer();
        try {
            File filePath = new File(uri);

            if (!filePath.exists()) {
                filePath.createNewFile();
            }

            FileInputStream is = new FileInputStream(filePath);
            mediaPlayer.setDataSource(is.getFD());
        } catch (IOException e) {
            e.printStackTrace();
        }
        video_view.requestFocus();
        video_view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                VideoViewActivity.this.finish();
            }
        });
        video_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.start();
                video_view.start();
            }
        });
        video_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub

                showDialog("This video can't play", VideoViewActivity.this);
                return true;
            }
        });*/
    }

    public void showDialog(String message, Context context) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder.setTitle("Notification !");
        alertDialogBuilder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public void PlayVideoUsingServerUrl(String url) {
        video_view.requestFocus();
        if (getIntent().getExtras() != null) {
            video_view.setVideoPath(getIntent().getExtras().getString(Constants.VIDEO));
            video_view.requestFocus();
            video_view.start();

        }
    }


}
