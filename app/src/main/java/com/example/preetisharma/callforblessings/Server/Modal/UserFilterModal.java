package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by preeti.sharma on 1/27/2017.
 */

public class UserFilterModal {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    String id;
    String userName;
}
