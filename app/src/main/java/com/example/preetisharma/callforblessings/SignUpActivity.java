package com.example.preetisharma.callforblessings;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.ValidRegisterationModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.DataHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 12/30/2016.
 */

public class SignUpActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.rd_grp_gender)
    RadioGroup rd_grp_gender;
    @BindView(R.id.rdbtn_female)
    RadioButton rdbtn_female;
    @BindView(R.id.rdbtn_male)
    RadioButton rdbtn_male;
    @BindView(R.id.edt_txt_first_name)
    AppCompatEditText edt_txt_first_name;
    @BindView(R.id.edt_txt_last_name)
    AppCompatEditText edt_txt_last_name;
    @BindView(R.id.txtvw_sign_here)
    AppCompatTextView txtvw_sign_here;
    @BindView(R.id.edt_txt_phone_number)
    AppCompatEditText edt_txt_phone_number;

    @BindView(R.id.edt_txt_email)
    AppCompatEditText edt_txt_email;
    @BindView(R.id.edt_txt_password)
    AppCompatEditText edt_txt_password;
    @BindView(R.id.txtvw_sign_up)
    AppCompatTextView txtvw_sign_up;
    private String gender;
    String device_id;

    @BindView(R.id.edt_txt_date_of_birth)
    AppCompatTextView edt_txt_date_of_birth;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_activity);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        updateStatusBar();

        ButterKnife.bind(this);


        txtvw_sign_here.setTextColor(ContextCompat.getColorStateList(SignUpActivity.this, R.color.textview_selector));
        /*txtvw_sign_here.setTextColor(getResources().getColorStateList(R.color.textview_selector));*/

        rd_grp_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdbtn_female:

                        gender = rdbtn_female.getText().toString();
                        Log.e("Id", "Id:=" + rdbtn_female.getId());

                        break;
                    case R.id.rdbtn_male:

                        gender = rdbtn_male.getText().toString();
                        Log.e("Id", "Id:=" + rdbtn_male.getId());

                        break;


                }
            }
        });


    }


   /* @OnClick(R.id.img_view_back)
    public void onBack() {
        onBackPressed();
    }*/

    @OnClick(R.id.edt_txt_date_of_birth)
    void date_picker_dialog() {
        openDateDia();
    }


    @OnClick(R.id.txtvw_sign_here)
    void signIn() {
        Intent signin = new Intent(this, SignInActivity.class);
        startActivity(signin);
    }

    @OnClick(R.id.txtvw_sign_up)
    void submit() {
        if (validation()) {
            // device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            device_id = getUserFcmToken();
            DataHolder.setFirst_name(edt_txt_first_name.getText().toString());
            DataHolder.setLast_name(edt_txt_last_name.getText().toString());
            DataHolder.setPhone_number(edt_txt_phone_number.getText().toString());
            DataHolder.setDate_of_birth(edt_txt_date_of_birth.getText().toString());
            DataHolder.setEmail(edt_txt_email.getText().toString().trim());
            DataHolder.setPassword(edt_txt_password.getText().toString().trim());

            DataHolder.setGender(gender);

            if (EasyPermissions.hasPermissions(this, Manifest.permission.INTERNET)) {
                if (isConnectedToInternet()) {
                    showLoading();
                    ServerAPI.getInstance().validateregisteration(APIServerResponse.VALIDREGISTERATION, edt_txt_email.getText().toString().trim(), edt_txt_phone_number.getText().toString(), this);
                } else {
                    showSnack(Constants.INTERNET_CONNECTION);
                }
            }

        }

    }


    public boolean validation() {
        if (edt_txt_first_name.getText().toString().isEmpty()) {
            showToast(Constants.FIRST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(edt_txt_first_name.getText().charAt(0))) {
            showToast(Constants.FIRST_NAME_START, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_last_name.getText().toString().isEmpty()) {
            showToast(Constants.LAST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(edt_txt_last_name.getText().charAt(0))) {
            showToast(Constants.EMAIL_START, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_phone_number.getText().toString().isEmpty()) {
            showToast(Constants.PHONE_NUMBER_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_phone_number.getText().toString().length() < 10) {
            showToast(Constants.PHONE_NUMBER_LENGTH, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_email.getText().toString().isEmpty()) {
            showToast(Constants.EMAIL_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(edt_txt_email.getText().charAt(0))) {

        } else if (!isValidEmail(edt_txt_email.getText().toString())) {
            showToast(Constants.EMAIL_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_password.getText().toString().isEmpty()) {
            showToast(Constants.PASSWORD_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_password.getText().toString().length() < 6) {
            showToast(Constants.PASSWORD_LENGTH, Toast.LENGTH_SHORT);
            return false;
        } else if (gender == "" || gender == null) {
            showToast("Select Gender", Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_date_of_birth.getText().toString().isEmpty()) {
            showToast("Enter Date of Birth", Toast.LENGTH_SHORT);
            return false;
        }
        return true;

    }


    Calendar calendar = Calendar.getInstance();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void openDateDia() {
        hideKeyboard();
        final Calendar cal = Calendar.getInstance(TimeZone.getDefault());

        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        try {
                            if (isValidDate(format)) {
                                edt_txt_date_of_birth.setText(format);
                            } else {
                                // showToast("Select Valid Date of Birth", Toast.LENGTH_SHORT);
                                edt_txt_date_of_birth.requestFocus();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        edt_txt_date_of_birth.setText(format);


//                        }else{
//                            Utils.showToast(BookAppointmentActivity.this, "Please select future date");
//                        }

//                        dateTime = year + "-" + (int)(monthOfYear+1) + "-" + dayOfMonth;

                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());

        datePicker.setCancelable(true);
        datePicker.setTitle("Select the date");
        datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }

    /*Check valid date of birth is valid*/
    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(pDateString);
        return new Date().before(date);
    }


    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        ValidRegisterationModal reg;
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.VALIDREGISTERATION:

                        reg = (ValidRegisterationModal) response.body();


                        if (reg.getStatus().equals("1")) {
                            Intent intent = new Intent(this, QuestionsActivity.class);
                            startActivity(intent);
                            setUserPhoneNumber(edt_txt_phone_number.getText().toString());
                            setFirstName(edt_txt_first_name.getText().toString());
                            setFullName(edt_txt_first_name.getText().toString() + " " + edt_txt_last_name.getText().toString());
                            setLastName(edt_txt_last_name.getText().toString().trim());
                            setUserEmailID(edt_txt_email.getText().toString());
                            setUserGender(gender);
                            finish();
                        } else if (reg.getStatus().equals("0")) {
                            hideLoading();
                            showToast(reg.getMessage(), Toast.LENGTH_SHORT);
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        switch (tag) {
            case APIServerResponse.VALIDREGISTERATION:
                System.out.println("Error");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getUserLoggedIn()) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
