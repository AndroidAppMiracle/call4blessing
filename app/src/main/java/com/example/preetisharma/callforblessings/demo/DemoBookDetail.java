package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.os.Handler;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.SlidingImage;
import com.example.preetisharma.callforblessings.PaymentGatewayActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BookDetailsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/4/2017.
 */

public class DemoBookDetail extends BaseActivity implements APIServerResponse {

    CollapsingToolbarLayout main_collapsing_bar_tab_layout;
    String bookID, bookAmount, bookname;
    FloatingActionButton fab_book_options;
    Toolbar main_toolbar;
    AppCompatTextView tv_book_language, tv_book_year, tv_book_payment_type, txtvw_created_at,
            txtvw_other_authors, txtvw_book_free_paid_amount, tv_event_detail_description, txtvw_book_free_paid;
    ImageView book_image;
    private ViewPager mPager;
    CirclePageIndicator indicator;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    ArrayList<String> bookImagesArray = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demodemo_book_detail);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            bookID = getIntent().getExtras().getString(Constants.BOOK_ID);
        }


        main_collapsing_bar_tab_layout = (CollapsingToolbarLayout) findViewById(R.id.main_collapsing_bar_tab_layout);
        fab_book_options = (FloatingActionButton) findViewById(R.id.fab_book_options);
        main_toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        tv_book_language = (AppCompatTextView) findViewById(R.id.tv_book_language);
        tv_book_year = (AppCompatTextView) findViewById(R.id.tv_book_year);
        tv_book_payment_type = (AppCompatTextView) findViewById(R.id.tv_book_payment_type);
        txtvw_created_at = (AppCompatTextView) findViewById(R.id.txtvw_created_at);
        txtvw_other_authors = (AppCompatTextView) findViewById(R.id.txtvw_other_authors);
        txtvw_book_free_paid_amount = (AppCompatTextView) findViewById(R.id.txtvw_book_free_paid_amount);
        tv_event_detail_description = (AppCompatTextView) findViewById(R.id.tv_event_detail_description);
        txtvw_book_free_paid = (AppCompatTextView) findViewById(R.id.txtvw_book_free_paid);
        book_image = (ImageView) findViewById(R.id.book_image);
        mPager = (ViewPager) findViewById(R.id.pager);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);


        try {
            setSupportActionBar(main_toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            main_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DemoBookDetail.this.finish();
                }
            });


            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().getBookDetails(APIServerResponse.BOOK_DETAILS, getUserSessionId(), bookID, this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                BookDetailsModal bookDetailsModal;

                switch (tag) {
                    case APIServerResponse.BOOK_DETAILS:

                        bookDetailsModal = (BookDetailsModal) response.body();
                        if (bookDetailsModal.getStatus().equals("1")) {

                            if (!bookDetailsModal.getDetail().getBook_cover().equals("")) {
                                //Glide.with(DemoBookDetail.this).load(bookDetailsModal.getDetail().getBook_cover()).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(book_image);
                                bookImagesArray.add(bookDetailsModal.getDetail().getBook_cover());

                            }

                            for (int i = 0; i < bookDetailsModal.getDetail().getFile().size(); i++) {
                                bookImagesArray.add(bookDetailsModal.getDetail().getFile().get(i).getName());
                            }

                            init(bookImagesArray);

                            if (bookDetailsModal.getDetail().getUser_info().getId() == Integer.valueOf(getUserID())) {
                                fab_book_options.setVisibility(View.GONE);
                                txtvw_book_free_paid.setText(Constants.BOOK_BY_YOU);
                                txtvw_book_free_paid_amount.setVisibility(View.GONE);

                            } else {
                                fab_book_options.setVisibility(View.VISIBLE);
                                /*Glide.with(DemoBookDetail.this).load(R.drawable.ic_accept).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(fab_book_options);*/
                                //txtvw_book_free_paid.setText(Constants.BOOK_BY_YOU);
                                txtvw_book_free_paid_amount.setVisibility(View.VISIBLE);
                                if (!bookDetailsModal.getDetail().getAmount().equalsIgnoreCase("0")) {
                                    txtvw_book_free_paid_amount.setText("Rs." + " " + bookDetailsModal.getDetail().getAmount());
                                } else {
                                    txtvw_book_free_paid_amount.setText("Free");
                                }
                            }
                            if (bookDetailsModal.getDetail().getPurchased_by_current_user().equalsIgnoreCase("True")) {
                                Glide.with(DemoBookDetail.this).load(R.drawable.ic_accept).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(fab_book_options);
                            }
                            main_collapsing_bar_tab_layout.setTitle(bookDetailsModal.getDetail().getName());
                            tv_book_language.setText(bookDetailsModal.getDetail().getLanguage());
                            tv_book_year.setText(bookDetailsModal.getDetail().getYear());
                            bookAmount = String.valueOf(bookDetailsModal.getDetail().getAmount());
                            bookname = bookDetailsModal.getDetail().getName();
                            tv_book_payment_type.setText(bookDetailsModal.getDetail().getPayment_type());
                            txtvw_created_at.setText(bookDetailsModal.getDetail().getCreated_at());
                            txtvw_other_authors.setText(bookDetailsModal.getDetail().getAuthor_other());
                            if (bookDetailsModal.getDetail().getPayment_type().equalsIgnoreCase("Free")) {
                                fab_book_options.setClickable(false);
                                fab_book_options.setVisibility(View.GONE);
                            }


                            tv_event_detail_description.setText(bookDetailsModal.getDetail().getDescription());
                        }
                        break;

                }

                hideLoading();

            } else {
                hideLoading();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @OnClick(R.id.fab_book_options)
    public void purchaseBook() {
        Intent purchasebookIntent = new Intent(this, PaymentGatewayActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.BOOK_ID, bookID);
        b.putString(Constants.AMOUNT, bookAmount);
        b.putString(Constants.BOOK_DESC, bookname);
        purchasebookIntent.putExtras(b);
        startActivity(purchasebookIntent);
        finish();
    }


    private void init(ArrayList<String> arrayList) {
//        for(int i=0;i<IMAGES.length;i++)
//         ImagesArray.add(IMAGES[i]);

        Log.e("Images link", "Images link" + arrayList.size() + "image" + arrayList.get(0));
        mPager.setAdapter(new SlidingImage(this, arrayList));


        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = arrayList.size();

        // Auto start of viewpager
        //final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };


        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

}
