package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsMainModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsSetModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.SettingOptionListActivity;
import com.example.preetisharma.callforblessings.demo.SettingsMainActivity;
import com.example.preetisharma.callforblessings.settings.SettingsBlock;
import com.example.preetisharma.callforblessings.settings.SettingsGeneral;
import com.example.preetisharma.callforblessings.settings.SettingsNotifications;
import com.example.preetisharma.callforblessings.settings.SettingsSecurity;
import com.example.preetisharma.callforblessings.settings.SettingsTagging;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 9/19/2017.
 */

public class SettingOptionListAdapter extends RecyclerView.Adapter<SettingOptionListAdapter.DataViewHolder> {

    public List<String> list;
    Activity mContext;
    private static final int ITEM = 0;
    private static final int ITEM_WITH_SPACE = 1;
    private int[] settingsImages = {R.drawable.ic_settings_wall,
            R.drawable.black_security_setting,
            R.drawable.black_lock,
            R.drawable.time_tag,
            R.drawable.block_setting,
            R.drawable.notification_setting
    };

    public SettingOptionListAdapter(Activity mContext, List<String> list) {
        this.list = list;
        this.mContext = mContext;
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_settings_detail_item, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.radioButtonSelected.setVisibility(View.GONE);
        holder.aiv_setting_image.setImageResource(settingsImages[position]);
        holder.atv_setting_name.setText(list.get(position) + "");

        if (position == 0) {
            holder.cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SettingsGeneral.class);
                    mContext.startActivity(intent);
                }
            });
        } else if (position == 1) {
            holder.cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SettingsSecurity.class);
                    mContext.startActivity(intent);
                }
            });
        } else if (position == 2) {
            holder.cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SettingsMainActivity.class);
                    mContext.startActivity(intent);
                }
            });
        }
        else if (position == 3) {
            holder.cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SettingsTagging.class);
                    mContext.startActivity(intent);
                }
            });
        }
        else if (position == 4) {
            holder.cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SettingsBlock.class);
                    mContext.startActivity(intent);
                }
            });
        }
        else{
            holder.cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SettingsNotifications.class);
                    mContext.startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_setting_name)
        AppCompatTextView atv_setting_name;

        @BindView(R.id.aiv_setting_image)
        AppCompatImageView aiv_setting_image;

        @BindView(R.id.cv_item)
        CardView cv_item;

        @BindView(R.id.radioButtonSelected)
        RadioButton radioButtonSelected;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

         /*   cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        if (!selectedSettingName.equalsIgnoreCase("")) {
                            ServerAPI.getInstance().setSettings(APIServerResponse.SETTINGS_SET, ((BaseActivity) mContext).getUserSessionId(), settingId, selectedSettingName, DataViewHolder.this);
                        } else {

                            ((BaseActivity) mContext).showToast("Please select an option", Toast.LENGTH_SHORT);
                        }
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }


                }
            });*/

        }



        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }
}
