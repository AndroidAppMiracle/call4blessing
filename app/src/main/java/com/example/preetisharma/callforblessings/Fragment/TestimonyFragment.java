package com.example.preetisharma.callforblessings.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.TestimonyAdapter;
import com.example.preetisharma.callforblessings.Adapter.TestimonyNewAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.HaleTestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModalNew;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/3/2017.
 */

public class TestimonyFragment extends Fragment implements APIServerResponse {


    @BindView(R.id.testimony_list)
    RecyclerView testimony_list;
    @BindView(R.id.ll_testimonial_of_day)
    LinearLayout ll_testimonial_of_day;
    List<TestimonyModal.ListBean> lisAllTestimonial;
    @BindView(R.id.txtvw_testimony_of_the_title)
    AppCompatTextView txtvw_testimony_of_the_title;
    @BindView(R.id.txtvw_testimony_of_the_day_description)
    AppCompatTextView txtvw_testimony_of_the_day_description;
    @BindView(R.id.txtvw_testimony_of_the_day_likes_count)
    AppCompatTextView txtvw_testimony_of_the_day_likes_count;
    @BindView(R.id.txtvw_testimony_of_the_day_comments_count)
    AppCompatTextView txtvw_testimony_of_the_day_comments_count;
    //@BindView(R.id.txtvw_testimony_of_the_day_like)
    //AppCompatTextView txtvw_testimony_of_the_day_like;
    @BindView(R.id.txtvw_testimony_of_the_day_comment)
    AppCompatTextView txtvw_testimony_of_the_day_comment;
    //@BindView(R.id.txtvw_testimony_of_the_day_amen)
    //AppCompatTextView txtvw_testimony_of_the_day_amen;
    @BindView(R.id.txtvw_testimony_of_the_day_halle_count)
    AppCompatTextView txtvw_testimony_of_the_day_halle_count;
    @BindView(R.id.card_view_title)
    CardView card_view_title;
    TestimonyAdapter adapter;
    private String prayerId;
    TestimonyModal testimonyModal;
    TestimonyModalNew testimonyModalNew;
    HaleTestimonyModal haleTestimonyModal;
    int halleCount = 0;

    AppCompatTextView txtvw_testimony_of_the_day_like;
    AppCompatTextView txtvw_testimony_of_the_day_amen;

    String hallecount;
    private static String likescount, commentsCount, amencount;
    private String liked = "not liked";
    private String hallelujah = "Not Hallelujah";

    private TestimonyNewAdapter adapterNew;
    private List<TestimonyModalNew.ListBean> listNew = new ArrayList<>();
    boolean flag, flagLike;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        setRetainInstance(true);
        View rootView = inflater.inflate(R.layout.testimony_tab, container, false);
        ButterKnife.bind(this, rootView);
        card_view_title.requestFocus();
        txtvw_testimony_of_the_day_like = (AppCompatTextView) rootView.findViewById(R.id.txtvw_testimony_of_the_day_like);
        txtvw_testimony_of_the_day_amen = (AppCompatTextView) rootView.findViewById(R.id.txtvw_testimony_of_the_day_amen);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        try {

            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                //ServerAPI.getInstance().getTestimony(APIServerResponse.TESTIMONY_INDEX, ((BaseActivity) getActivity()).getUserSessionId(), this);
                ServerAPI.getInstance().getTestimonyNew(APIServerResponse.TESTIMONY_INDEX, ((BaseActivity) getActivity()).getUserSessionId(), this);

            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }




                    /*if (flagLike) {
                        int imgResource = R.drawable.ic_liked;
                        txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        likescount = String.valueOf(Integer.parseInt(txtvw_testimony_of_the_day_likes_count.getText().toString()) + 1);
                        txtvw_testimony_of_the_day_likes_count.setText(likescount);
                        flagLike = false;
                    } else {
                        int imgResource = R.drawable.prayer_like;
                        txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);

                        likescount = String.valueOf(Integer.parseInt(txtvw_testimony_of_the_day_likes_count.getText().toString()) - 1);
                        txtvw_testimony_of_the_day_likes_count.setText(likescount);
                        flagLike = true;
                    }*/


        txtvw_testimony_of_the_day_amen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (testimonyModalNew != null) {
                    try {

                        if (txtvw_testimony_of_the_day_amen.isEnabled()) {
                            txtvw_testimony_of_the_day_amen.setEnabled(false);
                        }

                        hallelujah = testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_flag();
                        hallecount = testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_count();

                        if (((BaseActivity) getActivity()).isConnectedToInternet()) {

                            ServerAPI.getInstance().haleTestimonyPost(APIServerResponse.HALE_TESTIMONY, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(testimonyModalNew.getTestimony_of_day().get(0).getId()), TestimonyFragment.this);
                        } else {
                            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                   /* if (flag) {
                        int imgResource = R.drawable.ic_selected_amen;
                        txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        // amencount = String.valueOf(Integer.parseInt(txtvw_testimony_of_the_day_halle_count.getText().toString()) + 1);
                        txtvw_testimony_of_the_day_halle_count.setText(amencount + 1);
                        flag = false;
                    } else {
                        int imgResource = R.drawable.prayer_amen;
                        txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        //amencount = String.valueOf(Integer.parseInt(txtvw_testimony_of_the_day_halle_count.getText().toString()) - 1);
                        txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(Integer.parseInt(amencount) - 1));
                        flag = true;
                    }*/


            }
            //Log.i("Hall Flag ", testimonyModalNew.getTestimony_of_day().getHallelujah_flag());
            //Log.i("Hall amen ", testimonyModalNew.getTestimony_of_day().getHallelujah_count());

            // ((BaseActivity) getActivity()).showToast("testofdat " + testimonyModalNew.getTestimony_of_day().getHallelujah_flag(), Toast.LENGTH_LONG);

/*        if (hallelujah.equalsIgnoreCase("Not Hallelujah")) {
            int imgResource = R.drawable.ic_selected_amen;
            txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ((BaseActivity) getActivity()).showLoading();
                ServerAPI.getInstance().haleTestimonyPost(APIServerResponse.HALE_TESTIMONY, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(testimonyModal.getTestimony_of_day().getId()), this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }

        } else {
            int imgResource = R.drawable.prayer_amen;
            txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ((BaseActivity) getActivity()).showLoading();
                ServerAPI.getInstance().haleTestimonyPost(APIServerResponse.UNHALE, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(testimonyModal.getTestimony_of_day().getId()), this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }*/

        });
        return rootView;
    }

    @OnClick(R.id.txtvw_testimony_of_the_day_comment)
    public void comment_prayer_of_day() {
        if (testimonyModalNew != null) {
            Intent i = new Intent(getActivity(), CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(testimonyModalNew.getTestimony_of_day().get(0).getId()));
            b.putString(Constants.POSTTYPE, Constants.TESTIMONY);
            i.putExtras(b);
            startActivity(i);
        }
    }

    public void RefreshScroll() {
        testimony_list.scrollToPosition(0);
        testimony_list.smoothScrollToPosition(0);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            lisAllTestimonial = new ArrayList<>();

            /*AmenPrayerModal amenPrayerModal;*/
            LikeModal likeModal;
            ((BaseActivity) getActivity()).hideLoading();

            if (response.isSuccessful()) {


                switch (tag) {
                    case APIServerResponse.LIKE:

                    {
                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {
                            if (!txtvw_testimony_of_the_day_like.isEnabled()) {
                                txtvw_testimony_of_the_day_like.setEnabled(true);
                            }
                            int imgResource = R.drawable.ic_liked;
                            txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            int likescountValue = Integer.parseInt(likescount);
                            likescountValue++;
                            if (likescountValue == 0 || likescountValue == 1) {
                                txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " + "Like");
                            } else {
                                txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " + "Likes");
                            }
                        }


                    }
                    break;
                    case APIServerResponse.UNLIKE: {
                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {

                            if (!txtvw_testimony_of_the_day_like.isEnabled()) {
                                txtvw_testimony_of_the_day_like.setEnabled(true);
                            }
                            int imgResource = R.drawable.prayer_like;
                            txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            int likescountValue = Integer.parseInt(likescount);
                            likescountValue--;
                            liked = "not liked";
                            testimonyModalNew.getTestimony_of_day().get(0).setLike_flag("not liked");
                            testimonyModalNew.getTestimony_of_day().get(0).setLike_count(String.valueOf(likescountValue));

                            if (likescountValue == 0 || likescountValue == 1) {
                                txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " + "Like");
                            } else {
                                txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " + "Likes");
                            }
                        }


                    }
                    break;
                    case APIServerResponse.HALE_TESTIMONY: {
                        haleTestimonyModal = (HaleTestimonyModal) response.body();
                        if (haleTestimonyModal.getStatus().equals("1")) {
                            if (!txtvw_testimony_of_the_day_amen.isEnabled()) {
                                txtvw_testimony_of_the_day_amen.setEnabled(true);
                            }

                            if (haleTestimonyModal.getHallelujau().equalsIgnoreCase("YES")) {
                                int imgResource = R.drawable.ic_selected_amen;
                                txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                halleCount = Integer.parseInt(testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_count());
                                halleCount++;
                                hallelujah = "Hallelujah";
                                testimonyModalNew.getTestimony_of_day().get(0).setHallelujah_flag("Hallelujah");
                                testimonyModalNew.getTestimony_of_day().get(0).setHallelujah_count(String.valueOf(halleCount));
                                if (halleCount == 0 || halleCount == 1) {
                                    txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(halleCount) + " " + "Hallelujah");
                                } else {
                                    txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(halleCount) + " " + "Hallelujah");
                                }
                            } else {
                                int imgResource = R.drawable.prayer_amen;
                                txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                halleCount = Integer.parseInt(testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_count());
                                halleCount--;
                                /*testimonyModal.getTestimony_of_day().setHallelujah_count(halleCount);*/
                                hallelujah = "Not Hallelujah";
                                testimonyModalNew.getTestimony_of_day().get(0).setHallelujah_flag("Not Hallelujah");
                                testimonyModalNew.getTestimony_of_day().get(0).setHallelujah_count(String.valueOf(halleCount));

                                if (halleCount == 0 || halleCount == 1) {
                                    txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(halleCount) + " " + "Hallelujah");
                                } else {
                                    txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(halleCount) + " " + "Hallelujah");
                                }
                            }
                        }


                    }
                    break;
/*                    case APIServerResponse.UNHALE: {
                        haleTestimonyModal = (HaleTestimonyModal) response.body();
                        if (haleTestimonyModal.getStatus().equals("1")) {
                        }
                        int imgResource = R.drawable.prayer_amen;
                        txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        int likescountValue = Integer.parseInt(likescount);
                        likescountValue--;
                        hallelujah = "Not Hallelujah ";
                        testimonyModal.getTestimony_of_day().setHallelujah_flag("Not Hallelujah");
                        testimonyModal.getTestimony_of_day().setLike_count(String.valueOf(likescountValue));

                        if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + "Like");
                        } else {
                            txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + "Likes");
                        }
                    }
                    break;*/

/*                    case APIServerResponse.TESTIMONY_INDEX:
                        try {
                            testimonyModal = (TestimonyModal) response.body();
                            if (testimonyModal.getStatus().equals("1")) {

                                ll_testimonial_of_day.setVisibility(View.VISIBLE);
                                if (testimonyModal.getTestimony_of_day().getLike_flag().equalsIgnoreCase("Not liked")) {
                                    int imgResource = R.drawable.prayer_like_icon;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                } else {
                                    int imgResource = R.drawable.ic_liked;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                }
                                txtvw_testimony_of_the_title.setText(testimonyModal.getTestimony_of_day().getTitle());
                                txtvw_testimony_of_the_day_description.setText(testimonyModal.getTestimony_of_day().getDesc());
                                prayerId = String.valueOf(testimonyModal.getTestimony_of_day().getId());
                                lisAllTestimonial = testimonyModal.getList();

                                if (Integer.parseInt(testimonyModal.getTestimony_of_day().getLike_count()) == 0 || Integer.parseInt(testimonyModal.getTestimony_of_day().getLike_count()) == 1) {
                                    txtvw_testimony_of_the_day_likes_count.setText(testimonyModal.getTestimony_of_day().getLike_count() + "Like");
                                } else {
                                    txtvw_testimony_of_the_day_likes_count.setText(testimonyModal.getTestimony_of_day().getLike_count() + "Likes");
                                }
                                if (Integer.parseInt(testimonyModal.getTestimony_of_day().getComment_count()) == 0 || Integer.parseInt(testimonyModal.getTestimony_of_day().getComment_count()) == 1) {
                                    txtvw_testimony_of_the_day_comments_count.setText(testimonyModal.getTestimony_of_day().getComment_count() + "Comment");
                                } else {
                                    txtvw_testimony_of_the_day_comments_count.setText(testimonyModal.getTestimony_of_day().getComment_count() + "Comments");
                                }
                                if (Integer.parseInt(testimonyModal.getTestimony_of_day().getHallelujah_count()) == 0 || Integer.parseInt(testimonyModal.getTestimony_of_day().getHallelujah_count()) == 1) {
                                    txtvw_testimony_of_the_day_halle_count.setText(testimonyModal.getTestimony_of_day().getHallelujah_count() + "Hallelujah");
                                } else {
                                    txtvw_testimony_of_the_day_halle_count.setText(testimonyModal.getTestimony_of_day().getHallelujah_count() + "Hallelujahs");
                                }

                                likescount = testimonyModal.getTestimony_of_day().getLike_count();
                            } else {
                                ll_testimonial_of_day.setVisibility(View.GONE);
                                ((BaseActivity) getActivity()).showToast("No testimony found", Toast.LENGTH_LONG);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        adapter = new TestimonyAdapter(getActivity(), lisAllTestimonial);
                        testimony_list.setAdapter(adapter);

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        testimony_list.setLayoutManager(mLayoutManager);
                        testimony_list.setItemAnimator(new DefaultItemAnimator());
                        break;*/
                    case APIServerResponse.TESTIMONY_INDEX:

                        testimonyModalNew = (TestimonyModalNew) response.body();

                        if (testimonyModalNew.getStatus().equalsIgnoreCase("1")) {

                            if (testimonyModalNew.getTestimony_of_day().size() > 0) {
                                card_view_title.setVisibility(View.VISIBLE);
                                ll_testimonial_of_day.setVisibility(View.VISIBLE);
                                if (testimonyModalNew.getTestimony_of_day().get(0).getLike_flag().equalsIgnoreCase("Not liked")) {
                                    int imgResource = R.drawable.prayer_like_icon;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                } else {
                                    int imgResource = R.drawable.ic_liked;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                }
                                if (testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_flag().equalsIgnoreCase("Not hallelujah")) {
                                    int imgResource = R.drawable.prayer_amen;
                                    txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                } else {
                                    int imgResource = R.drawable.ic_selected_amen;
                                    txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                }
                                txtvw_testimony_of_the_title.setText(testimonyModalNew.getTestimony_of_day().get(0).getTitle());
                                txtvw_testimony_of_the_day_description.setText(testimonyModalNew.getTestimony_of_day().get(0).getDesc());
                                prayerId = String.valueOf(testimonyModalNew.getTestimony_of_day().get(0).getId());
                                if (Integer.parseInt(testimonyModalNew.getTestimony_of_day().get(0).getLike_count()) == 0 || Integer.parseInt(testimonyModalNew.getTestimony_of_day().get(0).getLike_count()) == 1) {
                                    txtvw_testimony_of_the_day_likes_count.setText(testimonyModalNew.getTestimony_of_day().get(0).getLike_count() + " " + "Like");
                                } else {
                                    txtvw_testimony_of_the_day_likes_count.setText(testimonyModalNew.getTestimony_of_day().get(0).getLike_count() + " " + "Likes");
                                }
                                if (Integer.parseInt(testimonyModalNew.getTestimony_of_day().get(0).getComment_count()) == 0 || Integer.parseInt(testimonyModalNew.getTestimony_of_day().get(0).getComment_count()) == 1) {
                                    txtvw_testimony_of_the_day_comments_count.setText(testimonyModalNew.getTestimony_of_day().get(0).getComment_count() + " " + "Comment");
                                } else {
                                    txtvw_testimony_of_the_day_comments_count.setText(testimonyModalNew.getTestimony_of_day().get(0).getComment_count() + " " + "Comments");
                                }
                                if (Integer.parseInt(testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_count()) == 0 || Integer.parseInt(testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_count()) == 1) {
                                    txtvw_testimony_of_the_day_halle_count.setText(testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_count() + " " + "Hallelujah");
                                } else {
                                    txtvw_testimony_of_the_day_halle_count.setText(testimonyModalNew.getTestimony_of_day().get(0).getHallelujah_count() + " " + "Hallelujah");
                                }

                                likescount = testimonyModalNew.getTestimony_of_day().get(0).getLike_count();
                            }
                            listNew = testimonyModalNew.getList();
                        }

                        adapterNew = new TestimonyNewAdapter(getActivity(), listNew);
                        testimony_list.setAdapter(adapterNew);
                        testimony_list.scrollToPosition(0);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        testimony_list.setLayoutManager(mLayoutManager);
                        testimony_list.setItemAnimator(new DefaultItemAnimator());

                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtvw_testimony_of_the_day_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (txtvw_testimony_of_the_day_like.isEnabled()) {
                        txtvw_testimony_of_the_day_like.setEnabled(false);
                    }
                    liked = testimonyModalNew.getTestimony_of_day().get(0).getLike_flag();
                    likescount = testimonyModalNew.getTestimony_of_day().get(0).getLike_count();
                    if (liked.equalsIgnoreCase("Not liked")) {
                        int imgResource = R.drawable.ic_liked;
                        txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                            ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(testimonyModalNew.getTestimony_of_day().get(0).getId()), "TESTIMONY", TestimonyFragment.this);
                        } else {
                            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }

                    } else {
                        int imgResource = R.drawable.prayer_like_icon;
                        txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                            ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(testimonyModalNew.getTestimony_of_day().get(0).getId()), "TESTIMONY", TestimonyFragment.this);
                        } else {
                            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {

            ((BaseActivity) getActivity()).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.TESTIMONY_INDEX:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*  @OnClick(R.id.txtvw_testimony_of_the_day_like)
      public void likeTestimonyOfTheDay() {


      }
  */


    @Override
    public void onResume() {
        super.onResume();
        card_view_title.requestFocus();
        card_view_title.setFocusable(true);
        testimony_list.smoothScrollToPosition(0);
        try {


            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getTestimonyNew(APIServerResponse.TESTIMONY_INDEX, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshList() {
        ((BaseActivity) getActivity()).showLoading();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().getTestimonyNew(APIServerResponse.TESTIMONY_INDEX, ((BaseActivity) getActivity()).getUserSessionId(), this);
        } else {
            ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void onEvent(DemoCommentModal event) {
        // your implementation
        ((BaseActivity) getActivity()).showLoading();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().getTestimonyNew(APIServerResponse.TESTIMONY_INDEX, ((BaseActivity) getActivity()).getUserSessionId(), this);
        } else {
            ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();

    }
}
