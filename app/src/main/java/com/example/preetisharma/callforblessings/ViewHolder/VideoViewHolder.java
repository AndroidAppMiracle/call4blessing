package com.example.preetisharma.callforblessings.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;

import com.callforblessings.R;


/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class VideoViewHolder extends RecyclerView.ViewHolder {


    public WebView getVideo_webview() {
        return video_webview;
    }

    public void setVideo_webview(WebView video_webview) {
        this.video_webview = video_webview;
    }

    private WebView video_webview;

    public VideoViewHolder(View v) {
        super(v);
        video_webview = (WebView) v.findViewById(R.id.viewholder_webview);
    }

}
