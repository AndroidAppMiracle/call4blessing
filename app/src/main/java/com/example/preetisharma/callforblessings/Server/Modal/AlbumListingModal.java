package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by preeti.sharma on 2/27/2017.
 */

public class AlbumListingModal {


    /**
     * status : 1
     * detail : {"id":2,"name":"Calvin Harris n Disciples","description":"Calvin Harris n Disciples","language":"English","payment_type":"FREE","amount":"","created_at":"3 weeks ago","is_featured":"NO","album_cover_image":"http://call4blessing.com/uploads/album/1493275729Lighthouse.jpg","files":[{"id":33,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":32,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":31,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":30,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":29,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":28,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":27,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":26,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":25,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":24,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":15,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":14,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":13,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":12,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":11,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":10,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":9,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":8,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":7,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":5,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"}],"is_downloaded":"false"}
     */

    private String status;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * id : 2
         * name : Calvin Harris n Disciples
         * description : Calvin Harris n Disciples
         * language : English
         * payment_type : FREE
         * amount :
         * created_at : 3 weeks ago
         * is_featured : NO
         * album_cover_image : http://call4blessing.com/uploads/album/1493275729Lighthouse.jpg
         * files : [{"id":33,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":32,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":31,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":30,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":29,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":28,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":27,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":26,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":25,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":24,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":15,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":14,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":13,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":12,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":11,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":10,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":9,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":8,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":7,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"},{"id":5,"album_id":2,"song_name":"How Deep Is Your Love","music_file":"http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3","payment_type":"FREE","is_downloaded":"false"}]
         * is_downloaded : false
         */

        private int id;
        private String name;
        private String description;
        private String language;
        private String payment_type;
        private String amount;
        private String created_at;
        private String is_featured;
        private String album_cover_image;
        private String is_downloaded;
        private List<FilesBean> files;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getIs_featured() {
            return is_featured;
        }

        public void setIs_featured(String is_featured) {
            this.is_featured = is_featured;
        }

        public String getAlbum_cover_image() {
            return album_cover_image;
        }

        public void setAlbum_cover_image(String album_cover_image) {
            this.album_cover_image = album_cover_image;
        }

        public String getIs_downloaded() {
            return is_downloaded;
        }

        public void setIs_downloaded(String is_downloaded) {
            this.is_downloaded = is_downloaded;
        }

        public List<FilesBean> getFiles() {
            return files;
        }

        public void setFiles(List<FilesBean> files) {
            this.files = files;
        }

        public static class FilesBean {
            /**
             * id : 33
             * album_id : 2
             * song_name : How Deep Is Your Love
             * music_file : http://call4blessing.com/uploads/music/1493275780How-Deep-Is-Your-Love---Calvin-Harris-n-Disciples-320Kbps.mp3
             * payment_type : FREE
             * is_downloaded : false
             */

            private int id;
            private int album_id;
            private String song_name;
            private String music_file;
            private String payment_type;
            private String is_downloaded;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getAlbum_id() {
                return album_id;
            }

            public void setAlbum_id(int album_id) {
                this.album_id = album_id;
            }

            public String getSong_name() {
                return song_name;
            }

            public void setSong_name(String song_name) {
                this.song_name = song_name;
            }

            public String getMusic_file() {
                return music_file;
            }

            public void setMusic_file(String music_file) {
                this.music_file = music_file;
            }

            public String getPayment_type() {
                return payment_type;
            }

            public void setPayment_type(String payment_type) {
                this.payment_type = payment_type;
            }

            public String getIs_downloaded() {
                return is_downloaded;
            }

            public void setIs_downloaded(String is_downloaded) {
                this.is_downloaded = is_downloaded;
            }
        }
    }
}
