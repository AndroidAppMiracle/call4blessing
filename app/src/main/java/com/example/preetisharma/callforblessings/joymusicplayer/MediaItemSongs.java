package com.example.preetisharma.callforblessings.joymusicplayer;

/**
 * Created by Kshitiz Bali on 5/17/2017.
 */

import com.devbrackets.android.playlistcore.manager.IPlaylistItem;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSongsModal;

/**
 * A custom {@link IPlaylistItem}
 * to hold the information pertaining to the audio and video items
 */

public class MediaItemSongs implements IPlaylistItem {

    private JoyMusicSongsModal.ListBean.SongsBean files;
    boolean isAudio;

    /*"http://www.sylviacuenca.com/images/no_album_art.jpg"*/

    public MediaItemSongs(JoyMusicSongsModal.ListBean.SongsBean files, boolean isAudio) {
        this.isAudio = isAudio;
        this.files = files;

    }


    @Override
    public long getId() {
        return 0;
    }

    @Override
    public long getPlaylistId() {
        return 0;
    }

    @Override
    public int getMediaType() {
        return isAudio ? PlaylistManagerSongs.AUDIO : PlaylistManagerSongs.VIDEO;
    }

    @Override
    public String getMediaUrl() {
        return files.getMusic_file();
    }

    @Override
    public String getDownloadedMediaUri() {
        return null;
    }

    @Override
    public String getThumbnailUrl() {

        return files.getAlbum_cover_image();
    }

    @Override
    public String getArtworkUrl() {
        return files.getAlbum_cover_image();
    }

    @Override
    public String getTitle() {
        return files.getSong_name();
    }

    @Override
    public String getAlbum() {
        return files.getAlbum_name();
    }

    @Override
    public String getArtist() {
        return files.getAlbum_name();
    }
}