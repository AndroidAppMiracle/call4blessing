package com.example.preetisharma.callforblessings.Server.Modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 6/12/2017.
 */

public class UpdateGroupParcebleModal implements Parcelable {

    private int ids;
    private String name;
    private boolean isGroupMember;

    public UpdateGroupParcebleModal(Parcel in) {
        ids = in.readInt();
        name = in.readString();
        isGroupMember = in.readByte() != 0;
    }

    public static final Creator<UpdateGroupParcebleModal> CREATOR = new Creator<UpdateGroupParcebleModal>() {
        @Override
        public UpdateGroupParcebleModal createFromParcel(Parcel in) {
            return new UpdateGroupParcebleModal(in);
        }

        @Override
        public UpdateGroupParcebleModal[] newArray(int size) {
            return new UpdateGroupParcebleModal[size];
        }
    };

    public UpdateGroupParcebleModal() {
        ids = 0;
        name = "";
        isGroupMember = false;
    }

    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGroupMember() {
        return isGroupMember;
    }

    public void setGroupMember(boolean groupMember) {
        isGroupMember = groupMember;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ids);
        dest.writeString(name);
        dest.writeByte((byte) (isGroupMember ? 1 : 0));
    }
}
