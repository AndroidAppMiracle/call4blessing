package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.SettingsDetailAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSettingsChanged;
import com.example.preetisharma.callforblessings.Server.Modal.SettingDetailModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsGetValueModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/27/2017.
 */

public class SettingsDetailActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.rv_settings_detail)
    RecyclerView rv_settings_detail;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.atv_title)
    AppCompatTextView atv_title;

    private String settingId = "", settingName = "";

    String[] settingOptionNames = {Constants.PUBLIC, Constants.FRIEND, Constants.ONLY_ME_SETTING};
    int[] settingOptionsImages = {R.mipmap.ic_settings_options_public,
            R.mipmap.ic_settings_options_friends,
            R.mipmap.ic_settings_options_only_me};

    List<SettingDetailModal> settingDetailList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_detail);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            settingId = extras.getString(Constants.SETTING_ID, Constants.SETTING_ID);
            settingName = extras.getString(Constants.SETTING_VALUE, Constants.SETTING_VALUE);
        }
        txtvw_header_title.setText(settingName);
        img_view_change_password.setVisibility(View.GONE);

        if (settingName.equalsIgnoreCase(Constants.WALL)) {
            atv_title.setText("Who can see my wall?");

        } else if (settingName.equalsIgnoreCase(Constants.MESSAGE)) {
            atv_title.setText("Who can send me message?");

        } else if (settingName.equalsIgnoreCase(Constants.ABOUT_INFO)) {
            atv_title.setText("Who can see my information?");

        } else if (settingName.equalsIgnoreCase(Constants.FRIEND_REQUEST_SETTING)) {
            atv_title.setText("Who can send me friend request?");
        }

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsDetailActivity.this.finish();
            }
        });


        if (isConnectedToInternet()) {

            ServerAPI.getInstance().getSettingValue(APIServerResponse.SETTING_VALUE, getUserSessionId(), settingId, this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {
                SettingsGetValueModal settingsGetValueModal;

                switch (tag) {

                    case APIServerResponse.SETTING_VALUE:

                        settingsGetValueModal = (SettingsGetValueModal) response.body();

                        if (settingsGetValueModal.getStatus().equalsIgnoreCase("1")) {


                            for (int i = 0; i < 3; i++) {
                                SettingDetailModal settingDetailModal = new SettingDetailModal();

                                settingDetailModal.setName(settingOptionNames[i]);
                                settingDetailModal.setSettingImage(settingOptionsImages[i]);
                                if (settingDetailModal.getName().equalsIgnoreCase(settingsGetValueModal.getSettings())) {
                                    settingDetailModal.setChecked(true);
                                } else {
                                    settingDetailModal.setChecked(false);
                                }

                                settingDetailList.add(settingDetailModal);

                            }

                            SettingsDetailAdapter settingsDetailAdapter = new SettingsDetailAdapter(SettingsDetailActivity.this, settingDetailList, settingId);
                            rv_settings_detail.setAdapter(settingsDetailAdapter);
                            rv_settings_detail.setItemAnimator(new DefaultItemAnimator());
                            rv_settings_detail.setLayoutManager(new LinearLayoutManager(SettingsDetailActivity.this));

                        }

                        break;
                    case APIServerResponse.SETTING_VALUE_UPDATED:
                        settingsGetValueModal = (SettingsGetValueModal) response.body();

                        if (settingsGetValueModal.getStatus().equalsIgnoreCase("1")) {

                            if (!settingDetailList.isEmpty()) {
                                settingDetailList.clear();
                            }

                            for (int i = 0; i < 3; i++) {
                                SettingDetailModal settingDetailModal = new SettingDetailModal();

                                settingDetailModal.setName(settingOptionNames[i]);
                                settingDetailModal.setSettingImage(settingOptionsImages[i]);
                                if (settingDetailModal.getName().equalsIgnoreCase(settingsGetValueModal.getSettings())) {
                                    settingDetailModal.setChecked(true);
                                } else {
                                    settingDetailModal.setChecked(false);
                                }

                                settingDetailList.add(settingDetailModal);

                            }

                            SettingsDetailAdapter settingsDetailAdapter = new SettingsDetailAdapter(SettingsDetailActivity.this, settingDetailList, settingId);
                            rv_settings_detail.setAdapter(settingsDetailAdapter);
                            rv_settings_detail.setItemAnimator(new DefaultItemAnimator());
                            rv_settings_detail.setLayoutManager(new LinearLayoutManager(SettingsDetailActivity.this));

                        }


                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

        throwable.printStackTrace();
    }


    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();


    }

    @Subscribe
    public void isGroupCreatedPost(EventBusSettingsChanged eventBusSettingsChanged) {

        /*if (isConnectedToInternet()) {
            try {
                if (eventBusSettingsChanged.getIsSettingChanged()) {
                    showToast("Changed", Toast.LENGTH_SHORT);
                    ServerAPI.getInstance().getSettingValue(APIServerResponse.SETTING_VALUE_UPDATED, getUserSessionId(), settingId, this);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }*/
    }
}
