package com.example.preetisharma.callforblessings.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.example.preetisharma.callforblessings.photos.AlbumsFragment;
import com.example.preetisharma.callforblessings.photos.PhotosOfYouFragment;
import com.example.preetisharma.callforblessings.photos.UploadedByYouFragment;

/**
 * Created by satoti.garg on 7/17/2017.
 */

public class PhotosTabsAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;

    public PhotosTabsAdapter(FragmentManager fm, int mNumOfTabs) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            /*case 0:
                fragment = new EventInvitesFragment();
                return fragment;
            case 1:
                fragment = new EventsCreatedFragment();
                return fragment;
            default:
                fragment = new EventInvitesFragment();
                return fragment;*/


          /*  case 0:
                fragment = new PhotosOfYouFragment();
                return fragment;*/
            case 0:
                fragment = new UploadedByYouFragment();
                return fragment;
            case 1:
                fragment = new AlbumsFragment();
                return fragment;
            default:
                fragment = new UploadedByYouFragment();
                return fragment;

        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    public Fragment getFragment(ViewPager container, int position, FragmentManager fm) {
        String name = makeFragmentName(container.getId(), position);
        return fm.findFragmentByTag(name);
    }

    private String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }
}
