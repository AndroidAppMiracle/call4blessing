package com.example.preetisharma.callforblessings.Utils;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by preeti.sharma on 2/21/2017.
 */

public class GlobalEventBus {
    private static EventBus sBus;
    public static EventBus getBus() {
        if (sBus == null)
            sBus = EventBus.getDefault();
        return sBus;
    }
}
