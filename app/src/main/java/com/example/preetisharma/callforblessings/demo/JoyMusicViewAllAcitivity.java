package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.PaginationAlbumsAdapter;
import com.example.preetisharma.callforblessings.Adapter.PaginationSongsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllAlbumModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllSongsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.listener.PaginationScrollListener;

import java.util.List;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/9/2017.
 */

public class JoyMusicViewAllAcitivity extends BaseActivity implements APIServerResponse {

    AppCompatTextView txtvw_header_title;
    AppCompatImageView img_view_back, img_view_change_password;
    String languageID = "", pageNumber = "1", viewAllFlag = "", languageName = "";
    RecyclerView rv_viewAllList;
    private static final int PAGE_NUMBER = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_NUMBER;
    GridLayoutManager gridLayoutManager;
    PaginationSongsAdapter adapter;
    PaginationAlbumsAdapter albumsAdapter;
    int songsPlaylistID = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joy_music_view_all);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setVisibility(View.GONE);

        if (getIntent().getExtras() != null) {

            if (getIntent().getExtras().getInt(Constants.PLAYLIST_ID_VIEW_ALL) != 0) {
                songsPlaylistID = getIntent().getExtras().getInt(Constants.PLAYLIST_ID_VIEW_ALL);
            }

            if (!getIntent().getExtras().getString(Constants.LANGUAGE_NAME).equalsIgnoreCase("")) {
                languageName = getIntent().getExtras().getString(Constants.LANGUAGE_NAME);
                txtvw_header_title.setText(languageName);
            }
            if (!getIntent().getExtras().getString(Constants.LANGUAGE_ID).equalsIgnoreCase("")) {
                languageID = getIntent().getExtras().getString(Constants.LANGUAGE_ID);
            }

            if (!getIntent().getExtras().getString(Constants.VIEW_ALL_FLAG).equalsIgnoreCase("")) {
                viewAllFlag = getIntent().getExtras().getString(Constants.VIEW_ALL_FLAG);
            }
        }
        adapter = new PaginationSongsAdapter(this, songsPlaylistID);
        albumsAdapter = new PaginationAlbumsAdapter(this);
        rv_viewAllList = (RecyclerView) findViewById(R.id.rv_viewAllList);
        gridLayoutManager = new GridLayoutManager(this, 2);
        rv_viewAllList.setLayoutManager(gridLayoutManager);
        rv_viewAllList.setItemAnimator(new DefaultItemAnimator());


        if (viewAllFlag != null && viewAllFlag.equalsIgnoreCase(Constants.SONG)) {
            rv_viewAllList.setAdapter(adapter);
        } else {
            rv_viewAllList.setAdapter(albumsAdapter);
        }


        setLayoutManagerSpanSize(gridLayoutManager, viewAllFlag);
        //setLayoutManagerSpanSizeAlbums(gridLayoutManager);


        //rv_viewAllList.setItemAnimator(new DefaultItemAnimator());

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        if (isConnectedToInternet()) {

            if (viewAllFlag.equalsIgnoreCase(Constants.SONG)) {
                showLoading();
                ServerAPI.getInstance().getJoyMusicSongsViewAll(APIServerResponse.JOY_MUSIC_SONGS_VIEW_ALL, getUserSessionId(), languageID, String.valueOf(currentPage), JoyMusicViewAllAcitivity.this);
            } else {
                showLoading();
                ServerAPI.getInstance().getJoyMusicAlbumsViewAll(APIServerResponse.JOY_MUSIC_ALBUMS_VIEW_ALL, getUserSessionId(), languageID, String.valueOf(currentPage), JoyMusicViewAllAcitivity.this);
            }

        }
        rv_viewAllList.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPages();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            JoyMusicViewAllSongsModal joyMusicViewAllSongsModal;
            JoyMusicViewAllAlbumModal joyMusicViewAllAlbumModal;

            if (response.isSuccessful()) {
                switch (tag) {

                    case APIServerResponse.JOY_MUSIC_SONGS_VIEW_ALL:

                        joyMusicViewAllSongsModal = (JoyMusicViewAllSongsModal) response.body();
                        if (joyMusicViewAllSongsModal.getStatus().equalsIgnoreCase("1")) {

                            TOTAL_PAGES = joyMusicViewAllSongsModal.getTotalPages();
                            List<JoyMusicViewAllSongsModal.ListBean> songsList = joyMusicViewAllSongsModal.getList();
                            //rv_viewAllList.setAdapter(adapter);
                            adapter.addAll(songsList);
                            if (currentPage < TOTAL_PAGES) adapter.addLoadingFooter();
                            else isLastPage = true;

                            /*List<JoyMusicViewAllSongsModal.ListBean> songsList = new ArrayList<>();
                            songsList = joyMusicViewAllSongsModal.getList();

                            JoyMusicViewAllSongsAdapter songsAdapter = new JoyMusicViewAllSongsAdapter(this, songsList);

                            rv_viewAllList.setAdapter(songsAdapter);*/


                        }

                        hideLoading();
                        break;
                    case APIServerResponse.JOY_MUSIC_ALBUMS_VIEW_ALL:

                        joyMusicViewAllAlbumModal = (JoyMusicViewAllAlbumModal) response.body();
                        if (joyMusicViewAllAlbumModal.getStatus().equalsIgnoreCase("1")) {

                            /*List<JoyMusicViewAllAlbumModal.ListBean> albumList = new ArrayList<>();
                            albumList = joyMusicViewAllAlbumModal.getList();

                            JoyMusicViewAllAlbumsAdapter allAlbumsAdapter = new JoyMusicViewAllAlbumsAdapter(this, albumList);
                            rv_viewAllList.setAdapter(allAlbumsAdapter);*/


                            TOTAL_PAGES = joyMusicViewAllAlbumModal.getTotalPages();
                            List<JoyMusicViewAllAlbumModal.ListBean> albumList = joyMusicViewAllAlbumModal.getList();
                            //rv_viewAllList.setAdapter(albumsAdapter);
                            albumsAdapter.addAll(albumList);
                            if (currentPage < TOTAL_PAGES) albumsAdapter.addLoadingFooter();
                            else isLastPage = true;

                        }
                        hideLoading();
                        break;

                    case APIServerResponse.JOY_MUSIC_VIEW_ALL_SONGS_PAGINATION:

                        joyMusicViewAllSongsModal = (JoyMusicViewAllSongsModal) response.body();
                        if (joyMusicViewAllSongsModal.getStatus().equalsIgnoreCase("1")) {

                            TOTAL_PAGES = joyMusicViewAllSongsModal.getTotalPages();
                            adapter.removeLoadingFooter();
                            isLoading = false;

                            List<JoyMusicViewAllSongsModal.ListBean> songsList = joyMusicViewAllSongsModal.getList();
                            //rv_viewAllList.setAdapter(adapter);
                            adapter.addAll(songsList);

                            if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                            else isLastPage = true;

                        }


                        break;

                    case APIServerResponse.JOY_MUSIC_VIEW_ALL_ALBUMS_PAGINATION:

                        joyMusicViewAllAlbumModal = (JoyMusicViewAllAlbumModal) response.body();
                        if (joyMusicViewAllAlbumModal.getStatus().equalsIgnoreCase("1")) {

                            TOTAL_PAGES = joyMusicViewAllAlbumModal.getTotalPages();
                            albumsAdapter.removeLoadingFooter();
                            isLoading = false;

                            List<JoyMusicViewAllAlbumModal.ListBean> albumList = joyMusicViewAllAlbumModal.getList();
                            //rv_viewAllList.setAdapter(albumsAdapter);
                            albumsAdapter.addAll(albumList);

                            if (currentPage != TOTAL_PAGES) albumsAdapter.addLoadingFooter();
                            else isLastPage = true;

                        }
                        break;
                }
            } else {
                hideLoading();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.getMessage();
        hideLoading();
    }


    private void loadNextPages() {

        Log.d("loadNextPage: ", "" + currentPage);
        if (isConnectedToInternet()) {

            if (viewAllFlag.equalsIgnoreCase(Constants.SONG)) {
                //showLoading();
                ServerAPI.getInstance().getJoyMusicSongsViewAll(APIServerResponse.JOY_MUSIC_VIEW_ALL_SONGS_PAGINATION, getUserSessionId(), languageID, String.valueOf(currentPage), JoyMusicViewAllAcitivity.this);
            } else {
                //showLoading();
                ServerAPI.getInstance().getJoyMusicAlbumsViewAll(APIServerResponse.JOY_MUSIC_VIEW_ALL_ALBUMS_PAGINATION, getUserSessionId(), languageID, String.valueOf(currentPage), JoyMusicViewAllAcitivity.this);
            }

        }
    }


    private void setLayoutManagerSpanSize(final GridLayoutManager gridLayoutManager, String flag) {
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (viewAllFlag.equalsIgnoreCase(Constants.SONG)) {
                    switch (adapter.getItemViewType(position)) {
                        case PaginationSongsAdapter.LOADING:
                            return gridLayoutManager.getSpanCount();
                        case PaginationSongsAdapter.ITEM:
                            return 1;
                        default:
                            return -1;
                    }
                } else {
                    switch (albumsAdapter.getItemViewType(position)) {
                        case PaginationAlbumsAdapter.LOADING:
                            return gridLayoutManager.getSpanCount();
                        case PaginationAlbumsAdapter.ITEM:
                            return 1;
                        default:
                            return -1;
                    }
                }

            }
        });
    }

    /*private void setLayoutManagerSpanSizeAlbums(final GridLayoutManager gridLayoutManager) {
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (albumsAdapter.getItemViewType(position)) {
                    case PaginationAlbumsAdapter.LOADING:
                        return gridLayoutManager.getSpanCount();
                    case PaginationAlbumsAdapter.ITEM:
                        return 1;
                    default:
                        return -1;
                }
            }
        });
    }*/
}
