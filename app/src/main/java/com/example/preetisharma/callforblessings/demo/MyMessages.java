package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.MessagesAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.MessagesModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/14/2017.
 */

public class MyMessages extends BaseActivity implements APIServerResponse {


    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    // @BindView(R.id.img_view_back)

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    //@BindView(R.id.img_view_change_password)
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.rv_messages_listing)
    RecyclerView rv_messages_listing;

    @BindView(R.id.atv_message)
    AppCompatTextView atv_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_layout);
        ButterKnife.bind(this);

        txtvw_header_title.setText("Messages");
        img_view_change_password.setVisibility(View.GONE);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMessages.this.finish();
            }
        });

        if (isConnectedToInternet()) {

            showLoading();
            ServerAPI.getInstance().getMyMessages(APIServerResponse.MY_MESSAGES, getUserSessionId(), this);


        }


    }

    @OnClick(R.id.img_view_back)
    public void exitMessages() {
        MyMessages.this.finish();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {

                MessagesModal messagesModal;

                switch (tag) {

                    case APIServerResponse.MY_MESSAGES:

                        messagesModal = (MessagesModal) response.body();

                        if (messagesModal.getStatus().equalsIgnoreCase("1")) {


                            List<MessagesModal.ListBean> myMessagesList = messagesModal.getList();
                            MessagesAdapter adapter = new MessagesAdapter(MyMessages.this, myMessagesList);
                            rv_messages_listing.setAdapter(adapter);
                            rv_messages_listing.setLayoutManager(new LinearLayoutManager(this));
                            rv_messages_listing.setItemAnimator(new DefaultItemAnimator());

                        } else {
                            rv_messages_listing.setVisibility(View.GONE);
                            atv_message.setVisibility(View.VISIBLE);
                        }

                        hideLoading();

                        break;

                }

            }

        } catch (Exception e) {
            hideLoading();
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }
}
