package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionCheckModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.JoyMusicSubscriptionActivity;
import com.example.preetisharma.callforblessings.downloadjoymusic.DownloadMainActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/27/2017.
 */

public class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.DataViewHolder> {
    List<AlbumListingModal.DetailBean.FilesBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;
    private AlbumListingModal.DetailBean detail;
    ArrayList<PlayMusicModal> albumSongsList = new ArrayList<>();
    public static boolean isDownloaded = false;
    private boolean activate;
    //private DownloadMainActivity downloadMainActivity;

    public AlbumListAdapter(Activity mContext, List<AlbumListingModal.DetailBean.FilesBean> list, AlbumListingModal.DetailBean detail, boolean activate) {
        this.list = list;
        this.mContext = mContext;
        this.detail = detail;
        this.activate = activate;
        //downloadMainActivity = new DownloadMainActivity();
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<AlbumListingModal.DetailBean.FilesBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public AlbumListAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_album_item, parent, false);
        AlbumListAdapter.DataViewHolder dataView = new AlbumListAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final AlbumListAdapter.DataViewHolder holder, final int position) {
        holder.txtvw_song_name.setText(list.get(position).getSong_name());


        if (list.get(holder.getAdapterPosition()).getIs_downloaded().equalsIgnoreCase(Constants.TRUE)) {
            holder.img_vw_download.setVisibility(View.VISIBLE);
            holder.img_vw_download.setImageResource(R.drawable.ic_action_done);
            //Glide.with(mContext).load(R.drawable.ic_action_done).placeholder(R.drawable.placeholder).thumbnail(0.1f).into(holder.img_vw_download);
        } else {
            holder.img_vw_download.setVisibility(View.VISIBLE);
            holder.img_vw_download.setImageResource(R.drawable.ic_download);

        }


       /* if (activate) {
            holder.img_vw_gif_playing.setVisibility(View.VISIBLE);
        } else {
            holder.img_vw_gif_playing.setVisibility(View.GONE);
        }
*/

      /*  holder.song_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.img_vw_gif_playing.setVisibility(View.GONE);
                holder.img_vw_gif_playing.setVisibility(View.VISIBLE);
            }
        });*/
        /*holder.song_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (detail.getPayment_type().equalsIgnoreCase(Constants.PAID))

                    if (!albumSongsList.isEmpty()) {
                        albumSongsList.clear();
                    }
                for (int i = 0; i < list.size(); i++) {
                    PlayMusicModal playMusicModal = new PlayMusicModal();
                    playMusicModal.setMediaUrl(list.get(i).getMusic_file());
                    playMusicModal.setThumbnailUrl(detail.getAlbum_cover_image());
                    playMusicModal.setArtworkUrl(detail.getAlbum_cover_image());
                    playMusicModal.setTitle(list.get(i).getSong_name());
                    playMusicModal.setAlbum(detail.getName());
                    playMusicModal.setArtist(detail.getName());
                    albumSongsList.add(playMusicModal);
                }


                Intent intent = new Intent(mContext, AudioPlayerActivity.class);
                intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, holder.getAdapterPosition());
                intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, list.get(holder.getAdapterPosition()).getAlbum_id());
                intent.putExtra(Constants.ALBUM_NAME, detail.getName());
                intent.putExtra(Constants.ALBUM_COVER, detail.getAlbum_cover_image());
                intent.putExtra(Constants.MUSIC, Constants.ALBUM);

                Bundle b = new Bundle();
                b.putParcelableArrayList(Constants.ALBUM_SONGS_LIST, albumSongsList);
                intent.putExtra("album_bundle", b);

                mContext.startActivity(intent);

               *//* holder.img_vw_gif_playing.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(R.drawable.equalizer_demo_gif).into(holder.img_vw_gif_playing);

                Intent player = new Intent(mContext, AlbumPlayerActivity.class);
                Bundle b = new Bundle();

                b.putInt(Constants.POSITION, holder.getAdapterPosition());
                b.putString(Constants.ALBUM_NAME, detail.getName());
                if (detail != null && detail.getAlbum_cover_image() != null) {
                    ((BaseActivity) mContext).showLoading();
                    b.putString(Constants.ALBUM_COVER, detail.getAlbum_cover_image());
                }
                player.putExtras(b);
                mContext.startActivity(player);*//*


            }
        });*/
       /* holder.img_vw_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comingSoonDialog("Joy Music");
            }
        });*/


    }

    public void comingSoonDialog(String title) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage("Will be available shortly.")

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    public int getcount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }

    }

    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_song_name)
        AppCompatTextView txtvw_song_name;
        @BindView(R.id.txtvw_song_description)
        AppCompatTextView txtvw_song_description;
        @BindView(R.id.song_card)
        CardView song_card;
        @BindView(R.id.img_vw_download)
        AppCompatImageView img_vw_download;

        @BindView(R.id.img_vw_gif_playing)
        AppCompatImageView img_vw_gif_playing;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            img_vw_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (list.get(getAdapterPosition()).getIs_downloaded().equalsIgnoreCase(Constants.FALSE)) {

                        if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase(Constants.FREE)) {

                            downloadMusic(getAdapterPosition());

                        } else {
                            if (((BaseActivity) mContext).isConnectedToInternet()) {
                                ((BaseActivity) mContext).showLoading();
                                ServerAPI.getInstance().checkjoyMusicSubscription(APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION, ((BaseActivity) mContext).getUserSessionId(), DataViewHolder.this);
                            }
                        }


                    } else {

                        playMusic(getAdapterPosition());
                    }

                }
            });

            song_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    playMusic(getAdapterPosition());

                   /* if (detail.getPayment_type().equalsIgnoreCase(Constants.PAID)) {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().checkjoyMusicSubscription(APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY, ((BaseActivity) mContext).getUserSessionId(), DataViewHolder.this);
                        }

                    } else {

                        //img_vw_gif_playing.setVisibility(View.GONE);
                        //img_vw_gif_playing.setVisibility(View.VISIBLE);
                        playMusic(getAdapterPosition());
                    }*/



               /* holder.img_vw_gif_playing.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(R.drawable.equalizer_demo_gif).into(holder.img_vw_gif_playing);

                Intent player = new Intent(mContext, AlbumPlayerActivity.class);
                Bundle b = new Bundle();

                b.putInt(Constants.POSITION, holder.getAdapterPosition());
                b.putString(Constants.ALBUM_NAME, detail.getName());
                if (detail != null && detail.getAlbum_cover_image() != null) {
                    ((BaseActivity) mContext).showLoading();
                    b.putString(Constants.ALBUM_COVER, detail.getAlbum_cover_image());
                }
                player.putExtras(b);
                mContext.startActivity(player);*/


                }
            });

        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {

                if (response.isSuccessful()) {

                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {
                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION:
                            joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                                //Download Songs

                                downloadMusic(getAdapterPosition());

                                /*DownloadDemo demo = new DownloadDemo();
                                demo.downloadInitiate();*/
                                //downloadMainActivity.downloadOrCancel(list.get(getAdapterPosition()).getMusic_file(), String.valueOf(list.get(getAdapterPosition()).getId()), Constants.SONG_CAPS);


                                //playMusic(getAdapterPosition());


                            } else {
                                Intent player = new Intent(mContext, JoyMusicSubscriptionActivity.class);
                                mContext.startActivity(player);
                            }


                            ((BaseActivity) mContext).hideLoading();
                            break;

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                            joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                                playMusic(getAdapterPosition());
                            } else {
                                Intent player = new Intent(mContext, JoyMusicSubscriptionActivity.class);
                                mContext.startActivity(player);
                            }


                            ((BaseActivity) mContext).hideLoading();

                            break;
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
        }

        public void setPlaying() {
            try {


                img_vw_gif_playing.setVisibility(View.GONE);
                img_vw_gif_playing.setVisibility(View.VISIBLE);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    private void playMusic(int position) {
        if (!albumSongsList.isEmpty()) {
            albumSongsList.clear();
        }
        for (int i = 0; i < list.size(); i++) {
            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(list.get(i).getMusic_file());
            playMusicModal.setThumbnailUrl(detail.getAlbum_cover_image());
            playMusicModal.setArtworkUrl(detail.getAlbum_cover_image());
            playMusicModal.setTitle(list.get(i).getSong_name());
            playMusicModal.setAlbum(detail.getName());
            playMusicModal.setArtist(detail.getName());
            playMusicModal.setPaymentType(detail.getPayment_type());
            playMusicModal.setDownloaded(list.get(i).getIs_downloaded());

            albumSongsList.add(playMusicModal);
        }


        Intent intent = new Intent(mContext, AudioPlayerActivity.class);
        intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, position);
        intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, list.get(position).getAlbum_id());
        intent.putExtra(Constants.ALBUM_NAME, detail.getName());
        intent.putExtra(Constants.ALBUM_COVER, detail.getAlbum_cover_image());
        intent.putExtra(Constants.MUSIC, Constants.ALBUM);

        //int id = list.get(position).getId();

        intent.putExtra(Constants.TYPE, Constants.SONG_CAPS);
        intent.putExtra(Constants.SONG_ID, list.get(position).getId());
        intent.putExtra(Constants.SONG_DOWNLOAD_URL, list.get(position).getMusic_file());
        intent.putExtra(Constants.POSITION, position);
        intent.putExtra(Constants.SONG_NAME, list.get(position).getSong_name());
        intent.putExtra(Constants.IS_DOWNLOADED, list.get(position).getIs_downloaded());

        Bundle b = new Bundle();
        b.putParcelableArrayList(Constants.ALBUM_SONGS_LIST, albumSongsList);
        intent.putExtra("album_bundle", b);

        mContext.startActivity(intent);
    }


    private void downloadMusic(int position) {

        try {
            Intent intent = new Intent(mContext, DownloadMainActivity.class);
            intent.putExtra(Constants.TYPE, Constants.SONG_CAPS);
            intent.putExtra(Constants.SONG_ID, list.get(position).getId());
            intent.putExtra(Constants.SONG_DOWNLOAD_URL, list.get(position).getMusic_file());
            intent.putExtra(Constants.POSITION, position);
            intent.putExtra(Constants.SONG_NAME, list.get(position).getSong_name());
            mContext.startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void activateButtons(boolean activate) {
        this.activate = activate;
        notifyDataSetChanged(); //need to call it for the child views to be re-created with buttons.
    }


}
