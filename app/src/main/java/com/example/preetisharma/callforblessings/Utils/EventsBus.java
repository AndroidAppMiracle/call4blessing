package com.example.preetisharma.callforblessings.Utils;

/**
 * Created by preeti.sharma on 2/21/2017.
 */

public class EventsBus {
    public static class AdapterRefreshEvent {
        String message;

        public AdapterRefreshEvent(String message) {
            this.message = message;

        }

        public String getMessage() {
            return message;
        }
    }
}
