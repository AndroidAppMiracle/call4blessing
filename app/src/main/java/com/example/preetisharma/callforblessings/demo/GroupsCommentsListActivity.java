package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GroupCommentsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusGroupCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupPostDetailCommentsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/9/2017.
 */

public class GroupsCommentsListActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.comments_recycler_view)
    RecyclerView comments_recycler_view;
    @BindView(R.id.txtvw_no_comments)
    AppCompatTextView txtvw_no_comments;
    @BindView(R.id.edt_txt_comment)
    AppCompatEditText edt_txt_comment;
    @BindView(R.id.txtvw_submit_comment)
    AppCompatTextView txtvw_submit_comment;

    private String groupPostId = "";
    private int groupPostPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_group_comment_list);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            groupPostId = extras.getString(Constants.GROUP_POST_ID, "0");
            groupPostPosition = extras.getInt(Constants.POSITION, 0);
            Log.i("GroupPostID", groupPostId);
        }


        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);

        txtvw_header_title.setText("Comments");
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GroupsCommentsListActivity.this.finish();
            }
        });


        if (isConnectedToInternet()) {
            ServerAPI.getInstance().getGroupPostDetailsAndComments(APIServerResponse.GROUP_POST_DETAIL, getUserSessionId(), groupPostId, this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


    }


    @OnClick(R.id.txtvw_submit_comment)
    public void commentOnPost() {
        try {
            if (!edt_txt_comment.getText().toString().equalsIgnoreCase("")) {

                hideKeyboard();
                ServerAPI.getInstance().commentOnGroupPost(APIServerResponse.GROUP_POST_COMMENT, getUserSessionId(), groupPostId, edt_txt_comment.getText().toString().trim(), this);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {

            if (response.isSuccessful()) {
                GroupPostDetailCommentsModal groupPostDetailCommentsModal;
                GroupCommentModal groupCommentModal;

                switch (tag) {

                    case APIServerResponse.GROUP_POST_DETAIL:

                        groupPostDetailCommentsModal = (GroupPostDetailCommentsModal) response.body();
                        if (groupPostDetailCommentsModal.getStatus().equalsIgnoreCase("1")) {

                            if (groupPostDetailCommentsModal.getDetail().getComments().size() > 0) {

                                comments_recycler_view.setVisibility(View.VISIBLE);
                                txtvw_no_comments.setVisibility(View.GONE);

                                List<GroupPostDetailCommentsModal.DetailBean.CommentsBean> commentsBeanList = groupPostDetailCommentsModal.getDetail().getComments();
                                GroupCommentsAdapter adapter = new GroupCommentsAdapter(GroupsCommentsListActivity.this, commentsBeanList);
                                comments_recycler_view.setAdapter(adapter);
                                comments_recycler_view.smoothScrollToPosition(commentsBeanList.size());
                                comments_recycler_view.setLayoutManager(new LinearLayoutManager(GroupsCommentsListActivity.this));
                                comments_recycler_view.setItemAnimator(new DefaultItemAnimator());

                            } else {
                                comments_recycler_view.setVisibility(View.GONE);
                                txtvw_no_comments.setVisibility(View.VISIBLE);
                            }


                        }

                        break;

                    case APIServerResponse.GROUP_POST_COMMENT:

                        groupCommentModal = (GroupCommentModal) response.body();

                        if (groupCommentModal.getStatus().equalsIgnoreCase("1")) {

                            EventBus.getDefault().post(new EventBusGroupCommentModal(1, groupPostPosition));
                            showToast("Commented", Toast.LENGTH_SHORT);
                            edt_txt_comment.setText("");
                            if (isConnectedToInternet()) {
                                ServerAPI.getInstance().getGroupPostDetailsAndComments(APIServerResponse.GROUP_POST_DETAIL, getUserSessionId(), groupPostId, this);
                            } else {
                                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                            }


                        }

                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();

    }
}
