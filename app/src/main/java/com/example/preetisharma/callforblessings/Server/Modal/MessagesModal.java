package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 5/3/2017.
 */

public class MessagesModal {


    /**
     * status : 1
     * list : [{"lastMessage":{"text":"Hiii","date":"16:40","senderId":214},"userInfo":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"YES"}},{"lastMessage":{"text":"Hi","date":"03 June","senderId":216},"userInfo":{"id":216,"username":"rajneeshjesus21","email":"rajneeshjesus21@gmail.com","updated":1,"profile_details":{"firstname":"Rajneesh ","lastname":"Kumar","d_o_b":"1988-08-21","phone":"9045171793","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://www.call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1496250098user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"lastMessage":{"text":"fghfhfhfg","date":"22 May","senderId":215},"userInfo":{"id":215,"username":"babitanayal1226","email":"babitanayal1226@gmail.com","updated":1,"profile_details":{"firstname":"Pushpeen","lastname":"Nayal","d_o_b":"1992-7-26","phone":"8800845355","gender":"FEMALE","location":"","country":"","state":"Andaman and Nicobar Islands","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1495437303WhatsApp Image 2017-05-22 at 07.57.36.jpeg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1492345123user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"lastMessage":{"text":"I'll kick you","date":"04 May","senderId":226},"userInfo":{"id":226,"username":"roomana4jesus","email":"roomana4jesus@gamil.com","updated":1,"profile_details":{"firstname":"Roomana","lastname":"Khan","d_o_b":"1991-08-20","phone":"9635853030","gender":"FEMALE","location":"","country":"","state":"","cover_pic":"http://www.call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1492607641user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"lastMessage":{"text":"I lik at ur ....","date":"04 May","senderId":219},"userInfo":{"id":219,"username":"ashwanim002","email":"ashwanim002@gmail.com","updated":1,"profile_details":{"firstname":"Ashwani","lastname":"Kumar","d_o_b":"1987-06-30","phone":"9897333222","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492375718user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497013149user_profile.jpg"},"is_friend":0,"request_respond":"NO"}}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * lastMessage : {"text":"Hiii","date":"16:40","senderId":214}
         * userInfo : {"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"YES"}
         */

        private LastMessageBean lastMessage;
        private UserInfoBean userInfo;

        public LastMessageBean getLastMessage() {
            return lastMessage;
        }

        public void setLastMessage(LastMessageBean lastMessage) {
            this.lastMessage = lastMessage;
        }

        public UserInfoBean getUserInfo() {
            return userInfo;
        }

        public void setUserInfo(UserInfoBean userInfo) {
            this.userInfo = userInfo;
        }

        public static class LastMessageBean {
            /**
             * text : Hiii
             * date : 16:40
             * senderId : 214
             */

            private String text;
            private String date;
            private int senderId;

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getSenderId() {
                return senderId;
            }

            public void setSenderId(int senderId) {
                this.senderId = senderId;
            }
        }

        public static class UserInfoBean {
            /**
             * id : 214
             * username : plash.jindal
             * email : plash.jindal@emptask.com
             * updated : 1
             * profile_details : {"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"}
             * is_friend : 0
             * request_respond : YES
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : Palash
                 * lastname : Jindal
                 * d_o_b : 29-5-1996
                 * phone : 1234567890
                 * gender : MALE
                 * location :
                 * country : Spain
                 * state : Madrid
                 * cover_pic : http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg
                 * profile_pic : http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }
    }

}
