package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSearchModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionCheckModal;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.EventsDetailsActivityNew;
import com.example.preetisharma.callforblessings.demo.JoyMusicSubscriptionActivity;
import com.example.preetisharma.callforblessings.demo.SinglePostView;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/8/2017.
 */

public class JoyMusicSearchResultAdapter extends RecyclerView.Adapter<JoyMusicSearchResultAdapter.DataViewHolder> {
    public List<JoyMusicSearchModal.ListBean> list;
    Activity mContext;

    ArrayList<PlayMusicModal> arrayListMusic = new ArrayList<>();

    public JoyMusicSearchResultAdapter(Activity mContext, List<JoyMusicSearchModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


 /*   public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }*/

  /*  public void addAllItems(List<MyBooksModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }*/


    @Override
    public JoyMusicSearchResultAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_joy_music_search_list_, parent, false);
        JoyMusicSearchResultAdapter.DataViewHolder dataView = new JoyMusicSearchResultAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final JoyMusicSearchResultAdapter.DataViewHolder holder, int position) {

        holder.atv_songName.setText(list.get(position).getSong_name());
        holder.atv_albumName.setText(list.get(position).getAlbum_name());


    }


    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.atv_songName)
        AppCompatTextView atv_songName;


        @BindView(R.id.atv_albumName)
        AppCompatTextView atv_albumName;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   /* if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase(Constants.PAID)) {


                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().checkjoyMusicSubscription(APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY, ((BaseActivity) mContext).getUserSessionId(), JoyMusicSearchResultAdapter.DataViewHolder.this);
                        }
                    } else {*/

                    playAudio(getAdapterPosition());
                    //}

                }
            });


        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                            joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                                playAudio(getAdapterPosition());
                            } else {
                                Intent player = new Intent(mContext, JoyMusicSubscriptionActivity.class);
                                mContext.startActivity(player);
                            }
                            ((BaseActivity) mContext).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }

    private void playAudio(int position) {

        if (!arrayListMusic.isEmpty()) {
            arrayListMusic.clear();
        }
        for (int i = 0; i < list.size(); i++) {
            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(list.get(i).getMusic_file());
            playMusicModal.setThumbnailUrl(list.get(i).getAlbum_cover_image());
            playMusicModal.setArtworkUrl(list.get(i).getAlbum_cover_image());
            playMusicModal.setTitle(list.get(i).getSong_name());
            playMusicModal.setAlbum(list.get(i).getAlbum_name());
            playMusicModal.setArtist(list.get(i).getAlbum_name());
            playMusicModal.setPaymentType(list.get(i).getPayment_type());
            playMusicModal.setDownloaded(list.get(i).getIs_downloaded());
            arrayListMusic.add(playMusicModal);
        }

        //mPlayListList = mList;

        Intent intent = new Intent(mContext, AudioPlayerActivity.class);
        intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, position);
        intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, list.get(position).getId());
        intent.putExtra(Constants.SONG_NAME, list.get(position).getSong_name());
        intent.putExtra(Constants.ALBUM_COVER, list.get(position).getAlbum_cover_image());
        intent.putExtra(Constants.MUSIC, Constants.SONG);

        intent.putExtra(Constants.ALBUM_NAME, list.get(position).getAlbum_name());
        intent.putExtra(Constants.TYPE, Constants.SONG_CAPS);
        intent.putExtra(Constants.SONG_ID, list.get(position).getId());
        intent.putExtra(Constants.SONG_DOWNLOAD_URL, list.get(position).getMusic_file());
        intent.putExtra(Constants.POSITION, position);
        intent.putExtra(Constants.IS_DOWNLOADED, list.get(position).getIs_downloaded());

        Bundle b = new Bundle();
        b.putParcelableArrayList(Constants.SONGS_LIST, arrayListMusic);
        intent.putExtra("songs_bundle", b);
        //intent.putParcelableArrayListExtra(Constants.SONGS_LIST, songsBeanArrayList);
        mContext.startActivity(intent);


    }
}

