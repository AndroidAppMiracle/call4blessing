package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.PostDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.TimeLineModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;
import com.example.preetisharma.callforblessings.demo.EditUserPostActivity;
import com.example.preetisharma.callforblessings.demo.SharePostActivity;
import com.example.preetisharma.callforblessings.demo.ViewFullScreenImage;
import com.example.preetisharma.callforblessings.joymusicplayer.FullScreenVideoPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;
import com.example.preetisharma.callforblessings.joymusicplayer.VideoPlayerActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 9/21/2017.
 */

public class FollowingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int LOADING = 0;
    public final int IMAGE = 2, VIDEO = 3, TEXT = 4;
    public final int SHARE = 1;
    public final int SHARE_IMAGE = 5;
    public final int SHARE_VIDEO = 6;
    public final int SHARE_TEXT = 7;
    public final int SHARE_REQUEST = 8;
    public final int PROFILE_PIC = 9;
    public final int COVER_PIC = 10;
    //private static final String BASE_URL_IMG = "https://image.tmdb.org/t/p/w150";

    private List<TimeLineModal.ListBean.PostBean> albumResults;
    private Context context;
    private static String likescount, commentsCount;
    private static String liked = "not liked";

    private boolean isLoadingAdded = false;
    private int userIDInt;
    private Resources resources;

    ArrayList<PlayMusicModal> albumSongsList = new ArrayList<>();

    public FollowingAdapter(Context context) {
        this.context = context;
        albumResults = new ArrayList<>();
        String userID = ((BaseActivity) this.context).getUserID();
        userIDInt = Integer.parseInt(userID);
        resources = context.getResources();
    }

    public List<TimeLineModal.ListBean.PostBean> getMovies() {
        return albumResults;
    }

    public void setMovies(List<TimeLineModal.ListBean.PostBean> albumResults) {
        this.albumResults = albumResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;

        switch (viewType) {

            case LOADING:
                v = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new FollowingAdapter.LoadingVH(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;

            case TEXT:
                v = inflater.inflate(R.layout.group_adapter_post_text_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderText(v);
                break;

            case IMAGE:
                v = inflater.inflate(R.layout.group_adapter_post_image_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderImage(v);
                break;

            case VIDEO:
                v = inflater.inflate(R.layout.group_adapter_post_video_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderVideo(v);

                break;
            case SHARE_IMAGE:
                v = inflater.inflate(R.layout.adapter_home_wall_post_share_image_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderShareImage(v);

                break;
            case SHARE_VIDEO:
                v = inflater.inflate(R.layout.adapter_home_wall_post_share_video_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderShareVideo(v);

                break;
            case SHARE_TEXT:
                v = inflater.inflate(R.layout.adapter_home_wall_post_share_text_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderShareText(v);

                break;

            case SHARE_REQUEST:
                v = inflater.inflate(R.layout.adapter_home_wall_post_share_request_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderShareRequest(v);

                break;

            case PROFILE_PIC:
                v = inflater.inflate(R.layout.adapter_home_wall_post_change_profile_or_cover_pic_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderChangeProfileOrCoverPic(v);

                break;

            case COVER_PIC:
                v = inflater.inflate(R.layout.adapter_home_wall_post_change_profile_or_cover_pic_layout, parent, false);
                viewHolder = new FollowingAdapter.DataViewHolderChangeProfileOrCoverPic(v);

                break;
            default:

        }
        return viewHolder;
    }

    /*@NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.homescreen_item_adapter_layout, parent, false);
        viewHolder = new FollowingAdapter.MovieVH(v1);
        return viewHolder;
    }*/

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        //HomeWallModal.PostsBean result = albumResults.get(position); // Movie

        switch (getItemViewType(position)) {

/*
            case IMAGE:

                final FollowingAdapter.DataViewHolderImage dataViewHolderImage = (FollowingAdapter.DataViewHolderImage) holder;

                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderImage.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderImage.img_vw_user_profile);

                }

                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderImage.img_vw_user_profile);

                dataViewHolderImage.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));
                dataViewHolderImage.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());

                if (albumResults.get(position).getTag_users() != null && albumResults.get(position).getTag_users().size() > 0) {

                    dataViewHolderImage.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    if (albumResults.get(position).getTag_users().size() == 1) {
                        dataViewHolderImage.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                    } else if (albumResults.get(position).getTag_users().size() == 2) {
                        dataViewHolderImage.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                    } else {
                        dataViewHolderImage.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getTag_users().size() - 1) + " others");

                    }

                }
                if (albumResults.get(position).getPost_by().getId() == userIDInt) {
                    dataViewHolderImage.iv_more_options.setVisibility(View.VISIBLE);
                }

                dataViewHolderImage.txtvw_post_details.setText(albumResults.get(position).getPost_content());


                Glide.with(context).load(albumResults.get(position).getMedia().get(0).getFile()).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        dataViewHolderImage.mProgress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        dataViewHolderImage.mProgress.setVisibility(View.GONE);
                        return false;
                    }
                }).thumbnail(0.1f).crossFade().into(dataViewHolderImage.iv_grou_post_image);

                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderImage.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderImage.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderImage.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderImage.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderImage.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderImage.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                break;
            case VIDEO:

                final FollowingAdapter.DataViewHolderVideo dataViewHolderVideo = (FollowingAdapter.DataViewHolderVideo) holder;


                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderVideo.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderVideo.img_vw_user_profile);

                }
                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderVideo.img_vw_user_profile);

                dataViewHolderVideo.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));
                dataViewHolderVideo.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());

                if (albumResults.get(position).getTag_users() != null && albumResults.get(position).getTag_users().size() > 0) {

                    dataViewHolderVideo.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    if (albumResults.get(position).getTag_users().size() == 1) {
                        dataViewHolderVideo.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                    } else if (albumResults.get(position).getTag_users().size() == 2) {
                        dataViewHolderVideo.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                    } else {
                        dataViewHolderVideo.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getTag_users().size() - 1) + " others");

                    }

                }
                if (albumResults.get(position).getPost_by().getId() == userIDInt) {
                    dataViewHolderVideo.iv_more_options.setVisibility(View.VISIBLE);
                }

                dataViewHolderVideo.txtvw_post_details.setText(albumResults.get(position).getPost_content());


                Glide.with(context).load(albumResults.get(position).getMedia().get(0).getFile()).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        dataViewHolderVideo.mProgress.setVisibility(View.GONE);
                        dataViewHolderVideo.iv_group_video_play.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        dataViewHolderVideo.mProgress.setVisibility(View.GONE);
                        dataViewHolderVideo.iv_group_video_play.setVisibility(View.VISIBLE);
                        return false;
                    }
                }).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).crossFade().into(dataViewHolderVideo.iv_group_video_post);

                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderVideo.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderVideo.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderVideo.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderVideo.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderVideo.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderVideo.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                break;
            case TEXT:


                final FollowingAdapter.DataViewHolderText dataViewHolderText = (FollowingAdapter.DataViewHolderText) holder;


                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderText.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderText.img_vw_user_profile);

                }

                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderText.img_vw_user_profile);

                dataViewHolderText.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));
                dataViewHolderText.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());

                if (albumResults.get(position).getTag_users() != null && albumResults.get(position).getTag_users().size() > 0) {

                    dataViewHolderText.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    if (albumResults.get(position).getTag_users().size() == 1) {
                        dataViewHolderText.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                    } else if (albumResults.get(position).getTag_users().size() == 2) {
                        dataViewHolderText.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                    } else {
                        dataViewHolderText.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getTag_users().size() - 1) + " others");

                    }

                }
                if (albumResults.get(position).getPost_by().getId() == userIDInt) {
                    dataViewHolderText.iv_more_options.setVisibility(View.VISIBLE);
                }

                dataViewHolderText.txtvw_post_details.setText(albumResults.get(position).getPost_content());

                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderText.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderText.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderText.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderText.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderText.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderText.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                break;
            case SHARE_IMAGE:

                final FollowingAdapter.DataViewHolderShareImage dataViewHolderShareImage = (FollowingAdapter.DataViewHolderShareImage) holder;


                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderShareImage.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderShareImage.img_vw_user_profile);

                }
                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderShareImage.img_vw_user_profile);

                dataViewHolderShareImage.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));
                dataViewHolderShareImage.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());

                if (albumResults.get(position).getTag_users() != null && albumResults.get(position).getTag_users().size() > 0) {

                    dataViewHolderShareImage.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    if (albumResults.get(position).getTag_users().size() == 1) {
                        dataViewHolderShareImage.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                    } else if (albumResults.get(position).getTag_users().size() == 2) {
                        dataViewHolderShareImage.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                    } else {
                        dataViewHolderShareImage.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getTag_users().size() - 1) + " others");

                    }

                } else {
                    dataViewHolderShareImage.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    dataViewHolderShareImage.txtvw_tagged_friends_change_pic_text.setText(R.string.shared);
                }

                if (albumResults.get(position).getPost_by().getId() == userIDInt) {
                    dataViewHolderShareImage.iv_more_options.setVisibility(View.VISIBLE);
                }

                dataViewHolderShareImage.txtvw_post_details.setText(albumResults.get(position).getPost_content());


                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderShareImage.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderShareImage.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderShareImage.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderShareImage.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderShareImage.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderShareImage.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                dataViewHolderShareImage.txtvw_post_details_shared.setText(albumResults.get(position).getShared_post().get(0).getPost_content());


                if (albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderShareImage.img_vw_user_profile_shared);
                } else if (albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderShareImage.img_vw_user_profile_shared);

                }

                //Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderShareImage.img_vw_user_profile_shared);

                dataViewHolderShareImage.txtvw_user_name_shared.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname()));
                dataViewHolderShareImage.txtvw_time_stamp_shared.setText(albumResults.get(position).getShared_post().get(0).getPosted_at());


                Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getMedia().get(0).getFile()).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        dataViewHolderShareImage.mProgress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        dataViewHolderShareImage.mProgress.setVisibility(View.GONE);
                        return false;
                    }
                }).thumbnail(0.1f).crossFade().into(dataViewHolderShareImage.iv_img_shared_post);


                break;
            case SHARE_VIDEO:

                final FollowingAdapter.DataViewHolderShareVideo dataViewHolderShareVideo = (FollowingAdapter.DataViewHolderShareVideo) holder;


                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderShareVideo.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderShareVideo.img_vw_user_profile);

                }


                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderShareVideo.img_vw_user_profile);

                dataViewHolderShareVideo.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));
                dataViewHolderShareVideo.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());

                if (albumResults.get(position).getTag_users() != null && albumResults.get(position).getTag_users().size() > 0) {

                    dataViewHolderShareVideo.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    if (albumResults.get(position).getTag_users().size() == 1) {
                        dataViewHolderShareVideo.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                    } else if (albumResults.get(position).getTag_users().size() == 2) {
                        dataViewHolderShareVideo.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                    } else {
                        dataViewHolderShareVideo.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getTag_users().size() - 1) + " others");

                    }

                } else {
                    dataViewHolderShareVideo.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    dataViewHolderShareVideo.txtvw_tagged_friends_change_pic_text.setText(R.string.shared);
                }

                if (albumResults.get(position).getPost_by().getId() == userIDInt) {
                    dataViewHolderShareVideo.iv_more_options.setVisibility(View.VISIBLE);
                }

                dataViewHolderShareVideo.txtvw_post_details.setText(albumResults.get(position).getPost_content());


                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderShareVideo.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderShareVideo.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderShareVideo.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderShareVideo.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderShareVideo.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderShareVideo.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                dataViewHolderShareVideo.txtvw_post_details_shared.setText(albumResults.get(position).getShared_post().get(0).getPost_content());


                if (albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderShareVideo.img_vw_user_profile_shared);
                } else if (albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderShareVideo.img_vw_user_profile_shared);

                }


                //Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderShareVideo.img_vw_user_profile_shared);

                dataViewHolderShareVideo.txtvw_user_name_shared.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname()));
                dataViewHolderShareVideo.txtvw_time_stamp_shared.setText(albumResults.get(position).getShared_post().get(0).getPosted_at());


                Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getMedia().get(0).getFile()).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        dataViewHolderShareVideo.iv_group_video_play.setVisibility(View.VISIBLE);
                        dataViewHolderShareVideo.mProgress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        dataViewHolderShareVideo.iv_group_video_play.setVisibility(View.VISIBLE);
                        dataViewHolderShareVideo.mProgress.setVisibility(View.GONE);
                        return false;
                    }
                }).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).crossFade().into(dataViewHolderShareVideo.iv_vid_thumb_shared_post);

                break;
            case SHARE_TEXT:

                final FollowingAdapter.DataViewHolderShareText dataViewHolderShareText = (FollowingAdapter.DataViewHolderShareText) holder;


                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderShareText.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderShareText.img_vw_user_profile);

                }


                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderShareText.img_vw_user_profile);

                dataViewHolderShareText.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));
                dataViewHolderShareText.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());

                if (albumResults.get(position).getTag_users() != null && albumResults.get(position).getTag_users().size() > 0) {

                    dataViewHolderShareText.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    if (albumResults.get(position).getTag_users().size() == 1) {
                        dataViewHolderShareText.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                    } else if (albumResults.get(position).getTag_users().size() == 2) {
                        dataViewHolderShareText.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                    } else {
                        dataViewHolderShareText.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getTag_users().size() - 1) + " others");

                    }

                } else {
                    dataViewHolderShareText.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    dataViewHolderShareText.txtvw_tagged_friends_change_pic_text.setText(R.string.shared);
                }

                if (albumResults.get(position).getPost_by().getId() == userIDInt) {
                    dataViewHolderShareText.iv_more_options.setVisibility(View.VISIBLE);
                }

                dataViewHolderShareText.txtvw_post_details.setText(albumResults.get(position).getPost_content());


                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderShareText.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderShareText.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderShareText.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderShareText.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderShareText.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderShareText.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                dataViewHolderShareText.txtvw_post_details_shared.setText(albumResults.get(position).getShared_post().get(0).getPost_content());


                if (albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderShareText.img_vw_user_profile_shared);
                } else if (albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderShareText.img_vw_user_profile_shared);

                }

                //Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderShareText.img_vw_user_profile_shared);

                dataViewHolderShareText.txtvw_user_name_shared.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname()));
                dataViewHolderShareText.txtvw_time_stamp_shared.setText(albumResults.get(position).getShared_post().get(0).getPosted_at());


                break;
            case SHARE_REQUEST:

                final FollowingAdapter.DataViewHolderShareRequest dataViewHolderShareRequest = (FollowingAdapter.DataViewHolderShareRequest) holder;


                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderShareRequest.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderShareRequest.img_vw_user_profile);

                }

                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderShareRequest.img_vw_user_profile);


                dataViewHolderShareRequest.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));
                dataViewHolderShareRequest.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());

                if (albumResults.get(position).getTag_users() != null && albumResults.get(position).getTag_users().size() > 0) {

                    dataViewHolderShareRequest.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    if (albumResults.get(position).getTag_users().size() == 1) {
                        dataViewHolderShareRequest.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                    } else if (albumResults.get(position).getTag_users().size() == 2) {
                        dataViewHolderShareRequest.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                    } else {
                        dataViewHolderShareRequest.txtvw_tagged_friends_change_pic_text.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getTag_users().size() - 1) + " others");

                    }

                } else {
                    dataViewHolderShareRequest.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                    dataViewHolderShareRequest.txtvw_tagged_friends_change_pic_text.setText(R.string.shared);
                }

                if (albumResults.get(position).getPost_by().getId() == userIDInt) {
                    dataViewHolderShareRequest.iv_more_options.setVisibility(View.VISIBLE);
                }

                dataViewHolderShareRequest.txtvw_post_details.setText(albumResults.get(position).getPost_content());


                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderShareRequest.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderShareRequest.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderShareRequest.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderShareRequest.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderShareRequest.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderShareRequest.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                dataViewHolderShareRequest.txtvw_user_name_shared.setText(albumResults.get(position).getShared_request().get(0).getTitle());

                dataViewHolderShareRequest.txtvw_post_details_shared.setText(albumResults.get(position).getShared_request().get(0).getDesc());


                if (!albumResults.get(position).getShared_request().get(0).getVideo_link().equalsIgnoreCase("")) {

                    dataViewHolderShareRequest.card_multimedia_item.setVisibility(View.VISIBLE);

                    Glide.with(context).load(albumResults.get(position).getShared_request().get(0).getVideo_link()).listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                            dataViewHolderShareRequest.movie_progress.setVisibility(View.GONE);
                            dataViewHolderShareRequest.iv_group_video_play.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                            dataViewHolderShareRequest.movie_progress.setVisibility(View.GONE);
                            dataViewHolderShareRequest.iv_group_video_play.setVisibility(View.VISIBLE);
                            return false;
                        }
                    }).thumbnail(0.1f).crossFade().into(dataViewHolderShareRequest.iv_vid_thumb_shared_post);

                } else if (!albumResults.get(position).getShared_request().get(0).getImage().equalsIgnoreCase("")) {

                    dataViewHolderShareRequest.card_multimedia_item.setVisibility(View.VISIBLE);

                    Glide.with(context).load(albumResults.get(position).getShared_request().get(0).getImage()).listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            dataViewHolderShareRequest.iv_group_video_play.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            dataViewHolderShareRequest.iv_group_video_play.setVisibility(View.GONE);
                            return false;
                        }
                    }).thumbnail(0.1f).crossFade().into(dataViewHolderShareRequest.iv_vid_thumb_shared_post);
                }


                break;

            case PROFILE_PIC:

                final FollowingAdapter.DataViewHolderChangeProfileOrCoverPic dataViewHolderChangeProfileOrCoverPic = (FollowingAdapter.DataViewHolderChangeProfileOrCoverPic) holder;

                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderChangeProfileOrCoverPic.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderChangeProfileOrCoverPic.img_vw_user_profile);

                }

                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderChangeProfileOrCoverPic.img_vw_user_profile);

                dataViewHolderChangeProfileOrCoverPic.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));

                dataViewHolderChangeProfileOrCoverPic.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                dataViewHolderChangeProfileOrCoverPic.txtvw_tagged_friends_change_pic_text.setText(R.string.updated_profile_pic);
                dataViewHolderChangeProfileOrCoverPic.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());
                if (albumResults.get(position).getMedia() != null && albumResults.get(position).getMedia().size() > 0) {
                    Glide.with(context).load(albumResults.get(position).getMedia().get(0).getFile()).listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            dataViewHolderChangeProfileOrCoverPic.mProgress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            dataViewHolderChangeProfileOrCoverPic.mProgress.setVisibility(View.GONE);
                            return false;
                        }
                    }).thumbnail(0.1f).crossFade().into(dataViewHolderChangeProfileOrCoverPic.iv_grou_post_image);
                }


                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderChangeProfileOrCoverPic.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderChangeProfileOrCoverPic.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderChangeProfileOrCoverPic.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderChangeProfileOrCoverPic.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderChangeProfileOrCoverPic.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderChangeProfileOrCoverPic.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }
                break;
            case COVER_PIC:


                final FollowingAdapter.DataViewHolderChangeProfileOrCoverPic dataViewHolderChangeProfileOrCoverPic1 = (FollowingAdapter.DataViewHolderChangeProfileOrCoverPic) holder;

                if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderChangeProfileOrCoverPic1.img_vw_user_profile);
                } else if (albumResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderChangeProfileOrCoverPic1.img_vw_user_profile);

                }

                //Glide.with(context).load(albumResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderChangeProfileOrCoverPic1.img_vw_user_profile);

                dataViewHolderChangeProfileOrCoverPic1.txtvw_user_name.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getPost_by().getProfile_details().getFirstname(), albumResults.get(position).getPost_by().getProfile_details().getLastname()));

                dataViewHolderChangeProfileOrCoverPic1.txtvw_tagged_friends_change_pic_text.setVisibility(View.VISIBLE);
                dataViewHolderChangeProfileOrCoverPic1.txtvw_tagged_friends_change_pic_text.setText(R.string.updated_cover_pitcure);
                dataViewHolderChangeProfileOrCoverPic1.txtvw_time_stamp.setText(albumResults.get(position).getPosted_at());
                if (albumResults.get(position).getMedia() != null && albumResults.get(position).getMedia().size() != 0) {

                    Glide.with(context).load(albumResults.get(position).getMedia().get(0).getFile()).listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            dataViewHolderChangeProfileOrCoverPic1.mProgress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            dataViewHolderChangeProfileOrCoverPic1.mProgress.setVisibility(View.GONE);
                            return false;
                        }
                    }).thumbnail(0.1f).crossFade().into(dataViewHolderChangeProfileOrCoverPic1.iv_grou_post_image);
                }


                if (albumResults.get(position).getLike_count().equalsIgnoreCase("1") || albumResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderChangeProfileOrCoverPic1.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.like)));
                } else {
                    dataViewHolderChangeProfileOrCoverPic1.txtvw_no_of_likes.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getLike_count(), resources.getString(R.string.likes)));
                }


                if (albumResults.get(position).getComment_count().equalsIgnoreCase("1") || albumResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderChangeProfileOrCoverPic1.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comment)));
                } else {
                    dataViewHolderChangeProfileOrCoverPic1.txtvw_no_of_comments.setText(String.format(resources.getString(R.string.no_of_like), albumResults.get(position).getComment_count(), resources.getString(R.string.comments)));
                }

                if (albumResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderChangeProfileOrCoverPic1.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderChangeProfileOrCoverPic1.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }

                break;
            case LOADING:

                //Nothing
                break;*/

        }
    }

    @Override
    public int getItemCount() {
        return albumResults == null ? 0 : albumResults.size();
    }

    @Override
    public int getItemViewType(int position) {

        int returnType = 0;

        if (position == albumResults.size() - 1 && isLoadingAdded) {

            returnType = LOADING;

        } else {
            if (albumResults != null && albumResults.get(position).getPost_type().equalsIgnoreCase(Constants.SHARE)) {

                if (albumResults.get(position).getShared_post() != null && albumResults.get(position).getShared_post().size() > 0) {
                    if (albumResults.get(position).getShared_post().get(0).getMedia() != null && albumResults.get(position).getShared_post().get(0).getMedia().size() > 0) {

                        if (albumResults.get(position).getShared_post().get(0).getMedia().get(0).getType().equalsIgnoreCase(Constants.IMAGE)) {

                            returnType = SHARE_IMAGE;
                        } else if (albumResults.get(position).getShared_post().get(0).getMedia().get(0).getType().equalsIgnoreCase(Constants.VIDEO)) {
                            returnType = SHARE_VIDEO;
                        }
                    } else {
                        returnType = SHARE_TEXT;
                    }

                } else if (albumResults.get(position).getShared_request() != null && albumResults.get(position).getShared_request().size() > 0) {

                    returnType = SHARE_REQUEST;
                }


            } else if (albumResults != null && albumResults.get(position).getPost_type().equalsIgnoreCase(Constants.WALL) || albumResults != null && albumResults.get(position).getPost_type().equalsIgnoreCase(Constants.USERWALL)) {

                if (albumResults.get(position).getMedia() != null && albumResults.get(position).getMedia().size() > 0) {
                    if (albumResults.get(position).getMedia().get(0).getType().equalsIgnoreCase(Constants.IMAGE)) {
                        returnType = IMAGE;
                    } else if (albumResults.get(position).getMedia().get(0).getType().equalsIgnoreCase(Constants.VIDEO)) {
                        returnType = VIDEO;
                    }
                } else {
                    returnType = TEXT;
                }
            } else if (albumResults != null && albumResults.get(position).getPost_type().equalsIgnoreCase(Constants.PROFILE_PIC)) {

                returnType = PROFILE_PIC;

            } else if (albumResults != null && albumResults.get(position).getPost_type().equalsIgnoreCase(Constants.COVER_PIC)) {

                returnType = COVER_PIC;
            }
        }


        return returnType;
        /*return (position == albumResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;*/
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(TimeLineModal.ListBean.PostBean r) {
        albumResults.add(r);
        notifyItemInserted(albumResults.size() - 1);
    }

    public void addAll(List<TimeLineModal.ListBean.PostBean> moveResults) {
        for (TimeLineModal.ListBean.PostBean result : moveResults) {
            add(result);
        }
    }

    public void remove(TimeLineModal.ListBean.PostBean r) {
        int position = albumResults.indexOf(r);
        if (position > -1) {
            albumResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeAll() {
        if (albumResults.size() != 0) {
            albumResults.clear();
            notifyDataSetChanged();
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new TimeLineModal.ListBean.PostBean());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = albumResults.size() - 1;
        TimeLineModal.ListBean.PostBean result = getItem(position);

        if (result != null) {
            albumResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public TimeLineModal.ListBean.PostBean getItem(int position) {
        return albumResults.get(position);
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }


    public String stripText(String text) {

        String regexp = "<p>.*?</p>";
        String replace = "";
        String finalText = text.replaceAll(regexp, replace);
        return finalText.replaceAll("<img.+?>", "");

    }

   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class MovieVH extends RecyclerView.ViewHolder implements APIServerResponse {
        //private TextView mMovieTitle;
        // private TextView mMovieDesc;
        // private TextView mYear; // displays "year | language"
        // private ImageView mPosterImg;
        @BindView(R.id.txtvw_tagged_friends)
        AppCompatTextView txtvw_tagged_friends;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;
        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;
        @BindView(R.id.viewholder_image)
        AppCompatImageView viewholder_image;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;
        @BindView(R.id.tv_changed_profile_cover_pic)
        AppCompatTextView tv_changed_profile_cover_pic;
        @BindView(R.id.video_play)
        AppCompatImageView video_play;

        //Shared Post
        @BindView(R.id.card_shared_post)
        CardView card_shared_post;
        @BindView(R.id.img_vw_user_profile_shared)
        AppCompatImageView img_vw_user_profile_shared;
        @BindView(R.id.txtvw_user_name_shared)
        AppCompatTextView txtvw_user_name_shared;
        @BindView(R.id.txtvw_time_stamp_shared)
        AppCompatTextView txtvw_time_stamp_shared;
        @BindView(R.id.txtvw_post_details_shared)
        AppCompatTextView txtvw_post_details_shared;
        @BindView(R.id.viewholder_image_shared)
        AppCompatImageView viewholder_image_shared;

        @BindView(R.id.tv_changed_profile_cover_pic_shared)
        AppCompatTextView tv_changed_profile_cover_pic_shared;

        public MovieVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

        @OnClick(R.id.iv_more_options)
        public void deleteeditPopup() {

            try {
                createPopupMenuOption();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @OnClick(R.id.txtvw_like)
        public void likePost() {

            if (txtvw_like.isEnabled()) {
                txtvw_like.setEnabled(false);
            }
            liked = albumResults.get(getAdapterPosition()).getLike_flag();
            likescount = albumResults.get(getAdapterPosition()).getLike_count();

            if (liked.equalsIgnoreCase("Not Liked")) {
                int imgResource = R.drawable.ic_liked;
                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) context).isConnectedToInternet()) {

                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.MovieVH.this);
                } else {
                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }

            } else {
                int imgResource = R.drawable.prayer_like_icon;
                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) context).isConnectedToInternet()) {

                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.MovieVH.this);
                } else {
                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {

            if (albumResults.size() > 0 && albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                if (albumResults.get(getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Image")) {
                    openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), albumResults.get(getAdapterPosition()).getMedia().get(0).getFile(), "", "");
                } else if (albumResults.get(getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Video")) {
                    openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", albumResults.get(getAdapterPosition()).getMedia().get(0).getFile(), albumResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());

                }
            } else {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", "", "");

            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentPost() {


            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            i.putExtras(b);
            context.startActivity(i);


        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                LikeModal likeModal;
                ShareModal shareModal;
                CommentModal commentModal;
                PostDeleteModal postDeleteModal;
                ((BaseActivity) context).hideLoading();
                if (response.isSuccessful()) {
                    switch (tag) {
                        case APIServerResponse.LIKE:

                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like.isEnabled()) {
                                    txtvw_like.setEnabled(true);
                                }
                                int imgResource = R.drawable.ic_liked;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue += 1;
                                liked = "liked";
                                albumResults.get(getAdapterPosition()).setLike_flag("liked");
                                albumResults.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));
                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                                } else {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                                }
                            }

                            break;
                        case APIServerResponse.UNLIKE:
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like.isEnabled()) {
                                    txtvw_like.setEnabled(true);
                                }
                                int imgResource = R.drawable.prayer_like;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue--;
                                liked = "not liked";
                                albumResults.get(getAdapterPosition()).setLike_flag("not liked");
                                albumResults.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                                } else {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                                }
                            }
                            break;
                        case APIServerResponse.SHARE:

                            shareModal = (ShareModal) response.body();
                            if (shareModal.getStatus().equals("1")) {
                                /*shareModal.getDetail().*/
                                ((BaseActivity) context).showToast("Shared successfully", Toast.LENGTH_SHORT);

                                notifyDataSetChanged();
                            }

                            break;
                        case APIServerResponse.COMMENT:

                            commentModal = (CommentModal) response.body();
                            if (commentModal.getStatus().equals("1")) {
                                /*shareModal.getDetail().*/
                                ((BaseActivity) context).showToast("Success", Toast.LENGTH_SHORT);
                                notifyDataSetChanged();
                            }

                            break;

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }

                            break;
                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        public void openConfirmationDialog(String postId, String post_text, String timeStamp, String post_image, String postVideoUrl, String postVideoThumbnail) {

            Intent shareDialog = new Intent(context, SharePostActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.POSTTYPE, "POST");
            b.putString(Constants.POSTIMAGE, post_image);
            b.putString(Constants.POSTTEXT, post_text);
            b.putString(Constants.POSTTIMESTAMP, timeStamp);
            b.putString(Constants.POSTID, postId);
            b.putString(Constants.POSTVIDEO, postVideoUrl);
            b.putString(Constants.POSTTHUMBNAIL, postVideoThumbnail);
            shareDialog.putExtras(b);
            context.startActivity(shareDialog);


        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.LIKE:
                    System.out.println("Error");
                    break;
                case APIServerResponse.SHARE:
                    System.out.println("Error");
                    break;
            }
        }


        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), FollowingAdapter.MovieVH.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        public void refresh(int position) {
            Log.e("Refreshed Position", "Pos" + position);
            albumResults.remove(position);
            notifyItemRemoved(position);
        }


    }


    protected class DataViewHolderText extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;
        @BindView(R.id.txtvw_tagged_friends_change_pic_text)
        AppCompatTextView txtvw_tagged_friends_change_pic_text;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;


        public DataViewHolderText(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }


        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);

                if (albumResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderText.this);


                } else {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderText.this);

                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {


            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }


        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @OnClick(R.id.iv_more_options)
        public void moreOptions() {
            createPopupMenuOption();
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    PostDeleteModal postDeleteModal;


                    switch (tag) {

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            //((BaseActivity) mContext).hideLoading();
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();
        }


        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), FollowingAdapter.DataViewHolderText.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    protected class DataViewHolderImage extends RecyclerView.ViewHolder implements APIServerResponse {


        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        @BindView(R.id.iv_grou_post_image)
        AppCompatImageView iv_grou_post_image;
        @BindView(R.id.txtvw_tagged_friends_change_pic_text)
        AppCompatTextView txtvw_tagged_friends_change_pic_text;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;
        @BindView(R.id.movie_progress)
        ProgressBar mProgress;


        public DataViewHolderImage(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.iv_more_options)
        public void moreOptions() {
            createPopupMenuOption();
        }

        @OnClick(R.id.iv_grou_post_image)
        public void viewImageFullscreen() {
            try {
                Intent intentFullscreenImage = new Intent(context, ViewFullScreenImage.class);
                Bundle b = new Bundle();
                b.putString(Constants.IMAGE, albumResults.get(getAdapterPosition()).getMedia().get(0).getFile());
                intentFullscreenImage.putExtras(b);
                context.startActivity(intentFullscreenImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);

                if (albumResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderImage.this);


                } else {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderImage.this);

                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {

            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }


        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), albumResults.get(getAdapterPosition()).getMedia().get(0).getFile(), "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    PostDeleteModal postDeleteModal;


                    switch (tag) {

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            //((BaseActivity) mContext).hideLoading();
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();
        }


        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), FollowingAdapter.DataViewHolderImage.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    protected class DataViewHolderVideo extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        @BindView(R.id.iv_group_video_post)
        AppCompatImageView iv_group_video_post;

        @BindView(R.id.iv_group_video_play)
        AppCompatImageView iv_group_video_play;

        @BindView(R.id.txtvw_tagged_friends_change_pic_text)
        AppCompatTextView txtvw_tagged_friends_change_pic_text;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;

        @BindView(R.id.movie_progress)
        ProgressBar mProgress;

        @BindView(R.id.card_video)
        CardView card_video;


        public DataViewHolderVideo(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.iv_more_options)
        public void moreOptions() {
            createPopupMenuOption();
        }

        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), this);

                if (albumResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderVideo.this);


                } else {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderVideo.this);

                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {
            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", albumResults.get(getAdapterPosition()).getMedia().get(0).getFile(), albumResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.card_video)
        public void playVideo() {

            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile());
            playMusicModal.setArtworkUrl(albumResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());
            playMusicModal.setThumbnailUrl(albumResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());
            playMusicModal.setAlbum("");
            playMusicModal.setArtist("");
            playMusicModal.setAudio(false);
            playMusicModal.setTitle("Video");
            playMusicModal.setPlaylistId(albumResults.get(getAdapterPosition()).getId());

            albumSongsList.add(playMusicModal);


            Intent intent = new Intent(context, FullScreenVideoPlayerActivity.class);
            intent.putExtra(VideoPlayerActivity.EXTRA_INDEX, 0);
            intent.putExtra(VideoPlayerActivity.PLAYLIST_FLAG, albumResults.get(getAdapterPosition()).getId());
            Bundle b = new Bundle();
            b.putParcelableArrayList(Constants.ALBUM_SONGS_LIST, albumSongsList);
            intent.putExtra("video_bundle", b);
            context.startActivity(intent);

        }


        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    PostDeleteModal postDeleteModal;


                    switch (tag) {

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            //((BaseActivity) mContext).hideLoading();
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();
        }


        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), FollowingAdapter.DataViewHolderVideo.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    protected class DataViewHolderShareImage extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        //@BindView(R.id.iv_grou_post_image)
        //AppCompatImageView iv_grou_post_image;
        @BindView(R.id.txtvw_tagged_friends_change_pic_text)
        AppCompatTextView txtvw_tagged_friends_change_pic_text;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;
        @BindView(R.id.movie_progress)
        ProgressBar mProgress;


        ///SharedPost Views

        @BindView(R.id.img_vw_user_profile_shared)
        AppCompatImageView img_vw_user_profile_shared;
        @BindView(R.id.txtvw_user_name_shared)
        AppCompatTextView txtvw_user_name_shared;
        @BindView(R.id.tv_tagged_or_changed_profile_cover_pic_shared)
        AppCompatTextView tv_tagged_or_changed_profile_cover_pic_shared;
        @BindView(R.id.txtvw_time_stamp_shared)
        AppCompatTextView txtvw_time_stamp_shared;
        @BindView(R.id.txtvw_post_details_shared)
        AppCompatTextView txtvw_post_details_shared;
        //@BindView(R.id.movie_progress)
        //ProgressBar movie_progress;
        @BindView(R.id.iv_img_shared_post)
        AppCompatImageView iv_img_shared_post;


        public DataViewHolderShareImage(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }


        @OnClick(R.id.iv_more_options)
        public void moreOptions() {
            createPopupMenuOption();
        }

        @OnClick(R.id.iv_img_shared_post)
        public void viewImageFullscreen() {
            try {
                Intent intentFullscreenImage = new Intent(context, ViewFullScreenImage.class);
                Bundle b = new Bundle();
                b.putString(Constants.IMAGE, albumResults.get(getAdapterPosition()).getShared_post().get(0).getMedia().get(0).getFile());
                intentFullscreenImage.putExtras(b);
                context.startActivity(intentFullscreenImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), this);

                if (albumResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderShareImage.this);


                } else {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderShareImage.this);

                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {
            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }


        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    PostDeleteModal postDeleteModal;


                    switch (tag) {

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();
        }

        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), FollowingAdapter.DataViewHolderShareImage.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    protected class DataViewHolderShareVideo extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        //@BindView(R.id.iv_grou_post_image)
        //AppCompatImageView iv_grou_post_image;
        @BindView(R.id.txtvw_tagged_friends_change_pic_text)
        AppCompatTextView txtvw_tagged_friends_change_pic_text;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;
        @BindView(R.id.movie_progress)
        ProgressBar mProgress;


        ///SharedPost Views

        @BindView(R.id.img_vw_user_profile_shared)
        AppCompatImageView img_vw_user_profile_shared;
        @BindView(R.id.txtvw_user_name_shared)
        AppCompatTextView txtvw_user_name_shared;
        @BindView(R.id.tv_tagged_or_changed_profile_cover_pic_shared)
        AppCompatTextView tv_tagged_or_changed_profile_cover_pic_shared;
        @BindView(R.id.txtvw_time_stamp_shared)
        AppCompatTextView txtvw_time_stamp_shared;
        @BindView(R.id.txtvw_post_details_shared)
        AppCompatTextView txtvw_post_details_shared;
        //@BindView(R.id.movie_progress)
        //ProgressBar movie_progress;
        @BindView(R.id.iv_vid_thumb_shared_post)
        AppCompatImageView iv_vid_thumb_shared_post;
        @BindView(R.id.iv_group_video_play)
        AppCompatImageView iv_group_video_play;

        @BindView(R.id.card_shared_video)
        CardView card_shared_video;


        public DataViewHolderShareVideo(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        @OnClick(R.id.iv_more_options)
        public void moreOptions() {
            createPopupMenuOption();
        }

        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), this);

                if (albumResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderShareVideo.this);


                } else {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderShareVideo.this);
                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {
            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.card_shared_video)
        public void playVideo() {

            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(albumResults.get(getAdapterPosition()).getShared_post().get(0).getMedia().get(0).getFile());
            playMusicModal.setArtworkUrl(albumResults.get(getAdapterPosition()).getShared_post().get(0).getMedia().get(0).getThumbnail_file());
            playMusicModal.setThumbnailUrl(albumResults.get(getAdapterPosition()).getShared_post().get(0).getMedia().get(0).getThumbnail_file());
            playMusicModal.setAlbum("");
            playMusicModal.setArtist("");
            playMusicModal.setAudio(false);
            playMusicModal.setTitle("Video");
            playMusicModal.setPlaylistId(albumResults.get(getAdapterPosition()).getId());

            albumSongsList.add(playMusicModal);


            Intent intent = new Intent(context, FullScreenVideoPlayerActivity.class);
            intent.putExtra(VideoPlayerActivity.EXTRA_INDEX, 0);
            intent.putExtra(VideoPlayerActivity.PLAYLIST_FLAG, albumResults.get(getAdapterPosition()).getId());
            Bundle b = new Bundle();
            b.putParcelableArrayList(Constants.ALBUM_SONGS_LIST, albumSongsList);
            intent.putExtra("video_bundle", b);
            context.startActivity(intent);

        }


        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    PostDeleteModal postDeleteModal;


                    switch (tag) {

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();
        }

        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), FollowingAdapter.DataViewHolderShareVideo.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected class DataViewHolderShareText extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;
        @BindView(R.id.txtvw_tagged_friends_change_pic_text)
        AppCompatTextView txtvw_tagged_friends_change_pic_text;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;


        ///SharedPost Views

        @BindView(R.id.img_vw_user_profile_shared)
        AppCompatImageView img_vw_user_profile_shared;
        @BindView(R.id.txtvw_user_name_shared)
        AppCompatTextView txtvw_user_name_shared;
        @BindView(R.id.tv_tagged_or_changed_profile_cover_pic_shared)
        AppCompatTextView tv_tagged_or_changed_profile_cover_pic_shared;
        @BindView(R.id.txtvw_time_stamp_shared)
        AppCompatTextView txtvw_time_stamp_shared;
        @BindView(R.id.txtvw_post_details_shared)
        AppCompatTextView txtvw_post_details_shared;


        public DataViewHolderShareText(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.iv_more_options)
        public void moreOptions() {
            createPopupMenuOption();
        }

        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), this);

                if (albumResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderShareText.this);


                } else {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderShareText.this);

                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {
            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    PostDeleteModal postDeleteModal;


                    switch (tag) {

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            //((BaseActivity) mContext).hideLoading();
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();
        }


        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), FollowingAdapter.DataViewHolderShareText.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    protected class DataViewHolderShareRequest extends RecyclerView.ViewHolder implements APIServerResponse {


        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        //@BindView(R.id.iv_grou_post_image)
        //AppCompatImageView iv_grou_post_image;
        @BindView(R.id.txtvw_tagged_friends_change_pic_text)
        AppCompatTextView txtvw_tagged_friends_change_pic_text;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;
        //@BindView(R.id.movie_progress)
        //ProgressBar mProgress;


        ///SharedPost Views

        /*@BindView(R.id.img_vw_user_profile_shared)
        AppCompatImageView img_vw_user_profile_shared;*/
        @BindView(R.id.txtvw_user_name_shared)
        AppCompatTextView txtvw_user_name_shared;
        /*@BindView(R.id.tv_tagged_or_changed_profile_cover_pic_shared)
        AppCompatTextView tv_tagged_or_changed_profile_cover_pic_shared;*/
        /*@BindView(R.id.txtvw_time_stamp_shared)
        AppCompatTextView txtvw_time_stamp_shared;*/
        @BindView(R.id.txtvw_post_details_shared)
        AppCompatTextView txtvw_post_details_shared;
        @BindView(R.id.movie_progress)
        ProgressBar movie_progress;
        @BindView(R.id.iv_vid_thumb_shared_post)
        AppCompatImageView iv_vid_thumb_shared_post;
        @BindView(R.id.iv_group_video_play)
        AppCompatImageView iv_group_video_play;
        @BindView(R.id.card_multimedia_item)
        CardView card_multimedia_item;


        public DataViewHolderShareRequest(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.iv_more_options)
        public void moreOptions() {
            createPopupMenuOption();
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.card_multimedia_item)
        public void viewMedia() {
            if (!albumResults.get(getAdapterPosition()).getShared_request().get(0).getVideo_link().equalsIgnoreCase("")) {
                //Play Video
                PlayMusicModal playMusicModal = new PlayMusicModal();
                playMusicModal.setMediaUrl(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile());
                playMusicModal.setArtworkUrl(albumResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());
                playMusicModal.setThumbnailUrl(albumResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());
                playMusicModal.setAlbum("");
                playMusicModal.setArtist("");
                playMusicModal.setAudio(false);
                playMusicModal.setTitle("Video");
                playMusicModal.setPlaylistId(albumResults.get(getAdapterPosition()).getId());

                albumSongsList.add(playMusicModal);


                Intent intent = new Intent(context, FullScreenVideoPlayerActivity.class);
                intent.putExtra(VideoPlayerActivity.EXTRA_INDEX, 0);
                intent.putExtra(VideoPlayerActivity.PLAYLIST_FLAG, albumResults.get(getAdapterPosition()).getId());
                Bundle b = new Bundle();
                b.putParcelableArrayList(Constants.ALBUM_SONGS_LIST, albumSongsList);
                intent.putExtra("video_bundle", b);
                context.startActivity(intent);

            } else if (!albumResults.get(getAdapterPosition()).getShared_request().get(0).getImage().equalsIgnoreCase("")) {
                //Play Image

                Intent intentFullscreenImage = new Intent(context, ViewFullScreenImage.class);
                Bundle b = new Bundle();
                b.putString(Constants.IMAGE, albumResults.get(getAdapterPosition()).getShared_request().get(0).getImage());
                intentFullscreenImage.putExtras(b);
                context.startActivity(intentFullscreenImage);
            }
        }

        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), this);

                if (albumResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderShareRequest.this);

                } else {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderShareRequest.this);
                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {
            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    PostDeleteModal postDeleteModal;


                    switch (tag) {

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();

        }

        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), FollowingAdapter.DataViewHolderShareRequest.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    protected class DataViewHolderChangeProfileOrCoverPic extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        //@BindView(R.id.txtvw_post_details)
        //AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        //@BindView(R.id.iv_grou_post_image)
        //AppCompatImageView iv_grou_post_image;
        @BindView(R.id.txtvw_tagged_friends_change_pic_text)
        AppCompatTextView txtvw_tagged_friends_change_pic_text;
        /*@BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;*/
        @BindView(R.id.movie_progress)
        ProgressBar mProgress;
        @BindView(R.id.iv_grou_post_image)
        AppCompatImageView iv_grou_post_image;


        public DataViewHolderChangeProfileOrCoverPic(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.iv_grou_post_image)
        public void viewImageFullscreen() {
            try {
                Intent intentFullscreenImage = new Intent(context, ViewFullScreenImage.class);
                Bundle b = new Bundle();
                b.putString(Constants.IMAGE, albumResults.get(getAdapterPosition()).getMedia().get(0).getFile());
                intentFullscreenImage.putExtras(b);
                context.startActivity(intentFullscreenImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), this);

                if (albumResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderChangeProfileOrCoverPic.this);

                } else {
                    albumResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    albumResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(albumResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", FollowingAdapter.DataViewHolderChangeProfileOrCoverPic.this);
                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {
            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), albumResults.get(getAdapterPosition()).getMedia().get(0).getFile(), "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    /*PostDeleteModal postDeleteModal;


                    switch (tag) {

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                            ((BaseActivity) context).hideLoading();
                            break;
                    }*/


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {

        }

        /*public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), DataViewHolderChangeProfileOrCoverPic.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


    public void openConfirmationDialog(String postId, String post_text, String timeStamp, String post_image, String postVideoUrl, String postVideoThumbnail) {

        Intent shareDialog = new Intent(context, SharePostActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.POSTTYPE, "POST");
        b.putString(Constants.POSTIMAGE, post_image);
        b.putString(Constants.POSTTEXT, post_text);
        b.putString(Constants.POSTTIMESTAMP, timeStamp);
        b.putString(Constants.POSTID, postId);
        b.putString(Constants.POSTVIDEO, postVideoUrl);
        b.putString(Constants.POSTTHUMBNAIL, postVideoThumbnail);
        shareDialog.putExtras(b);
        context.startActivity(shareDialog);


    }


    public void updateASingleItem(int position) {
        try {
            albumResults.get(position).setComment_count("" + (Integer.parseInt(albumResults.get(position).getComment_count()) + 1));
            notifyItemChanged(position);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        albumResults.remove(position);
        notifyItemRemoved(position);
    }


}

