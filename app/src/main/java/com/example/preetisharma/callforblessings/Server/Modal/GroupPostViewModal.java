package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 6/21/2017.
 */

public class GroupPostViewModal {


    /**
     * status : 1
     * detail : {"id":65,"post_by":{"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1495439742user_profile.jpg"},"is_friend":1,"request_respond":"NO"},"post_content":"nice","media":[{"file":"http://www.call4blessing.com/uploads/post/223/1498049545group_post_picture.jpg","type":"IMAGE","thumbnail_file":""}],"posted_at":"2  minutes   ago","comments":[{"comment_id":56,"group_post_id":65,"comment":"hahahaha","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"comment_id":57,"group_post_id":65,"comment":"hahahahanajajaja","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"comment_id":58,"group_post_id":65,"comment":"hahahahanajajaja","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"comment_id":59,"group_post_id":65,"comment":"hahahahanajajaja","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"comment_id":60,"group_post_id":65,"comment":"hahahahanajajaja","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}}],"like_count":"1","comment_count":"5","like_flag":"Liked"}
     */

    private String status;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * id : 65
         * post_by : {"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1495439742user_profile.jpg"},"is_friend":1,"request_respond":"NO"}
         * post_content : nice
         * media : [{"file":"http://www.call4blessing.com/uploads/post/223/1498049545group_post_picture.jpg","type":"IMAGE","thumbnail_file":""}]
         * posted_at : 2  minutes   ago
         * comments : [{"comment_id":56,"group_post_id":65,"comment":"hahahaha","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"comment_id":57,"group_post_id":65,"comment":"hahahahanajajaja","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"comment_id":58,"group_post_id":65,"comment":"hahahahanajajaja","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"comment_id":59,"group_post_id":65,"comment":"hahahahanajajaja","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}},{"comment_id":60,"group_post_id":65,"comment":"hahahahanajajaja","created_at":"1  minute   ago","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}}]
         * like_count : 1
         * comment_count : 5
         * like_flag : Liked
         */

        private int id;
        private PostByBean post_by;
        private String post_content;
        private String posted_at;
        private String like_count;
        private String comment_count;
        private String like_flag;
        private List<MediaBean> media;
        private List<CommentsBean> comments;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public PostByBean getPost_by() {
            return post_by;
        }

        public void setPost_by(PostByBean post_by) {
            this.post_by = post_by;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPosted_at() {
            return posted_at;
        }

        public void setPosted_at(String posted_at) {
            this.posted_at = posted_at;
        }

        public String getLike_count() {
            return like_count;
        }

        public void setLike_count(String like_count) {
            this.like_count = like_count;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getLike_flag() {
            return like_flag;
        }

        public void setLike_flag(String like_flag) {
            this.like_flag = like_flag;
        }

        public List<MediaBean> getMedia() {
            return media;
        }

        public void setMedia(List<MediaBean> media) {
            this.media = media;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public static class PostByBean {
            /**
             * id : 223
             * username : Mukesh.singh
             * email : Mukesh.singh@emptask.com
             * updated : 1
             * profile_details : {"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1495439742user_profile.jpg"}
             * is_friend : 1
             * request_respond : NO
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : Mukesh
                 * lastname : Kumar
                 * d_o_b : 1991/09/08
                 * phone : 9988549399
                 * gender : MALE
                 * location :
                 * country :
                 * state :
                 * cover_pic : http://www.call4blessing.com/uploads/coverphoto/1492602899image.jpg
                 * profile_pic : http://www.call4blessing.com/uploads/profilepic/1495439742user_profile.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }

        public static class MediaBean {
            /**
             * file : http://www.call4blessing.com/uploads/post/223/1498049545group_post_picture.jpg
             * type : IMAGE
             * thumbnail_file :
             */

            private String file;
            private String type;
            private String thumbnail_file;

            public String getFile() {
                return file;
            }

            public void setFile(String file) {
                this.file = file;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getThumbnail_file() {
                return thumbnail_file;
            }

            public void setThumbnail_file(String thumbnail_file) {
                this.thumbnail_file = thumbnail_file;
            }
        }

        public static class CommentsBean {
            /**
             * comment_id : 56
             * group_post_id : 65
             * comment : hahahaha
             * created_at : 1  minute   ago
             * user_info : {"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"},"is_friend":0,"request_respond":"NO"}
             */

            private int comment_id;
            private int group_post_id;
            private String comment;
            private String created_at;
            private UserInfoBean user_info;

            public int getComment_id() {
                return comment_id;
            }

            public void setComment_id(int comment_id) {
                this.comment_id = comment_id;
            }

            public int getGroup_post_id() {
                return group_post_id;
            }

            public void setGroup_post_id(int group_post_id) {
                this.group_post_id = group_post_id;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public UserInfoBean getUser_info() {
                return user_info;
            }

            public void setUser_info(UserInfoBean user_info) {
                this.user_info = user_info;
            }

            public static class UserInfoBean {
                /**
                 * id : 214
                 * username : plash.jindal
                 * email : plash.jindal@emptask.com
                 * updated : 1
                 * profile_details : {"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg"}
                 * is_friend : 0
                 * request_respond : NO
                 */

                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanX {
                    /**
                     * firstname : Palash
                     * lastname : Jindal
                     * d_o_b : 29-5-1996
                     * phone : 1234567890
                     * gender : MALE
                     * location :
                     * country : Spain
                     * state : Madrid
                     * cover_pic : http://www.call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg
                     * profile_pic : http://www.call4blessing.com/uploads/profilepic/1497353404user_profile.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }
        }
    }
}
