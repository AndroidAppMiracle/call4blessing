package com.example.preetisharma.callforblessings.Server.Modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by preeti.sharma on 2/1/2017.
 */

public class FriendRequestModal {
    /**
     * status : 1
     * list : [{"id":7,"status":"PENDING","user_info":{"id":57,"username":"user5","email":"user5@gmail.com","updated":1,"profile_details":{"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}},"requested_at":"2017-02-01 01:29:27","respond_at":"0000-00-00 00:00:00"}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 7
         * status : PENDING
         * user_info : {"id":57,"username":"user5","email":"user5@gmail.com","updated":1,"profile_details":{"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}}
         * requested_at : 2017-02-01 01:29:27
         * respond_at : 0000-00-00 00:00:00
         */

        private int id;
        private String status;
        private UserInfoBean user_info;
        private String requested_at;
        private String respond_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public String getRequested_at() {
            return requested_at;
        }

        public void setRequested_at(String requested_at) {
            this.requested_at = requested_at;
        }

        public String getRespond_at() {
            return respond_at;
        }

        public void setRespond_at(String respond_at) {
            this.respond_at = respond_at;
        }

        public static class UserInfoBean {
            /**
             * id : 57
             * username : user5
             * email : user5@gmail.com
             * updated : 1
             * profile_details : {"firstname":"new user","lastname":"user","d_o_b":"2017-01-09","phone":"12345678900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg"}
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private String WALL;
            @SerializedName("ABOUT INFO")
            private String about_info;
            @SerializedName("FRIEND REQUEST")
            private String friend_request;
            private String MESSAGE;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public String getWALL() {
                return WALL;
            }

            public void setWALL(String WALL) {
                this.WALL = WALL;
            }

            public String getabout_info() {
                return about_info;
            }

            public void setabout_info(String about_info) {
                this.about_info = about_info;
            }

            public String getfriend_request() {
                return friend_request;
            }

            public void setfriend_request(String friend_request) {
                this.friend_request = friend_request;
            }

            public String getMESSAGE() {
                return MESSAGE;
            }

            public void setMESSAGE(String MESSAGE) {
                this.MESSAGE = MESSAGE;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : new user
                 * lastname : user
                 * d_o_b : 2017-01-09
                 * phone : 12345678900
                 * gender : MALE
                 * location :
                 * country :
                 * state :
                 * cover_pic : http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg
                 * profile_pic : http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484779157user_profile.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }
    }
}
