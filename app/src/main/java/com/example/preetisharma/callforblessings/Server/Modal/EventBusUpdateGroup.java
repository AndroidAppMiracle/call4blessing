package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/12/2017.
 */

public class EventBusUpdateGroup {

    private final String isGroupUpdated;

    public EventBusUpdateGroup(String isGroupUpdated) {
        this.isGroupUpdated = isGroupUpdated;
    }

    public String getIsGroupUpdated() {
        return isGroupUpdated;
    }
}

