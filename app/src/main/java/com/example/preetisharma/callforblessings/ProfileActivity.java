package com.example.preetisharma.callforblessings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.FetchProfileModal;
import com.example.preetisharma.callforblessings.Server.Modal.ProfileModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.DataHolder;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/9/2017.
 */

public class ProfileActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks, ImagePickerCallback, APIServerResponse {

    @BindView(R.id.edt_txt_first_name)
    AppCompatEditText edt_txt_first_name;
    @BindView(R.id.edt_txt_last_name)
    AppCompatEditText edt_txt_last_name;
    @BindView(R.id.rd_grp_gender)
    RadioGroup rd_grp_gender;
    @BindView(R.id.rdbtn_male)
    RadioButton rdbtn_male;
    @BindView(R.id.rdbtn_female)
    RadioButton rdbtn_female;
    @BindView(R.id.edt_txt_phone_number)
    AppCompatEditText edt_txt_phone_number;
    @BindView(R.id.edt_txt_email_id)
    AppCompatEditText edt_txt_email_id;
    @BindView(R.id.edt_txt_location)
    AppCompatEditText edt_txt_location;
    @BindView(R.id.txtvw_state)
    AppCompatTextView txtvw_state;
    @BindView(R.id.txtvw_country)
    AppCompatTextView txtvw_country;
    @BindView(R.id.txtvw_add_profile)
    AppCompatTextView txtvw_add_profile;
    @BindView(R.id.img_view_profile_pic)
    AppCompatImageView img_view_profile_pic;
    @BindView(R.id.edt_txt_date_of_birth)
    AppCompatTextView edt_txt_date_of_birth;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    private ImagePicker imagePicker;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    private CameraImagePicker cameraPicker;
    private String pickerPath = "";
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int PLACE_PICKER_REQUEST = 1;
    //String[] perms = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET};
    //private int RC_LOCATION_INTERNET = 102;
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;
    private String gender = "Male";
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.edit_profile)
    FloatingActionButton edit_profile;
    @BindView(R.id.update_profile)
    AppCompatTextView update_profile;
    private int mYear, mMonth, mDay;
    //Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        updateStatusBar();
        ButterKnife.bind(this);
        txtvw_header_title.setText("Profile");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        img_view_change_password.setVisibility(View.VISIBLE);
        getmPrefs();
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().fetchPeofile(APIServerResponse.FETCHPROFILE, getUserSessionId(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void editDetails() {
        edt_txt_first_name.setEnabled(false);
        edt_txt_last_name.setEnabled(false);
        edt_txt_date_of_birth.setEnabled(false);
        edt_txt_email_id.setEnabled(false);
        edt_txt_phone_number.setEnabled(false);
        rdbtn_male.setClickable(false);
        update_profile.setVisibility(View.GONE);

        rdbtn_female.setClickable(false);
        rd_grp_gender.setClickable(false);
        img_view_profile_pic.setEnabled(false);

    }

    @OnClick(R.id.img_view_change_password)
    public void changePassword() {
        Intent changepwd = new Intent(this, ChangePassword.class);
        startActivity(changepwd);
    }

    @OnClick(R.id.edt_txt_date_of_birth)
    void date_picker_dialog() {
        hideKeyboard();
        selectDate();
        //openDateDia();
    }


/*
    public void openDateDia() {
        final Calendar cal = Calendar.getInstance(TimeZone.getDefault());

        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(calendar.getTime());
                        */
/*yyyy-MM-dd*//*

                        try {
                            if (isValidDate(format)) {
                                edt_txt_date_of_birth.setText(format);
                            } else {
                                //   showToast("Select Valid Date of Birth", Toast.LENGTH_SHORT);
                                edt_txt_date_of_birth.requestFocus();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


//                        }else{
//                            Utils.showToast(BookAppointmentActivity.this, "Please select future date");
//                        }

//                        dateTime = year + "-" + (int)(monthOfYear+1) + "-" + dayOfMonth;

                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());

        datePicker.setCancelable(true);
        datePicker.setTitle("Select the date");
        datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);
        datePicker.getDatePicker().setMaxDate(new Date().getTime());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }

    */
/*Check valid date of birth is valid*//*


    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).parse(pDateString);
        */
/*yyyy-MM-dd*//*

        return new Date().before(date);
    }
*/


    public void selectDate() {

        try {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            edt_txt_date_of_birth.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*@OnClick(R.id.edt_txt_location)
    public void getLocationPicker() {
        if (EasyPermissions.hasPermissions(this, perms)) {
            try {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                startActivityForResult(intentBuilder.build(this), PLACE_PICKER_REQUEST);

            } catch (GooglePlayServicesRepairableException
                    | GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.internet_access), RC_LOCATION_INTERNET, perms);
        }


    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
           /* if (requestCode == PLACE_PICKER_REQUEST) {

                final Place place = PlacePicker.getPlace(this, data);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();


                edt_txt_location.setText(name);
                txtvw_state.setText(place.getLocale().getCountry());
                txtvw_country.setText(place.getAddress());


            }*/
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
            Intent intent = intentBuilder.build(this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.img_view_profile_pic)
    public void imagePickerDialog() {
        new AlertDialog.Builder(this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }


    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            img_view_profile_pic.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);
    }

    @OnClick(R.id.txtvw_add_profile)
    public void saveProfile() {
        if (validation()) {
            if (EasyPermissions.hasPermissions(this, Manifest.permission.INTERNET)) {
                if (isConnectedToInternet()) {
                    showLoading();
                    if (pickerPath.equalsIgnoreCase("")) {
                        ServerAPI.getInstance().saveProfile(APIServerResponse.SAVEPROFILE, getUserSessionId(), edt_txt_first_name.getText().toString(), edt_txt_last_name.getText().toString(), gender, edt_txt_phone_number.getText().toString(), edt_txt_date_of_birth.getText().toString(), edt_txt_email_id.getText().toString(), this);
                    } else {
                        ServerAPI.getInstance().saveProfileWithProfile(APIServerResponse.SAVEPROFILE, getUserSessionId(), edt_txt_first_name.getText().toString(), edt_txt_last_name.getText().toString(), gender, edt_txt_phone_number.getText().toString(), edt_txt_date_of_birth.getText().toString(), edt_txt_email_id.getText().toString(), pickerPath, this);
                    }
                } else {
                    showSnack("Not connected to Internet ");
                }
            }

        }


    }

    public boolean validation() {
        if (edt_txt_first_name.getText().toString().isEmpty()) {
            showToast(Constants.FIRST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(edt_txt_first_name.getText().charAt(0))) {
            showToast(Constants.FIRST_NAME_START, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_last_name.getText().toString().isEmpty()) {
            showToast(Constants.LAST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(edt_txt_last_name.getText().charAt(0))) {
            showToast(Constants.EMAIL_START, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_phone_number.getText().toString().isEmpty()) {
            showToast(Constants.PHONE_NUMBER_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_phone_number.getText().toString().length() < 10) {
            showToast(Constants.PHONE_NUMBER_LENGTH, Toast.LENGTH_SHORT);
            return false;
        } else if (gender.isEmpty()) {
            showToast("Select Gender", Toast.LENGTH_SHORT);
            return false;
        } else {
            return true;
        }


    }

    @OnClick(R.id.edit_profile)
    public void allowEditing() {
        edt_txt_first_name.setEnabled(true);
        edt_txt_last_name.setEnabled(true);
        edt_txt_date_of_birth.setEnabled(true);
        edt_txt_email_id.setVisibility(View.GONE);
        edt_txt_phone_number.setEnabled(true);
        rd_grp_gender.setEnabled(true);
        edit_profile.setVisibility(View.GONE);
        txtvw_add_profile.setVisibility(View.VISIBLE);
        rdbtn_female.setEnabled(true);
        rdbtn_male.setEnabled(true);
        rdbtn_female.setClickable(true);
        rdbtn_male.setClickable(true);
        rd_grp_gender.setClickable(true);
        img_view_profile_pic.setEnabled(true);
        update_profile.setVisibility(View.VISIBLE);
    }


    @Override
    public void onSuccess(int tag, Response response) {
        ProfileModal profileModal;
        hideLoading();
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.SAVEPROFILE:

                        profileModal = (ProfileModal) response.body();


                        if (profileModal.getStatus().equals("1")) {

                            edit_profile.setVisibility(View.VISIBLE);
                            txtvw_add_profile.setVisibility(View.GONE);
                            editDetails();

                            showToast(profileModal.getMessage(), Toast.LENGTH_SHORT);
                            DataHolder.setFirst_name(profileModal.getDetail().getProfile_details().getFirstname());
                            DataHolder.setLast_name(profileModal.getDetail().getProfile_details().getLastname());
                            DataHolder.setGender(profileModal.getDetail().getProfile_details().getGender());
                            DataHolder.setPhone_number(profileModal.getDetail().getProfile_details().getPhone());
                            DataHolder.setProfilePic(profileModal.getDetail().getProfile_details().getProfile_pic());
                            setUserImage(profileModal.getDetail().getProfile_details().getProfile_pic());
                            setFirstName(profileModal.getDetail().getProfile_details().getFirstname());
                            setLastName(profileModal.getDetail().getProfile_details().getLastname());
                            setUserEmailID(profileModal.getDetail().getEmail());
                            setUserGender(profileModal.getDetail().getProfile_details().getGender());
                            setFullName(profileModal.getDetail().getProfile_details().getFirstname() + " " + profileModal.getDetail().getProfile_details().getLastname());

                        } else if (profileModal.getStatus().equals("0")) {

                            showToast(profileModal.getMessage(), Toast.LENGTH_SHORT);
                        }
                        break;
                    case APIServerResponse.FETCHPROFILE:
                        FetchProfileModal fetchProfile = (FetchProfileModal) response.body();
                        if (fetchProfile.getStatus().equals("1")) {
                            edt_txt_first_name.setText(fetchProfile.getDetail().getProfile_details().getFirstname());
                            edt_txt_last_name.setText(fetchProfile.getDetail().getProfile_details().getLastname());
                            if (fetchProfile.getDetail().getProfile_details().getGender().equalsIgnoreCase("Male")) {
                                rdbtn_male.setChecked(true);
                            } else {
                                rdbtn_female.setChecked(true);
                            }
                            edt_txt_phone_number.setText(fetchProfile.getDetail().getProfile_details().getPhone());
                            edt_txt_email_id.setText(fetchProfile.getDetail().getEmail());
                            edt_txt_email_id.setEnabled(false);
                            editDetails();
                            edt_txt_date_of_birth.setText(fetchProfile.getDetail().getProfile_details().getD_o_b());
                            gender = fetchProfile.getDetail().getProfile_details().getGender();

                            rd_grp_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(RadioGroup group, int checkedId) {
                                    switch (checkedId) {
                                        case R.id.rdbtn_female:
                                            gender = rdbtn_female.getText().toString();
                                            break;
                                        case R.id.rdbtn_male:
                                            gender = rdbtn_male.getText().toString();
                                            break;


                                    }
                                }
                            });
                            if (getUserGender().equalsIgnoreCase("male")) {
                                Glide.with(this).load(fetchProfile.getDetail().getProfile_details().getProfile_pic()).thumbnail(0.1f).fitCenter().placeholder(R.mipmap.ic_placeholder_male_avatar).into(img_view_profile_pic);
                            } else {
                                Glide.with(this).load(fetchProfile.getDetail().getProfile_details().getProfile_pic()).thumbnail(0.1f).fitCenter().placeholder(R.mipmap.ic_placeholder_female_avatar).into(img_view_profile_pic);

                            }
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }
}
