package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.preetisharma.callforblessings.Fragment.HelpRequestFragment;
import com.example.preetisharma.callforblessings.Fragment.PrayerFragment;
import com.example.preetisharma.callforblessings.Fragment.TestimonyFragment;

/**
 * Created by preeti.sharma on 1/12/2017.
 */

public class HTPTabAdapter extends FragmentPagerAdapter {
    //integer to count number of tabs
    int tabCount;
    Context mContext;

    //Constructor to the class
    public HTPTabAdapter(FragmentManager fm, int tabCount, Context mContext) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
        this.mContext = mContext;

    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new PrayerFragment();
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                return fragment;


            case 1:

                fragment = new TestimonyFragment();
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                return fragment;

            case 2:
                fragment = new HelpRequestFragment();
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                return fragment;

            default:
                fragment = new PrayerFragment();
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                notifyDataSetChanged();
                return fragment;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Prayer";
            case 1:
                return "Testimony";
            case 2:
                return "Help Request";
            default:
                return "Prayer";
        }
        //return null;
    }

    @Override
    public int getCount() {
        return tabCount;
    }


}
