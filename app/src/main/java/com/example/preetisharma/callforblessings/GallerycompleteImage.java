package com.example.preetisharma.callforblessings;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.TouchImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 2/9/2017.
 */

public class GallerycompleteImage extends BaseActivity {


    @BindView(R.id.full_image)
    TouchImageView full_image;
    @BindView(R.id.image_progress)
    ProgressBar image_progress;
    String imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_complete_image);
        updateStatusBar();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            imageUri = getIntent().getExtras().getString("image");
        }
        //Glide.with(this).load(imageUri).asBitmap().placeholder(R.drawable.placeholder).into(full_image);

        Glide.with(this).load(imageUri)
                .asBitmap()
                .placeholder(R.drawable.placeholder_callforblessings)
                .into(new BitmapImageViewTarget(full_image) {
                    @Override
                    public void onResourceReady(Bitmap drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        image_progress.setVisibility(View.GONE);
                    }
                });

    }
}
