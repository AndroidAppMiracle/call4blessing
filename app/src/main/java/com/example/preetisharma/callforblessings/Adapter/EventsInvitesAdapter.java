package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.demo.EventsDetailsActivityNew;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventsInvitesModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyInvitationRequestAcceptReject;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/8/2017.
 */

public class EventsInvitesAdapter extends RecyclerView.Adapter<EventsInvitesAdapter.DataViewHolder> {


    private Context mContext;
    private List<EventsInvitesModal.ListBean> myEventInvitesList = new ArrayList<>();

    public EventsInvitesAdapter(Context mContext, List<EventsInvitesModal.ListBean> list) {
        this.myEventInvitesList = list;
        this.mContext = mContext;
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_events_invitation_adapter_layout, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, int position) {

        try {
            holder.txtvw_event_invite_name.setText(myEventInvitesList.get(position).getEvent_name());
            holder.txtvw_event_invite_location.setText(myEventInvitesList.get(position).getEvent_location());
            holder.txtvw_event_invite_date.setText(myEventInvitesList.get(position).getEvent_date());
            Glide.with(mContext).load(myEventInvitesList.get(position).getEvent_photo()).placeholder(R.drawable.placeholder_callforblessings).centerCrop().into(holder.iv_event_invite_image);

            if (myEventInvitesList.get(position).getInvitation_status().equals("ACCEPTED")) {
                holder.txtvw_my_event_status_accepted.setVisibility(View.VISIBLE);
                holder.txtvw_my_event_status_accepted.setText(myEventInvitesList.get(position).getInvitation_status());
                holder.txtvw_event_invite_accept.setVisibility(View.INVISIBLE);
                holder.txtvw_event_invitation_reject.setVisibility(View.INVISIBLE);
            } else if (myEventInvitesList.get(position).getInvitation_status().equals("REJECTED")) {

                holder.txtvw_my_event_status_accepted.setVisibility(View.VISIBLE);
                holder.txtvw_my_event_status_accepted.setText(myEventInvitesList.get(position).getInvitation_status());
                holder.txtvw_event_invite_accept.setVisibility(View.INVISIBLE);
                holder.txtvw_event_invitation_reject.setVisibility(View.INVISIBLE);

            } else {
                holder.txtvw_my_event_status_accepted.setVisibility(View.GONE);
                //holder.txtvw_my_event_status_accepted.setText(myEventInvitesList.get(position).getInvites().get(position).getInvite_status());
                holder.txtvw_event_invite_accept.setVisibility(View.VISIBLE);
                holder.txtvw_event_invitation_reject.setVisibility(View.VISIBLE);
            }
            holder.ll_event_invite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, EventsDetailsActivityNew.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.EVENT_ID, String.valueOf(myEventInvitesList.get(holder.getAdapterPosition()).getId()));
                    b.putString(Constants.EVENT_TYPE_FLAG, "Event Invites");
                    b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(holder.getAdapterPosition()).getInvitation_status());
                    i.putExtras(b);
                    mContext.startActivity(i);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (myEventInvitesList.size() != 0) {
            return myEventInvitesList.size();
        } else {
            return 0;
        }

    }


    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_event_invite_name)
        AppCompatTextView txtvw_event_invite_name;

        @BindView(R.id.txtvw_event_invite_location)
        AppCompatTextView txtvw_event_invite_location;
        @BindView(R.id.txtvw_event_invite_date)
        AppCompatTextView txtvw_event_invite_date;

        @BindView(R.id.txtvw_event_invite_accept)
        AppCompatTextView txtvw_event_invite_accept;

        @BindView(R.id.txtvw_event_invitation_reject)
        AppCompatTextView txtvw_event_invitation_reject;

        @BindView(R.id.txtvw_my_event_status_accepted)
        AppCompatTextView txtvw_my_event_status_accepted;


        /*@BindView(R.id.txtvw_my_event_edit)
        AppCompatTextView txtvw_my_event_edit;*/

        @BindView(R.id.iv_event_invite_image)
        AppCompatImageView iv_event_invite_image;

        @BindView(R.id.ll_event_invite)
        LinearLayout ll_event_invite;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/

            txtvw_event_invite_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().acceptOrRejectEventInvitation(APIServerResponse.EVENT_INVITE_REQUEST_RESPONSE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myEventInvitesList.get(getAdapterPosition()).getId()), "ACCEPTED", DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });

            txtvw_event_invitation_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) mContext).showLoading();
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().acceptOrRejectEventInvitation(APIServerResponse.EVENT_INVITE_REQUEST_RESPONSE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myEventInvitesList.get(getAdapterPosition()).getId()), "REJECTED", DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    MyInvitationRequestAcceptReject myInvitationRequestAcceptReject;

                    switch (tag) {
                        case APIServerResponse.EVENT_INVITE_REQUEST_RESPONSE:
                            myInvitationRequestAcceptReject = (MyInvitationRequestAcceptReject) response.body();
                            if (myInvitationRequestAcceptReject.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast(myInvitationRequestAcceptReject.getMessage(), Toast.LENGTH_SHORT);
                                if (myInvitationRequestAcceptReject.getMessage().contains("ACCEPTED")) {
                                    myEventInvitesList.get(getAdapterPosition()).setInvitation_status("ACCEPTED");
                                    notifyDataList(myEventInvitesList);

                                } else {
                                    //for now showing rejected
                                    myEventInvitesList.get(getAdapterPosition()).setInvitation_status("REJECTED");
                                    notifyDataList(myEventInvitesList);

                                    //use this to remove when rejected
                                    //refresh(getAdapterPosition());
                                }

                            } else {
                                ((BaseActivity) mContext).showToast(myInvitationRequestAcceptReject.getMessage(), Toast.LENGTH_SHORT);
                            }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
           /* switch (tag) {
                case APIServerResponse.DELETEEVENT:
                    ((BaseActivity) mContext).showToast(throwable.getMessage().toString(), Toast.LENGTH_SHORT);

            }*/
        }
    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        myEventInvitesList.remove(position);
        notifyItemRemoved(position);
    }

    public void notifyDataList(List<EventsInvitesModal.ListBean> list) {
        Log.d("notifyData ", list.size() + "");
        this.myEventInvitesList = list;
        notifyDataSetChanged();
    }
}
