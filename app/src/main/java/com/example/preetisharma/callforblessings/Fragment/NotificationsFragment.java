package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.NotificationsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/12/2017.
 */

public class NotificationsFragment extends Fragment implements APIServerResponse {

    RecyclerView rv_noti_listing;

    List<NotificationsModal.ListBean> notificationList = new ArrayList<>();

    AppCompatTextView atv_message;
    NotificationsAdapter adapter;

    public static NotificationsFragment newInstance() {

        NotificationsFragment fragment = new NotificationsFragment();
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseActivity) getActivity()).dismissDialog();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.notifications, container, false);
        ButterKnife.bind(getActivity());
        ((BaseActivity) getActivity()).getmPrefs();

        rv_noti_listing = (RecyclerView) rootView.findViewById(R.id.rv_noti_listing);
        rv_noti_listing.setItemAnimator(new DefaultItemAnimator());
        /*rv_noti_listing.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));*/
        rv_noti_listing.setLayoutManager(new LinearLayoutManager(getActivity()));

        atv_message = (AppCompatTextView) rootView.findViewById(R.id.atv_message);


        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().getMyNotifications(APIServerResponse.MY_NOTIFICATIONS, ((BaseActivity) getActivity()).getUserSessionId(), this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        return rootView;
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            NotificationsModal notificationsModal;

            switch (tag) {

                case APIServerResponse.MY_NOTIFICATIONS:
                    notificationsModal = (NotificationsModal) response.body();
                    if (notificationsModal.getStatus().equals("1")) {

                        if (notificationsModal.getList().size() > 0) {

                            notificationList = notificationsModal.getList();
                            adapter = new NotificationsAdapter(getActivity(), notificationList);
                            rv_noti_listing.setAdapter(adapter);
                            /*rv_noti_listing.setItemAnimator(new DefaultItemAnimator());*/

                        } else {
                            rv_noti_listing.setVisibility(View.GONE);
                            atv_message.setVisibility(View.VISIBLE);
                        }


                    } else {
                        rv_noti_listing.setVisibility(View.GONE);
                        atv_message.setVisibility(View.VISIBLE);
                    }

                    ((BaseActivity) getActivity()).hideLoading();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        ((BaseActivity) getActivity()).hideLoading();
    }


    @Override
    public void onResume() {
        super.onResume();

        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().getMyNotifications(APIServerResponse.MY_NOTIFICATIONS, ((BaseActivity) getActivity()).getUserSessionId(), this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void repositionRecyclerViewOnTabClick() {
        try {

            if (adapter != null && adapter.getItemCount() > 0) {
                //home_listing.scrollToPosition(0);
                rv_noti_listing.smoothScrollToPosition(0);
                /*if (!nestedscroll.isSmoothScrollingEnabled()) {
                    nestedscroll.setSmoothScrollingEnabled(true);
                }
                nestedscroll.smoothScrollTo(0, 0);*/
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
