package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.ChangeProfileCoverPicModal;
import com.example.preetisharma.callforblessings.Server.Modal.RemoveProfileModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/13/2017.
 */

public class ChangeCoverProfilePicActivity extends BaseActivity implements APIServerResponse, ImagePickerCallback, EasyPermissions.PermissionCallbacks {
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;
    private CameraImagePicker cameraPicker;
    private String pickerPath = "";

    private ImagePicker imagePicker;
    private String choosenPic = "";
    @BindView(R.id.txtvw_remove_pic)
    AppCompatTextView txtvw_remove_pic;
    @BindView(R.id.txtvw_add_pic)
    AppCompatTextView txtvw_add_pic;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.cropImageView)
    CropImageView cropImageView;
    @BindView(R.id.txtvw_save_pic)
    AppCompatTextView txtvw_save_pic;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_user_profile)
    AppCompatImageView img_user_profile;
    @BindView(R.id.txtvw_profile_text)
    AppCompatTextView txtvw_profile_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_cover_pic_layout);
        updateStatusBar();
        ButterKnife.bind(this);

        txtvw_save_pic.setText("Set Cover Picture");
        txtvw_header_title.setText("Change Cover Picture");
        img_view_change_password.setVisibility(View.GONE);
        txtvw_remove_pic.setText("Remove cover pic");
        if (getIntent().getExtras().getBoolean(Constants.TIMELINE_ENABLED, false)) {
            txtvw_remove_pic.setVisibility(View.VISIBLE);
            txtvw_save_pic.setVisibility(View.VISIBLE);
            txtvw_profile_text.setVisibility(View.VISIBLE);
            txtvw_add_pic.setVisibility(View.VISIBLE);
        } else {
            txtvw_remove_pic.setVisibility(View.GONE);
            txtvw_save_pic.setVisibility(View.GONE);
            txtvw_profile_text.setVisibility(View.GONE);
            txtvw_add_pic.setVisibility(View.GONE);
        }
        if (getIntent().getExtras() != null) {
            Glide.with(this).load(getIntent().getExtras().getString(Constants.COVER_PREVIEW)).placeholder(R.drawable.placeholder_callforblessings).into(img_user_profile);
        }
        txtvw_remove_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isConnectedToInternet()) {
                    showLoading();
                    ServerAPI.getInstance().removeProfileOrCoverPic(APIServerResponse.REMOVEPROFILEPIC, getUserSessionId(), "COVER_PIC", ChangeCoverProfilePicActivity.this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        });
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        cropImageView.setVisibility(View.GONE);
    }

    @OnClick(R.id.txtvw_save_pic)
    public void save_cover_pic() {
        if (isConnectedToInternet()) {

            if (!pickerPath.equals("") && cropImageView.getImageUri() != null && cropImageView.getImageUri().getPath() != null) {
                showLoading();
                ServerAPI.getInstance().changeProfileOrCoverPic(APIServerResponse.CHANGE_PROFILE_COVER_PIC, getUserSessionId(), "", cropImageView.getImageUri().getPath(), ChangeCoverProfilePicActivity.this);
            } else {
                hideLoading();
                showToast("Please select an image", Toast.LENGTH_LONG);
            }

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.txtvw_add_pic)
    public void choosePic() {
        new AlertDialog.Builder(this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        choosenPic = "Profile";
                        takePicture();


                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        choosenPic = "Profile";
                        pickImageSingle();


                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        cropImageView.setVisibility(View.VISIBLE);
        txtvw_save_pic.setVisibility(View.VISIBLE);
        img_user_profile.setVisibility(View.GONE);
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            cropImageView.setImageUriAsync(Uri.fromFile(new File(pickerPath)));
            CropImage.activity(Uri.fromFile(new File(pickerPath))).setGuidelines(CropImageView.Guidelines.ON).start(ChangeCoverProfilePicActivity.this);


        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
           /* if (requestCode == PLACE_PICKER_REQUEST) {

                final Place place = PlacePicker.getPlace(this, data);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();


                edt_txt_location.setText(name);
                txtvw_state.setText(place.getLocale().getCountry());
                txtvw_country.setText(place.getAddress());


            }*/
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

                if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        Uri resultUri = result.getUri();
                    /*Glide.with(ChangeProfilePicActivity.this).load(resultUri).into(holder.iv_event_invite_image);*/
                        cropImageView.setImageUriAsync(resultUri);

                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        Exception error = result.getError();
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);

    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        try {

            ChangeProfileCoverPicModal changeProfileCoverPicModal;
            RemoveProfileModal removeProfileModal;
            switch (tag) {
                case APIServerResponse.CHANGE_PROFILE_COVER_PIC:
                    if (response.isSuccessful()) {
                        changeProfileCoverPicModal = (ChangeProfileCoverPicModal) response.body();

                        if (changeProfileCoverPicModal.getStatus().equals("1")) {
                            showToast("Cover pic updated successfully", Toast.LENGTH_SHORT);
                            Log.e("Cover pic", cropImageView.getImageUri().getPath());
                            setUserCoverImage(cropImageView.getImageUri().getPath());
                            finish();
                        } else {
                            showToast(changeProfileCoverPicModal.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
                case APIServerResponse.REMOVEPROFILEPIC:
                    if (response.isSuccessful()) {
                        removeProfileModal = (RemoveProfileModal) response.body();
                        if (removeProfileModal.getStatus().equals("1")) {
                            showToast("Cover pic removed successfully", Toast.LENGTH_SHORT);
                            Glide.with(this).load("").placeholder(R.drawable.placeholder_callforblessings).into(img_user_profile);
                            setUserCoverImage("");
                            finish();
                        } else {
                            showToast(removeProfileModal.getMessage(), Toast.LENGTH_SHORT);
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
