package com.example.preetisharma.callforblessings;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BookPaymentModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.OrderConfirmationActivity;
import com.payu.india.Extras.PayUSdkDetails;
import com.payu.india.Interfaces.OneClickPaymentListener;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.payuui.Activity.PayUBaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 4/3/2017.
 */

public class PaymentGatewayActivity extends BaseActivity implements OneClickPaymentListener, APIServerResponse {
    // These will hold all the payment parameters
    private PaymentParams mPaymentParams;
    private String merchantKey, userCredentials;
    // This sets the configuration
    private PayuConfig payuConfig;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.edt_txt_first_name)
    AppCompatEditText edt_txt_first_name;
    @BindView(R.id.edt_txt_last_name)
    AppCompatEditText edt_txt_last_name;
    @BindView(R.id.edt_txt_email)
    AppCompatEditText edt_txt_email;
    @BindView(R.id.edt_txt_city)
    AppCompatEditText edt_txt_city;
    @BindView(R.id.edt_txt_zipcode)
    AppCompatEditText edt_txt_zipcode;
    @BindView(R.id.edt_txt_mobile_number)
    AppCompatEditText edt_txt_mobile_number;
    @BindView(R.id.edt_txt_street_address)
    AppCompatEditText edt_txt_street_address;
    @BindView(R.id.txtvw_pay)
    AppCompatTextView txtvw_pay;
    @BindView(R.id.txtvw_amount)
    AppCompatTextView txtvw_amount;
    String bookId = "", bookAmount = "", bookName = "", downloadType = "";
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;


    PayuHashes payuHashes = new PayuHashes();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_payment_details);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            bookId = getIntent().getExtras().getString(Constants.BOOK_ID);
            bookAmount = getIntent().getExtras().getString(Constants.AMOUNT);
            bookName = getIntent().getExtras().getString(Constants.BOOK_DESC);
            downloadType = getIntent().getExtras().getString(Constants.TYPE);
        }
        //TODO Must write this code if integrating One Tap payments
        //  OnetapCallback.setOneTapCallback(PaymentGatewayActivity.this);
        //TODO Must write below code in your activity to set up initial context for PayU
        Payu.setInstance(this);
        //PayUSdkDetails payUSdkDetails = new PayUSdkDetails();
        txtvw_header_title.setText("Payment Details");
        txtvw_amount.setText("Rs. " + bookAmount);
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //Toast.makeText(this, "Build No: " + payUSdkDetails.getSdkBuildNumber() + "\n Build Type: " + payUSdkDetails.getSdkBuildType() + " \n Build Flavor: " + payUSdkDetails.getSdkFlavor() + "\n Application Id: " + payUSdkDetails.getSdkApplicationId() + "\n Version Code: " + payUSdkDetails.getSdkVersionCode() + "\n Version Name: " + payUSdkDetails.getSdkVersionName(), Toast.LENGTH_LONG).show();
    }

    public void sendPaymentDeatils(View view) {
        if (validation()) {
            merchantKey = getResources().getString(R.string.merchant_key);
            userCredentials = merchantKey + ":" + edt_txt_email.getText().toString().trim();
            if (isConnectedToInternet()) {

                if (downloadType.equalsIgnoreCase(Constants.BOOK)) {
                    ServerAPI.getInstance().sendPaymentDeatilstoServer(APIServerResponse.BOOKPAYMENTHASH, getUserSessionId(), bookId, "BOOK", edt_txt_first_name.getText().toString().trim(), edt_txt_last_name.getText().toString().trim(), edt_txt_email.getText().toString().trim(), edt_txt_city.getText().toString().trim(), edt_txt_zipcode.getText().toString().trim(), edt_txt_mobile_number.getText().toString().trim(), edt_txt_street_address.getText().toString().trim(), userCredentials, this);
                } else if (downloadType.equalsIgnoreCase(Constants.DONATION)) {
                    ServerAPI.getInstance().sendDonationPaymentDetailsToServer(APIServerResponse.DONATION_PAYMENT_HASH, getUserSessionId(), edt_txt_first_name.getText().toString().trim(), edt_txt_last_name.getText().toString().trim(), edt_txt_email.getText().toString().trim(), edt_txt_city.getText().toString().trim(), edt_txt_zipcode.getText().toString().trim(), edt_txt_mobile_number.getText().toString().trim(), edt_txt_street_address.getText().toString().trim(), bookAmount, this);
                }


            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }
    }

    public void navigateToBaseActivity(PayuHashes payhash, String transactionId) {

        String amount = bookAmount;
        /*String amount = txtvw_amount.getText().toString();*/
        //String email = edt_txt_email.getText().toString();


        //TODO Below are mandatory params for hash generation
        mPaymentParams = new PaymentParams();
        /**
         * For Test Environment, merchantKey = "gtKFFx"
         * For Production Environment, merchantKey should be your live key or for testing in live you can use "0MQaQP"
         */
        mPaymentParams.setKey(merchantKey);
        mPaymentParams.setAmount(amount);
        mPaymentParams.setProductInfo(bookName);
        mPaymentParams.setFirstName(edt_txt_first_name.getText().toString());
        mPaymentParams.setLastName(edt_txt_last_name.getText().toString());
        mPaymentParams.setEmail(edt_txt_email.getText().toString());
        mPaymentParams.setCity(edt_txt_city.getText().toString());
        mPaymentParams.setZipCode(edt_txt_zipcode.getText().toString());
        mPaymentParams.setPhone(edt_txt_mobile_number.getText().toString());
        mPaymentParams.setAddress1(edt_txt_street_address.getText().toString());
        mPaymentParams.setHash(payhash.getPaymentHash());

        /*
        * Transaction Id should be kept unique for each transaction.
        * */
        mPaymentParams.setTxnId(transactionId);

        /**
         * Surl --> Success url is where the transaction response is posted by PayU on successful transaction
         * Furl --> Failre url is where the transaction response is posted by PayU on failed transaction
         */
        mPaymentParams.setSurl(Constants.PAYU_URL);
        mPaymentParams.setFurl(Constants.PAYU_URL);

        /*mPaymentParams.setSurl("http://dev.miracleglobal.com/cal-php/web/payment/callback");
        mPaymentParams.setFurl("http://dev.miracleglobal.com/cal-php/web/payment/callback");*/

         /*
         * udf1 to udf5 are options params where you can pass additional information related to transaction.
         * If you don't want to use it, then send them as empty string like, udf1=""
         * */
        mPaymentParams.setUdf1("");
        mPaymentParams.setUdf2("");
        mPaymentParams.setUdf3("");
        mPaymentParams.setUdf4("");
        mPaymentParams.setUdf5("");

        /**
         * These are used for store card feature. If you are not using it then user_credentials = "default"
         * user_credentials takes of the form like user_credentials = "merchant_key : user_id"
         * here merchant_key = your merchant key,
         * user_id = unique id related to user like, email, phone number, etc.
         * */
        mPaymentParams.setUserCredentials(userCredentials);

        //TODO Pass this param only if using offer key
        //mPaymentParams.setOfferKey("cardnumber@8370");

        //TODO Sets the payment environment in PayuConfig object
        payuConfig = new PayuConfig();
        payuConfig.setEnvironment(PayuConstants.PRODUCTION_ENV);
        launchSdkUI(payhash);


    }

    @Override
    public HashMap<String, String> getAllOneClickHash(String userCredentials) {
        return null;
    }

    @Override
    public void getOneClickHash(String cardToken, String merchantKey, String userCredentials) {

    }

    /**
     * This method stores merchantHash and cardToken on merchant server.
     *
     * @param cardToken    card token received in transaction response
     * @param merchantHash merchantHash received in transaction response
     */
    private void storeMerchantHash(String cardToken, String merchantHash) {

        final String postParams = "merchant_key=" + merchantKey + "&user_credentials=" + userCredentials + "&card_token=" + cardToken + "&merchant_hash=" + merchantHash;

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {

                    //TODO Deploy a file on your server for storing cardToken and merchantHash nad replace below url with your server side file url.
                    URL url = new URL("https://payu.herokuapp.com/store_merchant_hash");
                    byte[] postParamsByte = postParams.getBytes("UTF-8");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);
                    InputStream responseInputStream = conn.getInputStream();
                    StringBuffer responseStringBuffer = new StringBuffer();
                    byte[] byteContainer = new byte[1024];
                    for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                        responseStringBuffer.append(new String(byteContainer, 0, i));
                    }
                    JSONObject response = new JSONObject(responseStringBuffer.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                this.cancel(true);
            }
        }.execute();
    }


    @Override
    public void saveOneClickHash(String cardToken, String oneClickHash) {

    }

    @Override
    public void deleteOneClickHash(String cardToken, String userCredentials) {

        final String postParams = "card_token=" + cardToken;

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    //TODO Replace below url with your server side file url.
                    URL url = new URL("https://payu.herokuapp.com/delete_merchant_hash");

                    byte[] postParamsByte = postParams.getBytes("UTF-8");

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                this.cancel(true);
            }
        }.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {

                    /**
                     * Here, data.getStringExtra("payu_response") ---> Implicit response sent by PayU
                     * data.getStringExtra("result") ---> Response received from merchant's Surl/Furl
                     *
                     * PayU sends the same response to merchant server and in app. In response check the value of key "status"
                     * for identifying status of transaction. There are two possible status like, success or failure
                     * */


                    try {
                        JSONObject jsonObject = new JSONObject(data.getStringExtra("payu_response"));

                        if (downloadType.equalsIgnoreCase(Constants.BOOK)) {
                            if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                                Intent sendData = new Intent(this, OrderConfirmationActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.TRANSACTIONSTATUS, Constants.SUCCESS);
                                b.putString(Constants.TRANSACTIONID, jsonObject.getString("txnid"));
                                sendData.putExtras(b);
                                startActivity(sendData);
                                finish();
                            } else {
                                Intent sendData = new Intent(this, OrderConfirmationActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.TRANSACTIONSTATUS, Constants.FAILURE);
                                b.putString(Constants.TRANSACTIONID, jsonObject.getString("txnid"));
                                sendData.putExtras(b);
                                startActivity(sendData);
                                finish();
                            }
                        } else if (downloadType.equalsIgnoreCase(Constants.DONATION)) {
                            if (jsonObject.getString("status").equalsIgnoreCase("success")) {

                                Intent intentDonationResult = new Intent();
                                intentDonationResult.setData(Uri.parse(Constants.SUCCESS));
                                setResult(RESULT_OK, intentDonationResult);
                                finish();
                            } else {
                                Intent intentDonationResult = new Intent();
                                intentDonationResult.setData(Uri.parse(Constants.FAILURE));
                                setResult(RESULT_OK, intentDonationResult);
                                finish();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Toast.makeText(this, getString(R.string.could_not_receive_data), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public boolean validation() {
        if (edt_txt_first_name.getText().toString().isEmpty()) {
            showToast(Constants.FIRST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_first_name.setError(Constants.FIRST_NAMENOT_BLANK);
            return false;
        } else if (edt_txt_last_name.getText().toString().isEmpty()) {
            showToast(Constants.LAST_NAMENOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_last_name.setError(Constants.LAST_NAMENOT_BLANK);
            return false;
        } else if (edt_txt_email.getText().toString().isEmpty()) {
            showToast(Constants.EMAIL_NOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_email.setError(Constants.EMAIL_NOT_BLANK);
            return false;
        } else if (Character.isDigit(edt_txt_email.getText().charAt(0))) {
            showToast(Constants.EMAIL_START, Toast.LENGTH_SHORT);
            return false;
        } else if (!isValidEmail(edt_txt_email.getText().toString())) {
            showToast(Constants.EMAIL_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_city.getText().toString().isEmpty()) {
            showToast("City should not be blank", Toast.LENGTH_SHORT);
            edt_txt_city.setError("City should not be blank");
            return false;
        } else if (edt_txt_zipcode.getText().toString().isEmpty()) {
            showToast("Zipcode should not be blank", Toast.LENGTH_SHORT);
            edt_txt_zipcode.setError("Zipcode should not be blank");
            return false;
        } else if (edt_txt_mobile_number.getText().toString().isEmpty()) {
            showToast(Constants.PHONE_NUMBER_NOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_mobile_number.setError(Constants.PHONE_NUMBER_NOT_BLANK);
            return false;
        } else if (edt_txt_mobile_number.getText().toString().isEmpty() /*&& !PhoneNumberUtils.isGlobalPhoneNumber(edt_txt_mobile_number.getText().toString().trim())*/) {
            showToast(getString(R.string.enter_a_valid_number), Toast.LENGTH_SHORT);
            edt_txt_mobile_number.setError(getString(R.string.enter_a_valid_number));
            return false;
        } else if (edt_txt_mobile_number.getText().toString().length() < 10) {
            showToast(getString(R.string.enter_a_valid_number), Toast.LENGTH_SHORT);
            return false;
        } else if (edt_txt_street_address.getText().toString().isEmpty()) {
            showToast("Street Address should not be blank", Toast.LENGTH_SHORT);
            return false;
        } else {
            return true;
        }


    }

    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    /**
     * This method generates hash from server.
     *
     * @param mPaymentParams payments params used for hash generation
     */
    public void generateHashFromServer(PaymentParams mPaymentParams) {
        //nextButton.setEnabled(false); // lets not allow the user to click the button again and again.

        // lets create the post params
        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayuConstants.KEY, mPaymentParams.getKey()));
        postParamsBuffer.append(concatParams(PayuConstants.AMOUNT, mPaymentParams.getAmount()));
        postParamsBuffer.append(concatParams(PayuConstants.TXNID, mPaymentParams.getTxnId()));
        postParamsBuffer.append(concatParams(PayuConstants.EMAIL, null == mPaymentParams.getEmail() ? "" : mPaymentParams.getEmail()));
        postParamsBuffer.append(concatParams(PayuConstants.PRODUCT_INFO, mPaymentParams.getProductInfo()));
        postParamsBuffer.append(concatParams(PayuConstants.FIRST_NAME, null == mPaymentParams.getFirstName() ? "" : mPaymentParams.getFirstName()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF1, mPaymentParams.getUdf1() == null ? "" : mPaymentParams.getUdf1()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF2, mPaymentParams.getUdf2() == null ? "" : mPaymentParams.getUdf2()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF3, mPaymentParams.getUdf3() == null ? "" : mPaymentParams.getUdf3()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF4, mPaymentParams.getUdf4() == null ? "" : mPaymentParams.getUdf4()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF5, mPaymentParams.getUdf5() == null ? "" : mPaymentParams.getUdf5()));
        postParamsBuffer.append(concatParams(PayuConstants.USER_CREDENTIALS, mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials()));

        // for offer_key
        if (null != mPaymentParams.getOfferKey())
            postParamsBuffer.append(concatParams(PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey()));

        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();

        // lets make an api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
        getHashesFromServerTask.execute(postParams);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        hideLoading();
        BookPaymentModal bookPayment;
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.BOOKPAYMENTHASH:

                        bookPayment = (BookPaymentModal) response.body();


                        if (bookPayment.getStatus().equals("1")) {
                            payuHashes.setPaymentHash(bookPayment.getHashes().getPayment_hash());
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(bookPayment.getHashes().getPayment_related_details_for_mobile_sdk_hash());
                            payuHashes.setVasForMobileSdkHash(bookPayment.getHashes().getVas_for_mobile_sdk_hash());
                            navigateToBaseActivity(payuHashes, bookPayment.getTxnid());
                        } else {
                            showToast(bookPayment.getError(), Toast.LENGTH_SHORT);
                        }

                        break;

                    case APIServerResponse.DONATION_PAYMENT_HASH:
                        bookPayment = (BookPaymentModal) response.body();

                        if (bookPayment.getStatus().equals("1")) {
                            payuHashes.setPaymentHash(bookPayment.getHashes().getPayment_hash());
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(bookPayment.getHashes().getPayment_related_details_for_mobile_sdk_hash());
                            payuHashes.setVasForMobileSdkHash(bookPayment.getHashes().getVas_for_mobile_sdk_hash());
                            bookName = "Donation";
                            navigateToBaseActivity(payuHashes, bookPayment.getTxnid());
                        } else {
                            showToast(bookPayment.getError(), Toast.LENGTH_SHORT);
                        }


                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    /**
     * This AsyncTask generates hash from server.
     */
    private class GetHashesFromServerTask extends AsyncTask<String, String, PayuHashes> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PaymentGatewayActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected PayuHashes doInBackground(String... postParams) {
            PayuHashes payuHashes = new PayuHashes();
            try {

                //TODO Below url is just for testing purpose, merchant needs to replace this with their server side hash generation url
                URL url = new URL("https://payu.herokuapp.com/get_hash");

                // get the payuConfig first
                String postParam = postParams[0];

                byte[] postParamsByte = postParam.getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }

                JSONObject response = new JSONObject(responseStringBuffer.toString());

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch (key) {
                        //TODO Below three hashes are mandatory for payment flow and needs to be generated at merchant server
                        /**
                         * Payment hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating payment_hash -
                         *
                         * sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||SALT)
                         *
                         */
                        case "payment_hash":
                            payuHashes.setPaymentHash(response.getString(key));
                            break;
                        /**
                         * vas_for_mobile_sdk_hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating vas_for_mobile_sdk_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be "default"
                         *
                         */
                        case "vas_for_mobile_sdk_hash":
                            payuHashes.setVasForMobileSdkHash(response.getString(key));
                            break;
                        /**
                         * payment_related_details_for_mobile_sdk_hash is one of the mandatory hashes that needs to be generated from merchant's server side
                         * Below is formula for generating payment_related_details_for_mobile_sdk_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "payment_related_details_for_mobile_sdk_hash":
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(response.getString(key));
                            break;

                        //TODO Below hashes only needs to be generated if you are using Store card feature
                        /**
                         * delete_user_card_hash is used while deleting a stored card.
                         * Below is formula for generating delete_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "delete_user_card_hash":
                            payuHashes.setDeleteCardHash(response.getString(key));
                            break;
                        /**
                         * get_user_cards_hash is used while fetching all the cards corresponding to a user.
                         * Below is formula for generating get_user_cards_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "get_user_cards_hash":
                            payuHashes.setStoredCardsHash(response.getString(key));
                            break;
                        /**
                         * edit_user_card_hash is used while editing details of existing stored card.
                         * Below is formula for generating edit_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "edit_user_card_hash":
                            payuHashes.setEditCardHash(response.getString(key));
                            break;
                        /**
                         * save_user_card_hash is used while saving card to the vault
                         * Below is formula for generating save_user_card_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be user credentials. If you are not using user_credentials then use "default"
                         *
                         */
                        case "save_user_card_hash":
                            payuHashes.setSaveCardHash(response.getString(key));
                            break;

                        //TODO This hash needs to be generated if you are using any offer key
                        /**
                         * check_offer_status_hash is used while using check_offer_status api
                         * Below is formula for generating check_offer_status_hash -
                         *
                         * sha512(key|command|var1|salt)
                         *
                         * here, var1 will be Offer Key.
                         *
                         */
                        case "check_offer_status_hash":
                            payuHashes.setCheckOfferStatusHash(response.getString(key));
                            break;
                        default:
                            break;
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return payuHashes;
        }

        @Override
        protected void onPostExecute(PayuHashes payuHashes) {
            super.onPostExecute(payuHashes);

            progressDialog.dismiss();
            launchSdkUI(payuHashes);
        }
    }

    /**
     * This method adds the Payuhashes and other required params to intent and launches the PayuBaseActivity.java
     *
     * @param payuHashes it contains all the hashes generated from merchant server
     */
    public void launchSdkUI(PayuHashes payuHashes) {

        Intent intent = new Intent(this, PayUBaseActivity.class);
        intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
        intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
        intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
        startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        //fetchMerchantHashes(intent);

    }

    private void fetchMerchantHashes(final Intent intent) {
        // now make the api call.
        final String postParams = "merchant_key=" + merchantKey + "&user_credentials=" + userCredentials;
        final Intent baseActivityIntent = intent;
        new AsyncTask<Void, Void, HashMap<String, String>>() {

            @Override
            protected HashMap<String, String> doInBackground(Void... params) {
                try {
                    //TODO Replace below url with your server side file url.
                    URL url = new URL("http://dev.miracleglobal.com/cal-php/web/payment/callback");

                    byte[] postParamsByte = postParams.getBytes("UTF-8");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                    conn.setDoOutput(true);
                    conn.getOutputStream().write(postParamsByte);
                    Log.e("conn.getInputStream()", "Response" + conn.getInputStream());
                    InputStream responseInputStream = conn.getInputStream();
                    StringBuffer responseStringBuffer = new StringBuffer();
                    byte[] byteContainer = new byte[1024];
                    for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                        responseStringBuffer.append(new String(byteContainer, 0, i));
                    }
                    Log.e("Response", "Response" + responseStringBuffer.toString());
                    // JSONObject response = new JSONObject(responseStringBuffer.toString());

                   /* HashMap<String, String> cardTokens = new HashMap<String, String>();
                    JSONArray oneClickCardsArray = response.getJSONArray("data");
                    int arrayLength;
                    if ((arrayLength = oneClickCardsArray.length()) >= 1) {
                        for (int i = 0; i < arrayLength; i++) {
                            cardTokens.put(oneClickCardsArray.getJSONArray(i).getString(0), oneClickCardsArray.getJSONArray(i).getString(1));
                        }
                        return cardTokens;
                   */
                    // pass these to next activity

                } catch (Exception e) {
                    e.printStackTrace();
                } /*catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                return null;
            }

            @Override
            protected void onPostExecute(HashMap<String, String> oneClickTokens) {
                super.onPostExecute(oneClickTokens);
                baseActivityIntent.putExtra(PayuConstants.ONE_CLICK_CARD_TOKENS, oneClickTokens);

                startActivityForResult(baseActivityIntent, PayuConstants.PAYU_REQUEST_CODE);
            }
        }.execute();
    }
}
