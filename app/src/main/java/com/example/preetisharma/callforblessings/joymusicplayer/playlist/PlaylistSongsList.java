package com.example.preetisharma.callforblessings.joymusicplayer.playlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.DemoDataPlayListAdapter;
import com.example.preetisharma.callforblessings.Adapter.DemoDataPlayListSongsAdaper;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ItemClickSupport;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satoti.garg on 7/26/2017.
 */

public class PlaylistSongsList extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.main_collapsing_bar_tab_layout)
    CollapsingToolbarLayout main_collapsing_bar_tab_layout;
    @BindView(R.id.iv_playlist_list_banner)
    ImageView iv_playlist_list_banner;
    @BindView(R.id.atv_playlist_name)
    AppCompatTextView atv_playlist_name;
    @BindView(R.id.button_PlayAll)
    Button button_PlayAll;
    @BindView(R.id.main_toolbar)
    Toolbar main_toolbar;
    @BindView(R.id.nestedscroll)
    NestedScrollView nestedscroll;
    @BindView(R.id.rv_playlist)
    RecyclerView rv_playlist;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.fab_book_options)
    FloatingActionButton fab_book_options;

    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;

    List<DemoDataModal> list = new ArrayList<>();

    ArrayList<PlayMusicModal> arrayListMusic = new ArrayList<>();
    DemoDataPlayListSongsAdaper adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_songs_list);
        ButterKnife.bind(this);

        setSupportActionBar(main_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        main_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaylistSongsList.this.finish();
            }
        });


        swipeRefreshLayout.setOnRefreshListener(this);

        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras.getString(Constants.PLAYLIST_ID, "1") != null && !extras.getString(Constants.PLAYLIST_ID, "1").equalsIgnoreCase("")) {
                main_collapsing_bar_tab_layout.setTitle("My Playlists " + extras.getString(Constants.PLAYLIST_ID, "1"));
            }
        }


        rv_playlist.setLayoutManager(new LinearLayoutManager(this));
        rv_playlist.setItemAnimator(new DefaultItemAnimator());
        rv_playlist.addItemDecoration(new DividerItemDecoration(this, GridLayoutManager.VERTICAL));
        rv_playlist.setNestedScrollingEnabled(false);

        actionModeCallback = new ActionModeCallback();

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        addItemsToList();
                    }
                }
        );

        ItemClickSupport.addTo(rv_playlist).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                if (adapter.getSelectedItemCount() > 0) {
                    enableActionMode(position);
                } else {
                    playAudio(position);
                }

            }


        }).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                enableActionMode(position);
                return true;
            }
        });


        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {


                ///Toast.makeText(MainActivity.this, "getAdapterPosition" + viewHolder.getAdapterPosition(), Toast.LENGTH_SHORT).show();
                adapter.removeData(viewHolder.getAdapterPosition());
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                Snackbar.make(rv_playlist, "Song removed from playlist.", Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (actionMode != null & adapter.getSelectedItemCount() > 0) return 0;
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rv_playlist);
    }


    @OnClick(R.id.fab_book_options)
    public void playAllSongs() {
        playAudio(0);
    }

    public void addItemsToList() {
        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Song " + (i + 1));
            demoDataModal.setId(i);
            demoDataModal.setSongUrl("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3");
            demoDataModal.setAlbumArt("http://www.flat-e.com/flate5/wp-content/uploads/cover-960x857.jpg");
            demoDataModal.setImage("http://www.happybirthdayvinyl.co.uk/images/album-artwork/big/grace-jones-nightclubbing-deluxe-edition-disc1.jpg");
            demoDataModal.setPaymentType("FREE");
            demoDataModal.setIsDownloaded(false);
            list.add(demoDataModal);
        }
        swipeRefreshLayout.setRefreshing(false);

        adapter = new DemoDataPlayListSongsAdaper(PlaylistSongsList.this, list);
        rv_playlist.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        addItemsToList();
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_delete, menu);

            // disable swipe refresh if action mode is enabled
            swipeRefreshLayout.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete_item:
                    // delete all the selected messages
                    deleteMessages();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.clearSelections();
            swipeRefreshLayout.setEnabled(true);
            actionMode = null;
            /*recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });*/
        }
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void deleteMessages() {
        List<Integer> selectedItemPositions =
                adapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            adapter.removeData(selectedItemPositions.get(i));
        }
        adapter.notifyDataSetChanged();
    }

    private void playAudio(int position) {

        if (!arrayListMusic.isEmpty()) {
            arrayListMusic.clear();
        }
        for (int i = 0; i < list.size(); i++) {
            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(list.get(i).getSongUrl());
            playMusicModal.setThumbnailUrl(list.get(i).getAlbumArt());
            playMusicModal.setArtworkUrl(list.get(i).getAlbumArt());
            playMusicModal.setTitle(list.get(i).getName());
            playMusicModal.setAlbum(list.get(i).getName());
            playMusicModal.setArtist(list.get(i).getName());
            playMusicModal.setPaymentType(list.get(i).getPaymentType());
            playMusicModal.setDownloaded(String.valueOf(list.get(i).getIsDownloaded()));
            arrayListMusic.add(playMusicModal);
        }

        //mPlayListList = mList;

        Intent intent = new Intent(PlaylistSongsList.this, AudioPlayerActivity.class);
        intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, position);
        intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, list.get(position).getId());
        intent.putExtra(Constants.SONG_NAME, list.get(position).getName());
        intent.putExtra(Constants.ALBUM_COVER, list.get(position).getAlbumArt());
        intent.putExtra(Constants.MUSIC, Constants.SONG);

        intent.putExtra(Constants.ALBUM_NAME, list.get(position).getName());
        intent.putExtra(Constants.TYPE, Constants.SONG_CAPS);
        intent.putExtra(Constants.SONG_ID, list.get(position).getId());
        intent.putExtra(Constants.SONG_DOWNLOAD_URL, list.get(position).getSongUrl());
        intent.putExtra(Constants.POSITION, position);
        intent.putExtra(Constants.IS_DOWNLOADED, list.get(position).getIsDownloaded());

        Bundle b = new Bundle();
        b.putParcelableArrayList(Constants.SONGS_LIST, arrayListMusic);
        intent.putExtra("songs_bundle", b);
        //intent.putParcelableArrayListExtra(Constants.SONGS_LIST, songsBeanArrayList);
        startActivity(intent);


    }
}
