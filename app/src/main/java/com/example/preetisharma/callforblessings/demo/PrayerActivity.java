/*
package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.PrayerAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AmenPrayerModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerIndexModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

*/
/**
 * Created by preeti.sharma on 1/19/2017.
 *//*


public class PrayerActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.prayers_list)
    RecyclerView prayers_list;
    PrayerAdapter _adapter;

    //Prayer of the Day
    @BindView(R.id.txtvw_prayer_of_the_title)
    AppCompatTextView txtvw_prayer_of_the_title;
    @BindView(R.id.txtvw_prayer_of_the_day_description)
    AppCompatTextView txtvw_prayer_of_the_day_description;
    @BindView(R.id.txtvw_prayer_of_the_day_likes_count)
    AppCompatTextView txtvw_prayer_of_the_day_likes_count;
    @BindView(R.id.txtvw_prayer_of_the_day_amen_count)
    AppCompatTextView txtvw_prayer_of_the_day_amen_count;
    @BindView(R.id.txtvw_prayer_of_the_day_comments_count)
    AppCompatTextView txtvw_prayer_of_the_day_comments_count;
    @BindView(R.id.txtvw_prayer_of_the_day_like)
    AppCompatTextView txtvw_prayer_of_the_day_like;
    @BindView(R.id.txtvw_prayer_of_the_day_comment)
    AppCompatTextView txtvw_prayer_of_the_day_comment;
    @BindView(R.id.txtvw_prayer_of_the_day_amen)
    AppCompatTextView txtvw_prayer_of_the_day_amen;
    List<PrayerIndexModal.ListBean> listAllPrayers;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    private String prayerId;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    PrayerIndexModal prayerIndexModal;
    private String likescount, amencount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prayer_tab);
        ButterKnife.bind(this);

        prayers_list = (RecyclerView) findViewById(R.id.prayers_list);
        try {
            setSupportActionBar(toolbar);
            txtvw_header_title.setText("Prayer");
            img_view_change_password.setVisibility(View.GONE);
            showLoading();
            if (isConnectedToInternet()) {
                ServerAPI.getInstance().getPrayers(APIServerResponse.PRAYER_INDEX, getUserSessionId(), this);
            } else {
                showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_back)
    public void onBackPressed() {
        super.onBackPressed();

    }

    @OnClick(R.id.txtvw_prayer_of_the_day_comment)
    public void comment_prayer_of_day() {
        Intent i = new Intent(this, CommentsListingActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.COMMENTS, String.valueOf(prayerIndexModal.getPrayer_of_day().getId()));
        b.putString(Constants.POSTTYPE, Constants.PRAYER);
        i.putExtras(b);
        startActivity(i);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {

            listAllPrayers = new ArrayList<>();
            LikeModal likeModal;
            AmenPrayerModal amenPrayerModal;
            hideLoading();
            if (response.isSuccessful()) {


                switch (tag) {


                    case APIServerResponse.LIKE: {
                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {
                            showToast("Prayer Liked", Toast.LENGTH_SHORT);
                        }
                        int imgResource = R.drawable.ic_liked;
                        txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        int likescountValue = Integer.parseInt(likescount);
                        likescountValue++;
                        if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_prayer_of_the_day_likes_count.setText(String.valueOf(likescountValue) + "Like");
                        } else {
                            txtvw_prayer_of_the_day_likes_count.setText(String.valueOf(likescountValue) + "Likes");
                        }
                    }
                    break;
                    case APIServerResponse.AMEN_PRAYER:
                        amenPrayerModal = (AmenPrayerModal) response.body();
                        if (amenPrayerModal.getStatus().equals("1")) {
                            showToast("Amen", Toast.LENGTH_SHORT);
                        }
                        */
/*int imgResource = R.drawable.ic_liked;
                        txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);*//*

                        int likescountValue = Integer.parseInt(amencount);
                        likescountValue++;
                        if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_prayer_of_the_day_amen_count.setText(String.valueOf(likescountValue) + "Amen");
                        } else {
                            txtvw_prayer_of_the_day_amen_count.setText(String.valueOf(likescountValue) + "Amens");
                        }
                        break;
                    case APIServerResponse.PRAYER_INDEX:
                       try {
                           prayerIndexModal = (PrayerIndexModal) response.body();
                           if (prayerIndexModal.getStatus().equals("1")) {
                               if (prayerIndexModal.getPrayer_of_day().getLike_flag().equalsIgnoreCase("Not Liked")) {
                                   int imgResource = R.drawable.prayer_like_icon;
                                   txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                               } else {
                                   int imgResource = R.drawable.ic_liked;
                                   txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                               }
                               txtvw_prayer_of_the_title.setText(prayerIndexModal.getPrayer_of_day().getTitle());
                               txtvw_prayer_of_the_day_description.setText(prayerIndexModal.getPrayer_of_day().getDesc());
                               prayerId = String.valueOf(prayerIndexModal.getPrayer_of_day().getId());
                               listAllPrayers = prayerIndexModal.getList();
                               _adapter = new PrayerAdapter(PrayerActivity.this, listAllPrayers);
                               likescount = prayerIndexModal.getPrayer_of_day().getLike_count();
                               amencount = prayerIndexModal.getPrayer_of_day().getAmen_count();
                           } else {
                               showToast("Error", Toast.LENGTH_LONG);
                           }

                           if (Integer.parseInt(prayerIndexModal.getPrayer_of_day().getLike_count()) == 0 || Integer.parseInt(prayerIndexModal.getPrayer_of_day().getLike_count()) == 1) {
                               txtvw_prayer_of_the_day_likes_count.setText(prayerIndexModal.getPrayer_of_day().getLike_count() + "Like");
                           } else {
                               txtvw_prayer_of_the_day_likes_count.setText(prayerIndexModal.getPrayer_of_day().getLike_count() + "Likes");
                           }
                           if (Integer.parseInt(prayerIndexModal.getPrayer_of_day().getComment_count()) == 0 || Integer.parseInt(prayerIndexModal.getPrayer_of_day().getComment_count()) == 1) {
                               txtvw_prayer_of_the_day_comments_count.setText(prayerIndexModal.getPrayer_of_day().getComment_count() + "Comment");
                           } else {
                               txtvw_prayer_of_the_day_comments_count.setText(prayerIndexModal.getPrayer_of_day().getComment_count() + "Comments");
                           }
                           if (Integer.parseInt(prayerIndexModal.getPrayer_of_day().getAmen_count()) == 0 || Integer.parseInt(prayerIndexModal.getPrayer_of_day().getAmen_count()) == 1) {
                               txtvw_prayer_of_the_day_amen_count.setText(prayerIndexModal.getPrayer_of_day().getAmen_count() + "Amen");
                           } else {
                               txtvw_prayer_of_the_day_amen_count.setText(prayerIndexModal.getPrayer_of_day().getAmen_count() + "Amens");
                           }
                       }catch (Exception e)
                       {
                           e.printStackTrace();
                       }
                        prayers_list.setAdapter(_adapter);
                        prayers_list.setItemAnimator(new DefaultItemAnimator());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PrayerActivity.this);
                        prayers_list.setLayoutManager(mLayoutManager);

                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.PRAYER_INDEX:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @OnClick(R.id.txtvw_prayer_of_the_day_like)
    public void likePrayerOfTheDay() {
        if (prayerIndexModal.getPrayer_of_day().getLike_flag().equalsIgnoreCase("Not liked")) {
            int imgResource = R.drawable.prayer_like;
            txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().like(APIServerResponse.LIKE, getUserSessionId(), String.valueOf(prayerIndexModal.getPrayer_of_day().getId()), "PRAYER", this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }

        } else {
            int imgResource = R.drawable.ic_liked;
            txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().like(APIServerResponse.LIKE, getUserSessionId(), String.valueOf(prayerIndexModal.getPrayer_of_day().getId()), "PRAYER", this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }
    }


    @OnClick(R.id.txtvw_prayer_of_the_day_amen)
    public void amenPrayerOfTheDayPost() {
        showLoading();
        ServerAPI.getInstance().amenPrayerPost(APIServerResponse.AMEN_PRAYER, getUserSessionId(), prayerId, this);
    }
}

*/
