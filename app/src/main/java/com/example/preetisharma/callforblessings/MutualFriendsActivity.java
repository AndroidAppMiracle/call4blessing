package com.example.preetisharma.callforblessings;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.MutualFriendsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/3/2017.
 */

public class MutualFriendsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, APIServerResponse {


    @BindView(R.id.rv_mutual_friends)
    RecyclerView rv_mutual_friends;
    private List<MyFriendsModal.ListBean> mList;
    MutualFriendsAdapter _adapter;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mutual_friends);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Mutual Friends");

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MutualFriendsActivity.this.finish();
            }
        });

        swipe_refresh_layout.setOnRefreshListener(this);


        swipe_refresh_layout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        if (isConnectedToInternet()) {
                            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, getUserSessionId(), MutualFriendsActivity.this);
                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    }
                }
        );


    }

    @Override
    public void onSuccess(int tag, Response response) {
        mList = new ArrayList<MyFriendsModal.ListBean>();
        hideLoading();

        MyFriendsModal userListingModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.MYFRIENDLIST:


                        userListingModal = (MyFriendsModal) response.body();


                        if (userListingModal.getStatus().equals("1")) {
                            Log.e("List data", "List data" + userListingModal.getList().size());
                            mList = userListingModal.getList();

                        } else {
                        }

                        _adapter = new MutualFriendsAdapter(MutualFriendsActivity.this, mList);
                        rv_mutual_friends.setAdapter(_adapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MutualFriendsActivity.this);
                        rv_mutual_friends.setLayoutManager(mLayoutManager);


                        break;


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();

    }

    @Override
    public void onRefresh() {
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, getUserSessionId(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, getUserSessionId(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }
}
