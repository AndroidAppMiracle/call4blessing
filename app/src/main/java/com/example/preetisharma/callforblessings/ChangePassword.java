package com.example.preetisharma.callforblessings;

import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.ChangePasswordModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by satoti.garg on 1/17/2017.
 */

public class ChangePassword extends BaseActivity implements APIServerResponse {

    @BindView(R.id.edt_txt_password)
    AppCompatEditText edt_txt_password;

    @BindView(R.id.edt_txt_confirm_password)
    AppCompatEditText edt_txt_confirm_password;

    @BindView(R.id.txtvw_save_password)
    AppCompatTextView txtvw_save_password;

    @BindView(R.id.edt_txt_old_password)
    AppCompatEditText edt_txt_old_password;
    private int RC_INTERNET = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.txtvw_save_password)
    public void changePassword() {
        try {
            if (Validation(edt_txt_old_password.getText().toString(), edt_txt_password.getText().toString(), edt_txt_confirm_password.getText().toString())) {

                if (EasyPermissions.hasPermissions(this, android.Manifest.permission.INTERNET)) {
                    if (isConnectedToInternet()) {
                        showLoading();
                        ServerAPI.getInstance().changePassword(APIServerResponse.CHANGE_PASSWORD, getUserSessionId(), edt_txt_old_password.getText().toString(), edt_txt_confirm_password.getText().toString(), this);
                    } else {
                        showSnack("Not connected to Internet ");
                    }
                } else {
                    EasyPermissions.requestPermissions(this, getString(R.string.internet_access),
                            RC_INTERNET, android.Manifest.permission.INTERNET);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public boolean Validation(String oldPassword, String password, String confirmPassword) {
        if (oldPassword.isEmpty()) {
            showToast(Constants.EMAIL_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (oldPassword.length() < 6) {
            showToast(Constants.EMAIL_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (password.isEmpty()) {
            showToast(Constants.EMAIL_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (password.length() < 6) {
            showToast(Constants.EMAIL_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (confirmPassword.isEmpty()) {
            showToast(Constants.PASSWORD_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (confirmPassword.length() < 6) {
            showToast(Constants.PASSWORD_LENGTH, Toast.LENGTH_SHORT);
            return false;
        } else if (!password.matches(password)) {
            return false;
        }
        return true;


    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            hideLoading();
            if (response.isSuccessful()) {
                ChangePasswordModal changePasswordModal = (ChangePasswordModal) response.body();
                if (changePasswordModal.getStatus().equals("1")) {
                    showToast(changePasswordModal.getMessage(), Toast.LENGTH_SHORT);
                    finish();
                } else {
                    showToast(changePasswordModal.getMessage(), Toast.LENGTH_SHORT);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        switch (tag) {
            case APIServerResponse.CHANGE_PASSWORD:
                System.out.println("Error");
                break;
        }
    }
}
