package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 5/29/2017.
 */

public class BooksBuyFreeBookModal {

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
