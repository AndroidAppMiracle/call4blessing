package com.example.preetisharma.callforblessings.Adapter;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.demo.CreatePostActivity;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by satoti.garg on 8/3/2017.
 */

public class DemoDataVedioAlbumsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public static final int ADD_ALBUM = 0;
    public final int ALBUM = 1;

    private static final int SELECT_VIDEO = 3;

    private Context mContext;
    private List<DemoDataModal> myGroupsList = new ArrayList<>();
    private SparseBooleanArray selectedItems;
    private static final int RC_CAMERA_PERM = 342;
    private static final int RC_GALLERY_PERM = 545;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private final static int CAMERA_RQ = 6969;
    private static final int RESULT_CODE_COMPRESS_VIDEO = 3;
    private static final int DEMO_RECORD_VIDEO_COMPRESSED = 4;

    String username, userImage, postText, postImage, postID;
    private static final String TAG = "CreatePostActivity";
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;

    private int x, y;

    public DemoDataVedioAlbumsAdapter(Context mContext, List<DemoDataModal> list) {
        this.myGroupsList = list;
        this.mContext = mContext;
        selectedItems = new SparseBooleanArray();
        x = (int) mContext.getResources().getDimension(R.dimen.height_video_item);
        y = (int) mContext.getResources().getDimension(R.dimen.height_video_item);
    }


    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            //Return "Create New Album" Item
            return ADD_ALBUM;
        } else {
            //Return Albums Item
            return ALBUM;
        }

        //return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;

        switch (viewType) {
            case ADD_ALBUM:
                v = inflater.inflate(R.layout.adapter_album_add_newvedio_layout, parent, false);
                viewHolder = new DemoDataVedioAlbumsAdapter.DataViewHolderAddAlbum(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;

            case ALBUM:
                v = inflater.inflate(R.layout.adapter_albums_layout, parent, false);
                viewHolder = new DemoDataVedioAlbumsAdapter.DataViewHolderAlbum(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        try {

            switch (getItemViewType(position)) {
                case ADD_ALBUM:

                    //Do Nothing

                    break;

                case ALBUM:


                    final DemoDataVedioAlbumsAdapter.DataViewHolderAlbum dataViewHolderAlbum = (DemoDataVedioAlbumsAdapter.DataViewHolderAlbum) holder;

                    dataViewHolderAlbum.atv_album_name.setText(myGroupsList.get(position).getName());


                    Glide.with(mContext)
                            .load(myGroupsList.get(position).getImage())
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    dataViewHolderAlbum.mProgress.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    // image ready, hide progress now
                                    dataViewHolderAlbum.mProgress.setVisibility(View.GONE);
                                    return false;   // return false if you want Glide to handle everything else.
                                }
                            })
                    /*.diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image*/
                            .centerCrop()
                            .override(x, y)
                            .thumbnail(0.1f)
                            .crossFade()
                            .into(dataViewHolderAlbum.atv_AlbumCover);

                    holder.itemView.setActivated(selectedItems.get(position, false));
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (myGroupsList.size() != 0) {
            return myGroupsList.size();
        } else {
            return 0;
        }

    }

    public DemoDataModal getItem(int position) {
        return myGroupsList.get(position);
    }

    public void toggleSelection(int pos) {
        /*currentSelectedIndex = pos;*/
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            /*animationItemsIndex.delete(pos);*/
        } else {
            selectedItems.put(pos, true);
            /*animationItemsIndex.put(pos, true);*/
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        /*reverseAllAnimations = true;*/
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        myGroupsList.remove(position);
        //resetCurrentIndex();
    }


    public class DataViewHolderAlbum extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_album_name)
        AppCompatTextView atv_album_name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

       /* @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.album_progress)
        ProgressBar mProgress;

        @BindView(R.id.cv_album_item)
        CardView cv_album_item;


        public DataViewHolderAlbum(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/


            /*ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, PlaylistSongsList.class);
                    *//*Bundle b = new Bundle();
                    b.putString(Constants.GROUP_ID, String.valueOf(myGroupsList.get(getAdapterPosition()).getId()));
                    i.putExtras(b);*//*
                    mContext.startActivity(i);

                   *//* if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myGroupsList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*//*
                }
            });*/

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

    }
    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }
    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickVideoSingle() {
        if (EasyPermissions.hasPermissions(mContext, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("video/*");
            ((BaseActivity)mContext).startActivityForResult(intent, RESULT_CODE_COMPRESS_VIDEO);
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }
    public void createVideo() {
        if (EasyPermissions.hasPermissions(mContext, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            ((BaseActivity)mContext).startActivityForResult(takeVideoIntent, DEMO_RECORD_VIDEO_COMPRESSED);
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public class DataViewHolderAddAlbum extends RecyclerView.ViewHolder {

        /*@BindView(R.id.atv_Name)
        AppCompatTextView atv_Name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.movie_progress)
        ProgressBar mProgress;*/

        @BindView(R.id.cv_album_item)
        CardView cv_album_item;


        public DataViewHolderAddAlbum(View v) {
            super(v);
            ButterKnife.bind(this, v);


            cv_album_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  /*  Intent intent = new Intent();
                    intent.setType("video*//**//*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    ((BaseActivity) mContext).startActivityForResult(Intent.createChooser(intent,"Select a Video "), SELECT_VIDEO);*/

                /*    hasPermissionInManifest(mContext, Manifest.permission.CAMERA);
                    new AlertDialog.Builder(mContext)

                            .setTitle("Select Video")
                            .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    createVideo();
                                }
                            })
                            .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    pickVideoSingle();
                                }
                            })
                            .setIcon(R.mipmap.ic_app_icon)
                            .show();*/
                }
            });

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/


            /*ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, PlaylistSongsList.class);
                    *//*Bundle b = new Bundle();
                    b.putString(Constants.GROUP_ID, String.valueOf(myGroupsList.get(getAdapterPosition()).getId()));
                    i.putExtras(b);*//*
                    mContext.startActivity(i);

                   *//* if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myGroupsList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*//*
                }
            });*/

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

    }


}