package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by preeti.sharma on 1/19/2017.
 */

public class LogoutModal {
    /**
     * status : 1
     * message : You are logged out successfully
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
