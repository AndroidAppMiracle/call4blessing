package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Switch;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 9/20/2017.
 */

public class SettingNotificationAdapter extends RecyclerView.Adapter<SettingNotificationAdapter.DataViewHolder> {

    public List<String> list;
    Activity mContext;

    public SettingNotificationAdapter(Activity mContext, List<String> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_settings_notifications, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.atv_general_setting_name.setText(list.get(position) + "");

    }

    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_general_setting_name)
        AppCompatTextView atv_general_setting_name;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        @BindView(R.id.switch1)
        Switch switchOnOff;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

         /*   cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        if (!selectedSettingName.equalsIgnoreCase("")) {
                            ServerAPI.getInstance().setSettings(APIServerResponse.SETTINGS_SET, ((BaseActivity) mContext).getUserSessionId(), settingId, selectedSettingName, DataViewHolder.this);
                        } else {

                            ((BaseActivity) mContext).showToast("Please select an option", Toast.LENGTH_SHORT);
                        }
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }


                }
            });*/

        }



        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }
}
