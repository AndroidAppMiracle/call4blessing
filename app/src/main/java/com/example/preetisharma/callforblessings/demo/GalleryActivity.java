package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GalleryTabAdapter;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 2/8/2017.
 */

public class GalleryActivity extends BaseActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    private ViewPager viewPager;

    AppCompatTextView txtvw_header_title;
    // @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    //@BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_activity);
        updateStatusBar();
        ButterKnife.bind(this);


        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);

        txtvw_header_title.setText("Gallery");
        img_view_change_password.setVisibility(View.GONE);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GalleryActivity.this.finish();
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tabs_gallery);

        //Adding the tabs using addTab() method

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.viewPager_gallerytab);
        //Creating our pager adapter
        createTabWithIcons();

        GalleryTabAdapter adapter = new GalleryTabAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);
        viewPager.setOnPageChangeListener(this);
        tabLayout.setupWithViewPager(viewPager);

    }

    public void createTabWithIcons() {
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        TextView tabHome = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHome.setText("Images");
        tabLayout.addTab((tabLayout.newTab().setCustomView(tabHome)));
        TextView tabFriends = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFriends.setText("Videos");
        tabLayout.addTab((tabLayout.newTab().setCustomView(tabFriends)));


    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
