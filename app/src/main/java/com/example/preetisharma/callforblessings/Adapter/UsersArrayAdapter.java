package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.UserListingModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by preeti.sharma on 1/25/2017.
 */

public class UsersArrayAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<UserListingModal.ListBean> originalList;
    public static List<UserListingModal.ListBean> suggestions = new ArrayList<>();
    private Filter filter = new CustomFilter();

    /**
     * @param context      Context
     * @param originalList Original list used to compare in constraints.
     */
    public UsersArrayAdapter(Context context, List<UserListingModal.ListBean> originalList) {
        this.context = context;
        this.originalList = originalList;
    }

    @Override
    public int getCount() {

        return suggestions.size(); // Return the size of the suggestions list.
    }


    @Override
    public Object getItem(int position) {

        return suggestions.get(position).getProfile_details().getFirstname();
    }

    public String getUserImage(int pos) {
        return suggestions.get(pos).getProfile_details().getProfile_pic();
    }

    public String getUserFullName(int pos) {
        return suggestions.get(pos).getProfile_details().getFirstname() + " " + suggestions.get(pos).getProfile_details().getLastname();
    }

    public String getUserCoverImage(int pos) {
        return suggestions.get(pos).getProfile_details().getCover_pic();
    }


    public String getUserPrivacyWall(int pos) {
        return suggestions.get(pos).getWALL();
    }

    public String getUserPrivacyAboutInfo(int pos) {
        return suggestions.get(pos).getabout_info();
    }


    public String getUserPrivacyFriendRequest(int pos) {
        return suggestions.get(pos).getfriend_request();
    }


    public String getUserPrivacyMessage(int pos) {
        return suggestions.get(pos).getMESSAGE();
    }

    public int getisFriend(int pos) {
        int userFlag = 0;
        if (suggestions.get(pos).getIs_friend() == 0 && suggestions.get(pos).getRequest_respond().equalsIgnoreCase("No")) {
            userFlag = Constants.USER_NOT_FRIEND;//when user is not my friend

        } else if (suggestions.get(pos).getIs_friend() == 0 && suggestions.get(pos).getRequest_respond().equalsIgnoreCase("Yes")) {
            userFlag = Constants.USER_REQUEST_COME;
        } else if (suggestions.get(pos).getIs_friend() == 2 && suggestions.get(pos).getRequest_respond().equalsIgnoreCase("No")) {
            userFlag = Constants.USER_REQUEST_SEND;//when user is friend
            //return 2;
        } else if (suggestions.get(pos).getIs_friend() == 1 && suggestions.get(pos).getRequest_respond().equalsIgnoreCase("NO")) {
            userFlag = Constants.USER_REQUEST_ACCEPT;//when request pending
            //return 3;
        } else {
            userFlag = Constants.USER_SELFTIMELINE;
        }

        return userFlag;
    }


    @Override
    public long getItemId(int position) {

        return suggestions.get(position).getId();
    }

    /**
     * This is where you inflate the layout and also where you set what you want to display.
     * Here we also implement a View Holder in order to recycle the views.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.textview_item,
                    parent,
                    false);
            holder = new ViewHolder();
            holder.autoText = (TextView) convertView.findViewById(R.id.viewholder_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.autoText.setText(suggestions.get(position).getProfile_details().getFirstname());

        return convertView;
    }


    @Override
    public Filter getFilter() {
        return filter;
    }

    private static class ViewHolder {
        TextView autoText;
    }

    /**
     * Our Custom Filter Class.
     */
    private class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();
            if (originalList != null && constraint != null) { // Check if the Original List and Constraint aren't null.
                for (int i = 0; i < originalList.size(); i++) {
                    if (originalList.get(i).getProfile_details().getFirstname().toLowerCase().contains(constraint) && originalList.get(i).getProfile_details().getFirstname().toLowerCase().startsWith(constraint.toString())) { // Compare item in original list if it contains constraints.
                        suggestions.add(originalList.get(i)); // If TRUE add item in Suggestions.
                    } else {

                    }
                }
            }
            FilterResults results = new FilterResults(); // Create new Filter Results and return this to publishResults;
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                if (constraint != null && constraint.length() != 0) {
                    ((BaseActivity) context).showToast("No results found", Toast.LENGTH_SHORT);
                }

                notifyDataSetInvalidated();
            }
        }
    }


}
