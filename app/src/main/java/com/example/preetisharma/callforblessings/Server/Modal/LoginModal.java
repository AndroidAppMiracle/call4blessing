package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by preeti.sharma on 1/10/2017.
 */

public class LoginModal {
    /**
     * status : 0
     * message : Please check, username or password is invalid!
     */

    private String status;
    private String message;
    /**
     * session_key : iwlBbyT7s_CBJ1wyLNt7fh40czZ1XxmV
     * detail : {"id":57,"username":"user5","email":"user5@gmail.com","profile_details":{"firstname":"abc","lastname":"abc","d_o_b":"1992-12-12","phone":"123456789900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/profilepic.jpg"},"post":[]}
     */

    private String session_key;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSession_key() {
        return session_key;
    }

    public void setSession_key(String session_key) {
        this.session_key = session_key;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }


    public static class DetailBean {
        /**
         * id : 57
         * username : user5
         * email : user5@gmail.com
         * profile_details : {"firstname":"abc","lastname":"abc","d_o_b":"1992-12-12","phone":"123456789900","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/profilepic.jpg"}
         * post : []
         */

        private String id;
        private String username;
        private String email;
        private ProfileDetailsBean profile_details;
        private List<?> post;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public ProfileDetailsBean getProfile_details() {
            return profile_details;
        }

        public void setProfile_details(ProfileDetailsBean profile_details) {
            this.profile_details = profile_details;
        }

        public List<?> getPost() {
            return post;
        }

        public void setPost(List<?> post) {
            this.post = post;
        }

        public static class ProfileDetailsBean {
            /**
             * firstname : abc
             * lastname : abc
             * d_o_b : 1992-12-12
             * phone : 123456789900
             * gender : MALE
             * location :
             * country :
             * state :
             * cover_pic : http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg
             * profile_pic : http://dev.miracleglobal.com/cal-php/web/theme/images/profilepic.jpg
             */

            private String firstname;
            private String lastname;
            private String d_o_b;
            private String phone;
            private String gender;
            private String location;
            private String country;
            private String state;
            private String cover_pic;
            private String profile_pic;

            public String getFirstname() {
                return firstname;
            }

            public void setFirstname(String firstname) {
                this.firstname = firstname;
            }

            public String getLastname() {
                return lastname;
            }

            public void setLastname(String lastname) {
                this.lastname = lastname;
            }

            public String getD_o_b() {
                return d_o_b;
            }

            public void setD_o_b(String d_o_b) {
                this.d_o_b = d_o_b;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCover_pic() {
                return cover_pic;
            }

            public void setCover_pic(String cover_pic) {
                this.cover_pic = cover_pic;
            }

            public String getProfile_pic() {
                return profile_pic;
            }

            public void setProfile_pic(String profile_pic) {
                this.profile_pic = profile_pic;
            }
        }
    }
}
