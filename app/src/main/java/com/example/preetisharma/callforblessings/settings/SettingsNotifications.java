package com.example.preetisharma.callforblessings.settings;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.SettingNotificationAdapter;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 7/24/2017.
 */

public class SettingsNotifications extends BaseActivity {

    List<String> settingsList = new ArrayList<>();
//rv_security_settings


    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.rv_security_settings)
    RecyclerView rv_security_settings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_notifications);
        ButterKnife.bind(this);

        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Notifications");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        settingsList.add("Like Post ");
        settingsList.add("Comments Post");
        settingsList.add("Share Post");
        settingsList.add("Wall Post");
        settingsList.add("Post Group");
        settingsList.add("Like Group ");
        settingsList.add("Share Group");
        settingsList.add("Comments Group");
        settingsList.add("Friend Request");
        settingsList.add("Post Tagging");

        SettingNotificationAdapter adapter = new SettingNotificationAdapter(SettingsNotifications.this, settingsList);
        rv_security_settings.setAdapter(adapter);
        rv_security_settings.setItemAnimator(new DefaultItemAnimator());
        rv_security_settings.setLayoutManager(new LinearLayoutManager(SettingsNotifications.this));
    }
}
