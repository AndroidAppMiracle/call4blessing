package com.example.preetisharma.callforblessings.pages;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdatePagesDescription extends BaseActivity {

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_page_delete)
    AppCompatImageView txtvw_page_delete;

    //txtvw_page_delete

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_pages_description);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UpdatePagesDescription.this.finish();
    }

    @OnClick(R.id.txtvw_page_delete)
    public void deletePage()
    {
        Toast.makeText(getApplicationContext(),"Deletion api pending ",Toast.LENGTH_LONG).show();
    }


}
