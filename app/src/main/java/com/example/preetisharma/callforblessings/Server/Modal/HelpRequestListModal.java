package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/23/2017.
 */

public class HelpRequestListModal {


    /**
     * status : 1
     * list : [{"id":6,"name":"ABc","desc":"testing api request testing api request helpas","title":"testing api request Help Required Help","video_link":"www.youtube.com","image":"http://dev.miracleglobal.com/cal-php/web/uploads/help/1485115178Penguins.jpg","like_flag":"Not Liked","comment_count":"0","like_count":"0","comments":[],"reach_count":"0"},{"id":5,"name":"ABc","desc":"testing api request testing api request","title":"testing api request Help Required","video_link":"www.youtube.com","image":"http://dev.miracleglobal.com/cal-php/web/uploads/help/1485115170Penguins.jpg","like_flag":"Not Liked","comment_count":"1","like_count":"0","comments":[{"id":70,"post_id":5,"post_by":{"id":68,"username":"kamal","email":"kamal@yopmail.com","updated":0,"profile_details":{"firstname":"kamal","lastname":"singh","d_o_b":"1991-08-13","phone":"9856789567","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/profilepic.jpg"}},"comment":"Sdfsdf dad ds dad dad","created_at":"3  hours   ago","post_type":"HELP REQUEST"}],"reach_count":"0"},{"id":3,"name":"Plash Jindal","desc":"testing api request testing api request","title":"testing api request","video_link":"www.youtube.com","image":"http://dev.miracleglobal.com/cal-php/web/uploads/help/1485114336Penguins.jpg","like_flag":"Not Liked","comment_count":"0","like_count":"0","comments":[],"reach_count":"0"},{"id":2,"name":"Plash Jindal","desc":"testing api request testing api request","title":"testing api request","video_link":"","image":"","like_flag":"Not Liked","comment_count":"0","like_count":"0","comments":[],"reach_count":"0"}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 6
         * name : ABc
         * desc : testing api request testing api request helpas
         * title : testing api request Help Required Help
         * video_link : www.youtube.com
         * image : http://dev.miracleglobal.com/cal-php/web/uploads/help/1485115178Penguins.jpg
         * like_flag : Not Liked
         * comment_count : 0
         * like_count : 0
         * comments : []
         * reach_count : 0
         */

        private int id;
        private String name;
        private String desc;
        private String title;
        private String video_link;
        private String image;
        private String like_flag;
        private String comment_count;
        private String like_count;
        private String reach_count;
        private List<?> comments;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getVideo_link() {
            return video_link;
        }

        public void setVideo_link(String video_link) {
            this.video_link = video_link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getLike_flag() {
            return like_flag;
        }

        public void setLike_flag(String like_flag) {
            this.like_flag = like_flag;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getLike_count() {
            return like_count;
        }

        public void setLike_count(String like_count) {
            this.like_count = like_count;
        }

        public String getReach_count() {
            return reach_count;
        }

        public void setReach_count(String reach_count) {
            this.reach_count = reach_count;
        }

        public List<?> getComments() {
            return comments;
        }

        public void setComments(List<?> comments) {
            this.comments = comments;
        }
    }
}
