package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/27/2017.
 */

public class SettingsSetModal {

    /**
     * status : 1
     * message : Settings updated successfully
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
