package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicMyDownloadsModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 5/22/2017.
 */

public class JoyMusicMyDownloadsAdapter extends RecyclerView.Adapter<JoyMusicMyDownloadsAdapter.DataViewHolder> {
    public List<JoyMusicMyDownloadsModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;
    ArrayList<PlayMusicModal> arrayListMusic = new ArrayList<>();

    public JoyMusicMyDownloadsAdapter(Activity mContext, List<JoyMusicMyDownloadsModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


 /*   public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }*/

  /*  public void addAllItems(List<MyBooksModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }*/


    @Override
    public JoyMusicMyDownloadsAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_joy_music_view_all, parent, false);
        JoyMusicMyDownloadsAdapter.DataViewHolder dataView = new JoyMusicMyDownloadsAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final JoyMusicMyDownloadsAdapter.DataViewHolder holder, int position) {

        holder.atv_Name.setText(list.get(position).getSong_name());

        Glide.with(mContext).load(list.get(position).getAlbum_cover_image()).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.mProgress.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.mProgress.setVisibility(View.GONE);
                return false;
            }
        }).centerCrop().crossFade().into(holder.atv_AlbumCover);


    }


    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.atv_Name)
        AppCompatTextView atv_Name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.movie_progress)
        ProgressBar mProgress;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ///to the player Activity

                    playAudio(getAdapterPosition());

                }
            });


        }

        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }

    private void playAudio(int position) {

        if (!arrayListMusic.isEmpty()) {
            arrayListMusic.clear();
        }
        for (int i = 0; i < list.size(); i++) {
            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(list.get(i).getMusic_file());
            playMusicModal.setThumbnailUrl(list.get(i).getAlbum_cover_image());
            playMusicModal.setArtworkUrl(list.get(i).getAlbum_cover_image());
            playMusicModal.setTitle(list.get(i).getSong_name());
            playMusicModal.setAlbum(list.get(i).getAlbum_name());
            playMusicModal.setArtist(list.get(i).getAlbum_name());
            playMusicModal.setPaymentType(list.get(i).getPayment_type());
            playMusicModal.setDownloaded(list.get(i).getIs_downloaded());
            arrayListMusic.add(playMusicModal);
        }

        //mPlayListList = mList;

        Intent intent = new Intent(mContext, AudioPlayerActivity.class);
        intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, position);
        intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, list.get(position).getId());
        intent.putExtra(Constants.SONG_NAME, list.get(position).getSong_name());
        intent.putExtra(Constants.ALBUM_COVER, list.get(position).getAlbum_cover_image());
        intent.putExtra(Constants.MUSIC, Constants.SONG);

        Bundle b = new Bundle();
        b.putParcelableArrayList(Constants.SONGS_LIST, arrayListMusic);
        intent.putExtra("songs_bundle", b);
        //intent.putParcelableArrayListExtra(Constants.SONGS_LIST, songsBeanArrayList);
        mContext.startActivity(intent);


    }
}

