package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by preeti.sharma on 2/23/2017.
 */

public class DemoShareModal {


    private final String comment_count;

    public DemoShareModal(String count) {
        this.comment_count = count;
    }

    public String getMessage() {
        return comment_count;
    }

    /*private final CommentsListModal message;

    public DemoEventBusModel(CommentsListModal commentsListModal) {
        this.message = commentsListModal;
    }

    public CommentsListModal getMessage() {
        return message;
    }*/

}
