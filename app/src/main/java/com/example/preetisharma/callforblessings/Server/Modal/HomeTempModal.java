package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.ArrayList;

/**
 * Created by preeti.sharma on 1/20/2017.
 */

public class HomeTempModal {
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTimeUpdation() {
        return timeUpdation;
    }

    public void setTimeUpdation(String timeUpdation) {
        this.timeUpdation = timeUpdation;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }


    private String userName;
    private String image, timeUpdation, videoUrl;

    public ArrayList<String> getPostList() {
        return postList;
    }

    public void setPostList(ArrayList<String> postList) {
        this.postList = postList;
    }

    private ArrayList<String> postList;
}
