package com.example.preetisharma.callforblessings.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.SettingOptionListAdapter;
import com.example.preetisharma.callforblessings.Adapter.SettingsMainAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsMainModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class SettingOptionListActivity extends BaseActivity implements APIServerResponse{

    @BindView(R.id.rv_settings)
    RecyclerView rv_settings;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;


    List<String> settingsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_option_list);
        ButterKnife.bind(this);


        txtvw_header_title.setText("Setting");
        img_view_change_password.setVisibility(View.GONE);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingOptionListActivity.this.finish();
            }
        });
        settingsList.add("General Setting");
        settingsList.add("Security");
        settingsList.add("Privacy");
        settingsList.add("Timeline & Tagging");
        settingsList.add("Blocking");
        settingsList.add("Notification");

        SettingOptionListAdapter adapter = new SettingOptionListAdapter(SettingOptionListActivity.this, settingsList);
        rv_settings.setAdapter(adapter);
        rv_settings.setItemAnimator(new DefaultItemAnimator());
        rv_settings.setLayoutManager(new LinearLayoutManager(SettingOptionListActivity.this));




        if (isConnectedToInternet()) {

            showLoading();
            ServerAPI.getInstance().getAllSettingsType(APIServerResponse.SETTINGS_MAIN, getUserSessionId(), this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }



    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {

            if (response.isSuccessful()) {
                SettingsMainModal settingsMainModal;

                switch (tag) {

                    case APIServerResponse.SETTINGS_MAIN:
                        settingsMainModal = (SettingsMainModal) response.body();
                        /*if (settingsMainModal.getStatus().equalsIgnoreCase("1")) {
                            List<SettingsMainModal.ListBean> settingsList = settingsMainModal.getList();
                            SettingOptionListAdapter adapter = new SettingOptionListAdapter(SettingOptionListActivity.this, settingsList);
                            rv_settings.setAdapter(adapter);
                            rv_settings.setItemAnimator(new DefaultItemAnimator());
                            rv_settings.setLayoutManager(new LinearLayoutManager(SettingOptionListActivity.this));
                        }*/
                        hideLoading();
                        break;
                }


            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }

}
