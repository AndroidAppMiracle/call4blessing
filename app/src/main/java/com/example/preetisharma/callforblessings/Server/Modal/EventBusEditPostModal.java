package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 3/1/2017.
 */

public class EventBusEditPostModal {

    private final String comment_count;

    public EventBusEditPostModal(String count) {
        this.comment_count = count;
    }

    public String getMessage() {
        return comment_count;
    }
}
