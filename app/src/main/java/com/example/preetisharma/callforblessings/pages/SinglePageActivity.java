package com.example.preetisharma.callforblessings.pages;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.PostItemsAdapter;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CreatePostActivity;
import com.example.preetisharma.callforblessings.photos.PhotosTabs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class SinglePageActivity extends BaseActivity implements APIServerResponse, EasyPermissions.PermissionCallbacks {

    @BindView(R.id.home_listing)
    RecyclerView home_listing;
    private LinearLayoutManager mLayoutManager;
    PostItemsAdapter postItemsAdapter;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;


    @BindView(R.id.img_vw_post)
    AppCompatImageView img_vw_post;
    @BindView(R.id.img_vw_photo)
    AppCompatImageView img_vw_photo;
    @BindView(R.id.edit_post_setting_img)
    AppCompatImageView edit_post_setting_img;
    @BindView(R.id.img_invite_friend)
    AppCompatImageView img_invite_friend;



    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_page);
        ButterKnife.bind(this);
        callApi(1);
        txtvw_header_title.setText("Your Page ");
        mLayoutManager = new LinearLayoutManager(SinglePageActivity.this);
        home_listing.setLayoutManager(mLayoutManager);
        postItemsAdapter = new PostItemsAdapter(this);
        home_listing.setNestedScrollingEnabled(false);
        home_listing.setHasFixedSize(false);
        mLayoutManager.setAutoMeasureEnabled(true);
        img_view_change_password.setVisibility(View.GONE);
        img_view_change_password.setImageDrawable(getResources().getDrawable(R.drawable.edit));

        img_view_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent changePic = new Intent(getApplicationContext(), UpdatePagesDescription.class);
                startActivity(changePic);
            }
        });
        home_listing.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(home_listing.getContext(),
                mLayoutManager.getOrientation());
        home_listing.addItemDecoration(dividerItemDecoration);
        home_listing.setAdapter(postItemsAdapter);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    public void callApi(int pageNumber) {


        if (isConnectedToInternet()) {
            try {
                ServerAPI.getInstance().Home(APIServerResponse.HOME, getUserSessionId(), pageNumber, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }


    @OnClick(R.id.open_gallery_layout)
    public void openGalleryLayout() {
        img_vw_post.setImageResource(R.drawable.post);
        img_invite_friend.setImageResource(R.drawable.invite_black);
        img_vw_photo.setImageResource(R.drawable.icon_red_camera);
        edit_post_setting_img.setImageResource(R.drawable.edit);
        Intent intentPhotos = new Intent(SinglePageActivity.this, PhotosTabs.class);
        startActivity(intentPhotos);
    }
    @OnClick(R.id.invite_friend_layout)
    public void inviteFriends() {

        img_vw_post.setImageResource(R.drawable.post);
        img_invite_friend.setImageResource(R.drawable.invite_red);
        img_vw_photo.setImageResource(R.drawable.icon_camera);
        edit_post_setting_img.setImageResource(R.drawable.edit);
    }

    @OnClick(R.id.post_new_layout)
    public void openPostActivity() {

        img_vw_post.setImageResource(R.drawable.post_red);
        img_invite_friend.setImageResource(R.drawable.invite_black);
        img_vw_photo.setImageResource(R.drawable.icon_camera);
        edit_post_setting_img.setImageResource(R.drawable.edit);

        Intent intentPhotos = new Intent(SinglePageActivity.this, CreatePostActivity.class);
        startActivity(intentPhotos);
    }

    @OnClick(R.id.edit_post_setting_layout)
    public void editPage() {

        img_vw_post.setImageResource(R.drawable.post);
        img_invite_friend.setImageResource(R.drawable.invite_black);
        img_vw_photo.setImageResource(R.drawable.icon_camera);
        edit_post_setting_img.setImageResource(R.drawable.edit_red);
    }


    //post_new_layout
 /*   @OnClick(R.id.post_new_layout)
    public void openPostSendingPage() {
        Intent intentPhotos = new Intent(SinglePageActivity.this, PhotosTabs.class);
        startActivity(intentPhotos);
    }*/
}
