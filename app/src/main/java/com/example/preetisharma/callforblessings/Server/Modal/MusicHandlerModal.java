package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.HashMap;

/**
 * Created by preeti.sharma on 3/2/2017.
 */

public class MusicHandlerModal {
    int count;

    public HashMap<Integer, String> getMapMusic() {
        return mapMusic;
    }

    public void setMapMusic(HashMap<Integer, String> mapMusic) {
        this.mapMusic = mapMusic;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    HashMap<Integer, String> mapMusic;
}
