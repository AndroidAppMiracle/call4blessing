package com.example.preetisharma.callforblessings;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventDetailsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/8/2017.
 */

public class EventsDetailsActivityNew extends BaseActivity implements APIServerResponse {

    String postID;
    ImageView event_image;
    Toolbar main_toolbar;
    FloatingActionButton fab_event_status;
    AppCompatTextView tv_event_host_info, tv_event_location, txtvw_event_invite_all_count, txtvw_event_invite_accepted_count,
            txtvw_event_invite_pending_count, tv_event_detail_description, tv_event_detail_description_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_details_new);


        if (getIntent().getExtras() != null) {
            postID = getIntent().getExtras().getString(Constants.EVENT_ID);
        }

        main_toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        event_image = (ImageView) findViewById(R.id.event_image);
        fab_event_status = (FloatingActionButton) findViewById(R.id.fab_event_status);
        tv_event_host_info = (AppCompatTextView) findViewById(R.id.tv_event_host_info);
        tv_event_location = (AppCompatTextView) findViewById(R.id.tv_event_location);
        txtvw_event_invite_all_count = (AppCompatTextView) findViewById(R.id.txtvw_event_invite_all_count);
        txtvw_event_invite_accepted_count = (AppCompatTextView) findViewById(R.id.txtvw_event_invite_accepted_count);
        txtvw_event_invite_pending_count = (AppCompatTextView) findViewById(R.id.txtvw_event_invite_pending_count);
        tv_event_detail_description = (AppCompatTextView) findViewById(R.id.tv_event_detail_description);
        tv_event_detail_description_time = (AppCompatTextView) findViewById(R.id.tv_event_detail_description_time);


        try {
            setSupportActionBar(main_toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            main_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventsDetailsActivityNew.this.finish();
                }
            });

            fab_event_status.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getEventDetails(APIServerResponse.EVENT_DETAILS, getUserSessionId(), postID, this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {
            hideLoading();
            if (response.isSuccessful()) {
                EventDetailsModal eventDetailsModal;
                Resources resources = getResources();

                switch (tag) {
                    case APIServerResponse.EVENT_DETAILS:
                        eventDetailsModal = (EventDetailsModal) response.body();
                        if (eventDetailsModal.getStatus().equals("1")) {
                            tv_event_location.setText(eventDetailsModal.getDetail().getEvent_location());

                            String text = String.format(resources.getString(R.string.two_parameters), eventDetailsModal.getDetail().getUser_info().getProfile_details().getFirstname(), eventDetailsModal.getDetail().getUser_info().getProfile_details().getLastname());
                            tv_event_host_info.setText(text);
                            getSupportActionBar().setTitle(eventDetailsModal.getDetail().getEvent_name());
                            if (!eventDetailsModal.getDetail().getEvent_photo().equals("")) {
                                Glide.with(EventsDetailsActivityNew.this).load(eventDetailsModal.getDetail().getEvent_photo()).placeholder(R.drawable.placeholder_callforblessings).into(event_image);
                            } else {
                                Glide.with(EventsDetailsActivityNew.this).load(R.drawable.placeholder_callforblessings).into(event_image);
                            }
                            tv_event_detail_description.setText(eventDetailsModal.getDetail().getEvent_desc());
                            tv_event_detail_description_time.setText(eventDetailsModal.getDetail().getEvent_date());
                            txtvw_event_invite_all_count.setText(eventDetailsModal.getDetail().getAll_count());
                            txtvw_event_invite_accepted_count.setText(eventDetailsModal.getDetail().getAccepted_count());
                            txtvw_event_invite_pending_count.setText(eventDetailsModal.getDetail().getPending_count());
                        }

                        break;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getEventDetails(APIServerResponse.EVENT_DETAILS, getUserSessionId(), postID, this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

        hideLoading();
    }
}
