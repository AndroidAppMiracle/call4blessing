package com.example.preetisharma.callforblessings.demo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.CommentsAdapter;
import com.example.preetisharma.callforblessings.Adapter.SinglePostViewCommentsAdapter;
import com.example.preetisharma.callforblessings.HomeActivity;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.SinglePostViewModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.Utilities;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/4/2017.
 */

public class SinglePostView extends BaseActivity implements APIServerResponse {
    @BindView(R.id.txtvw_tagged_friends)
    AppCompatTextView txtvw_tagged_friends;
    @BindView(R.id.txtvw_user_name)
    AppCompatTextView txtvw_user_name;
    @BindView(R.id.txtvw_time_stamp)
    AppCompatTextView txtvw_time_stamp;
    @BindView(R.id.txtvw_post_details)
    AppCompatTextView txtvw_post_details;
    @BindView(R.id.txtvw_no_of_likes)
    AppCompatTextView txtvw_no_of_likes;
    @BindView(R.id.txtvw_no_of_comments)
    AppCompatTextView txtvw_no_of_comments;
    @BindView(R.id.txtvw_like)
    AppCompatTextView txtvw_like;
    @BindView(R.id.txtvw_comment)
    AppCompatTextView txtvw_comment;
    @BindView(R.id.txtvw_share)
    AppCompatTextView txtvw_share;
    @BindView(R.id.img_vw_user_profile)
    AppCompatImageView img_vw_user_profile;
    @BindView(R.id.viewholder_image)
    AppCompatImageView viewholder_image;
    @BindView(R.id.iv_more_options)
    AppCompatImageView iv_more_options;
    @BindView(R.id.tv_changed_profile_cover_pic)
    AppCompatTextView tv_changed_profile_cover_pic;
    @BindView(R.id.video_play)
    AppCompatImageView video_play;

    //Shared Post
    @BindView(R.id.card_shared_post)
    CardView card_shared_post;
    @BindView(R.id.img_vw_user_profile_shared)
    AppCompatImageView img_vw_user_profile_shared;
    @BindView(R.id.txtvw_user_name_shared)
    AppCompatTextView txtvw_user_name_shared;
    @BindView(R.id.txtvw_time_stamp_shared)
    AppCompatTextView txtvw_time_stamp_shared;
    @BindView(R.id.txtvw_post_details_shared)
    AppCompatTextView txtvw_post_details_shared;
    @BindView(R.id.viewholder_image_shared)
    AppCompatImageView viewholder_image_shared;

    @BindView(R.id.tv_changed_profile_cover_pic_shared)
    AppCompatTextView tv_changed_profile_cover_pic_shared;


    /////////////Comments
    @BindView(R.id.edt_txt_comment)
    AppCompatEditText edt_txt_comment;
    @BindView(R.id.txtvw_submit_comment)
    AppCompatTextView txtvw_submit_comment;
    @BindView(R.id.rv_comments)
    RecyclerView rv_comments;

    String postID;
    boolean keyboardOpen;
    AppCompatTextView txtvw_header_title;
    NestedScrollView nestedscroll;


    //String isLiked = "";
    //String likeCount = "";
    List<SinglePostViewModal.DetailBean.CommentsBean> commentsList = new ArrayList<>();
    SinglePostViewModal singlePostViewModal;

    AppCompatImageView img_view_back, img_view_change_password;
    String notificationFlag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_post_layout);
        updateStatusBar();
        ButterKnife.bind(this);


        try {

            img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
            img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
            txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
            nestedscroll = (NestedScrollView) findViewById(R.id.nestedscroll);
            txtvw_header_title.setText("Post");
            img_view_change_password.setVisibility(View.GONE);

            img_view_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (notificationFlag != null && !notificationFlag.equalsIgnoreCase("")) {
                        Intent i = new Intent(SinglePostView.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        SinglePostView.this.finish();
                    }

                }
            });

            if (getIntent().getExtras() != null) {
                postID = getIntent().getExtras().getString(Constants.POSTID);

                if (getIntent().getExtras().getString("Notification_Flag") != null && !getIntent().getExtras().getString("Notification_Flag").equalsIgnoreCase("")) {
                    notificationFlag = getIntent().getExtras().getString("Notification_Flag");
                }

            }

            showLoading();
            ServerAPI.getInstance().getPost(APIServerResponse.MY_POST, getUserSessionId(), postID, Constants.POST, this);


            txtvw_submit_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideKeyboard();
                    Log.e("data", "Comment" + edt_txt_comment.getText().toString());
                    if (edt_txt_comment.getText().toString().trim().length() != 0 || !edt_txt_comment.getText().toString().isEmpty() || !edt_txt_comment.getText().toString().equals("")) {


                        if (isConnectedToInternet()) {
                            if (txtvw_submit_comment.isEnabled()) {
                                txtvw_submit_comment.setEnabled(false);
                            }
                            singlePostViewModal.getDetail().setComment_count("" + (Integer.parseInt(singlePostViewModal.getDetail().getComment_count()) + 1));
                            if (singlePostViewModal.getDetail().getComment_count().equalsIgnoreCase("0") || singlePostViewModal.getDetail().getComment_count().equalsIgnoreCase("1")) {
                                txtvw_no_of_comments.setText(singlePostViewModal.getDetail().getComment_count() + " Comment");
                            } else {
                                txtvw_no_of_comments.setText(singlePostViewModal.getDetail().getComment_count() + " Comments");
                            }

                            ServerAPI.getInstance().comment(APIServerResponse.COMMENT, getUserSessionId(), postID, Constants.POST, edt_txt_comment.getText().toString().trim(), SinglePostView.this);

                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    } else {
                        showToast("Enter comment", Toast.LENGTH_SHORT);
                    }
                }
            });

            txtvw_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (isConnectedToInternet()) {
                        if (txtvw_like.isEnabled()) {
                            txtvw_like.setEnabled(false);
                        }
                        if (singlePostViewModal.getDetail().getLike_flag().equalsIgnoreCase(getString(R.string.not_liked))) {
                            int imgResource = R.drawable.ic_liked;
                            txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            singlePostViewModal.getDetail().setLike_flag(Constants.LIKED);
                            singlePostViewModal.getDetail().setLike_count("" + (Integer.parseInt(singlePostViewModal.getDetail().getLike_count()) + 1));

                            if (singlePostViewModal.getDetail().getLike_count().equalsIgnoreCase("0") || singlePostViewModal.getDetail().getLike_count().equalsIgnoreCase("1")) {
                                txtvw_no_of_likes.setText(singlePostViewModal.getDetail().getLike_count() + " Like");
                            } else {
                                txtvw_no_of_likes.setText(singlePostViewModal.getDetail().getLike_count() + " Likes");
                            }

                            ServerAPI.getInstance().like(APIServerResponse.LIKE, getUserSessionId(), postID, "POST", SinglePostView.this);

                        } else {

                            int imgResource = R.drawable.prayer_like_icon;
                            txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            singlePostViewModal.getDetail().setLike_flag(Constants.NOT_LIKED);
                            singlePostViewModal.getDetail().setLike_count("" + (Integer.parseInt(singlePostViewModal.getDetail().getLike_count()) - 1));
                            if (singlePostViewModal.getDetail().getLike_count().equalsIgnoreCase("0") || singlePostViewModal.getDetail().getLike_count().equalsIgnoreCase("1")) {
                                txtvw_no_of_likes.setText(singlePostViewModal.getDetail().getLike_count() + " Like");
                            } else {
                                txtvw_no_of_likes.setText(singlePostViewModal.getDetail().getLike_count() + " Likes");
                            }

                            ServerAPI.getInstance().like(APIServerResponse.UNLIKE, getUserSessionId(), postID, "POST", SinglePostView.this);
                        }
                    } else {
                        showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }


                }
            });


            txtvw_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (singlePostViewModal.getDetail().getMedia().size() > 0) {
                        if (singlePostViewModal.getDetail().getMedia().get(0).getType().equalsIgnoreCase("Image")) {
                            openConfirmationDialog(String.valueOf(singlePostViewModal.getDetail().getId()), singlePostViewModal.getDetail().getPost_content(), singlePostViewModal.getDetail().getPosted_at(), singlePostViewModal.getDetail().getMedia().get(0).getFile(), "", "");
                        } else if (singlePostViewModal.getDetail().getMedia().get(0).getType().equalsIgnoreCase("Video")) {
                            openConfirmationDialog(String.valueOf(singlePostViewModal.getDetail().getId()), singlePostViewModal.getDetail().getPost_content(), singlePostViewModal.getDetail().getPosted_at(), "", singlePostViewModal.getDetail().getMedia().get(0).getFile(), singlePostViewModal.getDetail().getMedia().get(0).getThumbnail_file());

                        }
                    } else {
                        openConfirmationDialog(String.valueOf(singlePostViewModal.getDetail().getId()), singlePostViewModal.getDetail().getPost_content(), singlePostViewModal.getDetail().getPosted_at(), "", "", "");

                    }
                }
            });

            txtvw_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    edt_txt_comment.requestFocusFromTouch();
                    edt_txt_comment.requestFocus();

                    if (keyboardOpen) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edt_txt_comment.getWindowToken(), 0);
                        keyboardOpen = false;
                    } else {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(edt_txt_comment, InputMethodManager.SHOW_IMPLICIT);
                        keyboardOpen = true;
                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {

            CommentModal comment;
            LikeModal likeModal;

            switch (tag) {

                case APIServerResponse.LIKE:

                    likeModal = (LikeModal) response.body();
                    if (likeModal.getStatus().equals("1")) {


                        //int imgResource = R.drawable.ic_liked;
                        //xtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        //int likescountValue = Integer.parseInt(singlePostViewModal.getDetail().getLike_count());
                        // likescountValue += 1;
                        //isLiked = getString(R.string.liked);
                        //singlePostViewModal.getDetail().setLike_flag("Liked");
                        //list.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));
                       /* if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                            singlePostViewModal.getDetail().setLike_count(String.valueOf(likescountValue));
                            singlePostViewModal.getDetail().setLike_flag(getString(R.string.liked));

                        } else {
                            txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                            singlePostViewModal.getDetail().setLike_count(String.valueOf(likescountValue));
                            singlePostViewModal.getDetail().setLike_flag(getString(R.string.liked));
                        }*/

                        if (!txtvw_like.isEnabled()) {
                            txtvw_like.setEnabled(true);
                        }
                    }

                    break;
                case APIServerResponse.UNLIKE:
                    likeModal = (LikeModal) response.body();
                    if (likeModal.getStatus().equals("1")) {

                        if (!txtvw_like.isEnabled()) {
                            txtvw_like.setEnabled(true);
                        }

                       /* int imgResource = R.drawable.prayer_like;
                        txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        int likescountValue = Integer.parseInt(singlePostViewModal.getDetail().getLike_count());
                        likescountValue--;
                        //isLiked = getString(R.string.not_liked);
                        //list.get(getAdapterPosition()).setLike_flag("not liked");
                        //list.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                        if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                            singlePostViewModal.getDetail().setLike_count(String.valueOf(likescountValue));
                            singlePostViewModal.getDetail().setLike_flag(getString(R.string.not_liked));
                        } else {
                            txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                            singlePostViewModal.getDetail().setLike_count(String.valueOf(likescountValue));
                            singlePostViewModal.getDetail().setLike_flag(getString(R.string.not_liked));
                        }*/
                    }
                    break;

                case APIServerResponse.COMMENT:
                    comment = (CommentModal) response.body();
                    if (comment.getStatus().equals("1")) {
                        EventBus.getDefault().post(new DemoCommentModal(commentsList.size()));
                        showToast("Commented", Toast.LENGTH_SHORT);
                        edt_txt_comment.setText("");
                        if (!txtvw_submit_comment.isEnabled()) {
                            txtvw_submit_comment.setEnabled(true);
                        }
                        ServerAPI.getInstance().getPost(APIServerResponse.COMMENTLIST, getUserSessionId(), postID, Constants.POST, this);
                    }


                    break;

                case APIServerResponse.COMMENTLIST:

                    singlePostViewModal = (SinglePostViewModal) response.body();

                    if (singlePostViewModal.getStatus().equalsIgnoreCase("1")) {
                        commentsList = singlePostViewModal.getDetail().getComments();

                        SinglePostViewCommentsAdapter _commentsAdapter = new SinglePostViewCommentsAdapter(this, commentsList);


                        rv_comments.setItemAnimator(new DefaultItemAnimator());
                        rv_comments.setAdapter(_commentsAdapter);
                        //rv_comments.smoothScrollToPosition(commentsList.size());
                        if (commentsList.size() >= 2) {
                            if (!nestedscroll.isSmoothScrollingEnabled()) {
                                nestedscroll.setSmoothScrollingEnabled(true);
                            }
                            nestedscroll.smoothScrollTo(nestedscroll.computeVerticalScrollExtent(), nestedscroll.computeVerticalScrollExtent());
                        }

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this); /*{
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        };*/


                        rv_comments.setLayoutManager(mLayoutManager);
                    }

                    break;

                case APIServerResponse.MY_POST:

                    singlePostViewModal = (SinglePostViewModal) response.body();
                    if (singlePostViewModal.getStatus().equalsIgnoreCase("1")) {


                        commentsList = singlePostViewModal.getDetail().getComments();

                        SinglePostViewCommentsAdapter _commentsAdapter = new SinglePostViewCommentsAdapter(this, commentsList);


                        rv_comments.setItemAnimator(new DefaultItemAnimator());
                        rv_comments.setAdapter(_commentsAdapter);
                        rv_comments.smoothScrollToPosition(commentsList.size());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this) {
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        };


                        rv_comments.setLayoutManager(mLayoutManager);


                        //likeCount = singlePostViewModal.getDetail().getLike_count();

                        txtvw_user_name.setText(singlePostViewModal.getDetail().getPost_by().getProfile_details().getFirstname().trim() + " " + singlePostViewModal.getDetail().getPost_by().getProfile_details().getLastname().trim());
                        txtvw_time_stamp.setText(singlePostViewModal.getDetail().getPosted_at());

                        if (singlePostViewModal.getDetail().getPost_type().equals(Constants.PROFILE_PIC)) {
                            //       tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                            // tv_changed_profile_cover_pic.setText(Constants.CHANGED_PROFILE_PIC);
                        } else if (singlePostViewModal.getDetail().getPost_type().equals(Constants.COVER_PIC)) {
                            tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                            tv_changed_profile_cover_pic.setText(Constants.CHANGED_COVER_PIC);

                        } else if (singlePostViewModal.getDetail().getTag_users() != null && singlePostViewModal.getDetail().getTag_users().size() > 0) {
                            txtvw_tagged_friends.setVisibility(View.VISIBLE);
                            if (singlePostViewModal.getDetail().getTag_users().size() == 1) {
                                txtvw_tagged_friends.setText(" " + "with " + singlePostViewModal.getDetail().getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + singlePostViewModal.getDetail().getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                            } else if (singlePostViewModal.getDetail().getTag_users().size() == 2) {
                                txtvw_tagged_friends.setText(" " + "with " + singlePostViewModal.getDetail().getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + singlePostViewModal.getDetail().getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + singlePostViewModal.getDetail().getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + singlePostViewModal.getDetail().getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                            } else {
                                txtvw_tagged_friends.setText(" " + "with " + singlePostViewModal.getDetail().getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + singlePostViewModal.getDetail().getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (singlePostViewModal.getDetail().getTag_users().size() - 1) + " others");

                            }
                        } else if (singlePostViewModal.getDetail().getPost_type().equals(Constants.SHARE)) {
                            try {
                                card_shared_post.setVisibility(View.VISIBLE);
                                tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                                tv_changed_profile_cover_pic.setText(Constants.SHARED);

                                Glide.with(SinglePostView.this).load(singlePostViewModal.getDetail().getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).thumbnail(0.1f).placeholder(R.drawable.ic_me).into(img_vw_user_profile_shared);
                                txtvw_user_name_shared.setText(singlePostViewModal.getDetail().getShared_post().get(0).getPost_by().getProfile_details().getFirstname());
                                txtvw_time_stamp_shared.setText(singlePostViewModal.getDetail().getShared_post().get(0).getPosted_at());
                                txtvw_post_details_shared.setText(singlePostViewModal.getDetail().getShared_post().get(0).getPost_content());
                                if (singlePostViewModal.getDetail().getShared_post().get(0).getMedia().size() != 0 && singlePostViewModal.getDetail().getShared_post().get(0).getMedia().get(0).getType().equalsIgnoreCase(Constants.IMAGE)) {
                                    viewholder_image_shared.setVisibility(View.VISIBLE);
                                    Glide.with(SinglePostView.this).load(singlePostViewModal.getDetail().getShared_post().get(0).getMedia().get(0).getFile()).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(viewholder_image_shared);
                                }
                                if (singlePostViewModal.getDetail().getShared_post().get(0).getPost_type().equalsIgnoreCase(Constants.PROFILE_PIC)) {
                                    tv_changed_profile_cover_pic_shared.setVisibility(View.VISIBLE);
                                    tv_changed_profile_cover_pic_shared.setText(Constants.CHANGED_PROFILE_PIC);
                                } else if (singlePostViewModal.getDetail().getShared_post().get(0).getPost_type().equalsIgnoreCase(Constants.COVER_PIC)) {
                                    tv_changed_profile_cover_pic_shared.setVisibility(View.VISIBLE);
                                    tv_changed_profile_cover_pic_shared.setText(Constants.CHANGED_COVER_PIC);
                                } else {
                                    tv_changed_profile_cover_pic_shared.setVisibility(View.GONE);
                                    //holder.tv_changed_profile_cover_pic_shared.setText(Constants.SHARED);
                                }
                            } catch (Exception e)

                            {
                                e.printStackTrace();
                            }
                        } else {
                            card_shared_post.setVisibility(View.GONE);
                            tv_changed_profile_cover_pic.setVisibility(View.GONE);
                        }


                        if (String.valueOf(singlePostViewModal.getDetail().getPost_by().getId()).equalsIgnoreCase(getUserID()) && (!singlePostViewModal.getDetail().getPost_type().equals(Constants.PROFILE_PIC)
                                || !singlePostViewModal.getDetail().getPost_type().equals(Constants.COVER_PIC))) {
                            iv_more_options.setVisibility(View.GONE);
                            /*iv_more_options.setVisibility(View.VISIBLE);*/
                        } else {
                            iv_more_options.setVisibility(View.GONE);
                        }


                        if (singlePostViewModal.getDetail().getLike_count().equals("0") || singlePostViewModal.getDetail().getLike_count().equals("1")) {
                            txtvw_no_of_likes.setText(singlePostViewModal.getDetail().getLike_count() + " " + "Like");
                        } else {
                            txtvw_no_of_likes.setText(singlePostViewModal.getDetail().getLike_count() + " " + "Likes");
                        }
                        if (singlePostViewModal.getDetail().getComment_count().equals("0") || singlePostViewModal.getDetail().getComment_count().equals("1")) {
                            txtvw_no_of_comments.setText(singlePostViewModal.getDetail().getComment_count() + " " + "Comment");
                        } else {
                            txtvw_no_of_comments.setText(singlePostViewModal.getDetail().getComment_count() + " " + "Comments");

                        }
                        if (singlePostViewModal.getDetail().getLike_flag().equalsIgnoreCase("Not liked")) {
                            int imgResource = R.drawable.prayer_like_icon;
                            txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        } else {
                            int imgResource = R.drawable.ic_liked;
                            txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        }
                        txtvw_post_details.setText(Utilities.stripText(singlePostViewModal.getDetail().getPost_content()));
                        Glide.with(SinglePostView.this).load(singlePostViewModal.getDetail().getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).into(img_vw_user_profile);

                        if (singlePostViewModal.getDetail().getMedia().size() > 0) {

                            if (singlePostViewModal.getDetail().getMedia().get(0).getType().equalsIgnoreCase("Image")) {
                                viewholder_image.setVisibility(View.VISIBLE);
                                Glide.with(SinglePostView.this).load(Utilities.stripHtml(singlePostViewModal.getDetail().getMedia().get(0).getFile())).override(300, 300).thumbnail(0.1f)/*.placeholder(R.drawable.placeholder)*/.into(viewholder_image);

                            } else if (singlePostViewModal.getDetail().getMedia().get(0).getType().equalsIgnoreCase("Video")) {
                                try {
                                    final String videoFile = singlePostViewModal.getDetail().getMedia().get(0).getFile();
                                    viewholder_image.setVisibility(View.VISIBLE);
                                    video_play.setVisibility(View.VISIBLE);
                                    viewholder_image.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent videoIntent = new Intent(SinglePostView.this, VideoViewActivity.class);
                                            Bundle b = new Bundle();
                                            b.putString(Constants.FRAGMENT_NAME, "HOMEWALLADAPTER");
                                            b.putString(Constants.VIDEO, videoFile);
                                            videoIntent.putExtras(b);
                                            startActivity(videoIntent);

                                        }
                                    });

                                    Glide.with(SinglePostView.this).load(singlePostViewModal.getDetail().getMedia().get(0).getThumbnail_file()).override(300, 300).thumbnail(0.1f).placeholder(R.drawable.ic_play).into(viewholder_image);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            viewholder_image.setVisibility(View.GONE);
                        }
                        /*if (txtvw_like.getCompoundDrawables().equals(R.drawable.prayer_like_icon)) {
                            Log.e("Liked", "Liked");
                        }*/

                    }

                    hideLoading();
                    break;


            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {

        hideLoading();
    }


    public void openConfirmationDialog(String postId, String post_text, String timeStamp, String post_image, String postVideoUrl, String postVideoThumbnail) {

        Intent shareDialog = new Intent(SinglePostView.this, SharePostActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.POSTTYPE, "POST");
        b.putString(Constants.POSTIMAGE, post_image);
        b.putString(Constants.POSTTEXT, post_text);
        b.putString(Constants.POSTTIMESTAMP, timeStamp);
        b.putString(Constants.POSTID, postId);
        b.putString(Constants.POSTVIDEO, postVideoUrl);
        b.putString(Constants.POSTTHUMBNAIL, postVideoThumbnail);
        shareDialog.putExtras(b);
        startActivity(shareDialog);


    }

    @Override
    public void onBackPressed() {

        if (notificationFlag != null && !notificationFlag.equalsIgnoreCase("")) {
            Intent i = new Intent(SinglePostView.this, HomeActivity.class);
            startActivity(i);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
