package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/19/2017.
 */

public class AddEventModal {


    /**
     * status : 1
     * detail : {"id":1,"event_name":"NYE","event_desc":"test test test","event_location":"Chandigarh asd","event_date":"2017-02-01 12:12:00","event_photo":"/cal-php/web/theme/images/no-image-found.jpg","user_info":{"id":20,"username":"plash12333","email":"plash12333@abc.com","updated":1,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"}},"invites":[]}
     * message : Event uploaded successfully
     */

    private String status;
    private DetailBean detail;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DetailBean {
        /**
         * id : 1
         * event_name : NYE
         * event_desc : test test test
         * event_location : Chandigarh asd
         * event_date : 2017-02-01 12:12:00
         * event_photo : /cal-php/web/theme/images/no-image-found.jpg
         * user_info : {"id":20,"username":"plash12333","email":"plash12333@abc.com","updated":1,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"}}
         * invites : []
         */

        private int id;
        private String event_name;
        private String event_desc;
        private String event_location;
        private String event_date;
        private String event_photo;
        private UserInfoBean user_info;
        private List<?> invites;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getEvent_name() {
            return event_name;
        }

        public void setEvent_name(String event_name) {
            this.event_name = event_name;
        }

        public String getEvent_desc() {
            return event_desc;
        }

        public void setEvent_desc(String event_desc) {
            this.event_desc = event_desc;
        }

        public String getEvent_location() {
            return event_location;
        }

        public void setEvent_location(String event_location) {
            this.event_location = event_location;
        }

        public String getEvent_date() {
            return event_date;
        }

        public void setEvent_date(String event_date) {
            this.event_date = event_date;
        }

        public String getEvent_photo() {
            return event_photo;
        }

        public void setEvent_photo(String event_photo) {
            this.event_photo = event_photo;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public List<?> getInvites() {
            return invites;
        }

        public void setInvites(List<?> invites) {
            this.invites = invites;
        }

        public static class UserInfoBean {
            /**
             * id : 20
             * username : plash12333
             * email : plash12333@abc.com
             * updated : 1
             * profile_details : {"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"}
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : Plash
                 * lastname : Jindal
                 * d_o_b : 1992-08-29
                 * phone : 212156464545
                 * gender : MALE
                 * location :
                 * country :
                 * state :
                 * cover_pic : http://dev.miracleglobal.com/cal-php/web/theme/images/cover-picture.jpg
                 * profile_pic : http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }
    }
}
