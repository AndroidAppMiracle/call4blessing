package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/9/2017.
 */

public class EventBusGroupCommentModal {

    private final int status;
    private final int itemPosition;

    public EventBusGroupCommentModal(int status, int itemPosition) {
        this.status = status;
        this.itemPosition = itemPosition;
    }

    public int getStatus() {
        return status;
    }

    public int getItemPosition() {

        return itemPosition;
    }
}
