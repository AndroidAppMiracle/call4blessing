package com.example.preetisharma.callforblessings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

/**
 * Created by deepak on 15/2/16.
 */
public class FlowOrganizer {
    private static String _last_fragment_name = "";

    private int _id_parent_frame_view;
    private FragmentManager _fragment_manager;
    private AppCompatActivity _activity;


    public void clearBackStack() {
        try {
           /* _fragment_manager.popBackStack(null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
            _fragment_manager.popBackStackImmediate();
*/
            _fragment_manager.popBackStack();

        } catch (Exception e) {
        }
    }

    public FlowOrganizer(AppCompatActivity _activity, int idParentFameView) {
        this._activity = _activity;
        this._id_parent_frame_view = idParentFameView;
        this._fragment_manager = _activity.getSupportFragmentManager();
    }

    /**
     * @param toFragment Fragment support v4
     * @author Wild Coder
     */
    public void replace(Fragment toFragment) {

        replace(toFragment, null, false);
    }

    public void updateFragment(Bundle bundle, Fragment toFragmnt) {
        _fragment_manager.putFragment(bundle, _last_fragment_name, toFragmnt);

    }

    /**
     * @param toFragment  Fragment support v4
     * @param isAllowBack Allow Back To the screen
     * @author Wild Coder
     */
    public void replace(Fragment toFragment, boolean isAllowBack) {
        FragmentTransaction _fragment_transiction = _fragment_manager
                .beginTransaction();
        replace(toFragment, null, isAllowBack);
    }

    /**
     * @param toFragment  Fragment support v4
     * @param bundle      Bundle
     * @param isAllowBack Allow Back To the screen
     * @author Wild Coder
     */
    public void replace(Fragment toFragment, Bundle bundle, boolean isAllowBack) {
        if (toFragment == null)
            return;
        if (bundle != null) {
            toFragment.setArguments(bundle);
        }

        FragmentTransaction _fragment_transiction = _fragment_manager
                .beginTransaction();
        // _fragment_manager.executePendingTransactions();

        if (isAllowBack) {
            _fragment_transiction.addToBackStack(_last_fragment_name);
        }
        _fragment_transiction.replace(_id_parent_frame_view, toFragment,
                toFragment.getClass().getName()).commitAllowingStateLoss();
        _last_fragment_name = toFragment.getClass().getName();
    }

    /**
     * @param toFragment Fragment support v4
     * @author Wild Coder
     */
    public void add(Fragment toFragment) {
        add(toFragment, null, false);
    }

    /**
     * @param toFragment  Fragment support v4
     * @param isAllowBack Allow Back To the screen
     * @author Wild Coder
     */
    public void add(Fragment toFragment, boolean isAllowBack) {
        add(toFragment, null, isAllowBack);
    }

    /**
     * @param toFragment  Fragment support v4
     * @param bundle      Bundle
     * @param isAllowBack Allow Back To the screen
     * @author Wild Coder
     */
    public void add(Fragment toFragment, Bundle bundle, boolean isAllowBack) {
        if (toFragment == null)
            return;
        if (bundle != null) {
            toFragment.setArguments(bundle);
        }
        FragmentTransaction _fragment_transiction = _fragment_manager
                .beginTransaction();
        if (isAllowBack) {
            _fragment_transiction.addToBackStack(_last_fragment_name);
        }

        _fragment_transiction.add(_id_parent_frame_view, toFragment,
                toFragment.getClass().getName()).commitAllowingStateLoss();
        _last_fragment_name = toFragment.getClass().getName();
    }

    public boolean isToAdd(Fragment toFragment) {
        if (_activity == null)
            return true;
        List<Fragment> _list_fragment = _activity.getSupportFragmentManager()
                .getFragments();
        for (Fragment _fragment : _list_fragment) {
            if (_fragment == null)
                break;
            String name1 = toFragment.getClass().getName();
            String name2 = _fragment.getClass().getName();
            if (name1.equalsIgnoreCase(name2))
                return false;
        }
        return true;
    }

    public boolean isToAdd(FragmentManager fragmentManager, Fragment fragment) {
        if (fragment == null)
            return false;
        List<Fragment> _list_fragment = fragmentManager.getFragments();
        if (_list_fragment == null)
            return true;
        for (Fragment _fragment : _list_fragment) {
            if (_fragment == null)
                break;
            if (fragment.getClass().getName()
                    .equalsIgnoreCase(_fragment.getClass().getName()))
                return false;

        }
        return true;
    }

    public boolean hasNoMoreBack() {
        return _fragment_manager.getBackStackEntryCount() == 0;
    }

    public String getCurrentFragmentTag() {
        return _last_fragment_name;
    }

    public void onBackPress(String back) {
        if (back != null)
            _last_fragment_name = back;
    }
}
