package com.example.preetisharma.callforblessings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.TimeLineAdapter;
import com.example.preetisharma.callforblessings.Fragment.NotificationsFragment;
import com.example.preetisharma.callforblessings.Fragment.UserTimelineFragment;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.ChangeProfileCoverPicModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSendFriendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSubmitPostTimlineModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestAcceptedModal;
import com.example.preetisharma.callforblessings.Server.Modal.SendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.TimeLineModal;
import com.example.preetisharma.callforblessings.Server.Modal.UnfriendAFriendModal;
import com.example.preetisharma.callforblessings.Server.Modal.UploadPostModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.ChangeCoverProfilePicActivity;
import com.example.preetisharma.callforblessings.demo.ChangeProfilePicActivity;
import com.example.preetisharma.callforblessings.demo.ChatActivity;
import com.example.preetisharma.callforblessings.demo.CreateGroupActivity;
import com.example.preetisharma.callforblessings.demo.CreatePostActivity;
import com.example.preetisharma.callforblessings.demo.DirectMessageWebActivity;
import com.example.preetisharma.callforblessings.demo.GroupWallHomeScreen;
import com.example.preetisharma.callforblessings.demo.MyMessages;
import com.example.preetisharma.callforblessings.photos.PhotosTabs;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;


public class MaterialUpConceptActivity extends BaseActivity
        implements AppBarLayout.OnOffsetChangedListener, APIServerResponse, EasyPermissions.PermissionCallbacks, ImagePickerCallback {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;
    private String userId;

    private int mMaxScrollSize;

    @BindView(R.id.txtvw_user_name)
    AppCompatTextView txtvw_user_name;
    @BindView(R.id.iv_change_cover_pic)
    AppCompatImageView iv_change_cover_pic;

    @BindView(R.id.iv_add_friend)
    CircleImageView iv_add_friend;
    @BindView(R.id.iv_send_message)
    CircleImageView iv_send_message;
    @BindView(R.id.txtvw_request_pending)
    AppCompatTextView txtvw_request_pending;
    String notifyFlag;
    private String imagePreview = "";
    private String coverPreview = "";


    int is_friend;

    @BindView(R.id.profile_image)
    CircleImageView profile_image;

    @BindView(R.id.txtvw_preview_post)
    AppCompatTextView txtvw_preview_post;

    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;
    private CameraImagePicker cameraPicker;
    private String pickerPath = "";
    List<TimeLineModal.ListBean.PostBean> postList;
    private ImagePicker imagePicker;
    private String choosenPic = "";
    private boolean EnableTimeLine;
    @BindView(R.id.imgvw_preview_post)
    AppCompatImageView imgvw_preview_post;
    @BindView(R.id.lnr_layout_post_preview)
    LinearLayout lnr_layout_post_preview;
    @BindView(R.id.relative_preview)
    RelativeLayout relative_preview;
    @BindView(R.id.txtvw_new_post)
    AppCompatTextView txtvw_new_post;

    /*@BindView(R.id.iv_about)
    CircleImageView iv_about;*/

    @BindView(R.id.materialup_viewpager)
    ViewPager viewPager;

    String privacyWall = "", privacyAboutInfo = "", privacyFriendRequest = "", privacyMessage = "", emailID = "", dob = "", gender = "", state = "", country = "", friendId = "";

/*    static String */
    //iv_setting
    @BindView(R.id.iv_setting)
    CircleImageView iv_setting;


    @BindView(R.id.avtv_user_info_email_id)
    AppCompatTextView avtv_user_info_email_id;
    @BindView(R.id.avtv_user_info_dob)
    AppCompatTextView avtv_user_info_dob;
    @BindView(R.id.avtv_user_info_gender)
    AppCompatTextView avtv_user_info_gender;
    @BindView(R.id.avtv_user_info_from)
    AppCompatTextView avtv_user_info_from;
    @BindView(R.id.ll_user_info)
    LinearLayout ll_user_info;
    @BindView(R.id.atv_following)
    AppCompatTextView atv_following;
    static int i = 0;
    //
    @BindView(R.id.atv_profile_photos)
    AppCompatTextView atv_profile_photos;
    @BindView(R.id.atv_profile_mutual_friends)
    AppCompatTextView atv_profile_mutual_friends;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_timeline);
        try {
            ButterKnife.bind(this);

            //TabLayout tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
            viewPager = (ViewPager) findViewById(R.id.materialup_viewpager);
            AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);

            Toolbar toolbar = (Toolbar) findViewById(R.id.materialup_toolbar);

            if (getIntent().getExtras() != null) {
                userId = getIntent().getExtras().getString(Constants.SHARED_PREF_USER_ID);
                is_friend = getIntent().getExtras().getInt(Constants.IS_FRIEND);
                notifyFlag = getIntent().getExtras().getString("Notification_Flag");
                EnableTimeLine = getIntent().getExtras().getBoolean(Constants.TIMELINE_ENABLED);


                if (getIntent().getExtras().getString(Constants.PRIVACY_WALL) != null || !getIntent().getExtras().getString(Constants.PRIVACY_WALL).equalsIgnoreCase("")) {

                    privacyWall = getIntent().getExtras().getString(Constants.PRIVACY_WALL);
                }

                if (getIntent().getExtras().getString(Constants.PRIVACY_ABOUT_INFO) != null || !getIntent().getExtras().getString(Constants.PRIVACY_ABOUT_INFO).equalsIgnoreCase("")) {

                    privacyAboutInfo = getIntent().getExtras().getString(Constants.PRIVACY_ABOUT_INFO);

                }
                if (getIntent().getExtras().getString(Constants.PRIVACY_FRIEND_REQUEST) != null || !getIntent().getExtras().getString(Constants.PRIVACY_FRIEND_REQUEST).equalsIgnoreCase("")) {


                    privacyFriendRequest = getIntent().getExtras().getString(Constants.PRIVACY_FRIEND_REQUEST);
                }
                if (getIntent().getExtras().getString(Constants.PRIVACY_MESSAGE) != null || !getIntent().getExtras().getString(Constants.PRIVACY_MESSAGE).equalsIgnoreCase("")) {

                    privacyMessage = getIntent().getExtras().getString(Constants.PRIVACY_MESSAGE);
                }
            }

            if (EnableTimeLine) {
                profile_image.setClickable(true);
                profile_image.setEnabled(true);
                iv_change_cover_pic.setClickable(true);
            } else {
                profile_image.setClickable(false);
                profile_image.setEnabled(false);
                iv_change_cover_pic.setClickable(false);
                atv_profile_mutual_friends.setVisibility(View.VISIBLE);
            }
            if (is_friend == Constants.USER_NOT_FRIEND) {
                //iv_add_friend.setVisibility(View.VISIBLE);
                setPrivacySettingsNotFriend();
                txtvw_request_pending.setVisibility(View.GONE);

            } else if (is_friend == Constants.USER_REQUEST_ACCEPT) {
                //Friend
                setPrivacySettingsFriend();
                //iv_add_friend.setVisibility(View.GONE);
                txtvw_request_pending.setVisibility(View.VISIBLE);
                txtvw_request_pending.setText("Unfriend");
                txtvw_request_pending.setEnabled(true);
                txtvw_request_pending.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        new AlertDialog.Builder(MaterialUpConceptActivity.this)
                                .setTitle("Are you sure? ")
                                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (isConnectedToInternet()) {
                                            ServerAPI.getInstance().unfriend(APIServerResponse.UNFRIEND, getUserSessionId(), userId, MaterialUpConceptActivity.this);
                                        } else {
                                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
                                        }
                                    }
                                })
                                .setNegativeButton("Ignore", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //dismissDialog();
                                    }
                                })
                                .setIcon(R.mipmap.ic_app_icon)
                                .show();


                    }
                });
            } else if (is_friend == Constants.USER_REQUEST_SEND) {
                setPrivacySettingsRequestSentOrReceived();
                //iv_add_friend.setVisibility(View.GONE);
                txtvw_request_pending.setVisibility(View.VISIBLE);
            } else if (is_friend == Constants.USER_REQUEST_COME) {
                setPrivacySettingsRequestSentOrReceived();
                //iv_add_friend.setVisibility(View.GONE);
                txtvw_request_pending.setVisibility(View.VISIBLE);
                txtvw_request_pending.setText("Friend Request");
                txtvw_request_pending.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            new AlertDialog.Builder(MaterialUpConceptActivity.this)

                                    .setTitle("Accept friend request?")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // takePicture();
                                            if (isConnectedToInternet()) {
                                                ServerAPI.getInstance().request_accepted(APIServerResponse.REQUESTACCEPTED, getUserSessionId(), userId, "ACCEPT", MaterialUpConceptActivity.this);
                                            }

                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            //pickImageSingle();
                                            if (isConnectedToInternet()) {
                                                ServerAPI.getInstance().request_accepted(APIServerResponse.REQUESTIGNORED, getUserSessionId(), userId, "REJECT", MaterialUpConceptActivity.this);
                                            }

                                        }
                                    })
                                    .setIcon(R.mipmap.ic_app_icon)
                                    .show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                iv_add_friend.setVisibility(View.GONE);
            } else {
                iv_add_friend.setVisibility(View.VISIBLE);
                txtvw_request_pending.setVisibility(View.GONE);
                iv_send_message.setVisibility(View.VISIBLE);
            }

            //iv_send_message.setVisibility(View.GONE);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            appbarLayout.addOnOffsetChangedListener(this);
            mMaxScrollSize = appbarLayout.getTotalScrollRange();
            if (isConnectedToInternet()) {
                ServerAPI.getInstance().userTimeLine(APIServerResponse.TIMELINE, getUserSessionId(), userId, 1, this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
            viewPager.setAdapter(new TabsAdapter(getSupportFragmentManager()));
            //tabLayout.setupWithViewPager(viewPager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.iv_setting)
    public void showPopUp() {

     /*   LayoutInflater inflater = (LayoutInflater) MaterialUpConceptActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        PopupWindow pw = new PopupWindow(inflater.inflate(
                R.layout.layout_blk_unblk, null, false), 300, 400, true);

        if (i==0) {

            pw.showAtLocation(findViewById(R.id.iv_setting), Gravity.RIGHT, 0,
                    0);
            i = 1;
        } else {
            i = 0;
            pw.dismiss();
        }*/


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_blk_unblk, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        final TextView blockUnBlockTx = (TextView) dialogView.findViewById(R.id.block_unblock_tx);

        final TextView followUnFollow = (TextView) dialogView.findViewById(R.id.block_unblock_tx);

        blockUnBlockTx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blockUnBlockTx.setText("UnBlock");
                alertDialog.dismiss();
            }
        });

        followUnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followUnFollow.setText("UnFollow");
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @OnClick(R.id.iv_send_message)
    public void sendMessage() {


        if (is_friend == 1) {
            try {
                Intent chat = new Intent(MaterialUpConceptActivity.this, DirectMessageWebActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.FRIEND_ID, friendId);
                chat.putExtras(b);
                startActivity(chat);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Intent intentMessages = new Intent(MaterialUpConceptActivity.this, MyMessages.class);
            startActivity(intentMessages);
        }

    }


    @OnClick(R.id.atv_profile_photos)
    public void myPhots() {
        Intent intentPhotos = new Intent(MaterialUpConceptActivity.this, PhotosTabs.class);
        startActivity(intentPhotos);
    }

    @OnClick(R.id.atv_profile_mutual_friends)
    public void myMutualfriends() {
        Intent intentMutualFriends = new Intent(MaterialUpConceptActivity.this, MutualFriendsActivity.class);
        intentMutualFriends.putExtra("userId",userId);
        startActivity(intentMutualFriends);

    }

    @OnClick(R.id.atv_following)
    public void myFollow
            () {
        Intent intentPhotos = new Intent(MaterialUpConceptActivity.this, FollowingActivity.class);
        startActivity(intentPhotos);
    }

    /*@OnClick(R.id.iv_about)
    public void aboutMe() {

        try {

            userInfoDialog(txtvw_user_name.getText().toString().trim(), emailID, dob, gender, state, country, imagePreview);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

   /* @OnClick(R.id.txtvw_submit)
    public void submitTimeLinePost() {
        hideKeyboard();
        submitPost();
    }*/

   /* @OnClick(R.id.ic_post_image)
    public void uploadImage() {
        new AlertDialog.Builder(this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }*/

    /* public void submitPost() {
         if (edt_txt_new_post.getText().length() < 0) {
             if (pickerPath.equals("") || pickerPath.isEmpty() || pickerPath == null) {
                 showToast("Empty post cannot be created", Toast.LENGTH_SHORT);
             } else {
                 txtvw_preview_post.setVisibility(View.GONE);
                 if (isConnectedToInternet()) {
                     if (getUserID().equals(userId)) {
                         ServerAPI.getInstance().createPostWithImage(APIServerResponse.CREATE_POST, getUserSessionId(), getUserID(), "", pickerPath, "IMAGE", "WALL", this);
                     } else {
                         ServerAPI.getInstance().createPostWithImage(APIServerResponse.CREATE_POST, getUserSessionId(), userId, "", pickerPath, "IMAGE", "USERWALL", this);

                     }
                 } else {
                     hideLoading();

                 }


             }

         } else {
             if (pickerPath.equals("")) {

                *//* Pattern p = Pattern.compile(URL_REGEX);
                Matcher m = p.matcher(edt_txt_new_post.getText().toString());//replace with string to compare
                if (m.find()) {*//*
                if (isConnectedToInternet()) {
                    if (getUserID().equals(userId)) {
                        ServerAPI.getInstance().createPostWithoutImage(APIServerResponse.CREATE_POST, getUserID(), getUserSessionId(), edt_txt_new_post.getText().toString(), "WALL", this);
                    } else {
                        ServerAPI.getInstance().createPostWithoutImage(APIServerResponse.CREATE_POST, userId, getUserSessionId(), edt_txt_new_post.getText().toString(), "USERWALL", this);

                    }
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
                    *//*   }*//*

            } else {

                txtvw_preview_post.setText(edt_txt_new_post.getText().toString());
                if (getUserID().equals(userId)) {
                    ServerAPI.getInstance().createPostWithImage(APIServerResponse.CREATE_POST, getUserSessionId(), getUserID(), edt_txt_new_post.getText().toString(), pickerPath, "IMAGE", "WALL", this);
                } else {
                    ServerAPI.getInstance().createPostWithImage(APIServerResponse.CREATE_POST, getUserSessionId(), userId, edt_txt_new_post.getText().toString(), pickerPath, "IMAGE", "USERWALL", this);

                }

            }

        }


    }*/
    @OnClick(R.id.txtvw_new_post)
    public void newPost() {
        Intent newpost = new Intent(this, CreatePostActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.POSTTYPE, Constants.USERWALL);
        b.putString(Constants.FRIEND_ID, friendId);
        newpost.putExtras(b);
        startActivity(newpost);
    }

    @OnClick(R.id.iv_add_friend)
    public void sendrequest() {
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().sendRequestFriend(APIServerResponse.SEND_REQUEST, getUserSessionId(), userId, this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.profile_image)
    public void changeProfilePic() {
        try {

           /* //ServerAPI.getInstance().sendRequestFriend(APIServerResponse.SEND_REQUEST, getUserSessionId(), userId, this);
            new AlertDialog.Builder(this)

                    .setTitle("Select Image")
                    .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            choosenPic = "Profile";
                            takePicture();

                        }
                    })
                    .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            choosenPic = "Profile";
                            pickImageSingle();

                            if (isConnectedToInternet()) {
                                if (pickerPath.equals("")) {
                                    ServerAPI.getInstance().changeProfileOrCoverPic(APIServerResponse.CHANGE_PROFILE_COVER_PIC, getUserSessionId(), pickerPath, "", MaterialUpConceptActivity.this);
                                } else {
                                    showToast("Please select an image", Toast.LENGTH_LONG);

                                }
                            } else {
                                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                            }
                        }
                    })
                    .setIcon(R.mipmap.ic_app_icon)
                    .show();*/
            Intent changePic = new Intent(this, ChangeProfilePicActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.PROFILE_PREVIEW, imagePreview);
            b.putBoolean(Constants.TIMELINE_ENABLED, EnableTimeLine);
            changePic.putExtras(b);
            startActivity(changePic);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        try {
            if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)) {
                cameraPicker = new CameraImagePicker(this);
                cameraPicker.shouldGenerateMetadata(true);
                cameraPicker.shouldGenerateThumbnails(true);
                cameraPicker.setImagePickerCallback(this);
                pickerPath = cameraPicker.pickImage();
            } else {
                EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                        RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @OnClick(R.id.iv_change_cover_pic)
    public void changeCoverPic() {
        try {
            /*if (isConnectedToInternet()) {
                new AlertDialog.Builder(this)

                        .setTitle("Select Image")
                        .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                choosenPic = "Cover";

                                takePicture();

                            }
                        })
                        .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                choosenPic = "Cover";
                                pickImageSingle();

                                *//*if (isConnectedToInternet()) {
                                    if (pickerPath.equals("")) {
                                        ServerAPI.getInstance().changeProfileOrCoverPic(APIServerResponse.CHANGE_PROFILE_COVER_PIC, getUserSessionId(), "", pickerPath, MaterialUpConceptActivity.this);
                                    } else {
                                        showToast("Please select an image", Toast.LENGTH_LONG);

                                    }
                                } else {
                                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }*//*
                            }
                        })
                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
                //ServerAPI.getInstance().sendRequestFriend(APIServerResponse.SEND_REQUEST, getUserSessionId(), userId, this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }*/
            Intent changePic = new Intent(this, ChangeCoverProfilePicActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.PROFILE_PREVIEW, coverPreview);
            b.putBoolean(Constants.TIMELINE_ENABLED, EnableTimeLine);
            changePic.putExtras(b);
            startActivity(changePic);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;
            profile_image.animate().scaleY(0).scaleX(0).setDuration(200).start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            profile_image.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();


            relative_preview.setVisibility(View.VISIBLE);
            imgvw_preview_post.setVisibility(View.VISIBLE);
            lnr_layout_post_preview.setVisibility(View.VISIBLE);
            Glide.with(this).load(pickerPath).placeholder(R.drawable.placeholder_callforblessings).into(imgvw_preview_post);

           /* if (choosenPic.equals("Profile")) {
                profile_image.setImageURI(Uri.fromFile(new File(pickerPath)));
                if (isConnectedToInternet()) {
                    if (pickerPath.equals("")) {
                        ServerAPI.getInstance().changeProfileOrCoverPic(APIServerResponse.CHANGE_PROFILE_COVER_PIC, getUserSessionId(), pickerPath, "", MaterialUpConceptActivity.this);
                    } else {
                        showToast("Click an image", Toast.LENGTH_LONG);

                    }
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }

            } else {

                iv_change_cover_pic.setImageURI(Uri.fromFile(new File(pickerPath)));
                if (isConnectedToInternet()) {
                    if (pickerPath.equals("")) {
                        ServerAPI.getInstance().changeProfileOrCoverPic(APIServerResponse.CHANGE_PROFILE_COVER_PIC, getUserSessionId(), "", pickerPath, MaterialUpConceptActivity.this);
                    } else {
                        showToast("Please select an image", Toast.LENGTH_LONG);

                    }
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }*/


        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (is_friend > 4) {

                if (getUserGender().equalsIgnoreCase("male")) {

                    Glide.with(MaterialUpConceptActivity.this).load(getUserImage()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(profile_image);
                } else {

                    Glide.with(MaterialUpConceptActivity.this).load(getUserImage()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(profile_image);

                }
            }


            // String user = getUserImage();
            //Log.i("image ", user);
            if (isConnectedToInternet()) {
                ServerAPI.getInstance().userTimeLine(APIServerResponse.TIMELINE, getUserSessionId(), userId, 1, this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
           /* if (requestCode == PLACE_PICKER_REQUEST) {

                final Place place = PlacePicker.getPlace(this, data);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();


                edt_txt_location.setText(name);
                txtvw_state.setText(place.getLocale().getCountry());
                txtvw_country.setText(place.getAddress());


            }*/
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    if (isConnectedToInternet()) {
                        if (pickerPath.equals("")) {
                            ServerAPI.getInstance().changeProfileOrCoverPic(APIServerResponse.CHANGE_PROFILE_COVER_PIC, getUserSessionId(), pickerPath, "", MaterialUpConceptActivity.this);
                        } else {
                            showToast("Click an image", Toast.LENGTH_LONG);

                        }
                    } else {
                        showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);

    }

    private class TabsAdapter extends FragmentPagerAdapter {
        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0: {
                    fragment = new UserTimelineFragment();
                    Bundle b = new Bundle();

                    b.putString(Constants.SHARED_PREF_USER_ID, userId);
                    b.putInt(Constants.IS_FRIEND, is_friend);
                    fragment.setArguments(b);
                    return fragment;
                }
                case 1: {
                    fragment = new NotificationsFragment();

                    return fragment;
                }

            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "About";
                case 1:
                    return "Friends";
                case 2:
                    return "Gallery";
            }
            return "";
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {

            TimeLineModal timeLine;
            SendRequestModal sendModal;
            ChangeProfileCoverPicModal changeProfileCoverPicModal;
            FriendRequestAcceptedModal acceptedModal;
            UploadPostModal postModal;
            switch (tag) {


                case APIServerResponse.TIMELINE:
                    try {
                        timeLine = (TimeLineModal) response.body();
                        if (timeLine.getStatus().equals("1")) {

                            friendId = String.valueOf(timeLine.getList().getId());
                            txtvw_user_name.setText(timeLine.getList().getProfile_details().getFirstname() + " " + timeLine.getList().getProfile_details().getLastname());

                            if (ll_user_info.getVisibility() == View.GONE) {
                                ll_user_info.setVisibility(View.VISIBLE);
                            }
                            avtv_user_info_email_id.setText(timeLine.getList().getEmail());
                            avtv_user_info_dob.setText(timeLine.getList().getProfile_details().getD_o_b());
                            avtv_user_info_gender.setText(timeLine.getList().getProfile_details().getGender());
                            if (!timeLine.getList().getProfile_details().getState().equalsIgnoreCase("") && !timeLine.getList().getProfile_details().getCountry().equalsIgnoreCase("")) {

                                avtv_user_info_from.setVisibility(View.VISIBLE);
                                avtv_user_info_from.setText(String.format(getResources().getString(R.string.lives_in), timeLine.getList().getProfile_details().getState(), timeLine.getList().getProfile_details().getCountry()));

                            }


                            /*emailID = timeLine.getList().getEmail();
                            dob = timeLine.getList().getProfile_details().getD_o_b();
                            gender = timeLine.getList().getProfile_details().getGender();
                            state = timeLine.getList().getProfile_details().getState();
                            country = timeLine.getList().getProfile_details().getCountry();*/

                            if (is_friend > 4) {
                                imagePreview = timeLine.getList().getProfile_details().getProfile_pic();

                                setUserImage(timeLine.getList().getProfile_details().getProfile_pic());
                            } else {
                                imagePreview = timeLine.getList().getProfile_details().getProfile_pic();
                                if (timeLine.getList().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                                    Glide.with(MaterialUpConceptActivity.this).load(timeLine.getList().getProfile_details().getProfile_pic()).thumbnail(0.1f).placeholder(R.mipmap.ic_placeholder_male_avatar).into(profile_image);
                                } else {
                                    Glide.with(MaterialUpConceptActivity.this).load(timeLine.getList().getProfile_details().getProfile_pic()).thumbnail(0.1f).placeholder(R.mipmap.ic_placeholder_female_avatar).into(profile_image);
                                }

                            }


                            coverPreview = timeLine.getList().getProfile_details().getCover_pic();
                            Glide.with(MaterialUpConceptActivity.this).load(timeLine.getList().getProfile_details().getCover_pic()).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(iv_change_cover_pic);
                            postList = timeLine.getList().getPost();
                            TimeLineAdapter adapter = new TimeLineAdapter(this, postList);
                            adapter.notifyChange(postList);
                        } else {
                            showToast("No data found", Toast.LENGTH_SHORT);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case APIServerResponse.SEND_REQUEST:
                    sendModal = (SendRequestModal) response.body();
                    EventBus.getDefault().post(new EventBusSendFriendRequestModal(sendModal.getStatus()));
                    if (sendModal.getStatus().equals("1")) {
                        showToast("Request Sent successfully", Toast.LENGTH_SHORT);
                        iv_add_friend.setVisibility(View.INVISIBLE);
                        txtvw_request_pending.setVisibility(View.VISIBLE);
                    } else {
                        showToast(response.message(), Toast.LENGTH_SHORT);
                    }

                    break;
                case APIServerResponse.CREATE_POST:

                    postModal = (UploadPostModal) response.body();
                    if (postModal.getStatus().equals("1")) {
                        EventBus.getDefault().post(new EventBusSubmitPostTimlineModal(postModal.getStatus()));
                        txtvw_preview_post.setVisibility(View.GONE);
                        lnr_layout_post_preview.setVisibility(View.GONE);
                        showToast("Post created successfully", Toast.LENGTH_SHORT);
                        pickerPath = "";
                        if (isConnectedToInternet()) {
                            ServerAPI.getInstance().userTimeLine(APIServerResponse.TIMELINE, getUserSessionId(), userId, 1, this);
                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    } else {
                        showToast(postModal.getMessage(), Toast.LENGTH_SHORT);
                    }
                    break;

                case APIServerResponse.CHANGE_PROFILE_COVER_PIC:

                    changeProfileCoverPicModal = (ChangeProfileCoverPicModal) response.body();
                    if (changeProfileCoverPicModal.getStatus().equals("1")) {
                        showToast(changeProfileCoverPicModal.getMessage(), Toast.LENGTH_SHORT);
                    } else {
                        showToast(changeProfileCoverPicModal.getMessage(), Toast.LENGTH_SHORT);
                    }
                    break;
                case APIServerResponse.UNFRIEND:
                    UnfriendAFriendModal unfriendAFriendModal;
                    if (response.isSuccessful()) {
                        unfriendAFriendModal = (UnfriendAFriendModal) response.body();
                        if (unfriendAFriendModal.getStatus().equals("1")) {
                            showToast(unfriendAFriendModal.getMessage(), Toast.LENGTH_LONG);
                            txtvw_request_pending.setVisibility(View.GONE);
                            iv_add_friend.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
                case APIServerResponse.REQUESTACCEPTED:
                    acceptedModal = (FriendRequestAcceptedModal) response.body();
                    EventBus.getDefault().post(new DemoFriendsModal(acceptedModal.getStatus()));
                    if (acceptedModal.getStatus().equals("1")) {
                        showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                        iv_add_friend.setVisibility(View.GONE);
                        txtvw_request_pending.setVisibility(View.VISIBLE);
                        txtvw_request_pending.setText("Friends");
                    } else {
                        showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                    }
                    break;

                case APIServerResponse.REQUESTIGNORED:
                    acceptedModal = (FriendRequestAcceptedModal) response.body();
                    if (acceptedModal.getStatus().equals("1")) {
                        showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);
                        iv_add_friend.setVisibility(View.VISIBLE);
                        txtvw_request_pending.setVisibility(View.GONE);
                    } else {
                        showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                    }
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }


    public void comingSoonDialog(String title) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage("Coming Soon")

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    @Override
    public void onBackPressed() {

        EventBus.getDefault().post(new EventBusSendFriendRequestModal("1"));
        if (notifyFlag != null && notifyFlag.equals("true")) {
            Intent i = new Intent(MaterialUpConceptActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        } else {
            super.onBackPressed();
        }

    }


    private void setPrivacySettingsFriend() {

        try {
            if (privacyWall.equalsIgnoreCase(Constants.PUBLIC) || privacyWall.equalsIgnoreCase(Constants.FRIEND)) {

                if (viewPager.getVisibility() == View.GONE) {
                    viewPager.setVisibility(View.VISIBLE);
                }
            } else {
                if (viewPager.getVisibility() == View.VISIBLE) {
                    viewPager.setVisibility(View.GONE);
                }
            }

           /* if (privacyAboutInfo.equalsIgnoreCase(Constants.PUBLIC) || privacyAboutInfo.equalsIgnoreCase(Constants.FRIEND)) {

                if (iv_about.getVisibility() == View.GONE) {
                    iv_about.setVisibility(View.VISIBLE);
                }
            } else {
                if (iv_about.getVisibility() == View.VISIBLE) {
                    iv_about.setVisibility(View.GONE);
                }
            }*/

            if (privacyMessage.equalsIgnoreCase(Constants.PUBLIC) || privacyMessage.equalsIgnoreCase(Constants.FRIEND)) {

                if (iv_send_message.getVisibility() == View.GONE) {
                    iv_send_message.setVisibility(View.VISIBLE);
                }
            } else {
                if (iv_send_message.getVisibility() == View.VISIBLE) {
                    iv_send_message.setVisibility(View.GONE);
                }
            }

            if (privacyFriendRequest.equalsIgnoreCase(Constants.PUBLIC) || privacyFriendRequest.equalsIgnoreCase(Constants.FRIEND)) {

                if (iv_add_friend.getVisibility() == View.VISIBLE) {
                    iv_add_friend.setVisibility(View.GONE);
                }
            } else {
                if (iv_add_friend.getVisibility() == View.GONE) {
                    iv_add_friend.setVisibility(View.VISIBLE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setPrivacySettingsNotFriend() {
        try {
            if (privacyWall.equalsIgnoreCase(Constants.PUBLIC)) {

                if (viewPager.getVisibility() == View.GONE) {
                    viewPager.setVisibility(View.VISIBLE);
                }
            } else {
                if (viewPager.getVisibility() == View.VISIBLE) {
                    viewPager.setVisibility(View.GONE);
                }
            }

           /* if (privacyAboutInfo.equalsIgnoreCase(Constants.PUBLIC)) {

                if (iv_about.getVisibility() == View.GONE) {
                    iv_about.setVisibility(View.VISIBLE);
                }
            } else {
                if (iv_about.getVisibility() == View.VISIBLE) {
                    iv_about.setVisibility(View.GONE);
                }
            }*/

            if (privacyMessage.equalsIgnoreCase(Constants.PUBLIC)) {

                if (iv_send_message.getVisibility() == View.GONE) {
                    iv_send_message.setVisibility(View.VISIBLE);
                }
            } else {
                if (iv_send_message.getVisibility() == View.VISIBLE) {
                    iv_send_message.setVisibility(View.GONE);
                }
            }

            if (privacyFriendRequest.equalsIgnoreCase(Constants.PUBLIC)) {

                if (iv_add_friend.getVisibility() == View.GONE) {
                    iv_add_friend.setVisibility(View.VISIBLE);
                }
            } else {
                if (iv_add_friend.getVisibility() == View.VISIBLE) {
                    iv_add_friend.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setPrivacySettingsRequestSentOrReceived() {
        try {
            if (privacyWall.equalsIgnoreCase(Constants.PUBLIC)) {

                if (viewPager.getVisibility() == View.GONE) {
                    viewPager.setVisibility(View.VISIBLE);
                }
            } else {
                if (viewPager.getVisibility() == View.VISIBLE) {
                    viewPager.setVisibility(View.GONE);
                }
            }

           /* if (privacyAboutInfo.equalsIgnoreCase(Constants.PUBLIC)) {

                if (iv_about.getVisibility() == View.GONE) {
                    iv_about.setVisibility(View.VISIBLE);
                }
            } else {
                if (iv_about.getVisibility() == View.VISIBLE) {
                    iv_about.setVisibility(View.GONE);
                }
            }*/

            if (privacyMessage.equalsIgnoreCase(Constants.PUBLIC)) {

                if (iv_send_message.getVisibility() == View.GONE) {
                    iv_send_message.setVisibility(View.VISIBLE);
                }
            } else {
                if (iv_send_message.getVisibility() == View.VISIBLE) {
                    iv_send_message.setVisibility(View.GONE);
                }
            }

            if (iv_add_friend.getVisibility() == View.VISIBLE) {
                iv_add_friend.setVisibility(View.GONE);
            }

           /* if (privacyFriendRequest.equalsIgnoreCase(Constants.PUBLIC)) {

                if (iv_add_friend.getVisibility() == View.GONE) {
                    iv_add_friend.setVisibility(View.VISIBLE);
                }
            } else {
                if (iv_add_friend.getVisibility() == View.VISIBLE) {
                    iv_add_friend.setVisibility(View.GONE);
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*private void userInfoDialog(final String name, final String email, String dob, String gender, String state, String country, String profileImage) {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setTitle(String.format(res.getString(R.string.about_brand), brandName));
        dialog.setContentView(R.layout.dialog_user_about_info_layout);


        //LinearLayout ll_brand_info = (LinearLayout) dialog.findViewById(R.id.ll_brand_info);

        final ProgressBar progress = (ProgressBar) dialog.findViewById(R.id.progress);
        ImageView iv_user_image = (ImageView) dialog.findViewById(R.id.iv_user_image);

        TextInputEditText tet_name = (TextInputEditText) dialog.findViewById(R.id.tet_name);

        TextInputEditText tet_email = (TextInputEditText) dialog.findViewById(R.id.tet_email);

        TextInputEditText tet_dob = (TextInputEditText) dialog.findViewById(R.id.tet_dob);

        TextInputEditText tet_gender = (TextInputEditText) dialog.findViewById(R.id.tet_gender);

        TextInputEditText tet_state = (TextInputEditText) dialog.findViewById(R.id.tet_state);

        TextInputEditText tet_country = (TextInputEditText) dialog.findViewById(R.id.tet_country);

        Glide.with(MaterialUpConceptActivity.this).load(profileImage).asBitmap()
                .placeholder(R.drawable.placeholder_callforblessings)
                .into(new BitmapImageViewTarget(iv_user_image) {
                    @Override
                    public void onResourceReady(Bitmap drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        progress.setVisibility(View.GONE);
                    }
                });

        tet_name.setText(name);
        tet_email.setText(email);
        tet_dob.setText(dob);
        tet_gender.setText(gender);
        tet_state.setText(state);
        tet_country.setText(country);


        dialog.show();
    }*/
}