package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.EventInviteSelectedFriendsAdapter;
import com.example.preetisharma.callforblessings.Adapter.EventsInvitedFriendsAdapter;
import com.example.preetisharma.callforblessings.Adapter.InviteFriendsSearchListAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventInviteSelectedFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventsInvitesModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyEventsCreatedModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.SendEventInvitationModal;
import com.example.preetisharma.callforblessings.Server.Modal.UserFilterModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/9/2017.
 */

public class EventInviteFriendsActivity extends BaseActivity implements APIServerResponse {

    EventsInvitedFriendsAdapter adapter;
    //List<EventsInvitesModal.ListBean> list = new ArrayList<>();
    RecyclerView rv_invite_friends_recycler_view;
    RecyclerView rv_friends_selected_to_invite;

    @BindView(R.id.search_text)
    MultiAutoCompleteTextView search_text;
    @BindView(R.id.iv_close_icon)
    AppCompatImageView iv_close_icon;

    private List<MyFriendsModal.ListBean> mList;
    List<EventInviteSelectedFriendsModal> seletedFriendsList = new ArrayList<>();
    //private List<EventInviteSelectedFriendsModal> seletedFriendsList;
    private EventInviteSelectedFriendsAdapter eventInviteSelectedFriendsAdapter;

    FloatingActionButton fab_send_invite;
    HashMap<String, String> hash = new HashMap<String, String>();

    //String[] Users;
    String eventID;
    //String invitedFriends;
    LinearLayout ll_invites;
    InviteFriendsSearchListAdapter usersAdapter;
    ArrayList<UserFilterModal> Users = new ArrayList<>();
    int position;
    //public List<MyEventsCreatedModal.ListBean> list = new ArrayList<MyEventsCreatedModal.ListBean>();
    private List<MyEventsCreatedModal.ListBean.InvitesBean> usersInvitedList = new ArrayList<>();
    //public MyEventsCreatedAdapter myEventsCreatedAdapter;
    ArrayList<String> tempSelectedList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_invite_friends);
        ButterKnife.bind(this);


        rv_invite_friends_recycler_view = (RecyclerView) findViewById(R.id.rv_invite_friends_recycler_view);
        rv_friends_selected_to_invite = (RecyclerView) findViewById(R.id.rv_friends_selected_to_invite);
        fab_send_invite = (FloatingActionButton) findViewById(R.id.fab_send_invite);
        //iv_close_icon = (AppCompatImageView) findViewById(R.id.iv_close_icon);

        ll_invites = (LinearLayout) findViewById(R.id.ll_invites);

        if (getIntent().getExtras() != null) {
            eventID = getIntent().getExtras().getString(Constants.EVENT_ID);
            position = getIntent().getExtras().getInt(Constants.POSITION);
        }

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getMyCreatedEvents(APIServerResponse.MY_CREATED_EVENTS, getUserSessionId(), this);
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, getUserSessionId(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        iv_close_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_text.setText("");
            }
        });
        fab_send_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ll_invites.getVisibility() == View.VISIBLE) {
                    try {

                        String invitedFriends = "";
                        // String demo = null;

                        for (int i = 0; i < seletedFriendsList.size(); i++) {

                            // demo = android.text.TextUtils.join(",", seletedFriendsList);

                            if (i == 0) {
                                invitedFriends = String.valueOf(seletedFriendsList.get(i).getId());
                            } else {
                                invitedFriends = invitedFriends + "," + seletedFriendsList.get(i).getId();
                            }
                        }


                        if (isConnectedToInternet()) {
                            showLoading();
                            hideKeyboard();
                            ServerAPI.getInstance().sendEventInvite(APIServerResponse.SEND_INVITES, getUserSessionId(), eventID, invitedFriends, EventInviteFriendsActivity.this);
                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });

    }

   /* @Override
    protected void onResume() {
        super.onResume();
        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().getEventInvites(APIServerResponse.EVENT_INVITES, getUserSessionId(), this);
            } else {
                showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onSuccess(int tag, Response response) {

        try {


            if (response.isSuccessful()) {
                MyEventsCreatedModal myEventsCreatedModal;
                EventsInvitesModal eventsInvitesModal;
                final MyFriendsModal userListingModal;
                SendEventInvitationModal sendEventInvitationModal;


                switch (tag) {
                    case APIServerResponse.MY_CREATED_EVENTS:
                        myEventsCreatedModal = (MyEventsCreatedModal) response.body();
                        if (myEventsCreatedModal.getStatus().equals("1")) {
                            //list = myEventsCreatedModal.getList();
                            usersInvitedList = myEventsCreatedModal.getList().get(position).getInvites();
                            Log.i("inivitedlist", "" + usersInvitedList.get(0).getUser_to().getId() + " " + usersInvitedList.get(0).getUser_to().getProfile_details().getFirstname());

                        } else {
                            showToast("No Events Created", Toast.LENGTH_LONG);
                        }

                        adapter = new EventsInvitedFriendsAdapter(EventInviteFriendsActivity.this, usersInvitedList);

                        rv_invite_friends_recycler_view.setAdapter(adapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(EventInviteFriendsActivity.this);
                        rv_invite_friends_recycler_view.setLayoutManager(mLayoutManager);
                        hideLoading();
                        break;
                    /*case APIServerResponse.EVENT_INVITES:
                        eventsInvitesModal = (EventsInvitesModal) response.body();

                        if (eventsInvitesModal.getStatus().equals("1")) {
                            list = eventsInvitesModal.getList();
                            adapter = new EventsInvitedFriendsAdapter(EventInviteFriendsActivity.this, list);

                        }

                        rv_invite_friends_recycler_view.setAdapter(adapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(EventInviteFriendsActivity.this);
                        rv_invite_friends_recycler_view.setLayoutManager(mLayoutManager);

                        break;*/
                    case APIServerResponse.MYFRIENDLIST:
                        userListingModal = (MyFriendsModal) response.body();


                        //final MyFriendsModal.ListBean.UserInfoBean userInfoBean = new MyFriendsModal.ListBean.UserInfoBean();
                        if (userListingModal.getStatus().equals("1")) {
                            mList = userListingModal.getList();
                            for (int i = 0; i < mList.size(); i++) {
                                UserFilterModal user = new UserFilterModal();
                                user.setId(String.valueOf(mList.get(i).getId()));
                                user.setUserName(mList.get(i).getUser_info().getProfile_details().getFirstname());
                                Users.add(user);
                            }


                            Log.e("List data", "List data size array" + Users.size());
                            usersAdapter = new InviteFriendsSearchListAdapter(EventInviteFriendsActivity.this, mList);
                  /*  ArrayAdapter<UserFilterModal> adapter = new ArrayAdapter<UserFilterModal>
                            (getActivity(), android.R.layout.simple_list_item_1, Users);*/
                            search_text.setAdapter(usersAdapter);
                            search_text.setThreshold(1);
                            search_text.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());


                            search_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                                    try {


                                        //eventInviteSelectedFriendsModal.setProfile_pic(mList.get(i).getUser_info().getProfile_details().getProfile_pic());
                                        String selected = usersAdapter.getItemName(i);
                                        if (!tempSelectedList.isEmpty() && tempSelectedList.contains(selected)) {
                                            showToast("Already added", Toast.LENGTH_SHORT);
                                            search_text.setText("");
                                                /*EventInviteSelectedFriendsModal eventInviteSelectedFriendsModal = new EventInviteSelectedFriendsModal();
                                                eventInviteSelectedFriendsModal.setFriend_name(String.valueOf(adapterView.getAdapter().getItem(i)));
                                                eventInviteSelectedFriendsModal.setId((int) adapterView.getAdapter().getItemId(i));
                                                seletedFriendsList.add(eventInviteSelectedFriendsModal);
                                                eventInviteSelectedFriendsAdapter.notifyData(seletedFriendsList);*/
                                            //showToast("Already Added", Toast.LENGTH_LONG);
                                            //eventInviteSelectedFriendsAdapter.notifyItemInserted(mList.size() - 1);

                                        } else {

                                            // showToast(String.valueOf((int) adapterView.getAdapter().getItemId(i)), Toast.LENGTH_LONG);
                                            EventInviteSelectedFriendsModal eventInviteSelectedFriendsModal = new EventInviteSelectedFriendsModal();
                                            eventInviteSelectedFriendsModal.setFriend_name(String.valueOf(adapterView.getAdapter().getItem(i)));
                                            eventInviteSelectedFriendsModal.setId((int) adapterView.getAdapter().getItemId(i));
                                            seletedFriendsList.add(eventInviteSelectedFriendsModal);
                                            tempSelectedList.add(selected);
                                            search_text.setText("");
                                            eventInviteSelectedFriendsAdapter.notifyData(seletedFriendsList);
                                            if (seletedFriendsList.size() != 0) {
                                                ll_invites.setVisibility(View.VISIBLE);
                                            } else {
                                                ll_invites.setVisibility(View.GONE);
                                            }
                                        }


                                        //eventInviteSelectedFriendsAdapter = new EventInviteSelectedFriendsAdapter(EventInviteFriendsActivity.this, seletedFriendsList, ll_invites);

                                        //Log.e("Clicked here", "Clicked here" + mList.get(i).getId());
                                        //Intent timeLine = new Intent(EventInviteFriendsActivity.this, MaterialUpConceptActivity.class);
                                        // Bundle b = new Bundle();
                                        //b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(mList.get(i).getId()));
                                        //b.putBoolean(Constants.IS_FRIEND, mList.get(i).isIs_friend());
                                        //timeLine.putExtras(b);
                                        //startActivity(timeLine);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                }
                            });
                            if (seletedFriendsList.size() != 0) {
                                ll_invites.setVisibility(View.VISIBLE);
                            } else {
                                ll_invites.setVisibility(View.GONE);
                            }
                            eventInviteSelectedFriendsAdapter = new EventInviteSelectedFriendsAdapter(EventInviteFriendsActivity.this, seletedFriendsList, ll_invites);
                            rv_friends_selected_to_invite.setAdapter(eventInviteSelectedFriendsAdapter);
                            rv_friends_selected_to_invite.setLayoutManager(new LinearLayoutManager(EventInviteFriendsActivity.this, LinearLayoutManager.HORIZONTAL, false));

                        } else {
                            showToast(response.message(), Toast.LENGTH_SHORT);
                        }
                        hideLoading();
                        break;

                    case APIServerResponse.SEND_INVITES:
                        sendEventInvitationModal = (SendEventInvitationModal) response.body();

                        if (sendEventInvitationModal.getStatus().equals("1")) {
                            showToast(sendEventInvitationModal.getMessage(), Toast.LENGTH_LONG);
                            ll_invites.setVisibility(View.GONE);
                            showLoading();
                            ServerAPI.getInstance().getMyCreatedEvents(APIServerResponse.MY_CREATED_EVENTS, getUserSessionId(), this);
                        } else {
                            showToast("Unable to send invites.", Toast.LENGTH_LONG);
                        }
                        hideLoading();
                        break;
                }

            } else {
                hideLoading();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }
}
