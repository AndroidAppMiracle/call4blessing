package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.CommentsListModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 1/27/2017.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.DataViewHolder> {
    public List<CommentsListModal.DetailBean.CommentsBean> list;
    Context mContext;


    public CommentsAdapter(Context mContext, List<CommentsListModal.DetailBean.CommentsBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<CommentsListModal.DetailBean.CommentsBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public CommentsAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_comments, parent, false);
        CommentsAdapter.DataViewHolder dataView = new CommentsAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final CommentsAdapter.DataViewHolder holder, final int position) {
        holder.txtvw_user_name.setText(list.get(position).getPost_by().getUsername());
        holder.txtvw_comment.setText(list.get(position).getComment());
        if (list.get(holder.getAdapterPosition()).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {

            Glide.with(mContext).load(list.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).centerCrop().into(holder.user_profile_pic);
        } else if (list.get(holder.getAdapterPosition()).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
            Glide.with(mContext).load(list.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).centerCrop().into(holder.user_profile_pic);
        }

        holder.txtvw_time_stamp_post.setText(list.get(position).getCreated_at());
    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.user_profile_pic)
        AppCompatImageView user_profile_pic;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_time_stamp_post)
        AppCompatTextView txtvw_time_stamp_post;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }
    }
}
