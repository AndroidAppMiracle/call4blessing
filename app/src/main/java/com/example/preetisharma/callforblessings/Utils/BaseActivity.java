package com.example.preetisharma.callforblessings.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.callforblessings.R;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BaseActivity extends AppCompatActivity {

    private SharedPreferences mPrefs, tokenPrefs;
    private Snackbar snackbar;
    protected Fragment fragment;
    private AlertDialog loadingDialog;


    public SharedPreferences getmPrefs() {
        return mPrefs;
    }

    public SharedPreferences gettokenPrefs() {
        return tokenPrefs;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = getSharedPreferences(Constants.SHARED_PREF_NAME, MODE_PRIVATE);
        tokenPrefs = getSharedPreferences(Constants.DEVICE_ID, MODE_PRIVATE);
       /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
    }


    /**
     * function for fetching user ID
     *
     * @return user ID
     */
    public String getUserFcmToken() {
        return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
    }

    /**
     * returns firebase Token
     */

    public String getFirebaseToken() {

        if (gettokenPrefs().getString(Constants.FCM_TOKEN, "").equalsIgnoreCase("")) {
            getmPrefs().edit().putString(Constants.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken()).apply();

            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        } else {
            return gettokenPrefs().getString(Constants.FCM_TOKEN, "");
        }

    }

    /**
     * function for clearing shared SharedPreferences
     */
    public void clearPreferences() {
        mPrefs.edit().clear().apply();
        mPrefs.edit().clear().commit();


    }

    /**
     * function for fetching user ID
     *
     * @return user ID
     */
    public String getUserID() {
        return getmPrefs().getString(Constants.SHARED_PREF_USER_ID, "");
    }

    /**
     * Function to set user ID
     *
     * @param id user ID fetched from Server
     */
    public void setUserSessionId(String id) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_USER_SESSION_ID, id).apply();
    }

    /**
     * function for fetching user ID
     *
     * @return user ID
     */
    public String getUserSessionId() {
        return getmPrefs().getString(Constants.SHARED_PREF_USER_SESSION_ID, "");
    }

    /**
     * Function to set user ID
     *
     * @param id user ID fetched from Server
     */
    public void setUserID(String id) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_USER_ID, id).apply();
    }

    public void setFirstName(String firstName) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_FIRST_NAME, firstName).apply();
    }

    public void setLastName(String lastName) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_LAST_NAME, lastName).apply();
    }


    public void setUserGender(String gender) {
        getmPrefs().edit().putString(Constants.GENDER, gender).apply();
    }

    public String getUserGender() {
        return getmPrefs().getString(Constants.GENDER, "");
    }

    public void setUserLoggedIn(Boolean logged) {
        getmPrefs().edit().putBoolean(Constants.SHARED_PREF_NAME, true).apply();
    }

    public Boolean getUserLoggedIn() {
        return getmPrefs().getBoolean(Constants.SHARED_PREF_NAME, false);
    }

    public String getFirstName() {
        return getmPrefs().getString(Constants.SHARED_PREF_FIRST_NAME, "");
    }

    public String getLastName() {
        return getmPrefs().getString(Constants.SHARED_PREF_LAST_NAME, "");
    }

    public String getFullName() {
        return getmPrefs().getString(Constants.SHARED_PREF_FULL_NAME, "");
    }

    public void setFullName(String fullname) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_FULL_NAME, fullname).apply();
    }

    /**
     * For Fetching User Type
     *
     * @return USer Type of user
     */
    public String getUserType() {
        return getmPrefs().getString(Constants.SHARED_PREF_USER_TYPE, "");
    }

    /**
     * @param type ->0 Buyer
     *             ->1 Seller
     */
    public void setUserType(String type) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_USER_TYPE, type).apply();
    }

    public String getUserPhoneNumber() {
        return getmPrefs().getString(Constants.SHARED_PREF_USER_PHONE_NUMBER, "");
    }

    /**
     * @param type type is path of user phonenumber
     */
    public void setUserPhoneNumber(String type) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_USER_PHONE_NUMBER, type).apply();
    }


    /**
     * @param emaiID type is path of user phonenumber
     */
    public void setUserEmailID(String emaiID) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_USER_EMAIL_ID, emaiID).apply();
    }

    public String getUserEmailID() {
        return getmPrefs().getString(Constants.SHARED_PREF_USER_EMAIL_ID, "");
    }

    /**
     * For Fetching User Image
     *
     * @return Image of user
     */
    public String getUserImage() {
        return getmPrefs().getString(Constants.SHARED_PREF_USER_IMAGE, "");
    }

    public String getUserCoverImage() {
        return getmPrefs().getString(Constants.SHARED_PREF_USER_COVER_IMAGE, "");
    }

    /**
     * @param type type is path of user image
     */
    public void setUserImage(String type) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_USER_IMAGE, type).apply();
    }

    public void setUserCoverImage(String type) {
        getmPrefs().edit().putString(Constants.SHARED_PREF_USER_COVER_IMAGE, type).apply();
    }

    /**
     * Function to display Toast
     *
     * @param message  Message of the Toast
     * @param duration Duration of the Toast 0 for Short and 1 for Long Toast
     */
    public void showToast(String message, int duration) {
        if (message != null && message.length() > 0) {
            if (duration == 0) {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }


    /**
     * For Displaying Output in LogCat
     *
     * @param message Message to display
     */
    public void showOutput(String message) {
        System.out.println("------------------------");
        System.out.println(message);
        System.out.println("------------------------");
    }


    /**
     * For Displaying the Log Output in Logcat
     *
     * @param tag     Tag of the Log
     * @param message Message of the Log
     */
    public void showLog(String tag, String message) {
        System.out.println("--------------------");
        Log.d(tag, message);
        System.out.println("--------------------");
    }


    /**
     * function to check if internet connection is active or not.
     *
     * @return true if connected to internet else false
     */
    public boolean isConnectedToInternet() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null;
    }

    /**
     * Clearing the Fragment backStack
     */
    public void clearBackStack() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }


    /**
     * For Hiding the keyboard
     */
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    /**
     * For Displaying the snackBar
     *
     * @param msg message to display in the snackBar.
     */
    public void showSnack(String msg) {
        try {
            snackbar = Snackbar.make(getCurrentFocus(), msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            showToast(msg, 1);
            e.printStackTrace();
        }
    }


    /**
     * For Dismissing the snackBar
     */
    public void dismissSnack() {
        snackbar.dismiss();
    }

    /**
     * For Displaying the message at snackBar
     *
     * @param msg      message to display in the snackBar.
     * @param listener setting the action when button is clicked
     */
    public void showMsgSnack(String msg, View.OnClickListener listener) {
        if (!isConnectedToInternet())
            msg = "No Internet Connection";

        try {
            snackbar = Snackbar.make(getCurrentFocus(), msg, Snackbar.LENGTH_INDEFINITE).setAction("Retry", listener);
            snackbar.show();
        } catch (Exception e) {
            showToast(msg, 1);
            e.printStackTrace();
        }
    }

    /**
     * Replacing fragment
     *
     * @param fragment       Fragment to be replaced
     * @param addToBackStack true will add to back stack else not
     */

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        this.fragment = fragment;
        this.invalidateOptionsMenu();

        getSupportFragmentManager().beginTransaction().replace(R.id.pager, fragment).commit();


    }

    public void replaceHTPFragment(Fragment fragment, boolean addToBackStack) {
        this.fragment = fragment;
        this.invalidateOptionsMenu();

        getSupportFragmentManager().beginTransaction().replace(R.id.pager_htp, fragment).commit();


    }


    public void replaceFragment(FragmentManager manager, Fragment fragment, boolean addtobackstack) {
        this.fragment = fragment;
        this.invalidateOptionsMenu();

        manager.beginTransaction().replace(R.id.pager, fragment).commit();

    }
   /* public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        if (addToBackStack) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0 && getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName().equalsIgnoreCase(fragment.getClass().getName())) {
                System.out.println("Clicked Same >>>  ");
            } else {
                this.fragment = fragment;
                getSupportFragmentManager().beginTransaction().replace(R.id.pager, fragment).addToBackStack(fragment.getClass().getName()).apply();
            }
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0 && getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName().equalsIgnoreCase(fragment.getClass().getName())) {
                System.out.println("Clicked Same >>>  ");
            } else {
                this.fragment = fragment;
                getSupportFragmentManager().beginTransaction().replace(R.id.pager, fragment).apply();
            }
        }
        this.invalidateOptionsMenu();
    }
*/

    /**
     * Saving User Location
     *
     * @param address   Address of user
     * @param latitude  latitude of User
     * @param longitude Longitude of User
     */
    public void setUserLocation(String address, String latitude, String longitude) {
        getmPrefs().edit()
                .putString(Constants.SHARED_PREF_USER_ADDRESS, address)
                .putString(Constants.SHARED_PREF_USER_LAT, latitude)
                .putString(Constants.SHARED_PREF_USER_LONG, longitude)
                .apply();
    }


    /**
     * Fetching the user saved address
     *
     * @return Address of the user
     */
    public String getUserAddress() {
        return getmPrefs().getString(Constants.SHARED_PREF_USER_ADDRESS, "");
    }


    /**
     * Cntains user location
     *
     * @return true if available else false
     */
    public boolean containUserLocation() {
        if (getmPrefs().contains(Constants.SHARED_PREF_USER_ADDRESS) && getmPrefs().contains(Constants.SHARED_PREF_USER_LAT) && getmPrefs().contains(Constants.SHARED_PREF_USER_LONG)) {
            return true;
        }
        return false;
    }

    /**
     * For Displaying the loading when a service is hit
     */
    public void showLoading() {
        try {
            if (loadingDialog == null) {
                LayoutInflater factory = LayoutInflater.from(this);
                final View deleteDialogView = factory.inflate(R.layout.layout_dialog, null);
                loadingDialog = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).create();
                loadingDialog.setView(deleteDialogView);
                loadingDialog.setCanceledOnTouchOutside(false);
                loadingDialog.setCancelable(false);
            }
            loadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissDialog() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
                loadingDialog = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,3})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * For hiding the loading
     */
    public void hideLoading() {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorStatusBar));
        }
    }


    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }


}
