package com.example.preetisharma.callforblessings.photos;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.SlidingImage;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/3/2017.
 */

public class PhotosSlideView extends BaseActivity {


    @BindView(R.id.pager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;

    @BindView(R.id.image_progress)
    ProgressBar image_progress;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_slide_view);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {

            currentPage = getIntent().getExtras().getInt(Constants.CURRENT_POSITION);

            Bundle b = getIntent().getBundleExtra(Constants.IMAGE_BUNDLE);

            ArrayList<DemoDataModal> list = new ArrayList<>();
            list = b.getParcelableArrayList(Constants.IMAGE_SLIDESHOW);

            init(list);
        }


    }

    private void init(ArrayList<DemoDataModal> arrayList) {
//        for(int i=0;i<IMAGES.length;i++)
//         ImagesArray.add(IMAGES[i]);

        Log.e("Images link", "Images link" + arrayList.size() + "image" + arrayList.get(0));
        mPager.setAdapter(new SlidingImagesCustom(this, arrayList, image_progress));


        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = arrayList.size();

        // Auto start of viewpager
        //final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };


        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }
}
