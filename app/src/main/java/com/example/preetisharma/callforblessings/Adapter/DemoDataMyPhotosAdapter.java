package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 8/3/2017.
 */

public class DemoDataMyPhotosAdapter extends RecyclerView.Adapter<DemoDataMyPhotosAdapter.DataViewHolder> {


    private Context mContext;
    private List<DemoDataModal> myGroupsList = new ArrayList<>();
    private SparseBooleanArray selectedItems;
    private int x, y;

    public DemoDataMyPhotosAdapter(Context mContext, List<DemoDataModal> list) {
        this.myGroupsList = list;
        this.mContext = mContext;
        selectedItems = new SparseBooleanArray();

        x = (int) mContext.getResources().getDimension(R.dimen.height_video_item);
        y = (int) mContext.getResources().getDimension(R.dimen.height_video_item);
    }

    @Override
    public DemoDataMyPhotosAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_photos_mine, parent, false);
        DemoDataMyPhotosAdapter.DataViewHolder dataView = new DemoDataMyPhotosAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DemoDataMyPhotosAdapter.DataViewHolder holder, final int position) {

        try {


            Glide.with(mContext)
                    .load(myGroupsList.get(position).getImage())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.mProgress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            // image ready, hide progress now
                            holder.mProgress.setVisibility(View.GONE);
                            return false;   // return false if you want Glide to handle everything else.
                        }
                    })
                    /*.diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image*/
                    .centerCrop()
                    .override(x, y)
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(holder.atv_my_photo_Cover);

            holder.itemView.setActivated(selectedItems.get(position, false));


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (myGroupsList.size() != 0) {
            return myGroupsList.size();
        } else {
            return 0;
        }

    }

    public DemoDataModal getItem(int position) {
        return myGroupsList.get(position);
    }

    public void toggleSelection(int pos) {
        /*currentSelectedIndex = pos;*/
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            /*animationItemsIndex.delete(pos);*/
        } else {
            selectedItems.put(pos, true);
            /*animationItemsIndex.put(pos, true);*/
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        /*reverseAllAnimations = true;*/
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        myGroupsList.remove(position);
        //resetCurrentIndex();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.atv_my_photo_Cover)
        AppCompatImageView atv_my_photo_Cover;

        @BindView(R.id.my_photo_progress)
        ProgressBar mProgress;

        @BindView(R.id.cv_my_photo_item)
        CardView cv_my_photo_item;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/


            /*ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, PlaylistSongsList.class);
                    *//*Bundle b = new Bundle();
                    b.putString(Constants.GROUP_ID, String.valueOf(myGroupsList.get(getAdapterPosition()).getId()));
                    i.putExtras(b);*//*
                    mContext.startActivity(i);

                   *//* if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myGroupsList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*//*
                }
            });*/

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

    }


}