package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 2/24/2017.
 */

public class EventBusSubmitPostTimlineModal {

    private final String comment_count;

    public EventBusSubmitPostTimlineModal(String count) {
        this.comment_count = count;
    }

    public String getMessage() {
        return comment_count;
    }
}
