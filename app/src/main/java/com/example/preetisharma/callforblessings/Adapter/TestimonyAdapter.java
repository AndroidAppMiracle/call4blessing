package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.HaleTestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 1/18/2017.
 */

public class TestimonyAdapter extends RecyclerView.Adapter<TestimonyAdapter.DataViewHolder> {


    public List<TestimonyModal.ListBean> mList = new ArrayList<>();
    Activity mContext;
    private boolean expand = false;
    private TestimonyModal.TestimonyOfDayBean testimonyOfDayBean;
    private static String likescount, commentsCount;
    private static String liked = "not liked";

    public TestimonyAdapter(Activity mContext, List<TestimonyModal.ListBean> list) {
        mList = list;
        this.mContext = mContext;
        Log.e("List size", "size" + mList.size());


    }

   /* public void addItem(TestimonyModal testimonyModal) {
        mList.add(testimonyModal);
        notifyItemInserted(mList.size() - 1);
    }*/

    public void deleteItem(int index) {
        mList.remove(index);
        notifyItemRemoved(index);
    }

    /*public void addAllItems(List<TestimonyModal> items) {
        mList.addAll(items);
        notifyDataSetChanged();
    }

    public void updateItem(int index, TestimonyModal item) {
        mList.set(index, item);
        notifyItemChanged(index);
    }*/

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.prayer_item, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {


        holder.txtvw_prayer_title.setText(mList.get(position).getTitle());
        holder.txtvw_prayer_description.setText(mList.get(position).getDesc());
        holder.child_layout.setVisibility(View.VISIBLE);
        /*holder.txtvw_prayer_title.setText("prayer 1");
        holder.txtvw_prayer_description.setText("Good going");*/

        if (Integer.parseInt(mList.get(position).getLike_count()) == 0 || Integer.parseInt(mList.get(position).getLike_count()) == 1) {
            holder.txtvw_likes_count.setText(mList.get(position).getLike_count()  +" "+ "Like");
        } else {
            holder.txtvw_likes_count.setText(mList.get(position).getLike_count() +" "+ "Likes");
        }
        if (Integer.parseInt(mList.get(position).getComment_count()) == 0 || Integer.parseInt(mList.get(position).getComment_count()) == 1) {
            holder.txtvw_comments_count.setText(mList.get(position).getComment_count()  +" "+ "Comment");
        } else {
            holder.txtvw_comments_count.setText(mList.get(position).getComment_count() +" "+ "Comments");
        }
        if (Integer.parseInt(mList.get(position).getHallelujah_count()) == 0 || Integer.parseInt(mList.get(position).getHallelujah_count()) == 1) {
            holder.txtvw_hallelujah_count.setText(mList.get(position).getHallelujah_count() + "Hallelujah");
        } else {
            holder.txtvw_hallelujah_count.setText(mList.get(position).getHallelujah_count()  +" "+ "Hallelujah");
        }
        if (mList.get(holder.getAdapterPosition()).getLike_flag().equalsIgnoreCase("Not liked")) {
            int imgResource = R.drawable.prayer_like_icon;
            holder.txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
        } else {
            int imgResource = R.drawable.ic_liked;
            holder.txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
        }

        holder.txtvw_amen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int likescountValue = Integer.parseInt(mList.get(position).getHallelujah_count());
                likescountValue++;
                if (likescountValue == 0 || likescountValue == 1) {
                    holder.txtvw_hallelujah_count.setText(String.valueOf(likescountValue)  +" "+ "Hallelujah");
                } else {
                    holder.txtvw_hallelujah_count.setText(String.valueOf(likescountValue) +" "+ "Hallelujah");
                }
            }
        });

        holder.txtvw_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(mContext, CommentsListingActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.COMMENTS, String.valueOf(mList.get(holder.getAdapterPosition()).getId()));
                b.putString(Constants.POSTTYPE, Constants.TESTIMONY);
                i.putExtras(b);
                mContext.startActivity(i);
            }
        });
       /* holder.txtvw_prayer_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!expand) {
                    expand = true;
                    holder.child_layout.setVisibility(View.VISIBLE);
                } else {
                    holder.child_layout.setVisibility(View.GONE);
                    expand = false;
                }

            }
        });*/

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {


        //Prayer List
        @BindView(R.id.txtvw_prayer_title)
        AppCompatTextView txtvw_prayer_title;
        @BindView(R.id.child_layout)
        LinearLayout child_layout;
        @BindView(R.id.txtvw_prayer_description)
        AppCompatTextView txtvw_prayer_description;
        @BindView(R.id.txtvw_like_prayer)
        AppCompatTextView txtvw_like_prayer;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_amen)
        AppCompatTextView txtvw_amen;
        @BindView(R.id.txtvw_likes_count)
        AppCompatTextView txtvw_likes_count;
        @BindView(R.id.txtvw_comments_count)
        AppCompatTextView txtvw_comments_count;
        @BindView(R.id.txtvw_hallelujah_count)
        AppCompatTextView txtvw_hallelujah_count;
        String likescount, hallecount;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            txtvw_amen.setText(mContext.getString(R.string.hallelujah));


        }


        @OnClick(R.id.txtvw_amen)
        public void haleTestimonyPost() {


            ServerAPI.getInstance().haleTestimonyPost(APIServerResponse.HALE_TESTIMONY, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(mList.get(getAdapterPosition()).getId()), DataViewHolder.this);
        }

        @OnClick(R.id.txtvw_like_prayer)
        public void likePost() {
            liked = mList.get(getAdapterPosition()).getLike_flag();
            likescount = mList.get(getAdapterPosition()).getLike_count();

            if (liked.equalsIgnoreCase("Not Liked")) {
                int imgResource = R.drawable.ic_liked;
                txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) mContext).isConnectedToInternet()) {
                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(mList.get(getAdapterPosition()).getId()), "TESTIMONY", this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }

            } else {
                int imgResource = R.drawable.prayer_like_icon;
                txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) mContext).isConnectedToInternet()) {
                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(mList.get(getAdapterPosition()).getId()), "TESTIMONY", this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        }


        @Override
        public void onSuccess(int tag, Response response) {

            try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    LikeModal likeModal;
                    HaleTestimonyModal haleTestimonyModal;

                    switch (tag) {
                        case APIServerResponse.LIKE: {
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                new BaseActivity().showToast("Post Liked", Toast.LENGTH_SHORT);
                            }
                            int imgResource = R.drawable.ic_liked;
                            txtvw_likes_count.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            int likescountValue = Integer.parseInt(likescount);
                            likescountValue += 1;
                            liked = "liked";
                            mList.get(getAdapterPosition()).setLike_flag("liked");
                            mList.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                            if (likescountValue == 0 || likescountValue == 1) {
                                txtvw_likes_count.setText(String.valueOf(likescountValue)  +" "+ "Like");
                            } else {
                                txtvw_likes_count.setText(String.valueOf(likescountValue)  +" "+ "Likes");
                            }
                        }
                        break;
                        case APIServerResponse.UNLIKE: {
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast("Prayer Liked", Toast.LENGTH_SHORT);
                            }
                            int imgResource = R.drawable.prayer_like;
                            txtvw_likes_count.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            int likescountValue = Integer.parseInt(likescount);
                            likescountValue--;
                            liked = "not liked";
                            mList.get(getAdapterPosition()).setLike_flag("not liked");
                            mList.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                            if (likescountValue == 0 || likescountValue == 1) {
                                txtvw_likes_count.setText(String.valueOf(likescountValue)  +" "+ "Like");
                            } else {
                                txtvw_likes_count.setText(String.valueOf(likescountValue)  +" "+ "Likes");
                            }
                        }
                        break;
                        case APIServerResponse.HALE_TESTIMONY:
                            haleTestimonyModal = (HaleTestimonyModal) response.body();
                            //((BaseActivity) mContext).showToast("Halle Liked " + haleTestimonyModal.getHallelujau(), Toast.LENGTH_SHORT);
                            if (haleTestimonyModal.getStatus().equals("1")) {
                                if (haleTestimonyModal.getHallelujau().equalsIgnoreCase("YES")) {
                                    int hallecount = Integer.parseInt(mList.get(getAdapterPosition()).getHallelujah_count());
                                    hallecount++;
                                    mList.get(getAdapterPosition()).setHallelujah_count(String.valueOf(hallecount));
                                    if (hallecount == 0 || hallecount == 1) {
                                        txtvw_likes_count.setText(String.valueOf(hallecount)  +" "+ "Hallelujah");
                                    } else {
                                        txtvw_likes_count.setText(String.valueOf(hallecount) +" "+ "Hallelujah");
                                    }
                                } else {
                                    int hallecount = Integer.parseInt(mList.get(getAdapterPosition()).getHallelujah_count());
                                    hallecount--;
                                    mList.get(getAdapterPosition()).setHallelujah_count(String.valueOf(hallecount));
                                    if (hallecount == 0 || hallecount == 1) {
                                        txtvw_likes_count.setText(String.valueOf(hallecount)  +" "+ "Hallelujah");
                                    } else {
                                        txtvw_likes_count.setText(String.valueOf(hallecount)  +" "+ "Hallelujah");
                                    }
                                }
                            }

                            break;
                        default:
                            break;

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {

            try {
                ((BaseActivity) mContext).hideLoading();
                throwable.printStackTrace();
                switch (tag) {
                    case APIServerResponse.LIKE:
                        System.out.println("Error");
                        break;
                    case APIServerResponse.HALE_TESTIMONY:
                        System.out.println("Error");
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
