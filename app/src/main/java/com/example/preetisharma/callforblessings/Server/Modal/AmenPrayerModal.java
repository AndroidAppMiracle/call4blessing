package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 1/18/2017.
 */

public class AmenPrayerModal {


    /**
     * status : 1
     * Liked :yes no
     * Amen : yes no
     * hallelujah : yes no
     */


    private String status;
    private String amen;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmen() {
        return amen;
    }

    public void setAmen(String amen) {
        this.amen = amen;
    }
}
