package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.FilterModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/8/2017.
 */

public class TaggedFriendsAdapter extends RecyclerView.Adapter<TaggedFriendsAdapter.DataViewHolder> {


    private Context mContext;
    private List<MyFriendsModal.ListBean> myFriends = new ArrayList<>();
    private boolean checkedStatus = false;
    private FilterModal modal;
    private ArrayList<FilterModal> myTaggedFriends = new ArrayList<>();
    private ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> taggedFriendsDemo = new ArrayList<>();
    private ArrayList<MyFriendsModal.ListBean.UserInfoBean> taggedFriendsIds = new ArrayList<>();

    public TaggedFriendsAdapter(Context mContext, List<MyFriendsModal.ListBean> list) {
        this.myFriends = list;
        this.mContext = mContext;
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.selectable_list, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        myTaggedFriends.clear();
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, int position) {

        try {

            holder.txtvw_friends_request_name.setText(myFriends.get(position).getUser_info().getProfile_details().getFirstname() + " " + myFriends.get(position).getUser_info().getProfile_details().getLastname());

            holder.checkbox_select_friend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        Log.i("isChecked", "Checked");

                        taggedFriendsDemo.add(myFriends.get(holder.getAdapterPosition()).getUser_info().getProfile_details());
                        taggedFriendsIds.add(myFriends.get(holder.getAdapterPosition()).getUser_info());

                       /* Log.i("isChecked Size", "Checked " + taggedFriendsDemo.size());
                        modal = new FilterModal();
                        modal.setStatus(false);
                        modal.setStatus(!modal.getStatus());
                        if (modal.getStatus()) {
                            modal.setList(myFriends.get(holder.getAdapterPosition()));
                            myTaggedFriends.add(modal);
                            modal.setStatus(false);
                        }*/

                    } else {
                        Log.i("isChecked", "notChecked");
                        taggedFriendsDemo.remove(myFriends.get(holder.getAdapterPosition()).getUser_info().getProfile_details());
                        taggedFriendsIds.remove(myFriends.get(holder.getAdapterPosition()).getUser_info());
                       /* Log.i("isChecked Size", "notChecked " + taggedFriendsDemo.size());
                        modal = new FilterModal();
                        modal.setStatus(false);
                        modal.setStatus(!modal.getStatus());
                        if (modal.getStatus()) {
                            //modal.setList(myFriends.get(holder.getAdapterPosition()));
                            //myTaggedFriends.add(modal);
                            myTaggedFriends.remove(modal);
                            modal.setStatus(false);
                        }*/
                    }


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ArrayList<FilterModal> unselectList() {
        FilterModal filterModal = new FilterModal();
        filterModal.setFriendsArray(myTaggedFriends);
        return myTaggedFriends;

    }

    public ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> selectedList() {

        ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> list = new ArrayList<>();
        list = taggedFriendsDemo;
        return list;
    }

    public ArrayList<MyFriendsModal.ListBean.UserInfoBean> selectedListIds() {
        ArrayList<MyFriendsModal.ListBean.UserInfoBean> list = new ArrayList<>();
        list = taggedFriendsIds;
        return list;
    }

    @Override
    public int getItemCount() {
        if (myFriends.size() != 0) {
            return myFriends.size();
        } else {
            return 0;
        }

    }


    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;
        @BindView(R.id.checkbox_select_friend)
        AppCompatCheckBox checkbox_select_friend;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @Override
        public void onSuccess(int tag, Response response) {


        }

        @Override
        public void onError(int tag, Throwable throwable) {

        }
    }


    public void notifyDataList(List<MyFriendsModal.ListBean> list) {
        Log.d("notifyData ", list.size() + "");
        this.myFriends = list;
        notifyDataSetChanged();
    }


}
