package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by preeti.sharma on 1/9/2017.
 */

public class ValidRegisterationModal {


    /**
     * status : 1
     */

    private String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
