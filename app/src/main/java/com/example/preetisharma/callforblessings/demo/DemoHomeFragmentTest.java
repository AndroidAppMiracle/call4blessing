package com.example.preetisharma.callforblessings.demo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.HomeWallPaginationAdapterNew;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoHomeModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusEditPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusHomeWallCommented;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusHomeWallPostCreated;
import com.example.preetisharma.callforblessings.Server.Modal.HomeWallModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.listener.PaginationScrollListenerDemo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/28/2017.
 */

public class DemoHomeFragmentTest extends Fragment implements APIServerResponse {

    /*DemoPaginationHomeWallAdapter adapter;*/
    HomeWallPaginationAdapterNew adapter;

    private static final int PAGE_NUMBER = 2;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int currentPage = PAGE_NUMBER;
    private int TOTAL_PAGES = 2;
    @BindView(R.id.home_listing)
    RecyclerView home_listing;
    @BindView(R.id.img_vw_profile)
    AppCompatImageView img_vw_profile;
    @BindView(R.id.edt_txt_new_post)
    AppCompatTextView edt_txt_new_post;
    @BindView(R.id.imgvw_post_image)
    AppCompatImageView imgvw_post_image;
    @BindView(R.id.imgvw_post_video)
    AppCompatImageView imgvw_post_video;
    @BindView(R.id.txtvw_no_posts)
    AppCompatTextView txtvw_no_posts;
    @BindView(R.id.imgvw_preview_post)
    AppCompatImageView imgvw_preview_post;
    @BindView(R.id.txtvw_preview_post)
    AppCompatTextView txtvw_preview_post;
    //HomeWallAdapter _adapter;
    /*@BindView(R.id.lnr_layout_post_preview)
    LinearLayout lnr_layout_post_preview;*/
    @BindView(R.id.title_container)
    LinearLayout title_container;
    List<HomeWallModal.PostsBean> listBeenPagination = new ArrayList<>();

    @BindView(R.id.img_vw_joy)
    AppCompatImageView img_vw_joy;
    @BindView(R.id.img_vw_books)
    AppCompatImageView img_vw_books;
    @BindView(R.id.imgvw_matrimonial)
    AppCompatImageView imgvw_matrimonial;
    @BindView(R.id.imgvw_video)
    AppCompatImageView imgvw_video;
    /*@BindView(R.id.nestedscroll)
    NestedScrollView nestedscroll;*/
    /* @BindView(R.id.swipe_refresh)
     SwipeRefreshLayout swipe_refresh;*/
    @BindView(R.id.ll_header)
    LinearLayout ll_header;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        /*View rootView = inflater.inflate(R.layout.demodemo_home_fragment_normal, container, false);*/
        View rootView = inflater.inflate(R.layout.demohomefragmenttest_layout, container, false);
        ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        ((BaseActivity) getActivity()).getmPrefs();


        /*adapter = new DemoPaginationHomeWallAdapter(getActivity());*/
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        adapter = new HomeWallPaginationAdapterNew(getActivity());
        /* {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };*/

        home_listing.setNestedScrollingEnabled(false);
        home_listing.setLayoutManager(linearLayoutManager);
        home_listing.setItemAnimator(new DefaultItemAnimator());
        home_listing.setAdapter(adapter);


        try {
            img_vw_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), MaterialUpConceptActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.SHARED_PREF_USER_ID, (((BaseActivity) getActivity()).getUserID()));
                    b.putBoolean(Constants.TIMELINE_ENABLED, true);
                    b.putString(Constants.USER_IMAGE, (((BaseActivity) getActivity()).getUserImage()));
                    Log.e("Cover", "Cover Image" + (((BaseActivity) getActivity()).getUserCoverImage()));
                    b.putString(Constants.COVER_PIC, (((BaseActivity) getActivity()).getUserCoverImage()));
                    b.putString(Constants.USER_NAME, (((BaseActivity) getActivity()).getFullName()));
                    b.putInt(Constants.IS_FRIEND, Constants.USER_SELFTIMELINE);
                    b.putString(Constants.PRIVACY_WALL, "");
                    b.putString(Constants.PRIVACY_ABOUT_INFO, "");
                    b.putString(Constants.PRIVACY_FRIEND_REQUEST, "");
                    b.putString(Constants.PRIVACY_MESSAGE, "");
                    i.putExtras(b);
                    startActivity(i);
                }
            });


            img_vw_books.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), BookTabs.class);
                /*Intent intent = new Intent(HomeActivity.this, BooksListingActivity.class);*/
                    startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    try {
                        currentPage = 1;
                        TOTAL_PAGES = 1;
                        isLastPage = false;
                        adapter.removeAll();
                        ServerAPI.getInstance().getHomePostsDemo(APIServerResponse.DEMO_HOME_POST, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(currentPage), DemoHomeFragment.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    ((BaseActivity) getActivity()).hideLoading();
                    ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        });*/

        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                currentPage = 1;
                TOTAL_PAGES = 1;
                isLastPage = false;
                adapter.removeAll();
                ServerAPI.getInstance().getHomePostsDemo(APIServerResponse.DEMO_HOME_POST, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(currentPage), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


        home_listing.addOnScrollListener(new PaginationScrollListenerDemo(linearLayoutManager, ll_header) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPages();
                    }
                }, 1000);
            }


            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }


        });

        return rootView;

    }


    @OnClick(R.id.imgvw_post_video)
    public void uploadVideo() {
        Intent createPostIntent = new Intent(getActivity(), CreatePostActivity.class);
        startActivity(createPostIntent);
    }

    @OnClick(R.id.imgvw_post_image)
    public void uploadImage() {

        Intent createPostIntent = new Intent(getActivity(), CreatePostActivity.class);
        startActivity(createPostIntent);

    }


    @OnClick(R.id.edt_txt_new_post)
    public void createPostText() {
        Intent createPostIntent = new Intent(getActivity(), CreatePostActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.POSTTYPE, Constants.WALL);
        createPostIntent.putExtras(b);
        startActivity(createPostIntent);
    }

    @OnClick(R.id.img_vw_joy)
    public void joy_musicDialog() {
        Intent intent = new Intent(getActivity(), JoyMusicActivity.class);
        startActivity(intent);

    }

    @OnClick(R.id.imgvw_matrimonial)
    public void matrimonialDialog() {
        comingSoonDialog("Matrimonial");
        /*Intent intent = new Intent(getActivity(), demodemoactivity.class);
        startActivity(intent);*/
    }

    @OnClick(R.id.imgvw_video)
    public void videoDialog() {
        Intent intentVideo = new Intent(getActivity(), VideoOnWallActivity.class);
        startActivity(intentVideo);
        //comingSoonDialog("Videos");
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        ((BaseActivity) getActivity()).dismissDialog();

        super.onDestroy();
    }

    @OnClick(R.id.title_container)
    public void createPost() {
        Intent createPostIntent = new Intent(getActivity(), CreatePostActivity.class);
        startActivity(createPostIntent);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {


            if (response.isSuccessful()) {
                DemoHomeModal demoHomeModal;

                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).hideLoading();
                }
                switch (tag) {

                    case APIServerResponse.DEMO_HOME_POST:
                        demoHomeModal = (DemoHomeModal) response.body();
                        if (demoHomeModal.getStatus().equalsIgnoreCase("1")) {

                            //TOTAL_PAGES = joyMusicViewAllSongsModal.getTotalPages();
                          /*  if (adapter != null && adapter.getItemCount() < demoHomeModal.getPosts().size() && isLastPage) {

                                adapter.add(demoHomeModal.getPosts().get(0));
                                adapter.notifyItemInserted(0);

                            } else {*/
                            TOTAL_PAGES = Integer.parseInt(demoHomeModal.getTotalPages());
                            currentPage = Integer.parseInt(demoHomeModal.getCurrentPage());

                            if (demoHomeModal.getPosts().size() > 0) {
                                List<DemoHomeModal.PostsBean> listBeen = demoHomeModal.getPosts();
                                adapter.addAll(listBeen);
                            }

                            if (((BaseActivity) getActivity()).getUserGender().equalsIgnoreCase("male")) {
                                Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.drawable.male).centerCrop().into(img_vw_profile);
                            } else {
                                Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.drawable.female).centerCrop().into(img_vw_profile);

                            }

                            //currentPage = Integer.parseInt(demoHomeModal.getCurrentPage());

                            if (currentPage < TOTAL_PAGES)
                                adapter.addLoadingFooter();
                            else isLastPage = true;

/*
                            } else {
                                //TOTAL_PAGES = currentPage;
                                txtvw_no_posts.setVisibility(View.VISIBLE);
                            }*/
                          /*  }*/


                            //rv_viewAllList.setAdapter(adapter);


                        }


                        break;
                    case APIServerResponse.DEMO_HOME_POST_PAGINATION:
                        demoHomeModal = (DemoHomeModal) response.body();
                        if (demoHomeModal.getStatus().equalsIgnoreCase("1")) {

                            //TOTAL_PAGES = joyMusicViewAllAlbumModal.getTotalPages();

                           /* if (adapter != null && adapter.getItemCount() < demoHomeModal.getPosts().size() && isLastPage) {

                                adapter.add(demoHomeModal.getPosts().get(0));
                                adapter.notifyItemInserted(0);

                            } else {*/
                            TOTAL_PAGES = Integer.parseInt(demoHomeModal.getTotalPages());
                            currentPage = Integer.parseInt(demoHomeModal.getCurrentPage());
                            adapter.removeLoadingFooter();
                            isLoading = false;

                            if (((BaseActivity) getActivity()).getUserGender().equalsIgnoreCase("male")) {
                                Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.drawable.male).centerCrop().into(img_vw_profile);
                            } else {
                                Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.drawable.female).centerCrop().into(img_vw_profile);

                            }

                            //currentPage = Integer.parseInt(demoHomeModal.getCurrentPage());


                            if (demoHomeModal.getPosts().size() > 0) {
                                List<DemoHomeModal.PostsBean> list = demoHomeModal.getPosts();
                                adapter.addAll(list);
                            }

                            if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                            else isLastPage = true;
                             /*else {
                                //TOTAL_PAGES = currentPage;
                                txtvw_no_posts.setVisibility(View.VISIBLE);
                            }*/
                           /* }*/


                        }


                        break;
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideLoading();
        }
        switch (tag) {
            case APIServerResponse.HOME:
                System.out.println("Error");
                throwable.getMessage();
                break;
            case APIServerResponse.HOME_PAGINATION:
                System.out.println("Error");
                throwable.getMessage();
                break;
        }


    }

   /* @Override
    public void onResume() {
        super.onResume();


        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
        currentPage = 1;
        TOTAL_PAGES = 1;
        isLastPage = false;
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideKeyboard();
            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                try {
                    ServerAPI.getInstance().getHomePostsDemo(APIServerResponse.DEMO_HOME_POST, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(currentPage), this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ((BaseActivity) getActivity()).hideLoading();
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }

        }

        *//*home_listing.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPages();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });*//*
    }*/


    /*@Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) getActivity()).hideKeyboard();
        ((BaseActivity) getActivity()).showLoading();
        currentPage = 1;
        TOTAL_PAGES = 1;
        isLastPage = false;
        adapter.removeAll();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                ServerAPI.getInstance().getHomePostsDemo(APIServerResponse.DEMO_HOME_POST, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(currentPage), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
       *//* if (((BaseActivity) getActivity()).getUserGender().equalsIgnoreCase("male")) {
            Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.drawable.male).centerCrop().into(img_vw_profile);
        } else {
            Glide.with(getActivity()).load(((BaseActivity) getActivity()).getUserImage()).placeholder(R.drawable.female).centerCrop().into(img_vw_profile);

        }*//*
    }*/

    private void loadNextPages() {

        Log.d("loadNextPage: ", "" + currentPage);


        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                ServerAPI.getInstance().getHomePostsDemo(APIServerResponse.DEMO_HOME_POST_PAGINATION, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(currentPage), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

       /* if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                //showLoading();
                callApi(currentPage);

            }
        }*/

    }


    public void callApi(int pageNumber) {


        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                ServerAPI.getInstance().getHomePostsDemo(APIServerResponse.DEMO_HOME_POST_PAGINATION, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(pageNumber), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }


    @Subscribe
    public void onCommentPost(DemoCommentModal share) {
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                callApi(currentPage);
            /*    ServerAPI.getInstance().Home(APIServerResponse.HOME, ((BaseActivity) getActivity()).getUserSessionId(), this);
         */
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void onSharePost(DemoShareModal share) {
        currentPage = 1;
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            try {
                callApi(currentPage);
             /*   ServerAPI.getInstance().Home(APIServerResponse.HOME, ((BaseActivity) getActivity()).getUserSessionId(), this);
            */
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ((BaseActivity) getActivity()).hideLoading();
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void onEditPost(EventBusEditPostModal eventBusEditPostModal) {
        if (eventBusEditPostModal.getMessage().equals("1")) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                try {
                    currentPage = 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ((BaseActivity) getActivity()).hideLoading();
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }
    }

    public void comingSoonDialog(String title) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage("Coming Soon")

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }


    public void repositionRecyclerViewOnTabClick() {
        try {

            if (adapter != null && adapter.getMovies().size() > 0) {
                //home_listing.scrollToPosition(0);
                home_listing.smoothScrollToPosition(0);

                /*if (!nestedscroll.isSmoothScrollingEnabled()) {
                    nestedscroll.setSmoothScrollingEnabled(true);
                }
                nestedscroll.smoothScrollTo(0, 0);*/
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Subscribe
    public void onHomeWallPostCreated(EventBusHomeWallPostCreated eventBusHomeWallPostCreated) {

        try {

            if (eventBusHomeWallPostCreated.getIsNewPostCreated().equalsIgnoreCase("1")) {

                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    adapter.removeAll();
                    currentPage = 1;
                    currentPage = 1;
                    TOTAL_PAGES = 1;
                    isLastPage = false;

                    ServerAPI.getInstance().getHomePostsDemo(APIServerResponse.DEMO_HOME_POST, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(currentPage), this);

                } else {
                    ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onCommentPost(EventBusHomeWallCommented eventBusHomeWallCommented) {
        try {

            if (eventBusHomeWallCommented.getStatus() == 1) {
                adapter.updateASingleItem(eventBusHomeWallCommented.getItemPosition());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

