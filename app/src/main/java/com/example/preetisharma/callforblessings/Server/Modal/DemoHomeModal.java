package com.example.preetisharma.callforblessings.Server.Modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by satoti.garg on 5/30/2017.
 */

public class DemoHomeModal {


    private String status;
    private int user_id;
    private List<PostsBean> posts;
    private String totalPages;
    private String currentPage;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public List<PostsBean> getPosts() {
        return posts;
    }

    public void setPosts(List<PostsBean> posts) {
        this.posts = posts;
    }

    public static class PostsBean {


        private int id;
        private PostByBean post_by;
        private String post_content;
        private String posted_at;
        private String post_type;
        private String like_count;
        private String comment_count;
        private String like_flag;
        private List<CommentsBean> comments;
        private List<SharedPostBean> shared_post;
        private List<SharedRequestBean> shared_request;
        private List<MediaBean> media;
        private List<TagUsersBeans> tag_users;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public PostByBean getPost_by() {
            return post_by;
        }

        public void setPost_by(PostByBean post_by) {
            this.post_by = post_by;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPosted_at() {
            return posted_at;
        }

        public void setPosted_at(String posted_at) {
            this.posted_at = posted_at;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getLike_count() {
            return like_count;
        }

        public void setLike_count(String like_count) {
            this.like_count = like_count;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getLike_flag() {
            return like_flag;
        }

        public void setLike_flag(String like_flag) {
            this.like_flag = like_flag;
        }

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public List<SharedPostBean> getShared_post() {
            return shared_post;
        }

        public void setShared_post(List<SharedPostBean> shared_post) {
            this.shared_post = shared_post;
        }

        public List<SharedRequestBean> getShared_request() {
            return shared_request;
        }

        public void setShared_request(List<SharedRequestBean> shared_request) {
            this.shared_request = shared_request;
        }

        public List<MediaBean> getMedia() {
            return media;
        }

        public void setMedia(List<MediaBean> media) {
            this.media = media;
        }

        public List<TagUsersBeans> getTag_users() {
            return tag_users;
        }

        public void setTag_users(List<TagUsersBeans> tag_users) {
            this.tag_users = tag_users;
        }

        public static class PostByBean {


            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private String WALL;
            @SerializedName("ABOUT INFO")
            private String about_info;
            @SerializedName("FRIEND REQUEST")
            private String friend_request;
            private String MESSAGE;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public String getWALL() {
                return WALL;
            }

            public void setWALL(String WALL) {
                this.WALL = WALL;
            }

            public String getabout_info() {
                return about_info;
            }

            public void setabout_info(String about_info) {
                this.about_info = about_info;
            }

            public String getfriend_request() {
                return friend_request;
            }

            public void setfriend_request(String friend_request) {
                this.friend_request = friend_request;
            }

            public String getMESSAGE() {
                return MESSAGE;
            }

            public void setMESSAGE(String MESSAGE) {
                this.MESSAGE = MESSAGE;
            }


            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {


                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }

        public static class MediaBean {
            /**
             * file : http://www.call4blessing.com/uploads/post/152/1490694566user_profile.jpg
             * type : IMAGE
             */

            private String file;
            private String type;
            private String thumbnail_file;

            public String getThumbnail_file() {
                return thumbnail_file;
            }

            public void setThumbnail_file(String thumbnail_file) {
                this.thumbnail_file = thumbnail_file;
            }


            public String getFile() {
                return file;
            }

            public void setFile(String file) {
                this.file = file;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }

        public static class TagUsersBeans {


            /**
             * user_info : {"id":127,"username":"ganeshbehera2011ss","email":"ganeshbehera2011ss@gmail.com","updated":0,"profile_details":{"firstname":"Ganesh","lastname":"Behera","d_o_b":"1991-9-11","phone":"123456789","gender":"MALE","location":"","country":"India","state":"Maharashtra","cover_pic":"http://www.call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/148619392015672971_1277436022279726_8123819922948438827_n.jpg"},"is_friend":0,"request_respond":"NO"}
             */

            private UserInfoBean user_info;

            public UserInfoBean getUser_info() {
                return user_info;
            }

            public void setUser_info(UserInfoBean user_info) {
                this.user_info = user_info;
            }

            public static class UserInfoBean {
                /**
                 * id : 127
                 * username : ganeshbehera2011ss
                 * email : ganeshbehera2011ss@gmail.com
                 * updated : 0
                 * profile_details : {"firstname":"Ganesh","lastname":"Behera","d_o_b":"1991-9-11","phone":"123456789","gender":"MALE","location":"","country":"India","state":"Maharashtra","cover_pic":"http://www.call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/148619392015672971_1277436022279726_8123819922948438827_n.jpg"}
                 * is_friend : 0
                 * request_respond : NO
                 */

                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBean profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBean getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBean profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBean {
                    /**
                     * firstname : Ganesh
                     * lastname : Behera
                     * d_o_b : 1991-9-11
                     * phone : 123456789
                     * gender : MALE
                     * location :
                     * country : India
                     * state : Maharashtra
                     * cover_pic : http://www.call4blessing.com/theme/images/cover-picture.jpg
                     * profile_pic : http://www.call4blessing.com/uploads/profilepic/148619392015672971_1277436022279726_8123819922948438827_n.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }
        }

        public static class CommentsBean {
            /**
             * id : 370
             * post_id : 778
             * post_by : {"id":152,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-8-29","phone":"1234567890","gender":"MALE","location":"","country":"","state":"Andaman and Nicobar Islands","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1489485266image.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1490073476image.jpg"},"is_friend":0,"request_respond":"NO"}
             * comment : Xgvv
             * created_at : 3  minutes   ago
             * post_type : POST
             */

            private int id;
            private int post_id;
            private PostByBeanX post_by;
            private String comment;
            private String created_at;
            private String post_type;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getPost_id() {
                return post_id;
            }

            public void setPost_id(int post_id) {
                this.post_id = post_id;
            }

            public PostByBeanX getPost_by() {
                return post_by;
            }

            public void setPost_by(PostByBeanX post_by) {
                this.post_by = post_by;
            }

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getPost_type() {
                return post_type;
            }

            public void setPost_type(String post_type) {
                this.post_type = post_type;
            }

            public static class PostByBeanX {
                /**
                 * id : 152
                 * username : plash.jindal
                 * email : plash.jindal@emptask.com
                 * updated : 1
                 * profile_details : {"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-8-29","phone":"1234567890","gender":"MALE","location":"","country":"","state":"Andaman and Nicobar Islands","cover_pic":"http://www.call4blessing.com/uploads/coverphoto/1489485266image.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/1490073476image.jpg"}
                 * is_friend : 0
                 * request_respond : NO
                 */

                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanX {
                    /**
                     * firstname : Plash
                     * lastname : Jindal
                     * d_o_b : 1992-8-29
                     * phone : 1234567890
                     * gender : MALE
                     * location :
                     * country :
                     * state : Andaman and Nicobar Islands
                     * cover_pic : http://www.call4blessing.com/uploads/coverphoto/1489485266image.jpg
                     * profile_pic : http://www.call4blessing.com/uploads/profilepic/1490073476image.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }
        }

        /*public static class TagUsersBean {
            *//**
         * id : 127
         * username : ganeshbehera2011ss
         * email : ganeshbehera2011ss@gmail.com
         * updated : 0
         * profile_details : {"firstname":"Ganesh","lastname":"Behera","d_o_b":"1991-9-11","phone":"123456789","gender":"MALE","location":"","country":"India","state":"Maharashtra","cover_pic":"http://www.call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/148619392015672971_1277436022279726_8123819922948438827_n.jpg"}
         * is_friend : 0
         * request_respond : NO
         *//*

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBeanX profile_details;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBeanX getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBeanX profile_details) {
                this.profile_details = profile_details;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBeanX {
                */

        /**
         * firstname : Ganesh
         * lastname : Behera
         * d_o_b : 1991-9-11
         * phone : 123456789
         * gender : MALE
         * location :
         * country : India
         * state : Maharashtra
         * cover_pic : http://www.call4blessing.com/theme/images/cover-picture.jpg
         * profile_pic : http://www.call4blessing.com/uploads/profilepic/148619392015672971_1277436022279726_8123819922948438827_n.jpg
         *//*

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }*/

        public static class SharedPostBean {


            private int id;
            private PostByBeanXX post_by;
            private String post_content;
            private String posted_at;
            private String post_type;
            private String like_count;
            private String comment_count;
            private String like_flag;
            private List<?> comments;
            private List<?> shared_post;
            private List<MediaBean> media;
            private List<TagUsersBean> tag_users;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public PostByBeanXX getPost_by() {
                return post_by;
            }

            public void setPost_by(PostByBeanXX post_by) {
                this.post_by = post_by;
            }

            public String getPost_content() {
                return post_content;
            }

            public void setPost_content(String post_content) {
                this.post_content = post_content;
            }

            public String getPosted_at() {
                return posted_at;
            }

            public void setPosted_at(String posted_at) {
                this.posted_at = posted_at;
            }

            public String getPost_type() {
                return post_type;
            }

            public void setPost_type(String post_type) {
                this.post_type = post_type;
            }

            public String getLike_count() {
                return like_count;
            }

            public void setLike_count(String like_count) {
                this.like_count = like_count;
            }

            public String getComment_count() {
                return comment_count;
            }

            public void setComment_count(String comment_count) {
                this.comment_count = comment_count;
            }

            public String getLike_flag() {
                return like_flag;
            }

            public void setLike_flag(String like_flag) {
                this.like_flag = like_flag;
            }

            public List<?> getComments() {
                return comments;
            }

            public void setComments(List<?> comments) {
                this.comments = comments;
            }

            public List<?> getShared_post() {
                return shared_post;
            }

            public void setShared_post(List<?> shared_post) {
                this.shared_post = shared_post;
            }

            public List<MediaBean> getMedia() {
                return media;
            }

            public void setMedia(List<MediaBean> media) {
                this.media = media;
            }

            public List<TagUsersBean> getTag_users() {
                return tag_users;
            }

            public void setTag_users(List<TagUsersBean> tag_users) {
                this.tag_users = tag_users;
            }

            public static class PostByBeanXX {


                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanXX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanXX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanXX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanXX {
                    /**
                     * firstname : Plash
                     * lastname : Jindal
                     * d_o_b : 1992-8-29
                     * phone : 1234567890
                     * gender : MALE
                     * location :
                     * country :
                     * state : Andaman and Nicobar Islands
                     * cover_pic : http://www.call4blessing.com/uploads/coverphoto/1489485266image.jpg
                     * profile_pic : http://www.call4blessing.com/uploads/profilepic/1490073476image.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }

            public static class MediaBean {
                /**
                 * file : http://www.call4blessing.com/uploads/post/152/1490694566user_profile.jpg
                 * type : IMAGE
                 */

                private String file;
                private String type;
                private String thumbnail_file;

                public String getFile() {
                    return file;
                }

                public void setFile(String file) {
                    this.file = file;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getThumbnail_file() {
                    return thumbnail_file;
                }

                public void setThumbnail_file(String thumbnail_file) {
                    this.thumbnail_file = thumbnail_file;
                }
            }

            public static class TagUsersBean {
                /**
                 * id : 127
                 * username : ganeshbehera2011ss
                 * email : ganeshbehera2011ss@gmail.com
                 * updated : 0
                 * profile_details : {"firstname":"Ganesh","lastname":"Behera","d_o_b":"1991-9-11","phone":"123456789","gender":"MALE","location":"","country":"India","state":"Maharashtra","cover_pic":"http://www.call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://www.call4blessing.com/uploads/profilepic/148619392015672971_1277436022279726_8123819922948438827_n.jpg"}
                 * is_friend : 0
                 * request_respond : NO
                 */

                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanXXX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanXXX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanXXX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanXXX {
                    /**
                     * firstname : Ganesh
                     * lastname : Behera
                     * d_o_b : 1991-9-11
                     * phone : 123456789
                     * gender : MALE
                     * location :
                     * country : India
                     * state : Maharashtra
                     * cover_pic : http://www.call4blessing.com/theme/images/cover-picture.jpg
                     * profile_pic : http://www.call4blessing.com/uploads/profilepic/148619392015672971_1277436022279726_8123819922948438827_n.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }
        }

        public static class SharedRequestBean {
            /**
             * id : 1
             * name : test 1
             * desc : Help request
             * title : test1
             * video_link :
             * image : http://call4blessing.com/uploads/help/1492584181bannerbottom.jpg
             * like_flag : Not Liked
             * comment_count : 1
             * like_count : 3
             * comments : [{"id":20,"post_id":1,"post_by":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"2001-11-17","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"},"is_friend":0,"request_respond":"NO"},"comment":"aaaaa","created_at":"4  weeks   ago","post_type":"HELP_REQUEST"}]
             * reach_count : 3
             */

            private int id;
            private String name;
            private String desc;
            private String title;
            private String video_link;
            private String image;
            private String like_flag;
            private String comment_count;
            private String like_count;
            private String reach_count;
            private List<CommentsBean> comments;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getVideo_link() {
                return video_link;
            }

            public void setVideo_link(String video_link) {
                this.video_link = video_link;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getLike_flag() {
                return like_flag;
            }

            public void setLike_flag(String like_flag) {
                this.like_flag = like_flag;
            }

            public String getComment_count() {
                return comment_count;
            }

            public void setComment_count(String comment_count) {
                this.comment_count = comment_count;
            }

            public String getLike_count() {
                return like_count;
            }

            public void setLike_count(String like_count) {
                this.like_count = like_count;
            }

            public String getReach_count() {
                return reach_count;
            }

            public void setReach_count(String reach_count) {
                this.reach_count = reach_count;
            }

            public List<CommentsBean> getComments() {
                return comments;
            }

            public void setComments(List<CommentsBean> comments) {
                this.comments = comments;
            }

            public static class CommentsBean {
                /**
                 * id : 20
                 * post_id : 1
                 * post_by : {"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"2001-11-17","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"},"is_friend":0,"request_respond":"NO"}
                 * comment : aaaaa
                 * created_at : 4  weeks   ago
                 * post_type : HELP_REQUEST
                 */

                private int id;
                private int post_id;
                private PostByBeanX post_by;
                private String comment;
                private String created_at;
                private String post_type;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getPost_id() {
                    return post_id;
                }

                public void setPost_id(int post_id) {
                    this.post_id = post_id;
                }

                public PostByBeanX getPost_by() {
                    return post_by;
                }

                public void setPost_by(PostByBeanX post_by) {
                    this.post_by = post_by;
                }

                public String getComment() {
                    return comment;
                }

                public void setComment(String comment) {
                    this.comment = comment;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getPost_type() {
                    return post_type;
                }

                public void setPost_type(String post_type) {
                    this.post_type = post_type;
                }

                public static class PostByBeanX {
                    /**
                     * id : 214
                     * username : plash.jindal
                     * email : plash.jindal@emptask.com
                     * updated : 1
                     * profile_details : {"firstname":"Palash","lastname":"Jindal","d_o_b":"2001-11-17","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"}
                     * is_friend : 0
                     * request_respond : NO
                     */

                    private int id;
                    private String username;
                    private String email;
                    private int updated;
                    private ProfileDetailsBeanX profile_details;
                    private int is_friend;
                    private String request_respond;

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public String getUsername() {
                        return username;
                    }

                    public void setUsername(String username) {
                        this.username = username;
                    }

                    public String getEmail() {
                        return email;
                    }

                    public void setEmail(String email) {
                        this.email = email;
                    }

                    public int getUpdated() {
                        return updated;
                    }

                    public void setUpdated(int updated) {
                        this.updated = updated;
                    }

                    public ProfileDetailsBeanX getProfile_details() {
                        return profile_details;
                    }

                    public void setProfile_details(ProfileDetailsBeanX profile_details) {
                        this.profile_details = profile_details;
                    }

                    public int getIs_friend() {
                        return is_friend;
                    }

                    public void setIs_friend(int is_friend) {
                        this.is_friend = is_friend;
                    }

                    public String getRequest_respond() {
                        return request_respond;
                    }

                    public void setRequest_respond(String request_respond) {
                        this.request_respond = request_respond;
                    }

                    public static class ProfileDetailsBeanX {
                        /**
                         * firstname : Palash
                         * lastname : Jindal
                         * d_o_b : 2001-11-17
                         * phone : 1234567890
                         * gender : MALE
                         * location :
                         * country : Spain
                         * state : Madrid
                         * cover_pic : http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg
                         * profile_pic : http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg
                         */

                        private String firstname;
                        private String lastname;
                        private String d_o_b;
                        private String phone;
                        private String gender;
                        private String location;
                        private String country;
                        private String state;
                        private String cover_pic;
                        private String profile_pic;

                        public String getFirstname() {
                            return firstname;
                        }

                        public void setFirstname(String firstname) {
                            this.firstname = firstname;
                        }

                        public String getLastname() {
                            return lastname;
                        }

                        public void setLastname(String lastname) {
                            this.lastname = lastname;
                        }

                        public String getD_o_b() {
                            return d_o_b;
                        }

                        public void setD_o_b(String d_o_b) {
                            this.d_o_b = d_o_b;
                        }

                        public String getPhone() {
                            return phone;
                        }

                        public void setPhone(String phone) {
                            this.phone = phone;
                        }

                        public String getGender() {
                            return gender;
                        }

                        public void setGender(String gender) {
                            this.gender = gender;
                        }

                        public String getLocation() {
                            return location;
                        }

                        public void setLocation(String location) {
                            this.location = location;
                        }

                        public String getCountry() {
                            return country;
                        }

                        public void setCountry(String country) {
                            this.country = country;
                        }

                        public String getState() {
                            return state;
                        }

                        public void setState(String state) {
                            this.state = state;
                        }

                        public String getCover_pic() {
                            return cover_pic;
                        }

                        public void setCover_pic(String cover_pic) {
                            this.cover_pic = cover_pic;
                        }

                        public String getProfile_pic() {
                            return profile_pic;
                        }

                        public void setProfile_pic(String profile_pic) {
                            this.profile_pic = profile_pic;
                        }
                    }
                }
            }
        }
    }


    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }
}
