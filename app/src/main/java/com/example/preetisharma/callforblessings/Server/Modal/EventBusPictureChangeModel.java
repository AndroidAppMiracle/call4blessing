package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 3/2/2017.
 */

public class EventBusPictureChangeModel {

    private final String comment_count;

    public EventBusPictureChangeModel(String count) {
        this.comment_count = count;
    }

    public String getMessage() {
        return comment_count;
    }
}
