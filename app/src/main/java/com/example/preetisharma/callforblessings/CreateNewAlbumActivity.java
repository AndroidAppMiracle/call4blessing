package com.example.preetisharma.callforblessings;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class CreateNewAlbumActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.edit_album_name)
    AppCompatEditText editAlbumName;
    @BindView(R.id.edit_album_desc)
    AppCompatEditText editAlbumDesc;

    @BindView(R.id.txtvw_create_album)
    AppCompatTextView txtCreateAlbum;

    private int RC_INTERNET = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_album);
        ButterKnife.bind(this);
    }

    @Override
    public void onSuccess(int tag, Response response) {

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }


    @OnClick(R.id.txtvw_create_album)
    public void changePassword() {
        try {
            if (Validation(editAlbumName.getText().toString(), editAlbumDesc.getText().toString())) {

                if (EasyPermissions.hasPermissions(this, android.Manifest.permission.INTERNET)) {
                    if (isConnectedToInternet()) {
                        //   showLoading();
                        // ServerAPI.getInstance().changePassword(APIServerResponse.CHANGE_PASSWORD, getUserSessionId(), edt_txt_old_password.getText().toString(), edt_txt_confirm_password.getText().toString(), this);
                        finish();
                    } else {
                        showSnack("Not connected to Internet ");
                    }
                } else {
                    EasyPermissions.requestPermissions(this, getString(R.string.internet_access),
                            RC_INTERNET, android.Manifest.permission.INTERNET);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean Validation(String albumName, String albumDesc) {
        if (albumName.isEmpty()) {
            showToast(Constants.ALBUM_NAME_NOT_BLENK, Toast.LENGTH_SHORT);
            return false;
        } else if (albumName.length() < 4) {
            showToast(Constants.ALBUM_NAME_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (albumDesc.isEmpty()) {
            showToast(Constants.ALBUM_DESC_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (albumDesc.length() < 4) {
            showToast(Constants.ALBUM_DESC_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (!albumDesc.matches(albumDesc)) {
            return false;
        }
        return true;


    }
}
