package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.PostDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.TimeLineModal;
import com.example.preetisharma.callforblessings.Server.Modal.UploadPostModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;
import com.example.preetisharma.callforblessings.demo.EditUserPostActivity;
import com.example.preetisharma.callforblessings.demo.SharePostActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/13/2017.
 */

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.DataViewHolder> {
    public List<TimeLineModal.ListBean.PostBean> list;
    Activity mContext;
    private static int countNext = 0;
    HashMap<String, String> hashmap = new HashMap<>();
    RadioButton rd_btn;
    UploadPostModal.DetailBean uploadPost;
    private String likescount, commentsCount;
    private static String liked = "not liked";
    private int userIDInt;
    @BindView(R.id.lnr_timeLine)
    LinearLayout lnr_timeLine;
    boolean isLoading = false, isMoreDataAvailable = true, loaderEnabled = false;

    public TimeLineAdapter(Activity mContext, List<TimeLineModal.ListBean.PostBean> list) {
        this.list = list;
        this.mContext = mContext;
        String userID = ((BaseActivity) mContext).getUserID();
        userIDInt = Integer.parseInt(userID);
    }


    public void updateAllItems(ArrayList<TimeLineModal.ListBean.PostBean> list) {
        this.list = list;
    }

    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<TimeLineModal.ListBean.PostBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.homescreen_item_adapter_layout, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }


    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;

    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        if (list != null) {

            try {
                holder.txtvw_user_name.setText(list.get(position).getPost_by().getProfile_details().getFirstname());
                holder.txtvw_time_stamp.setText("updated" + " " + list.get(position).getPosted_at());


                if (list.get(position).getPost_type().equals(Constants.PROFILE_PIC)) {
                    holder.tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                    holder.tv_changed_profile_cover_pic.setText(Constants.CHANGED_PROFILE_PIC);
                } else if (list.get(position).getPost_type().equals(Constants.COVER_PIC)) {
                    holder.tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                    holder.tv_changed_profile_cover_pic.setText(Constants.CHANGED_COVER_PIC);
                } else if (list.get(position).getPost_type().equals(Constants.SHARE)) {
                    holder.card_shared_post.setVisibility(View.VISIBLE);
                    holder.tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                    holder.tv_changed_profile_cover_pic.setText(Constants.SHARED);
                    Glide.with(mContext).load(list.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).thumbnail(0.1f).placeholder(R.drawable.ic_me).into(holder.img_vw_user_profile_shared);
                    holder.txtvw_user_name_shared.setText(list.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname());
                    holder.txtvw_time_stamp_shared.setText(list.get(position).getShared_post().get(0).getPosted_at());
                    holder.txtvw_post_details_shared.setText(list.get(position).getShared_post().get(0).getPost_content());
                    if (list.get(position).getShared_post().get(0).getMedia().size() != 0 && list.get(position).getShared_post().get(0).getMedia().get(0).getType().equalsIgnoreCase(Constants.IMAGE)) {
                        holder.viewholder_image_shared.setVisibility(View.VISIBLE);
                        Glide.with(mContext).load(list.get(position).getShared_post().get(0).getMedia().get(0).getFile()).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(holder.viewholder_image_shared);
                    }
                    if (list.get(position).getShared_post().get(0).getPost_type().equalsIgnoreCase(Constants.PROFILE_PIC)) {
                        holder.tv_changed_profile_cover_pic_shared.setVisibility(View.VISIBLE);
                        holder.tv_changed_profile_cover_pic_shared.setText(Constants.CHANGED_PROFILE_PIC);
                    } else if (list.get(position).getShared_post().get(0).getPost_type().equalsIgnoreCase(Constants.COVER_PIC)) {
                        holder.tv_changed_profile_cover_pic_shared.setVisibility(View.VISIBLE);
                        holder.tv_changed_profile_cover_pic_shared.setText(Constants.CHANGED_COVER_PIC);
                    } else {
                        holder.tv_changed_profile_cover_pic_shared.setVisibility(View.GONE);
                        //holder.tv_changed_profile_cover_pic_shared.setText(Constants.SHARED);
                    }
                } else {
                    holder.card_shared_post.setVisibility(View.GONE);
                    holder.tv_changed_profile_cover_pic.setVisibility(View.GONE);
                }

                if (list.get(position).getPost_by().getId() == userIDInt && !list.get(position).getPost_type().equals(Constants.COVER_PIC)) {
                    holder.iv_more_options.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_more_options.setVisibility(View.GONE);
                }

                if (list.get(position).getPost_by().getId() == userIDInt && !list.get(position).getPost_type().equals(Constants.PROFILE_PIC)) {
                    holder.iv_more_options.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_more_options.setVisibility(View.GONE);
                }


                if (list.get(position).getLike_count().equals("0") || list.get(position).getLike_count().equals("1")) {
                    holder.txtvw_no_of_likes.setText(list.get(position).getLike_count() + " " + "Like");
                } else {
                    holder.txtvw_no_of_likes.setText(list.get(position).getLike_count() + " " + "Likes");

                }
                if (list.get(position).getComment_count().equals("0") || list.get(position).getComment_count().equals("1")) {
                    holder.txtvw_no_of_comments.setText(list.get(position).getComment_count() + " " + "Comment");
                } else {
                    holder.txtvw_no_of_comments.setText(list.get(position).getComment_count() + " " + "Comments");

                }

                if (list.get(position).getLike_flag().equalsIgnoreCase("Not liked")) {
                    int imgResource = R.drawable.prayer_like_icon;
                    holder.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.ic_liked;
                    holder.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }
                holder.txtvw_post_details.setText(stripText(list.get(position).getPost_content()));
                Glide.with(mContext).load(list.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).into(holder.img_vw_user_profile);

                if (list.get(position).getMedia().size() > 0) {

                    if (list.get(position).getMedia().get(0).getType().equalsIgnoreCase("Image")) {
                        holder.viewholder_image.setVisibility(View.VISIBLE);
                        Glide.with(mContext).load(stripHtml(list.get(holder.getAdapterPosition()).getMedia().get(0).getFile().toString())).override(300, 300).thumbnail(0.1f)/*.placeholder(R.drawable.placeholder)*/.into(holder.viewholder_image);

                    } else {
                        holder.viewholder_image.setVisibility(View.GONE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public String stripText(String text) {

        String regexp = "<p>.*?</p>";
        String replace = "";
        String finalText = text.replaceAll(regexp, replace);
        return finalText.replaceAll("<img.+?>", "");

    }
    /*public String stripText(String text) {
       *//* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH);
        }*//*

        String regexp = "<p>.*?</p>";
        String replace = "";
        return text.replaceAll(regexp, replace);


    }*/

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;
        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;
        @BindView(R.id.viewholder_image)
        AppCompatImageView viewholder_image;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;
        @BindView(R.id.tv_changed_profile_cover_pic)
        AppCompatTextView tv_changed_profile_cover_pic;
        //Shared
        //Shared Post
        @BindView(R.id.card_shared_post)
        CardView card_shared_post;
        @BindView(R.id.img_vw_user_profile_shared)
        AppCompatImageView img_vw_user_profile_shared;
        @BindView(R.id.txtvw_user_name_shared)
        AppCompatTextView txtvw_user_name_shared;
        @BindView(R.id.txtvw_time_stamp_shared)
        AppCompatTextView txtvw_time_stamp_shared;
        @BindView(R.id.txtvw_post_details_shared)
        AppCompatTextView txtvw_post_details_shared;
        @BindView(R.id.viewholder_image_shared)
        AppCompatImageView viewholder_image_shared;

        @BindView(R.id.tv_changed_profile_cover_pic_shared)
        AppCompatTextView tv_changed_profile_cover_pic_shared;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.iv_more_options)
        public void deleteeditPopup() {

            try {
                createPopupMenuOption();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.txtvw_like)
        public void likePost() {
            {
                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                liked = list.get(getAdapterPosition()).getLike_flag();
                likescount = list.get(getAdapterPosition()).getLike_count();

                if (liked.equalsIgnoreCase("Not Liked")) {

                    if (((BaseActivity) mContext).isConnectedToInternet()) {

                        ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), "POST", this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }

                } else {

                    if (((BaseActivity) mContext).isConnectedToInternet()) {

                        ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), "POST", this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            }
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {

            /*if (txtvw_share.isEnabled()) {
                txtvw_share.setEnabled(false);
            }
*/
            if (list.get(getAdapterPosition()).getMedia().size() > 0) {
                if (list.get(getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Image")) {
                    openConfirmationDialog(String.valueOf(list.get(getAdapterPosition()).getId()), list.get(getAdapterPosition()).getPost_content(), list.get(getAdapterPosition()).getPosted_at(), list.get(getAdapterPosition()).getMedia().get(0).getFile(), "", "");
                } else if (list.get(getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Video")) {
                    openConfirmationDialog(String.valueOf(list.get(getAdapterPosition()).getId()), list.get(getAdapterPosition()).getPost_content(), list.get(getAdapterPosition()).getPosted_at(), "", list.get(getAdapterPosition()).getMedia().get(0).getFile(), list.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());

                }
            } else {
                openConfirmationDialog(String.valueOf(list.get(getAdapterPosition()).getId()), list.get(getAdapterPosition()).getPost_content(), list.get(getAdapterPosition()).getPosted_at(), "", "", "");

            }

        }


        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu

                PopupMenu popup = new PopupMenu(mContext, iv_more_options);

                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) mContext).isConnectedToInternet()) {
                                    ((BaseActivity) mContext).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), DataViewHolder.this);
                                } else {
                                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(mContext, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(list.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, list.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + list.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, list.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(list.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, list.get(getAdapterPosition()).getPost_type());
                                if (list.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(list.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }

                                i.putExtras(b);
                                mContext.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.txtvw_comment)
        public void commentPost() {


            Intent i = new Intent(mContext, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(list.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            i.putExtras(b);
            mContext.startActivity(i);


        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                LikeModal likeModal;
                ShareModal shareModal;
                PostDeleteModal postDeleteModal;
                CommentModal commentModal;
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    switch (tag) {
                        case APIServerResponse.LIKE:

                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like.isEnabled()) {
                                    txtvw_like.setEnabled(true);
                                }
                                int imgResource = R.drawable.ic_liked;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue++;
                                list.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));
                                list.get(getAdapterPosition()).setLike_flag("liked");
                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                                } else {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                                }
                            }

                            break;
                        case APIServerResponse.UNLIKE:
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like.isEnabled()) {
                                    txtvw_like.setEnabled(true);
                                }
                                int imgResource = R.drawable.prayer_like;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue--;
                                liked = "not liked";
                                list.get(getAdapterPosition()).setLike_flag("not liked");
                                list.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                                } else {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                                }
                            }
                            break;
                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }

                            break;
                        case APIServerResponse.COMMENT:

                            commentModal = (CommentModal) response.body();
                            if (commentModal.getStatus().equals("1")) {
                                /*shareModal.getDetail().*/
                                ((BaseActivity) mContext).showToast("Success", Toast.LENGTH_SHORT);
                                notifyDataSetChanged();
                            }

                            break;

                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        public void refresh(int position) {
            Log.e("Refreshed Position", "Pos" + position);
            list.remove(position);
            notifyItemRemoved(position);
        }

        Dialog dialog;

        public void openConfirmationDialog(String postId, String post_text, String timeStamp, String post_image, String postVideoUrl, String postVideoThumbnail) {

            Intent shareDialog = new Intent(mContext, SharePostActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.POSTTYPE, "POST");
            b.putString(Constants.POSTIMAGE, post_image);
            b.putString(Constants.POSTTEXT, post_text);
            b.putString(Constants.POSTTIMESTAMP, timeStamp);
            b.putString(Constants.POSTID, postId);
            b.putString(Constants.POSTVIDEO, postVideoUrl);
            b.putString(Constants.POSTTHUMBNAIL, postVideoThumbnail);
            shareDialog.putExtras(b);
            mContext.startActivity(shareDialog);


        }


        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.LIKE:
                    System.out.println("Error");
                    break;
                case APIServerResponse.SHARE:
                    System.out.println("Error");
                    break;
            }
        }

       /* txtvw_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServerAPI.getInstance().share(APIServerResponse.SHARE, new BaseActivity().getUserSessionId(), String.valueOf(list.get(position).getId()), "POST", "Shared Text", DataViewHolder);
            }
        });*/
    }

    public void notifyChange(List<TimeLineModal.ListBean.PostBean> mlist) {
        list = mlist;
        notifyDataSetChanged();
    }
}
