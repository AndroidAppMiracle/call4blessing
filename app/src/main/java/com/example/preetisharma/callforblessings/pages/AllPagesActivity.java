package com.example.preetisharma.callforblessings.pages;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.callforblessings.R;

public class AllPagesActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_pages);

    }
}
