package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.FilterModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.UpdateGroupParcebleModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/5/2017.
 */

public class CreateGroupAddFriendsAdapter extends RecyclerView.Adapter<CreateGroupAddFriendsAdapter.DataViewHolder> {


    private Context mContext;
    private List<MyFriendsModal.ListBean> myFriends = new ArrayList<>();
    /*private boolean checkedStatus = false;*/
    private FilterModal modal;
    //private ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> taggedFriendsDemo = new ArrayList<>();
    private ArrayList<MyFriendsModal.ListBean.UserInfoBean> taggedFriendsIds = new ArrayList<>();
    private List<MyFriendsModal.ListBean> myFriendsAll = new ArrayList<>();
    private ArrayList<UpdateGroupParcebleModal> groupMembers = new ArrayList<>();
    //private ArrayList<UpdateGroupParcebleModal> allFriends = new ArrayList<>();

    public CreateGroupAddFriendsAdapter(Context mContext, List<MyFriendsModal.ListBean> list) {
        this.myFriends = list;
        this.mContext = mContext;
        this.myFriendsAll.addAll(myFriends);
    }


    @Override
    public CreateGroupAddFriendsAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_create_group_add_friends_layout, parent, false);
        CreateGroupAddFriendsAdapter.DataViewHolder dataView = new CreateGroupAddFriendsAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }


    @Override
    public void onBindViewHolder(final CreateGroupAddFriendsAdapter.DataViewHolder holder, int position) {

        try {

            holder.txtvw_friends_request_name.setText(myFriends.get(position).getUser_info().getProfile_details().getFirstname() + " " + myFriends.get(position).getUser_info().getProfile_details().getLastname());


            holder.checkbox_select_friend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        Log.i("isChecked", "Checked");
                        taggedFriendsIds.add(myFriends.get(holder.getAdapterPosition()).getUser_info());

                    } else {
                        Log.i("isChecked", "notChecked");
                        taggedFriendsIds.remove(myFriends.get(holder.getAdapterPosition()).getUser_info());
                    }


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* public ArrayList<FilterModal> unselectList() {
        FilterModal filterModal = new FilterModal();
        filterModal.setFriendsArray(myTaggedFriends);
        return myTaggedFriends;

    }*/

    /*public ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> selectedList() {

        ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> list = new ArrayList<>();
        list = taggedFriendsDemo;
        return list;
    }*/

    public ArrayList<MyFriendsModal.ListBean.UserInfoBean> selectedListIds() {
        ArrayList<MyFriendsModal.ListBean.UserInfoBean> list = new ArrayList<>();
        list = taggedFriendsIds;
        return list;
    }

    @Override
    public int getItemCount() {
        if (myFriends.size() != 0) {
            return myFriends.size();
        } else {
            return 0;
        }

    }


    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;
        @BindView(R.id.checkbox_select_friend)
        AppCompatCheckBox checkbox_select_friend;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @Override
        public void onSuccess(int tag, Response response) {


        }

        @Override
        public void onError(int tag, Throwable throwable) {

        }
    }


    public void notifyDataList(List<MyFriendsModal.ListBean> list) {
        Log.d("notifyData ", list.size() + "");
        this.myFriends = list;
        notifyDataSetChanged();
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        myFriends.clear();
        if (charText.length() == 0) {
            myFriends.addAll(myFriendsAll);
        } else {
            for (MyFriendsModal.ListBean wp : myFriendsAll) {
                if (wp.getUser_info().getProfile_details().getFirstname().toLowerCase(Locale.getDefault()).contains(charText)) {
                    myFriends.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }


}
