package com.example.preetisharma.callforblessings.joymusicplayer;

import com.devbrackets.android.playlistcore.manager.IPlaylistItem;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumListingModal;

/**
 * A custom {@link IPlaylistItem}
 * to hold the information pertaining to the audio and video items
 */
public class MediaItem implements IPlaylistItem {

    private PlayMusicModal files;
    boolean isAudio;

    /*"http://www.sylviacuenca.com/images/no_album_art.jpg"*/

    public MediaItem(PlayMusicModal files, boolean isAudio) {
        this.isAudio = isAudio;
        this.files = files;
    }


    @Override
    public long getId() {
        return 0;
    }

    @Override
    public long getPlaylistId() {
        return 0;
    }

    @Override
    public int getMediaType() {
        return isAudio ? PlaylistManager.AUDIO : PlaylistManager.VIDEO;
    }

    @Override
    public String getMediaUrl() {
        return files.getMediaUrl();
    }

    @Override
    public String getDownloadedMediaUri() {
        return files.isDownloaded();
    }

    @Override
    public String getThumbnailUrl() {
        return files.getThumbnailUrl();
    }

    @Override
    public String getArtworkUrl() {
        return files.getArtworkUrl();
    }

    @Override
    public String getTitle() {
        return files.getTitle();
    }

    @Override
    public String getAlbum() {
        return files.getAlbum();
    }

    @Override
    public String getArtist() {
        return files.getAlbum();
    }
}