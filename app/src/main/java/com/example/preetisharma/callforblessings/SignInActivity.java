package com.example.preetisharma.callforblessings;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.LoginModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.DataHolder;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;


public class SignInActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks, APIServerResponse {
    @BindView(R.id.edt_txt_email)
    AppCompatEditText edt_txt_email;
    @BindView(R.id.edt_txt_password)
    AppCompatEditText edt_txt_password;
    @BindView(R.id.txtvw_login)
    AppCompatTextView txtvw_login;
    @BindView(R.id.txtvw_forgot)
    AppCompatTextView txtvw_forgot;
    private String device_id;
    private int RC_INTERNET = 101;
    private SharedPreferences userSession;
    private SharedPreferences.Editor editSession;
    TimeZone tz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
       /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        updateStatusBar();
        ButterKnife.bind(this);
        Calendar cal = Calendar.getInstance();
        tz = cal.getTimeZone();
        txtvw_forgot.setTextColor(getResources().getColorStateList(R.color.textview_selector));

    }

    @OnClick(R.id.txtvw_forgot)
    public void forgotPassword() {
        Intent forgotPassword = new Intent(this, ForgotPassword.class);
        startActivity(forgotPassword);
    }

    @OnClick(R.id.txtvw_login)
    public void login() {
        Log.e("time zone", "time zone" + tz.getDisplayName() + "id tz" + tz.getID());

        if (Validation(edt_txt_email.getText().toString().trim(), edt_txt_password.getText().toString())) {
            //   device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            /*device_id = getUserFcmToken();*/
            device_id = getFirebaseToken();
            Log.e("Device id", "device id" + device_id);
            if (EasyPermissions.hasPermissions(this, Manifest.permission.INTERNET)) {
                if (isConnectedToInternet()) {
                    if (device_id == null) {
                        showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    } else {
                        showLoading();
                        hideKeyboard();
                        ServerAPI.getInstance().login(APIServerResponse.LOGIN, edt_txt_email.getText().toString().trim(), edt_txt_password.getText().toString(), getString(R.string.Device_type), device_id, tz.getID(), this);
                    }
                } else {
                    showSnack("Not connected to Internet ");
                }
            } else {
                EasyPermissions.requestPermissions(this, getString(R.string.internet_access),
                        RC_INTERNET, Manifest.permission.INTERNET);
            }

        }
    }

    public boolean Validation(String email, String password) {
        if (email.isEmpty()) {
            showToast(Constants.EMAIL_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (Character.isDigit(email.charAt(0))) {
            showToast(Constants.EMAIL_START, Toast.LENGTH_SHORT);
            return false;
        } else if (!isValidEmail(email)) {
            showToast(Constants.EMAIL_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (password.isEmpty()) {
            showToast(Constants.PASSWORD_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (password.length() < 6) {
            showToast(Constants.PASSWORD_LENGTH, Toast.LENGTH_SHORT);
            return false;
        }
        return true;


    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().login(APIServerResponse.LOGIN, edt_txt_email.getText().toString().trim(), edt_txt_password.getText().toString(), getString(R.string.Device_type), device_id, tz.getID(), this);

        } else {
            showToast("Not connected to Internet ", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dismissDialog();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        LoginModal loginModal;
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.LOGIN:

                        loginModal = (LoginModal) response.body();


                        if (loginModal.getStatus().equals("1")) {
                            DataHolder.setFirst_name(loginModal.getDetail().getProfile_details().getFirstname());
                            DataHolder.setLast_name(loginModal.getDetail().getProfile_details().getLastname());
                            DataHolder.setGender(loginModal.getDetail().getProfile_details().getGender());
                            DataHolder.setPhone_number(loginModal.getDetail().getProfile_details().getPhone());
                            DataHolder.setEmail(loginModal.getDetail().getEmail());
                            DataHolder.setSession_key(loginModal.getSession_key());
                            DataHolder.setDate_of_birth(loginModal.getDetail().getProfile_details().getD_o_b());
                            getmPrefs();
                            setUserLoggedIn(true);
                            setUserID(String.valueOf(loginModal.getDetail().getId()));
                            setUserSessionId(loginModal.getSession_key());
                            setFirstName(loginModal.getDetail().getProfile_details().getFirstname());
                            setLastName(loginModal.getDetail().getProfile_details().getLastname());
                            setUserEmailID(loginModal.getDetail().getEmail());
                            setFullName(loginModal.getDetail().getProfile_details().getFirstname() + " " + loginModal.getDetail().getProfile_details().getLastname());
                            setUserGender(loginModal.getDetail().getProfile_details().getGender());
                            setUserPhoneNumber(loginModal.getDetail().getProfile_details().getPhone());
                            setUserImage(loginModal.getDetail().getProfile_details().getProfile_pic());
                            setUserCoverImage(loginModal.getDetail().getProfile_details().getCover_pic());
                            Intent intent = new Intent(this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else if (loginModal.getStatus().equals("0")) {
                            hideLoading();

                            showToast(loginModal.getMessage(), Toast.LENGTH_SHORT);
                        }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
        switch (tag) {
            case APIServerResponse.LOGIN:
                System.out.println("Error");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
