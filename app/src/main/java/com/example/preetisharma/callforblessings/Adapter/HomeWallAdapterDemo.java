/*
package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.preetisharma.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.HomeWallModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.UploadPostModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.HetrogeneousView;
import com.example.preetisharma.callforblessings.ViewHolder.ImageViewHolder;
import com.example.preetisharma.callforblessings.ViewHolder.TextViewHolder;
import com.example.preetisharma.callforblessings.ViewHolder.VideoViewHolder;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

*/
/**
 * Created by preeti.sharma on 1/13/2017.
 *//*


public class HomeWallAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<HomeWallModal.PostsBean> list;
    Activity mContext;
    private static int countNext = 0;
    HashMap<String, String> hashmap = new HashMap<>();
    RadioButton rd_btn;
    UploadPostModal.DetailBean uploadPost;

    public HomeWallAdapter(Activity mContext, List<HomeWallModal.PostsBean> list) {
        this.list = list;
        this.mContext = mContext;
    }

    public HomeWallAdapter(Activity mContext, UploadPostModal.DetailBean uploadPost) {
        this.uploadPost = uploadPost;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position).getMedia().size() == 0) {
            return HetrogeneousView.textView;
        } else {
            for (int i = 0; i < list.get(position).getMedia().size(); i++)
                if (list.get(position).getMedia().get(i).getType().equalsIgnoreCase("Image")) {
                    return HetrogeneousView.imageView;
                } else {
                    return HetrogeneousView.videoView;
                }
        }
        return -1;
    }


    public void updateAllItems(List<HomeWallModal.PostsBean> list) {
        this.list = list;
        Log.e("List size", "size" + list.size());
    }

    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<HomeWallModal.PostsBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
       // Constants.setAppFont((ViewGroup) inflater, parent.getContext());
        switch (viewType) {
            case HetrogeneousView.textView:
                View v1 = inflater.inflate(R.layout.textview_item, parent, false);
                viewHolder = new TextViewHolder(v1);

                break;
            case HetrogeneousView.imageView:
                View v2 = inflater.inflate(R.layout.image_item, parent, false);
                viewHolder = new ImageViewHolder(v2);
                break;
            case HetrogeneousView.videoView:
                View v3 = inflater.inflate(R.layout.webview_item, parent, false);
                viewHolder = new VideoViewHolder(v3);
                break;

        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (list != null) {

            switch (holder.getItemViewType()) {
                case HetrogeneousView.textView:
                    TextViewHolder vh1 = (TextViewHolder) holder;
                    configureDefaultViewHolder(vh1, position);

                    break;
                case HetrogeneousView.imageView:
                    ImageViewHolder vh2 = (ImageViewHolder) holder;
                    configureViewHolder1(vh2, position);
                    break;
            }
        }

    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    private void configureDefaultViewHolder(TextViewHolder vh, int position) {
        vh.getTxtpost().setText(list.get(position).getPost_content().toString());
    }

    private void configureViewHolder1(ImageViewHolder vh1, int position) {
        Glide.with(mContext).load(list.get(position).getMedia().get(0).getFile()).centerCrop().placeholder(R.drawable.placeholder).into(vh1.getImageView());


    }

   */
/* private void configureViewHolder2( vh2) {
        vh2.getImageView().setImageResource(R.drawable.sample_golden_gate);
    }*//*


    public class ViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;
        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;
       */
/* @BindView(R.id.txtvw_amen)
        AppCompatTextView txtvw_amen;*//*


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.txtvw_like)
        public void likePost() {
            ((BaseActivity) mContext).showLoading();
            ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), "POST", ViewHolder.this);
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {

            //Make a shareDialog
            ServerAPI.getInstance().share(APIServerResponse.SHARE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), "POST", "Shared Text", ViewHolder.this);

        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                LikeModal likeModal;
                ShareModal shareModal;
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    switch (tag) {
                        case APIServerResponse.LIKE:

                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast("Post Liked", Toast.LENGTH_SHORT);
                                int imgResource = R.drawable.ic_liked;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);

                            }

                            break;
                        case APIServerResponse.SHARE:

                            shareModal = (ShareModal) response.body();
                            if (shareModal.getStatus().equals("1")) {
                                */
/*shareModal.getDetail().*//*

                                ((BaseActivity) mContext).showToast("Shared successfully", Toast.LENGTH_SHORT);
                            }

                            break;

                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.LIKE:
                    System.out.println("Error");
                    break;
                case APIServerResponse.SHARE:
                    System.out.println("Error");
                    break;
            }
        }

       */
/* txtvw_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServerAPI.getInstance().share(APIServerResponse.SHARE, new BaseActivity().getUserSessionId(), String.valueOf(list.get(position).getId()), "POST", "Shared Text", DataViewHolder);
            }
        });*//*

    }
}
*/
