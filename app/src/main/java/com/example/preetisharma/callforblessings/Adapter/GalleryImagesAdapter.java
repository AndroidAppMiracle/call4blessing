package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.GallerycompleteImage;
import com.example.preetisharma.callforblessings.Server.Modal.GalleryModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 2/9/2017.
 */

public class GalleryImagesAdapter extends RecyclerView.Adapter<GalleryImagesAdapter.DataViewHolder> {
    public ArrayList<GalleryModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;

    public GalleryImagesAdapter(Activity mContext, ArrayList<GalleryModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(ArrayList<GalleryModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public GalleryImagesAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_item, parent, false);
        GalleryImagesAdapter.DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final GalleryImagesAdapter.DataViewHolder holder, final int position) {
        if (list.get(position).getFile().equals("")) {
            ((BaseActivity) mContext).showToast("No Images Found", Toast.LENGTH_SHORT);

        } else {
            Glide.with(mContext).load(list.get(position).getFile()).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).centerCrop().into(holder.img_vw_image);
            holder.img_vw_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent image = new Intent(mContext, GallerycompleteImage.class);
                    Bundle imageUri = new Bundle();
                    imageUri.putString("image", list.get(position).getFile());
                    image.putExtras(imageUri);
                    mContext.startActivity(image);
                }
            });
        }
    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_vw_image)
        AppCompatImageView img_vw_image;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }
    }
}
