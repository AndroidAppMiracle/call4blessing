package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSearchModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionCheckModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.GroupWallHomeScreen;
import com.example.preetisharma.callforblessings.demo.JoyMusicSubscriptionActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/6/2017.
 */

public class GroupHomeWallPostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<GroupHomeWallModal.DetailBean> list;
    Activity mContext;
    private final int IMAGE = 2, VIDEO = 3, TEXT = 1;


    public GroupHomeWallPostsAdapter(Activity mContext, List<GroupHomeWallModal.DetailBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


 /*   public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }*/

  /*  public void addAllItems(List<MyBooksModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }*/


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        RecyclerView.ViewHolder dataView = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case IMAGE:
                v = inflater.inflate(R.layout.group_adapter_post_image_layout, parent, false);
                dataView = new GroupHomeWallPostsAdapter.DataViewHolderImage(v);
                Constants.setAppFont((ViewGroup) v, parent.getContext());

                break;
            case VIDEO:
                v = inflater.inflate(R.layout.group_adapter_post_video_layout, parent, false);
                dataView = new GroupHomeWallPostsAdapter.DataViewHolderVideo(v);
                Constants.setAppFont((ViewGroup) v, parent.getContext());

                break;
            case TEXT:
                v = inflater.inflate(R.layout.group_adapter_post_text_layout, parent, false);
                dataView = new GroupHomeWallPostsAdapter.DataViewHolderText(v);
                Constants.setAppFont((ViewGroup) v, parent.getContext());

                break;

        }

        return dataView;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {

            case IMAGE:

                DataViewHolderImage dataViewHolderImage = (DataViewHolderImage) holder;

                dataViewHolderImage.txtvw_user_name.setText(list.get(position).getGroup_name());
                dataViewHolderImage.txtvw_time_stamp.setText(list.get(position).getPost().get(position).getPosted_at());
                dataViewHolderImage.txtvw_post_details.setText(list.get(position).getPost().get(position).getPost_content());
                dataViewHolderImage.txtvw_no_of_likes.setText(list.get(position).getPost().get(position).getLike_count());
                dataViewHolderImage.txtvw_no_of_comments.setText(list.get(position).getPost().get(position).getComment_count());
                Glide.with(mContext).load(list.get(position).getPost().get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderImage.img_vw_user_profile);

                Glide.with(mContext).load(list.get(position).getPost().get(position).getMedia().get(0).getFile()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderImage.iv_grou_post_image);

                if (list.get(position).getPost().get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderImage.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderImage.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                break;

            case VIDEO:

                DataViewHolderVideo dataViewHolderVideo = (DataViewHolderVideo) holder;

                dataViewHolderVideo.txtvw_user_name.setText(list.get(position).getGroup_name());

                dataViewHolderVideo.txtvw_user_name.setText(list.get(position).getGroup_name());
                dataViewHolderVideo.txtvw_time_stamp.setText(list.get(position).getPost().get(position).getPosted_at());
                dataViewHolderVideo.txtvw_post_details.setText(list.get(position).getPost().get(position).getPost_content());
                dataViewHolderVideo.txtvw_no_of_likes.setText(list.get(position).getPost().get(position).getLike_count());
                dataViewHolderVideo.txtvw_no_of_comments.setText(list.get(position).getPost().get(position).getComment_count());
                Glide.with(mContext).load(list.get(position).getPost().get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderVideo.img_vw_user_profile);

                Glide.with(mContext).load(list.get(position).getPost().get(position).getMedia().get(0).getThumbnail_file()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderVideo.iv_group_video_post);

                if (list.get(position).getPost().get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderVideo.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderVideo.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }

                break;

            case TEXT:

                DataViewHolderText dataViewHolderText = (DataViewHolderText) holder;

                dataViewHolderText.txtvw_user_name.setText(list.get(position).getGroup_name());

                dataViewHolderText.txtvw_user_name.setText(list.get(position).getGroup_name());
                dataViewHolderText.txtvw_time_stamp.setText(list.get(position).getPost().get(position).getPosted_at());
                dataViewHolderText.txtvw_post_details.setText(list.get(position).getPost().get(position).getPost_content());
                dataViewHolderText.txtvw_no_of_likes.setText(list.get(position).getPost().get(position).getLike_count());
                dataViewHolderText.txtvw_no_of_comments.setText(list.get(position).getPost().get(position).getComment_count());
                Glide.with(mContext).load(list.get(position).getPost().get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderText.img_vw_user_profile);


                if (list.get(position).getPost().get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderText.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderText.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                break;

        }


    }

    @Override
    public int getItemViewType(int position) {

        int returnType = 1;

        if (list.get(position).getPost().get(position).getMedia() != null && !list.get(position).getPost().get(position).getMedia().get(0).getFile().equalsIgnoreCase("")) {
            if (list.get(position).getPost().get(position).getMedia().get(0).getType().equalsIgnoreCase(Constants.IMAGE)) {
                returnType = IMAGE;
            } else if (list.get(position).getPost().get(position).getMedia().get(0).getType().equalsIgnoreCase(Constants.VIDEO)) {
                returnType = VIDEO;
            }
        } else {
            returnType = TEXT;

        }


        return returnType;
    }

    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }


    public class DataViewHolderText extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;


        public DataViewHolderText(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                            ((BaseActivity) mContext).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    public class DataViewHolderImage extends RecyclerView.ViewHolder implements APIServerResponse {


        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        @BindView(R.id.iv_grou_post_image)
        AppCompatImageView iv_grou_post_image;


        public DataViewHolderImage(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                            joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                            } else {
                                Intent player = new Intent(mContext, JoyMusicSubscriptionActivity.class);
                                mContext.startActivity(player);
                            }
                            ((BaseActivity) mContext).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    public class DataViewHolderVideo extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        @BindView(R.id.iv_group_video_post)
        AppCompatImageView iv_group_video_post;

        @BindView(R.id.iv_group_video_play)
        AppCompatImageView iv_group_video_play;


        public DataViewHolderVideo(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                            joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                            } else {
                                Intent player = new Intent(mContext, JoyMusicSubscriptionActivity.class);
                                mContext.startActivity(player);
                            }
                            ((BaseActivity) mContext).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }
}


