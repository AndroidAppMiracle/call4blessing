package com.example.preetisharma.callforblessings.settings;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 9/20/2017.
 */

public class SettingsBlock extends BaseActivity {


    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_block);
        ButterKnife.bind(this);

        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Block List");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
