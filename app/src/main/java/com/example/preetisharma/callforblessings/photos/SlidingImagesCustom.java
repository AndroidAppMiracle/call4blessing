package com.example.preetisharma.callforblessings.photos;

import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.TouchImageSlidingView;

import java.util.ArrayList;

/**
 * Created by satoti.garg on 8/3/2017.
 */

public class SlidingImagesCustom extends PagerAdapter {
    private ArrayList<DemoDataModal> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    private ProgressBar progressBar;
    // private AQuery aQuery;


    public SlidingImagesCustom(Context context, ArrayList<DemoDataModal> IMAGES, ProgressBar progressBar) {
        this.context = context;
        //aQuery = new AQuery(context);
        this.IMAGES = IMAGES;
        this.progressBar = progressBar;
        Log.e("Images ", "Images" + IMAGES.get(0));
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages, view, false);

        assert imageLayout != null;
        final TouchImageSlidingView imageView = (TouchImageSlidingView) imageLayout
                .findViewById(R.id.image);
        // aQuery.id(imageView).image(IMAGES.get(position), true, true, 0, 0, null, AQuery.FADE_IN);
        //Picasso.with(context).load(Uri.parse(IMAGES.get(position))).resize(100, 100).into(imageView);

        Glide.with(context)
                .load(Uri.parse(IMAGES.get(position).getImage()))
                .listener(new RequestListener<Uri, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                }).thumbnail(0.1f).centerCrop()
                .into(imageView);

        //imageView.setMaxZoom(4f);
        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
