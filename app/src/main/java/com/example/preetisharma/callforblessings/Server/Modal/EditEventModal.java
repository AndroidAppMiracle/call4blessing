package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 2/10/2017.
 */

public class EditEventModal {

    /**
     * status : 1
     * detail : {"id":34,"event_name":"Testing update","event_desc":"Get Together at the church","event_location":"Chandigarh","event_date":"1970-01-01 06:03:37","event_photo":"http://dev.miracleglobal.com/cal-php/web/uploads/event/1486665837Lighthouse.jpg","user_info":{"id":20,"username":"plash12333","email":"plash12333@abc.com","updated":1,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"},"is_friend":0,"request_respond":"NO"},"invites":[{"event_id":34,"user_to":{"id":68,"username":"kamal","email":"kamal@yopmail.com","updated":1,"profile_details":{"firstname":"kamal","lastname":"singh","d_o_b":"1991-08-13","phone":"9856789567","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486431354image.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1486573639image.jpg"},"is_friend":0,"request_respond":"NO"},"user_from":{"id":20,"username":"plash12333","email":"plash12333@abc.com","updated":1,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"},"is_friend":0,"request_respond":"NO"},"invite_status":"ACCEPTED"}],"is_invited":"NO","invitation_status":"FALSE","all_count":"1","accepted_count":"1","pending_count":"0"}
     * message : Event updated successfully
     */

    private String status;
    private DetailBean detail;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class DetailBean {
        /**
         * id : 34
         * event_name : Testing update
         * event_desc : Get Together at the church
         * event_location : Chandigarh
         * event_date : 1970-01-01 06:03:37
         * event_photo : http://dev.miracleglobal.com/cal-php/web/uploads/event/1486665837Lighthouse.jpg
         * user_info : {"id":20,"username":"plash12333","email":"plash12333@abc.com","updated":1,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"},"is_friend":0,"request_respond":"NO"}
         * invites : [{"event_id":34,"user_to":{"id":68,"username":"kamal","email":"kamal@yopmail.com","updated":1,"profile_details":{"firstname":"kamal","lastname":"singh","d_o_b":"1991-08-13","phone":"9856789567","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486431354image.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1486573639image.jpg"},"is_friend":0,"request_respond":"NO"},"user_from":{"id":20,"username":"plash12333","email":"plash12333@abc.com","updated":1,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"},"is_friend":0,"request_respond":"NO"},"invite_status":"ACCEPTED"}]
         * is_invited : NO
         * invitation_status : FALSE
         * all_count : 1
         * accepted_count : 1
         * pending_count : 0
         */

        private int id;
        private String event_name;
        private String event_desc;
        private String event_location;
        private String event_date;
        private String event_photo;
        private UserInfoBean user_info;
        private String is_invited;
        private String invitation_status;
        private String all_count;
        private String accepted_count;
        private String pending_count;
        private List<InvitesBean> invites;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getEvent_name() {
            return event_name;
        }

        public void setEvent_name(String event_name) {
            this.event_name = event_name;
        }

        public String getEvent_desc() {
            return event_desc;
        }

        public void setEvent_desc(String event_desc) {
            this.event_desc = event_desc;
        }

        public String getEvent_location() {
            return event_location;
        }

        public void setEvent_location(String event_location) {
            this.event_location = event_location;
        }

        public String getEvent_date() {
            return event_date;
        }

        public void setEvent_date(String event_date) {
            this.event_date = event_date;
        }

        public String getEvent_photo() {
            return event_photo;
        }

        public void setEvent_photo(String event_photo) {
            this.event_photo = event_photo;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public String getIs_invited() {
            return is_invited;
        }

        public void setIs_invited(String is_invited) {
            this.is_invited = is_invited;
        }

        public String getInvitation_status() {
            return invitation_status;
        }

        public void setInvitation_status(String invitation_status) {
            this.invitation_status = invitation_status;
        }

        public String getAll_count() {
            return all_count;
        }

        public void setAll_count(String all_count) {
            this.all_count = all_count;
        }

        public String getAccepted_count() {
            return accepted_count;
        }

        public void setAccepted_count(String accepted_count) {
            this.accepted_count = accepted_count;
        }

        public String getPending_count() {
            return pending_count;
        }

        public void setPending_count(String pending_count) {
            this.pending_count = pending_count;
        }

        public List<InvitesBean> getInvites() {
            return invites;
        }

        public void setInvites(List<InvitesBean> invites) {
            this.invites = invites;
        }

        public static class UserInfoBean {
            /**
             * id : 20
             * username : plash12333
             * email : plash12333@abc.com
             * updated : 1
             * profile_details : {"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"}
             * is_friend : 0
             * request_respond : NO
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : Plash
                 * lastname : Jindal
                 * d_o_b : 1992-08-29
                 * phone : 212156464545
                 * gender : MALE
                 * location :
                 * country :
                 * state :
                 * cover_pic : http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg
                 * profile_pic : http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }

        public static class InvitesBean {
            /**
             * event_id : 34
             * user_to : {"id":68,"username":"kamal","email":"kamal@yopmail.com","updated":1,"profile_details":{"firstname":"kamal","lastname":"singh","d_o_b":"1991-08-13","phone":"9856789567","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486431354image.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1486573639image.jpg"},"is_friend":0,"request_respond":"NO"}
             * user_from : {"id":20,"username":"plash12333","email":"plash12333@abc.com","updated":1,"profile_details":{"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"},"is_friend":0,"request_respond":"NO"}
             * invite_status : ACCEPTED
             */

            private int event_id;
            private UserToBean user_to;
            private UserFromBean user_from;
            private String invite_status;

            public int getEvent_id() {
                return event_id;
            }

            public void setEvent_id(int event_id) {
                this.event_id = event_id;
            }

            public UserToBean getUser_to() {
                return user_to;
            }

            public void setUser_to(UserToBean user_to) {
                this.user_to = user_to;
            }

            public UserFromBean getUser_from() {
                return user_from;
            }

            public void setUser_from(UserFromBean user_from) {
                this.user_from = user_from;
            }

            public String getInvite_status() {
                return invite_status;
            }

            public void setInvite_status(String invite_status) {
                this.invite_status = invite_status;
            }

            public static class UserToBean {
                /**
                 * id : 68
                 * username : kamal
                 * email : kamal@yopmail.com
                 * updated : 1
                 * profile_details : {"firstname":"kamal","lastname":"singh","d_o_b":"1991-08-13","phone":"9856789567","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486431354image.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1486573639image.jpg"}
                 * is_friend : 0
                 * request_respond : NO
                 */

                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanX {
                    /**
                     * firstname : kamal
                     * lastname : singh
                     * d_o_b : 1991-08-13
                     * phone : 9856789567
                     * gender : MALE
                     * location :
                     * country :
                     * state :
                     * cover_pic : http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486431354image.jpg
                     * profile_pic : http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1486573639image.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }

            public static class UserFromBean {
                /**
                 * id : 20
                 * username : plash12333
                 * email : plash12333@abc.com
                 * updated : 1
                 * profile_details : {"firstname":"Plash","lastname":"Jindal","d_o_b":"1992-08-29","phone":"212156464545","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg","profile_pic":"http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg"}
                 * is_friend : 0
                 * request_respond : NO
                 */

                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanXX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanXX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanXX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanXX {
                    /**
                     * firstname : Plash
                     * lastname : Jindal
                     * d_o_b : 1992-08-29
                     * phone : 212156464545
                     * gender : MALE
                     * location :
                     * country :
                     * state :
                     * cover_pic : http://dev.miracleglobal.com/cal-php/web/uploads/coverphoto/1486506163Penguins.jpg
                     * profile_pic : http://dev.miracleglobal.com/cal-php/web/uploads/profilepic/1484158527Tulips.jpg
                     */

                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }
        }
    }
}
