package com.example.preetisharma.callforblessings.demo;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.VideoOnWallModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/16/2017.
 */

public class VideoOnWallActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    // @BindView(R.id.img_view_back)
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    //@BindView(R.id.img_view_change_password)
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;


    //First View. Link.
    @BindView(R.id.card_video_link)
    CardView card_video_link;
    @BindView(R.id.movie_progress)
    ProgressBar movie_progress;
    @BindView(R.id.web_video_link)
    WebView web_video_link;
    @BindView(R.id.atv_video_link_Name)
    AppCompatTextView atv_video_link_Name;


    //Second View. Link
    @BindView(R.id.card_video_youtube)
    CardView card_video_youtube;
    @BindView(R.id.movie_progress_youtube)
    ProgressBar movie_progress_youtube;
    @BindView(R.id.web_video_youtube)
    WebView web_video_youtube;
    @BindView(R.id.atv_video_youtube_Name)
    AppCompatTextView atv_video_youtube_Name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_on_wall_layout);
        ButterKnife.bind(this);

        txtvw_header_title.setText("Video On Wall");
        img_view_change_password.setVisibility(View.GONE);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoOnWallActivity.this.finish();
            }
        });

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getVideoOnWall(APIServerResponse.VIDEO_ON_WALL, getUserSessionId(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            web_video_youtube.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    movie_progress_youtube.setVisibility(View.GONE);
                }

            });
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            web_video_link.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    return false;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);

                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    movie_progress.setVisibility(View.GONE);
                }
            });

        }


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            web_video_youtube.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    movie_progress_youtube.setVisibility(View.GONE);
                }

            });
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            web_video_youtube.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    return false;
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);

                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    movie_progress.setVisibility(View.GONE);
                }
            });

        }


        WebSettings webSettingsLink = web_video_link.getSettings();
        webSettingsLink.setJavaScriptEnabled(true);

        WebSettings webSettingsYoutube = web_video_youtube.getSettings();
        webSettingsYoutube.setJavaScriptEnabled(true);


    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {

                VideoOnWallModal videoOnWallModal;

                switch (tag) {

                    case APIServerResponse.VIDEO_ON_WALL:


                        videoOnWallModal = (VideoOnWallModal) response.body();

                        if (videoOnWallModal.getStatus().equalsIgnoreCase("1")) {


                            if (!videoOnWallModal.getDetail().getFile().equalsIgnoreCase("") && !videoOnWallModal.getDetail().getYoutube_link().equalsIgnoreCase("")) {

                                card_video_link.setVisibility(View.VISIBLE);
                                Log.i("LINK", videoOnWallModal.getDetail().getFile());
                                web_video_link.loadUrl(videoOnWallModal.getDetail().getFile());
                                atv_video_link_Name.setText(videoOnWallModal.getDetail().getTitle());
                            } else {
                                if (!videoOnWallModal.getDetail().getFile().equalsIgnoreCase("")) {
                                    card_video_link.setVisibility(View.VISIBLE);
                                    Log.i("LINK", videoOnWallModal.getDetail().getFile());
                                    web_video_link.loadUrl(videoOnWallModal.getDetail().getFile());
                                    atv_video_link_Name.setText(videoOnWallModal.getDetail().getTitle());
                                }

                                if (!videoOnWallModal.getDetail().getYoutube_link().equalsIgnoreCase("")) {

                                    card_video_youtube.setVisibility(View.VISIBLE);
                                    Log.i("LINK YOUTUBE", videoOnWallModal.getDetail().getYoutube_link());
                                    web_video_youtube.loadUrl(videoOnWallModal.getDetail().getYoutube_link());
                                    atv_video_youtube_Name.setText(videoOnWallModal.getDetail().getTitle());
                                }
                            }


                        }


                        hideLoading();
                        break;


                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        web_video_youtube.stopLoading();
        web_video_link.stopLoading();
        VideoOnWallActivity.this.finish();
    }
}
