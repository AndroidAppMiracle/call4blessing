package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by Kshitiz Bali on 1/18/2017.
 */

public class TestimonyModal {

    /**
     * status : 1
     * testimony_of_day : {"id":1,"name":"cbcvb","desc":"cvb","title":"cvbc","video_link":"","image":"","hallelujah_count":"3","hallelujah_flag":"Not Hallelujah","like_flag":"Not Liked","comment_count":"0","like_count":"2","comments":[]}
     * list : [{"id":5,"name":"test","desc":"test","title":"test","video_link":"","image":"","hallelujah_count":"1","hallelujah_flag":"Not Hallelujah","like_flag":"Not Liked","comment_count":"0","like_count":"0","comments":[]},{"id":4,"name":"testimony request","desc":"hgsdhfgjhzdgfshgdfgsh","title":"Testimony","video_link":"","image":"","hallelujah_count":"0","hallelujah_flag":"Not Hallelujah","like_flag":"Not Liked","comment_count":"0","like_count":"0","comments":[]},{"id":3,"name":"test","desc":"test","title":"test","video_link":"","image":"","hallelujah_count":"0","hallelujah_flag":"Not Hallelujah","like_flag":"Not Liked","comment_count":"0","like_count":"0","comments":[]},{"id":2,"name":"fghf","desc":"cbbbvb","title":"fhhhhgh","video_link":"","image":"","hallelujah_count":"0","hallelujah_flag":"Not Hallelujah","like_flag":"Not Liked","comment_count":"0","like_count":"0","comments":[]}]
     */

    private String status;
    private TestimonyOfDayBean testimony_of_day;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TestimonyOfDayBean getTestimony_of_day() {
        return testimony_of_day;
    }

    public void setTestimony_of_day(TestimonyOfDayBean testimony_of_day) {
        this.testimony_of_day = testimony_of_day;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class TestimonyOfDayBean {
        /**
         * id : 1
         * name : cbcvb
         * desc : cvb
         * title : cvbc
         * video_link :
         * image :
         * hallelujah_count : 3
         * hallelujah_flag : Not Hallelujah
         * like_flag : Not Liked
         * comment_count : 0
         * like_count : 2
         * comments : []
         */

        private int id;
        private String name;
        private String desc;
        private String title;
        private String video_link;
        private String image;
        private String hallelujah_count;
        private String hallelujah_flag;
        private String like_flag;
        private String comment_count;
        private String like_count;
        private List<?> comments;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getVideo_link() {
            return video_link;
        }

        public void setVideo_link(String video_link) {
            this.video_link = video_link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getHallelujah_count() {
            return hallelujah_count;
        }

        public void setHallelujah_count(String hallelujah_count) {
            this.hallelujah_count = hallelujah_count;
        }

        public String getHallelujah_flag() {
            return hallelujah_flag;
        }

        public void setHallelujah_flag(String hallelujah_flag) {
            this.hallelujah_flag = hallelujah_flag;
        }

        public String getLike_flag() {
            return like_flag;
        }

        public void setLike_flag(String like_flag) {
            this.like_flag = like_flag;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getLike_count() {
            return like_count;
        }

        public void setLike_count(String like_count) {
            this.like_count = like_count;
        }

        public List<?> getComments() {
            return comments;
        }

        public void setComments(List<?> comments) {
            this.comments = comments;
        }
    }

    public static class ListBean {
        /**
         * id : 5
         * name : test
         * desc : test
         * title : test
         * video_link :
         * image :
         * hallelujah_count : 1
         * hallelujah_flag : Not Hallelujah
         * like_flag : Not Liked
         * comment_count : 0
         * like_count : 0
         * comments : []
         */

        private int id;
        private String name;
        private String desc;
        private String title;
        private String video_link;
        private String image;
        private String hallelujah_count;
        private String hallelujah_flag;
        private String like_flag;
        private String comment_count;
        private String like_count;
        private List<?> comments;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getVideo_link() {
            return video_link;
        }

        public void setVideo_link(String video_link) {
            this.video_link = video_link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getHallelujah_count() {
            return hallelujah_count;
        }

        public void setHallelujah_count(String hallelujah_count) {
            this.hallelujah_count = hallelujah_count;
        }

        public String getHallelujah_flag() {
            return hallelujah_flag;
        }

        public void setHallelujah_flag(String hallelujah_flag) {
            this.hallelujah_flag = hallelujah_flag;
        }

        public String getLike_flag() {
            return like_flag;
        }

        public void setLike_flag(String like_flag) {
            this.like_flag = like_flag;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getLike_count() {
            return like_count;
        }

        public void setLike_count(String like_count) {
            this.like_count = like_count;
        }

        public List<?> getComments() {
            return comments;
        }

        public void setComments(List<?> comments) {
            this.comments = comments;
        }
    }
}
