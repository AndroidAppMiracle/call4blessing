package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.CustomPagerAdapter;
import com.example.preetisharma.callforblessings.Adapter.CustomPagerAdapterSongsList;
import com.example.preetisharma.callforblessings.Adapter.JoyMusicAdapter;
import com.example.preetisharma.callforblessings.Adapter.JoyMusicAdapterSongs;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSongsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;
import com.example.preetisharma.callforblessings.joymusicplayer.playlist.PlaylistListing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/24/2017.
 */

public class JoyMusicActivity extends BaseActivity implements APIServerResponse {
    @BindView(R.id.joy_music_recycler_view)
    RecyclerView joy_music_recycler_view;
    @BindView(R.id.view_pager_joy_albums)
    ViewPager view_pager_joy_albums;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.atv_AlbumTab)
    AppCompatTextView atv_AlbumTab;
    @BindView(R.id.atv_SongsTab)
    AppCompatTextView atv_SongsTab;

    CustomPagerAdapter mCustomPagerAdapter;
    List<JoyMusicModal.ListBean> joyMusicList = new ArrayList<>();
    List<JoyMusicSongsModal.ListBean> joyMusicSongsList = new ArrayList<>();
    JoyMusicAdapter _adapter;
    boolean isAlbumSelected = true, isSongsSelected = false;
    CustomPagerAdapterSongsList mCustomPagerSongsListAdapter;
    JoyMusicAdapterSongs songsListAdapter;
    @BindView(R.id.ll_tab)
    LinearLayout ll_tab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_joy_music);
        updateStatusBar();
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtvw_header_title.setText("Joy Music");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        img_view_change_password.setVisibility(View.GONE);
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getJoyMusicList(APIServerResponse.JOY_MUSIC_LISTING, getUserSessionId(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        //File ad = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "Downloads");
        //File imgDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "directory");


       /* File rootDataDir = this.getFilesDir();
        File rootEnvironment = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);

        Log.i("DIRECTORY", rootDataDir.getAbsolutePath());
        String dire = rootEnvironment.getAbsolutePath();
        Log.i("DIRECTORY Environ", dire);
        File asd = new File(rootDataDir.getAbsolutePath() + File.separator + "Download" + File.separator + "JoyMusic");
        //File asdnew = new File(getFilesDir() + File.separator + "Downloads");
        //Log.i("DIRECTORY Mid", asdnew.toString());
        Log.i("DIRECTORY Full", asd.toString());

        if (getFilesDir().exists()) {
            showToast("YESSS 00", Toast.LENGTH_SHORT);
        }
        if (rootEnvironment.exists()) {
            showToast("YESSS 2222", Toast.LENGTH_SHORT);
            File f = new File(rootEnvironment.getAbsolutePath());
            File[] files = f.listFiles();
            Log.i("DIRECTORY size", " " + files.length);
        }
*/





        /*if (ad.exists()) {
            String path = ad.getAbsolutePath();
        }
        if (imgDir.exists()) {
            String path2 = imgDir.getAbsolutePath();
        }*/

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            atv_SongsTab.setBackgroundResource(R.drawable.button_backg_joymusic_red);
            atv_AlbumTab.setBackgroundResource(R.drawable.button_backg_joymusic_white);
        }*/

        atv_AlbumTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // if (!isSeleted) {
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (isSongsSelected) {
                    atv_AlbumTab.setBackgroundResource(R.drawable.button_backg_joymusic_red);
                    atv_SongsTab.setBackgroundResource(R.drawable.button_backg_joymusic_white);
                    isAlbumSelected = true;
                    isSongsSelected = false;
                    if (isConnectedToInternet()) {
                        showLoading();
                        ServerAPI.getInstance().getJoyMusicList(APIServerResponse.JOY_MUSIC_LISTING, getUserSessionId(), JoyMusicActivity.this);
                    } else {
                        showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }


                //} else {
                //    atv_AlbumTab.setBackground(getResources().getDrawable(R.drawable.button_backg_joymusic_red));
                //     atv_SongsTab.setBackground(getResources().getDrawable(R.drawable.button_backg_joymusic_white));
                // }
                //    isSeleted = true;
                //  }

            }
        });


        atv_SongsTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                //isSeleted = false;
                if (isAlbumSelected) {
                    atv_SongsTab.setBackgroundResource(R.drawable.button_backg_joymusic_red);
                    atv_AlbumTab.setBackgroundResource(R.drawable.button_backg_joymusic_white);
                    isAlbumSelected = false;
                    isSongsSelected = true;
                    if (isConnectedToInternet()) {
                        showLoading();
                        ServerAPI.getInstance().getJoyMusicSongs(APIServerResponse.JOY_MUSIC_SONGS, getUserSessionId(), JoyMusicActivity.this);
                    }

                }

             /*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    atv_SongsTab.setBackgroundResource(R.drawable.button_backg_joymusic_red);
                    atv_AlbumTab.setBackground(getResources().getDrawable(R.drawable.button_backg_joymusic_white, null));
                } else {
                    atv_SongsTab.setBackground(getResources().getDrawable(R.drawable.button_backg_joymusic_red));
                    atv_AlbumTab.setBackground(getResources().getDrawable(R.drawable.button_backg_joymusic_white));
                }*/
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_search, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_search:

                Intent intentSearch = new Intent(JoyMusicActivity.this, JoyMusicSearch.class);
                startActivity(intentSearch);

                break;
            case R.id.menu_my_downloads:

                Intent intentSubscribe = new Intent(JoyMusicActivity.this, JoyMusicMyDownloads.class);
                startActivity(intentSubscribe);

                break;
            case R.id.menu_my_playlist:

                Intent intentPlaylist = new Intent(JoyMusicActivity.this, PlaylistListing.class);
                startActivity(intentPlaylist);


                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        JoyMusicModal musicModal;
        JoyMusicSongsModal joyMusicSongsModal;

        if (response.isSuccessful()) {
            switch (tag) {
                case APIServerResponse.JOY_MUSIC_LISTING:
                    musicModal = (JoyMusicModal) response.body();
                    if (musicModal.getStatus().equals("1")) {
                        mCustomPagerAdapter = new CustomPagerAdapter(this, musicModal.getTrending_albums());

                        joyMusicList = musicModal.getList();
                        _adapter = new JoyMusicAdapter(this, joyMusicList);


                    }
                    view_pager_joy_albums.setAdapter(mCustomPagerAdapter);
                    joy_music_recycler_view.setAdapter(_adapter);
                    joy_music_recycler_view.smoothScrollToPosition(0);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    joy_music_recycler_view.setLayoutManager(mLayoutManager);

                    if (ll_tab.getVisibility() != View.VISIBLE) {
                        ll_tab.setVisibility(View.VISIBLE);
                    }
                    hideLoading();
                    break;

                case APIServerResponse.JOY_MUSIC_SONGS:
                    joyMusicSongsModal = (JoyMusicSongsModal) response.body();
                    if (joyMusicSongsModal.getStatus().equalsIgnoreCase("1")) {

                        mCustomPagerSongsListAdapter = new CustomPagerAdapterSongsList(this, joyMusicSongsModal.getTrending_albums());

                        joyMusicSongsList = joyMusicSongsModal.getList();

                        songsListAdapter = new JoyMusicAdapterSongs(this, joyMusicSongsList);
                        joy_music_recycler_view.setAdapter(songsListAdapter);
                        joy_music_recycler_view.smoothScrollToPosition(0);
                        joy_music_recycler_view.setLayoutManager(new LinearLayoutManager(this));


                    }
                    if (ll_tab.getVisibility() != View.VISIBLE) {
                        ll_tab.setVisibility(View.VISIBLE);
                    }
                    hideLoading();
                    break;

            }
        } else {
            hideLoading();
        }


    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
    }
}
