package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GalleryImagesAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GalleryModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/9/2017.
 */

public class GalleryImagesFragment extends Fragment implements APIServerResponse {
    @BindView(R.id.gallery_images_recycler_view)
    RecyclerView gallery_images_recycler_view;
    ArrayList<GalleryModal.ListBean> mList;
    GalleryImagesAdapter _adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.gallery_fragment_layout, container, false);
        ButterKnife.bind(this, rootView);

        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().galleryListing(APIServerResponse.GALLERYIMAGES, ((BaseActivity) getActivity()).getUserSessionId(), "IMAGE", this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        return rootView;

    }

    @Override
    public void onSuccess(int tag, Response response) {

        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideLoading();
        }
        mList = new ArrayList<>();
        GalleryModal galleryData = null;
        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.GALLERYIMAGES:

                    {
                        galleryData = (GalleryModal) response.body();


                        if (galleryData.getStatus().equals("1")) {
                            mList = galleryData.getList();
                            _adapter = new GalleryImagesAdapter(getActivity(), mList);
                            gallery_images_recycler_view.setAdapter(_adapter);
                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(gallery_images_recycler_view.getContext(), DividerItemDecoration.VERTICAL);
                            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                            gallery_images_recycler_view.setLayoutManager(mLayoutManager);

                            gallery_images_recycler_view.addItemDecoration(dividerItemDecoration);

                        } else {
                            ((BaseActivity) getActivity()).showToast(response.message(), Toast.LENGTH_SHORT);
                        }

                    }

                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
