package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.JoyMusicSearchResultAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSearchModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.List;

import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/8/2017.
 */

public class JoyMusicSearch extends BaseActivity implements /*View.OnClickListener,*/ APIServerResponse {

    AppCompatImageView aiv_back, aiv_clear;
    AppCompatEditText aet_search;
    RecyclerView rv_search_list;
    AppCompatTextView atv_message;
    // boolean isSearchable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joy_music_search);

        aiv_back = (AppCompatImageView) findViewById(R.id.aiv_back);
        aiv_clear = (AppCompatImageView) findViewById(R.id.aiv_clear);
        aet_search = (AppCompatEditText) findViewById(R.id.aet_search);
        atv_message = (AppCompatTextView) findViewById(R.id.atv_message);
        rv_search_list = (RecyclerView) findViewById(R.id.rv_search_list);


        aet_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                //showToast("" + s.toString().trim(), Toast.LENGTH_SHORT);
                if (s.toString().trim().length() >= 3) {
                    //rv_search_list.setVisibility(View.VISIBLE);
                    ServerAPI.getInstance().searchMusic(APIServerResponse.SEARCH_JOY_MUSIC, getUserSessionId(), s.toString().trim(), JoyMusicSearch.this);
                } /*else {
                    rv_search_list.setVisibility(View.GONE);
                }*/


            }
        });

        aet_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchQuery = aet_search.getText().toString().trim();
                    if (searchQuery.equalsIgnoreCase("")) {

                        showToast("Please enter search query", Toast.LENGTH_SHORT);

                    } else {

                        showToast(searchQuery, Toast.LENGTH_SHORT);
                        // SearchAPI
                        //showLoading();

                    }

                    return true;
                }

                return false;
            }
        });

        aiv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                JoyMusicSearch.this.finish();
            }
        });


        aiv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aet_search.setText("");
            }
        });


    }

    /*  @Override
      public void onClick(View v) {
          switch (v.getId()) {

              case R.id.aiv_back:

                  JoyMusicSearch.this.finish();

                  break;

              case R.id.aiv_clear:
                  aet_search.setText("");
                  break;


          }
      }
  */
    @Override
    public void onSuccess(int tag, Response response) {
        try {

            JoyMusicSearchModal joyMusicSearchModal;
            switch (tag) {
                case APIServerResponse.SEARCH_JOY_MUSIC:

                    joyMusicSearchModal = (JoyMusicSearchModal) response.body();
                    if (joyMusicSearchModal.getStatus().equalsIgnoreCase("1")) {
                        //isSearchable = true;
                        //showToast("Size " + joyMusicSearchModal.getList().size(), Toast.LENGTH_SHORT);


                        if (joyMusicSearchModal.getList().size() > 0) {
                            JoyMusicSearchResultAdapter adapter = new JoyMusicSearchResultAdapter(JoyMusicSearch.this, joyMusicSearchModal.getList());
                            rv_search_list.setAdapter(adapter);
                            rv_search_list.setLayoutManager(new LinearLayoutManager(this));
                            if (rv_search_list.getVisibility() == View.GONE) {
                                atv_message.setVisibility(View.GONE);
                                rv_search_list.setVisibility(View.VISIBLE);
                            }
                        } else {

                            JoyMusicSearchResultAdapter adapter = new JoyMusicSearchResultAdapter(JoyMusicSearch.this, joyMusicSearchModal.getList());
                            rv_search_list.setAdapter(adapter);
                            rv_search_list.setLayoutManager(new LinearLayoutManager(this));
                            if (rv_search_list.getVisibility() == View.VISIBLE) {
                                rv_search_list.setVisibility(View.GONE);
                                atv_message.setVisibility(View.VISIBLE);
                            }
                        }


                    } else {
                        // isSearchable = false;
                        rv_search_list.setVisibility(View.GONE);
                        atv_message.setVisibility(View.VISIBLE);
                    }

                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyboard();
    }


}
