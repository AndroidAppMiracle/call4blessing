package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GroupHomeWallFragmentAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusGroupCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusGroupCreatePostModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.listener.PaginationScrollListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/7/2017.
 */

public class GroupWallHomeScreenFragment extends Fragment implements APIServerResponse {


    @BindView(R.id.my_recycler_view)
    RecyclerView my_recycler_view;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh;
    private static final int PAGE_NUMBER = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_NUMBER;
    GroupHomeWallFragmentAdapter groupHomeWallFragmentAdapter;
    String groupId = "";
    @BindView(R.id.atv_no_posts)
    AppCompatTextView atv_no_posts;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_group_home_wall_screen_layout, container, false);
        ButterKnife.bind(this, view);

        if (getArguments().getString(Constants.GROUP_ID) != null) {
            groupId = getArguments().getString(Constants.GROUP_ID);
            Log.i("GroupID", groupId);
        }

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        groupHomeWallFragmentAdapter = new GroupHomeWallFragmentAdapter(getActivity());
        my_recycler_view.setLayoutManager(linearLayoutManager);
        my_recycler_view.setItemAnimator(new DefaultItemAnimator());
        my_recycler_view.setAdapter(groupHomeWallFragmentAdapter);


        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    currentPage = 1;
                    groupHomeWallFragmentAdapter.removeAll();
                    ServerAPI.getInstance().getGroupHomeWallPosts(APIServerResponse.GROUP_HOME_WALL_POSTS, ((BaseActivity) getActivity()).getUserSessionId(), groupId, String.valueOf(currentPage), GroupWallHomeScreenFragment.this);

                } else {
                    ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }

            }
        });

        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().getGroupHomeWallPosts(APIServerResponse.GROUP_HOME_WALL_POSTS, ((BaseActivity) getActivity()).getUserSessionId(), groupId, String.valueOf(currentPage), this);

        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

        my_recycler_view.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPages();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        return view;
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {

            if (response.isSuccessful()) {
                GroupHomeWallModal groupHomeWallModal;

                switch (tag) {
                    case APIServerResponse.GROUP_HOME_WALL_POSTS:
                        groupHomeWallModal = (GroupHomeWallModal) response.body();

                        if (groupHomeWallModal.getStatus().equalsIgnoreCase("1")) {


                            TOTAL_PAGES = groupHomeWallModal.getDetail().getTotalPages();
                            currentPage = groupHomeWallModal.getDetail().getCurrentPage();

                            if (groupHomeWallModal.getDetail().getPost().size() > 0) {
                                List<GroupHomeWallModal.DetailBean.PostBean> detailList = groupHomeWallModal.getDetail().getPost();
                                groupHomeWallFragmentAdapter.addAll(detailList);
                                if (swipe_refresh.getVisibility() == View.GONE) {
                                    swipe_refresh.setVisibility(View.VISIBLE);
                                }

                                if (atv_no_posts.getVisibility() == View.VISIBLE) {
                                    atv_no_posts.setVisibility(View.GONE);
                                }
                            } else {
                                if (swipe_refresh.getVisibility() == View.VISIBLE) {
                                    swipe_refresh.setVisibility(View.GONE);
                                }

                                if (atv_no_posts.getVisibility() == View.GONE) {
                                    atv_no_posts.setVisibility(View.VISIBLE);
                                }
                            }

                            //rv_viewAllList.setAdapter(adapter);

                            if (currentPage < TOTAL_PAGES)
                                groupHomeWallFragmentAdapter.addLoadingFooter();
                            else isLastPage = true;

                            if (swipe_refresh.isRefreshing()) {
                                swipe_refresh.setRefreshing(false);
                            }
                        }
                        break;
                    case APIServerResponse.GROUP_HOME_WALL_POSTS_PAGINATION:
                        groupHomeWallModal = (GroupHomeWallModal) response.body();

                        if (groupHomeWallModal.getStatus().equalsIgnoreCase("1")) {

                            TOTAL_PAGES = groupHomeWallModal.getDetail().getTotalPages();
                            currentPage = groupHomeWallModal.getDetail().getCurrentPage();
                            groupHomeWallFragmentAdapter.removeLoadingFooter();
                            isLoading = false;


                            if (groupHomeWallModal.getDetail().getPost().size() > 0) {
                                List<GroupHomeWallModal.DetailBean.PostBean> detailList = groupHomeWallModal.getDetail().getPost();
                                groupHomeWallFragmentAdapter.addAll(detailList);
                            }

                            if (currentPage != TOTAL_PAGES)
                                groupHomeWallFragmentAdapter.addLoadingFooter();
                            else isLastPage = true;

                        }

                        break;

                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.getMessage();
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();

    }

    private void loadNextPages() {

        Log.d("loadNextPage: ", "" + currentPage);
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().getGroupHomeWallPosts(APIServerResponse.GROUP_HOME_WALL_POSTS_PAGINATION, ((BaseActivity) getActivity()).getUserSessionId(), groupId, String.valueOf(currentPage), this);

        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

    }


    @Subscribe
    public void onGroupPostCreated(EventBusGroupCreatePostModal eventBusGroupCreatePostModal) {

        try {

            if (eventBusGroupCreatePostModal.getIsNewPostCreated().equalsIgnoreCase("1")) {

                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    currentPage = 1;
                    groupHomeWallFragmentAdapter.removeAll();
                    ServerAPI.getInstance().getGroupHomeWallPosts(APIServerResponse.GROUP_HOME_WALL_POSTS, ((BaseActivity) getActivity()).getUserSessionId(), groupId, String.valueOf(currentPage), GroupWallHomeScreenFragment.this);

                } else {
                    ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Subscribe
    public void onCommentPost(EventBusGroupCommentModal eventBusGroupCommentModal) {
        try {

            if (eventBusGroupCommentModal.getStatus() == 1) {
                groupHomeWallFragmentAdapter.updateASingleItem(eventBusGroupCommentModal.getItemPosition());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
