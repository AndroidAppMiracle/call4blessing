package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoHomeModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.PostDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;
import com.example.preetisharma.callforblessings.demo.EditUserPostActivity;
import com.example.preetisharma.callforblessings.demo.SharePostActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.FullScreenVideoPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;
import com.example.preetisharma.callforblessings.joymusicplayer.VideoPlayerActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 5/23/2017.
 */

public class DemoPaginationHomeWallAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM = 0;
    public static final int LOADING = 1;
    //private static final String BASE_URL_IMG = "https://image.tmdb.org/t/p/w150";

    private List<DemoHomeModal.PostsBean> albumResults;
    private Context context;
    private static String likescount, commentsCount;
    private static String liked = "not liked";

    private boolean isLoadingAdded = false;
    private int userIDInt;
    ArrayList<PlayMusicModal> albumSongsList = new ArrayList<>();


    public DemoPaginationHomeWallAdapter(Context context) {
        this.context = context;
        albumResults = new ArrayList<>();
        String userID = ((BaseActivity) this.context).getUserID();
        userIDInt = Integer.parseInt(userID);
    }

    public List<DemoHomeModal.PostsBean> getMovies() {
        return albumResults;
    }

    public void setMovies(List<DemoHomeModal.PostsBean> albumResults) {
        this.albumResults = albumResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new DemoPaginationHomeWallAdapter.LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.homescreen_item_adapter_layout, parent, false);
        viewHolder = new DemoPaginationHomeWallAdapter.MovieVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        //HomeWallModal.PostsBean result = albumResults.get(position); // Movie

        switch (getItemViewType(position)) {
            case ITEM:

                try {
                    if (albumResults.size() != 0) {
                        MovieVH dataViewHolder = (MovieVH) holder;
                        dataViewHolder.txtvw_user_name.setText(albumResults.get(position).getPost_by().getProfile_details().getFirstname().trim() + " " + albumResults.get(position).getPost_by().getProfile_details().getLastname().trim());
                        dataViewHolder.txtvw_time_stamp.setText(/*"posted " + " " + */albumResults.get(holder.getAdapterPosition()).getPosted_at());

                        if (albumResults.get(position).getPost_type().equals(Constants.PROFILE_PIC)) {
                            dataViewHolder.tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                            dataViewHolder.tv_changed_profile_cover_pic.setText(Constants.CHANGED_PROFILE_PIC);
                        } else if (albumResults.get(position).getPost_type().equals(Constants.COVER_PIC)) {
                            dataViewHolder.tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                            dataViewHolder.tv_changed_profile_cover_pic.setText(Constants.CHANGED_COVER_PIC);

                        } else if (albumResults.get(position).getPost_type().equals(Constants.SHARE)) {
                            try {


                                if (!albumResults.get(position).getShared_post().isEmpty()) {

                                    if (!albumResults.get(holder.getAdapterPosition()).getTag_users().isEmpty()) {

                                        dataViewHolder.txtvw_tagged_friends.setVisibility(View.VISIBLE);
                                        dataViewHolder.card_shared_post.setVisibility(View.VISIBLE);
                                        dataViewHolder.tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                                        dataViewHolder.tv_changed_profile_cover_pic.setText(Constants.SHARED);

                                        Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).thumbnail(0.1f).placeholder(R.drawable.ic_me).into(dataViewHolder.img_vw_user_profile_shared);
                                        dataViewHolder.txtvw_user_name_shared.setText(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname());
                                        dataViewHolder.txtvw_time_stamp_shared.setText(albumResults.get(position).getShared_post().get(0).getPosted_at());
                                        dataViewHolder.txtvw_post_details_shared.setText(albumResults.get(position).getShared_post().get(0).getPost_content());
                                        if (albumResults.get(position).getShared_post().get(0).getMedia().size() != 0 && albumResults.get(position).getShared_post().get(0).getMedia().get(0).getType().equalsIgnoreCase(Constants.IMAGE)) {
                                            dataViewHolder.viewholder_image_shared.setVisibility(View.VISIBLE);
                                            Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getMedia().get(0).getFile()).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(dataViewHolder.viewholder_image_shared);
                                        }
                                        if (albumResults.get(position).getShared_post().get(0).getPost_type().equalsIgnoreCase(Constants.PROFILE_PIC)) {
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setVisibility(View.VISIBLE);
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setText(Constants.CHANGED_PROFILE_PIC);
                                        } else if (albumResults.get(position).getShared_post().get(0).getPost_type().equalsIgnoreCase(Constants.COVER_PIC)) {
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setVisibility(View.VISIBLE);
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setText(Constants.CHANGED_COVER_PIC);
                                        } else {
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setVisibility(View.GONE);
                                            //holder.tv_changed_profile_cover_pic_shared.setText(Constants.SHARED);
                                        }

                                        if (albumResults.get(position).getTag_users().size() == 1) {
                                            dataViewHolder.txtvw_tagged_friends.setText(" " + "with " + albumResults.get(position).getShared_post().get(holder.getAdapterPosition()).getTag_users().get(0).getProfile_details().getFirstname() + " " + albumResults.get(position).getShared_post().get(holder.getAdapterPosition()).getTag_users().get(0).getProfile_details().getLastname().trim());
                                        } else if (albumResults.get(position).getTag_users().size() == 2) {
                                            dataViewHolder.txtvw_tagged_friends.setText(" " + "with " + albumResults.get(position).getShared_post().get(holder.getAdapterPosition()).getTag_users().get(0).getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getShared_post().get(holder.getAdapterPosition()).getTag_users().get(0).getProfile_details().getFirstname() + " " + albumResults.get(position).getShared_post().get(holder.getAdapterPosition()).getTag_users().get(0).getProfile_details().getLastname().trim());
                                        } else {
                                            dataViewHolder.txtvw_tagged_friends.setText(" " + "with " + albumResults.get(position).getShared_post().get(holder.getAdapterPosition()).getTag_users().get(0).getProfile_details().getFirstname() + " " + albumResults.get(position).getShared_post().get(holder.getAdapterPosition()).getTag_users().get(0).getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getShared_post().get(holder.getAdapterPosition()).getTag_users().size() - 1) + " others");

                                        }
                                    } else {
                                        dataViewHolder.card_shared_post.setVisibility(View.VISIBLE);
                                        dataViewHolder.tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                                        dataViewHolder.tv_changed_profile_cover_pic.setText(Constants.SHARED);

                                        Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getProfile_pic()).thumbnail(0.1f).placeholder(R.drawable.ic_me).into(dataViewHolder.img_vw_user_profile_shared);
                                        dataViewHolder.txtvw_user_name_shared.setText(albumResults.get(position).getShared_post().get(0).getPost_by().getProfile_details().getFirstname());
                                        dataViewHolder.txtvw_time_stamp_shared.setText(albumResults.get(position).getShared_post().get(0).getPosted_at());
                                        dataViewHolder.txtvw_post_details_shared.setText(albumResults.get(position).getShared_post().get(0).getPost_content());
                                        if (albumResults.get(position).getShared_post().get(0).getMedia().size() != 0 && albumResults.get(position).getShared_post().get(0).getMedia().get(0).getType().equalsIgnoreCase(Constants.IMAGE)) {
                                            dataViewHolder.viewholder_image_shared.setVisibility(View.VISIBLE);
                                            Glide.with(context).load(albumResults.get(position).getShared_post().get(0).getMedia().get(0).getFile()).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(dataViewHolder.viewholder_image_shared);
                                        }
                                        if (albumResults.get(position).getShared_post().get(0).getPost_type().equalsIgnoreCase(Constants.PROFILE_PIC)) {
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setVisibility(View.VISIBLE);
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setText(Constants.CHANGED_PROFILE_PIC);
                                        } else if (albumResults.get(position).getShared_post().get(0).getPost_type().equalsIgnoreCase(Constants.COVER_PIC)) {
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setVisibility(View.VISIBLE);
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setText(Constants.CHANGED_COVER_PIC);
                                        } else {
                                            dataViewHolder.tv_changed_profile_cover_pic_shared.setVisibility(View.GONE);
                                            //holder.tv_changed_profile_cover_pic_shared.setText(Constants.SHARED);
                                        }

                                    }

                                } else if (!albumResults.get(position).getShared_request().isEmpty()) {
                                    dataViewHolder.card_shared_post.setVisibility(View.VISIBLE);
                                    dataViewHolder.tv_changed_profile_cover_pic.setVisibility(View.VISIBLE);
                                    dataViewHolder.tv_changed_profile_cover_pic.setText(Constants.SHARED);
                                    dataViewHolder.img_vw_user_profile_shared.setVisibility(View.GONE);
                                    dataViewHolder.txtvw_time_stamp_shared.setVisibility(View.GONE);
                            /*Glide.with(context).load(albumResults.get(position).getShared_request().get(0).getImage()).thumbnail(0.1f).placeholder(R.drawable.ic_me).into(dataViewHolder.img_vw_user_profile_shared);*/
                                    dataViewHolder.txtvw_user_name_shared.setText(albumResults.get(position).getShared_request().get(0).getTitle());

                                    dataViewHolder.txtvw_post_details_shared.setText(albumResults.get(position).getShared_request().get(0).getDesc());


                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (albumResults.get(position).getTag_users() != null && albumResults.get(position).getTag_users().size() > 0) {
                            dataViewHolder.txtvw_tagged_friends.setVisibility(View.VISIBLE);
                            if (albumResults.get(position).getTag_users().size() == 1) {
                                dataViewHolder.txtvw_tagged_friends.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim());
                            } else if (albumResults.get(position).getTag_users().size() == 2) {
                                dataViewHolder.txtvw_tagged_friends.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " " + "and " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(1).getUser_info().getProfile_details().getLastname().trim());
                            } else {
                                dataViewHolder.txtvw_tagged_friends.setText(" " + "with " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getFirstname() + " " + albumResults.get(position).getTag_users().get(0).getUser_info().getProfile_details().getLastname().trim() + " and " + (albumResults.get(position).getTag_users().size() - 1) + " others");

                            }
                        } else {
                            dataViewHolder.card_shared_post.setVisibility(View.GONE);
                            dataViewHolder.tv_changed_profile_cover_pic.setVisibility(View.GONE);
                        }


                        if (albumResults.get(position).getPost_by().getId() == userIDInt && (!albumResults.get(position).getPost_type().equals(Constants.PROFILE_PIC)
                                || !albumResults.get(position).getPost_type().equals(Constants.COVER_PIC))) {
                            dataViewHolder.iv_more_options.setVisibility(View.VISIBLE);
                        } else {
                            dataViewHolder.iv_more_options.setVisibility(View.GONE);
                        }


                        if (albumResults.get(position).getLike_count().equals("0") || albumResults.get(holder.getAdapterPosition()).getLike_count().equals("1")) {
                            dataViewHolder.txtvw_no_of_likes.setText(albumResults.get(holder.getAdapterPosition()).getLike_count() + " " + "Like");
                        } else {
                            dataViewHolder.txtvw_no_of_likes.setText(albumResults.get(holder.getAdapterPosition()).getLike_count() + " " + "Likes");
                        }
                        if (albumResults.get(holder.getAdapterPosition()).getComment_count().equals("0") || albumResults.get(holder.getAdapterPosition()).getComment_count().equals("1")) {
                            dataViewHolder.txtvw_no_of_comments.setText(albumResults.get(holder.getAdapterPosition()).getComment_count() + " " + "Comment");
                        } else {
                            dataViewHolder.txtvw_no_of_comments.setText(albumResults.get(holder.getAdapterPosition()).getComment_count() + " " + "Comments");

                        }
                        if (albumResults.get(holder.getAdapterPosition()).getLike_flag().equalsIgnoreCase("Not liked")) {
                            int imgResource = R.drawable.prayer_like_icon;
                            dataViewHolder.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        } else {
                            int imgResource = R.drawable.ic_liked;
                            dataViewHolder.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        }
                        dataViewHolder.txtvw_post_details.setText(stripText(albumResults.get(holder.getAdapterPosition()).getPost_content()));
                        Glide.with(context).load(albumResults.get(holder.getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).into(dataViewHolder.img_vw_user_profile);

                        if (albumResults.get(holder.getAdapterPosition()).getMedia().size() > 0) {

                            if (albumResults.get(holder.getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Image")) {
                                dataViewHolder.viewholder_image.setVisibility(View.VISIBLE);
                                Glide.with(context).load(stripHtml(albumResults.get(holder.getAdapterPosition()).getMedia().get(0).getFile())).override(300, 300).thumbnail(0.1f)/*.placeholder(R.drawable.placeholder)*/.into(dataViewHolder.viewholder_image);



                            } else if (albumResults.get(holder.getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Video")) {
                                try {
                                    dataViewHolder.viewholder_image.setVisibility(View.VISIBLE);
                                    dataViewHolder.video_play.setVisibility(View.VISIBLE);
                                    dataViewHolder.viewholder_image.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                    /*Intent videoIntent = new Intent(context, VideoViewActivity.class);
                                    Bundle b = new Bundle();
                                    b.putString(Constants.FRAGMENT_NAME, "HOMEWALLADAPTER");
                                    b.putString(Constants.VIDEO, list.get(holder.getAdapterPosition()).getMedia().get(0).getFile());
                                    videoIntent.putExtras(b);
                                    context.startActivity(videoIntent);*/


                                            PlayMusicModal playMusicModal = new PlayMusicModal();
                                            playMusicModal.setMediaUrl(albumResults.get(holder.getAdapterPosition()).getMedia().get(0).getFile());
                                            playMusicModal.setArtworkUrl(albumResults.get(holder.getAdapterPosition()).getMedia().get(0).getThumbnail_file());
                                            playMusicModal.setThumbnailUrl(albumResults.get(holder.getAdapterPosition()).getMedia().get(0).getThumbnail_file());
                                            playMusicModal.setAlbum("");
                                            playMusicModal.setArtist("");
                                            playMusicModal.setAudio(false);
                                            playMusicModal.setTitle("Video");
                                            playMusicModal.setPlaylistId(albumResults.get(holder.getAdapterPosition()).getId());

                                            albumSongsList.add(playMusicModal);


                                            Intent intent = new Intent(context, FullScreenVideoPlayerActivity.class);
                                            intent.putExtra(VideoPlayerActivity.EXTRA_INDEX, 0);
                                            intent.putExtra(VideoPlayerActivity.PLAYLIST_FLAG, albumResults.get(holder.getAdapterPosition()).getId());
                                            Bundle b = new Bundle();
                                            b.putParcelableArrayList(Constants.ALBUM_SONGS_LIST, albumSongsList);
                                            intent.putExtra("video_bundle", b);
                                            context.startActivity(intent);

                                        }
                                    });

                                    Glide.with(context).load(albumResults.get(position).getMedia().get(0).getThumbnail_file()).override(300, 300).thumbnail(0.1f).placeholder(R.drawable.ic_play).into(dataViewHolder.viewholder_image);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            dataViewHolder.viewholder_image.setVisibility(View.GONE);
                        }
                        /*if (dataViewHolder.txtvw_like.getCompoundDrawables().equals(R.drawable.prayer_like_icon)) {
                            Log.e("Liked", "Liked");
                        }*/

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return albumResults == null ? 0 : albumResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == albumResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(DemoHomeModal.PostsBean r) {
        albumResults.add(r);
        notifyItemInserted(albumResults.size() - 1);
    }

    public void addAll(List<DemoHomeModal.PostsBean> moveResults) {
        for (DemoHomeModal.PostsBean result : moveResults) {
            add(result);
        }
    }

    public void remove(DemoHomeModal.PostsBean r) {
        int position = albumResults.indexOf(r);
        if (position > -1) {
            albumResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeAll() {
        if (albumResults.size() != 0) {
            albumResults.clear();
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new DemoHomeModal.PostsBean());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = albumResults.size() - 1;
        DemoHomeModal.PostsBean result = getItem(position);

        if (result != null) {
            albumResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public DemoHomeModal.PostsBean getItem(int position) {
        return albumResults.get(position);
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }


    public String stripText(String text) {

        String regexp = "<p>.*?</p>";
        String replace = "";
        String finalText = text.replaceAll(regexp, replace);
        return finalText.replaceAll("<img.+?>", "");

    }

   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class MovieVH extends RecyclerView.ViewHolder implements APIServerResponse {
        //private TextView mMovieTitle;
        // private TextView mMovieDesc;
        // private TextView mYear; // displays "year | language"
        // private ImageView mPosterImg;
        @BindView(R.id.txtvw_tagged_friends)
        AppCompatTextView txtvw_tagged_friends;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;
        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;
        @BindView(R.id.viewholder_image)
        AppCompatImageView viewholder_image;
        @BindView(R.id.iv_more_options)
        AppCompatImageView iv_more_options;
        @BindView(R.id.tv_changed_profile_cover_pic)
        AppCompatTextView tv_changed_profile_cover_pic;
        @BindView(R.id.video_play)
        AppCompatImageView video_play;

        //Shared Post
        @BindView(R.id.card_shared_post)
        CardView card_shared_post;
        @BindView(R.id.img_vw_user_profile_shared)
        AppCompatImageView img_vw_user_profile_shared;
        @BindView(R.id.txtvw_user_name_shared)
        AppCompatTextView txtvw_user_name_shared;
        @BindView(R.id.txtvw_time_stamp_shared)
        AppCompatTextView txtvw_time_stamp_shared;
        @BindView(R.id.txtvw_post_details_shared)
        AppCompatTextView txtvw_post_details_shared;
        @BindView(R.id.viewholder_image_shared)
        AppCompatImageView viewholder_image_shared;

        @BindView(R.id.tv_changed_profile_cover_pic_shared)
        AppCompatTextView tv_changed_profile_cover_pic_shared;

        public MovieVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }

        @OnClick(R.id.iv_more_options)
        public void deleteeditPopup() {

            try {
                createPopupMenuOption();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @OnClick(R.id.txtvw_like)
        public void likePost() {

            if (txtvw_like.isEnabled()) {
                txtvw_like.setEnabled(false);
            }
            liked = albumResults.get(getAdapterPosition()).getLike_flag();
            likescount = albumResults.get(getAdapterPosition()).getLike_count();

            if (liked.equalsIgnoreCase("Not Liked")) {
                int imgResource = R.drawable.ic_liked;
                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) context).isConnectedToInternet()) {

                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", MovieVH.this);
                } else {
                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }

            } else {
                int imgResource = R.drawable.prayer_like_icon;
                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) context).isConnectedToInternet()) {

                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), "POST", MovieVH.this);
                } else {
                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {

            if (albumResults.size() > 0 && albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                if (albumResults.get(getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Image")) {
                    openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), albumResults.get(getAdapterPosition()).getMedia().get(0).getFile(), "", "");
                } else if (albumResults.get(getAdapterPosition()).getMedia().get(0).getType().equalsIgnoreCase("Video")) {
                    openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", albumResults.get(getAdapterPosition()).getMedia().get(0).getFile(), albumResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());

                }
            } else {
                openConfirmationDialog(String.valueOf(albumResults.get(getAdapterPosition()).getId()), albumResults.get(getAdapterPosition()).getPost_content(), albumResults.get(getAdapterPosition()).getPosted_at(), "", "", "");

            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentPost() {


            Intent i = new Intent(context, CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
            b.putString(Constants.POSTTYPE, Constants.POST);
            i.putExtras(b);
            context.startActivity(i);


        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                LikeModal likeModal;
                ShareModal shareModal;
                CommentModal commentModal;
                PostDeleteModal postDeleteModal;
                ((BaseActivity) context).hideLoading();
                if (response.isSuccessful()) {
                    switch (tag) {
                        case APIServerResponse.LIKE:

                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like.isEnabled()) {
                                    txtvw_like.setEnabled(true);
                                }
                                int imgResource = R.drawable.ic_liked;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue += 1;
                                liked = "liked";
                                albumResults.get(getAdapterPosition()).setLike_flag("liked");
                                albumResults.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));
                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                                } else {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                                }
                            }

                            break;
                        case APIServerResponse.UNLIKE:
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like.isEnabled()) {
                                    txtvw_like.setEnabled(true);
                                }
                                int imgResource = R.drawable.prayer_like;
                                txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue--;
                                liked = "not liked";
                                albumResults.get(getAdapterPosition()).setLike_flag("not liked");
                                albumResults.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Like");
                                } else {
                                    txtvw_no_of_likes.setText(String.valueOf(likescountValue) + " " + "Likes");
                                }
                            }
                            break;
                        case APIServerResponse.SHARE:

                            shareModal = (ShareModal) response.body();
                            if (shareModal.getStatus().equals("1")) {
                                /*shareModal.getDetail().*/
                                ((BaseActivity) context).showToast("Shared successfully", Toast.LENGTH_SHORT);

                                notifyDataSetChanged();
                            }

                            break;
                        case APIServerResponse.COMMENT:

                            commentModal = (CommentModal) response.body();
                            if (commentModal.getStatus().equals("1")) {
                                /*shareModal.getDetail().*/
                                ((BaseActivity) context).showToast("Success", Toast.LENGTH_SHORT);
                                notifyDataSetChanged();
                            }

                            break;

                        case APIServerResponse.DELETE_POST:
                            postDeleteModal = (PostDeleteModal) response.body();

                            if (postDeleteModal.getStatus().equals("1")) {
                                ((BaseActivity) context).showToast("Post Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }

                            break;
                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        public void openConfirmationDialog(String postId, String post_text, String timeStamp, String post_image, String postVideoUrl, String postVideoThumbnail) {

            Intent shareDialog = new Intent(context, SharePostActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.POSTTYPE, "POST");
            b.putString(Constants.POSTIMAGE, post_image);
            b.putString(Constants.POSTTEXT, post_text);
            b.putString(Constants.POSTTIMESTAMP, timeStamp);
            b.putString(Constants.POSTID, postId);
            b.putString(Constants.POSTVIDEO, postVideoUrl);
            b.putString(Constants.POSTTHUMBNAIL, postVideoThumbnail);
            shareDialog.putExtras(b);
            context.startActivity(shareDialog);


        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) context).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.LIKE:
                    System.out.println("Error");
                    break;
                case APIServerResponse.SHARE:
                    System.out.println("Error");
                    break;
            }
        }


        public void createPopupMenuOption() {
            try {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, iv_more_options);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_popup_options, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        try {
                            if (item.getItemId() == R.id.menu_delete_post) {

                                if (((BaseActivity) context).isConnectedToInternet()) {
                                    ((BaseActivity) context).showLoading();
                                    ServerAPI.getInstance().deletePost(APIServerResponse.DELETE_POST, ((BaseActivity) context).getUserSessionId(), String.valueOf(albumResults.get(getAdapterPosition()).getId()), MovieVH.this);
                                } else {
                                    ((BaseActivity) context).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                                }
                                return true;
                            } else if (item.getItemId() == R.id.menu_edit_post) {

                                Intent i = new Intent(context, EditUserPostActivity.class);
                                Bundle b = new Bundle();
                                b.putString(Constants.POSTID, String.valueOf(albumResults.get(getAdapterPosition()).getId()));
                                b.putString(Constants.USER_NAME, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getFirstname() + " " + albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getLastname());
                                b.putString(Constants.USER_IMAGE, albumResults.get(getAdapterPosition()).getPost_by().getProfile_details().getProfile_pic());
                                b.putString(Constants.POST_TEXT, stripText(albumResults.get(getAdapterPosition()).getPost_content()));
                                b.putString(Constants.PIC_CHANGE, albumResults.get(getAdapterPosition()).getPost_type());
                                if (albumResults.get(getAdapterPosition()).getMedia().size() > 0) {
                                    b.putString(Constants.POST_IMAGE, stripHtml(albumResults.get(getAdapterPosition()).getMedia().get(0).getFile()));
                                } else {
                                    b.putString(Constants.POST_IMAGE, null);
                                }
                                i.putExtras(b);
                                context.startActivity(i);

                                return true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });
                popup.show();//showing popup menu
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        public void refresh(int position) {
            Log.e("Refreshed Position", "Pos" + position);
            albumResults.remove(position);
            notifyItemRemoved(position);
        }


    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
