package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallPostLike;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionCheckModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.GroupsCommentsListActivity;
import com.example.preetisharma.callforblessings.demo.SharePostActivity;
import com.example.preetisharma.callforblessings.demo.ViewFullScreenImage;
import com.example.preetisharma.callforblessings.joymusicplayer.FullScreenVideoPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;
import com.example.preetisharma.callforblessings.joymusicplayer.VideoPlayerActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/7/2017.
 */

public class GroupHomeWallFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int LOADING = 0;
    public final int IMAGE = 2, VIDEO = 3, TEXT = 1;

    //private static final String BASE_URL_IMG = "https://image.tmdb.org/t/p/w150";

    private List<GroupHomeWallModal.DetailBean.PostBean> movieResults;
    private Context context;
    private Resources res;
    private ArrayList<PlayMusicModal> albumSongsList = new ArrayList<>();

    private boolean isLoadingAdded = false;

    public GroupHomeWallFragmentAdapter(Context context) {
        this.context = context;
        movieResults = new ArrayList<>();
        res = context.getResources();
    }

    public List<GroupHomeWallModal.DetailBean.PostBean> getMovies() {
        return movieResults;
    }

    public void setMovies(List<GroupHomeWallModal.DetailBean.PostBean> movieResults) {
        this.movieResults = movieResults;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View v;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            /*case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;*/
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new GroupHomeWallFragmentAdapter.LoadingVH(v2);
                break;

            case IMAGE:
                v = inflater.inflate(R.layout.group_adapter_post_image_layout, parent, false);
                viewHolder = new GroupHomeWallFragmentAdapter.DataViewHolderImage(v);
                Constants.setAppFont((ViewGroup) v, parent.getContext());

                break;
            case VIDEO:
                v = inflater.inflate(R.layout.group_adapter_post_video_layout, parent, false);
                viewHolder = new GroupHomeWallFragmentAdapter.DataViewHolderVideo(v);
                Constants.setAppFont((ViewGroup) v, parent.getContext());

                break;
            case TEXT:
                v = inflater.inflate(R.layout.group_adapter_post_text_layout, parent, false);
                viewHolder = new GroupHomeWallFragmentAdapter.DataViewHolderText(v);
                Constants.setAppFont((ViewGroup) v, parent.getContext());

                break;
        }
        return viewHolder;
    }

    /*   @NonNull
       private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
           RecyclerView.ViewHolder viewHolder;
           View v1 = inflater.inflate(R.layout.adapter_joy_music_view_all, parent, false);
           viewHolder = new GroupHomeWallFragmentAdapter.MovieVH(v1);
           return viewHolder;
       }
   */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        switch (getItemViewType(position)) {

            case IMAGE:

                final GroupHomeWallFragmentAdapter.DataViewHolderImage dataViewHolderImage = (GroupHomeWallFragmentAdapter.DataViewHolderImage) holder;


                dataViewHolderImage.txtvw_user_name.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getPost_by().getProfile_details().getFirstname(), movieResults.get(position).getPost_by().getProfile_details().getLastname()));
                dataViewHolderImage.txtvw_time_stamp.setText(movieResults.get(position).getPosted_at());
                dataViewHolderImage.txtvw_post_details.setText(movieResults.get(position).getPost_content());

                if (movieResults.get(position).getLike_count().equalsIgnoreCase("1") || movieResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderImage.txtvw_no_of_likes.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getLike_count(), res.getString(R.string.like)));
                } else {
                    dataViewHolderImage.txtvw_no_of_likes.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getLike_count(), res.getString(R.string.likes)));
                }


                if (movieResults.get(position).getComment_count().equalsIgnoreCase("1") || movieResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderImage.txtvw_no_of_comments.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getComment_count(), res.getString(R.string.comment)));
                } else {
                    dataViewHolderImage.txtvw_no_of_comments.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getComment_count(), res.getString(R.string.comments)));
                }
                //dataViewHolderImage.txtvw_no_of_likes.setText(movieResults.get(position).getLike_count());
                //dataViewHolderImage.txtvw_no_of_comments.setText(movieResults.get(position).getComment_count());
                if (movieResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderImage.img_vw_user_profile);
                } else if (movieResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderImage.img_vw_user_profile);

                }
                //Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderImage.img_vw_user_profile);

                //Glide.with(context).load(movieResults.get(position).getPosts().get(position).getMedia().getFile()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderImage.iv_grou_post_image);

                Log.i("ivSize", "W " + dataViewHolderImage.iv_grou_post_image.getWidth());

                //Glide.with(context).load(movieResults.get(position).getMedia().get(0).getFile()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderImage.iv_grou_post_image);

                if (movieResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderImage.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderImage.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                dataViewHolderImage.mProgress.setVisibility(View.GONE);
                Glide.with(context).load(movieResults.get(position).getMedia().get(0).getFile()).crossFade().into(dataViewHolderImage.iv_grou_post_image);

                //setPic(dataViewHolderImage.iv_grou_post_image, movieResults.get(position).getMedia().get(0).getFile(), dataViewHolderImage.mProgress);


                /*Glide.with(context)
                        .load(movieResults.get(position).getMedia().get(0).getFile())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                dataViewHolderImage.mProgress.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                // image ready, hide progress now
                                dataViewHolderImage.mProgress.setVisibility(View.GONE);
                                return false;   // return false if you want Glide to handle everything else.
                            }
                        })
                            *//*.diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image*//*
                        .crossFade()
                        .into(dataViewHolderImage.iv_grou_post_image);*/

                break;

            case VIDEO:

                final GroupHomeWallFragmentAdapter.DataViewHolderVideo dataViewHolderVideo = (GroupHomeWallFragmentAdapter.DataViewHolderVideo) holder;

                dataViewHolderVideo.txtvw_user_name.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getPost_by().getProfile_details().getFirstname(), movieResults.get(position).getPost_by().getProfile_details().getLastname()));

                dataViewHolderVideo.txtvw_time_stamp.setText(movieResults.get(position).getPosted_at());
                dataViewHolderVideo.txtvw_post_details.setText(movieResults.get(position).getPost_content());
                if (movieResults.get(position).getLike_count().equalsIgnoreCase("1") || movieResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderVideo.txtvw_no_of_likes.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getLike_count(), res.getString(R.string.like)));
                } else {
                    dataViewHolderVideo.txtvw_no_of_likes.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getLike_count(), res.getString(R.string.likes)));
                }

                if (movieResults.get(position).getComment_count().equalsIgnoreCase("1") || movieResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderVideo.txtvw_no_of_comments.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getComment_count(), res.getString(R.string.comment)));
                } else {
                    dataViewHolderVideo.txtvw_no_of_comments.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getComment_count(), res.getString(R.string.comments)));
                }

                if (movieResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderVideo.img_vw_user_profile);
                } else if (movieResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderVideo.img_vw_user_profile);

                }


                //Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderVideo.img_vw_user_profile);

                //Glide.with(context).load(movieResults.get(position).getMedia().get(0).getThumbnail_file()).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(dataViewHolderVideo.iv_group_video_post);

                if (movieResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderVideo.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderVideo.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                //setPic(dataViewHolderVideo.iv_group_video_post, movieResults.get(position).getMedia().get(0).getFile(), dataViewHolderVideo.mProgress);

                if (!movieResults.get(position).getMedia().get(0).getThumbnail_file().equalsIgnoreCase("")) {
                    Glide
                            .with(context)
                            .load(movieResults.get(position).getMedia().get(0).getThumbnail_file())
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    dataViewHolderVideo.mProgress.setVisibility(View.GONE);
                                    //dataViewHolderVideo.iv_group_video_play.setVisibility(View.VISIBLE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    // image ready, hide progress now
                                    dataViewHolderVideo.mProgress.setVisibility(View.GONE);
                                    dataViewHolderVideo.iv_group_video_play.setVisibility(View.VISIBLE);
                                    return false;   // return false if you want Glide to handle everything else.
                                }
                            })
                            /*.diskCacheStrategy(DiskCacheStrategy.ALL)*/
                            .crossFade()
                            .into(dataViewHolderVideo.iv_group_video_post);
                } else {

                    Glide.with(context).load(R.drawable.placeholder_callforblessings).listener(new RequestListener<Integer, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, Integer model, Target<GlideDrawable> target, boolean isFirstResource) {
                            dataViewHolderVideo.mProgress.setVisibility(View.GONE);
                            dataViewHolderVideo.iv_group_video_play.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, Integer model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            dataViewHolderVideo.mProgress.setVisibility(View.GONE);
                            dataViewHolderVideo.iv_group_video_play.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                            /*.diskCacheStrategy(DiskCacheStrategy.ALL)*/
                            .crossFade().into(dataViewHolderVideo.iv_group_video_post);


                }


                break;

            case TEXT:

                GroupHomeWallFragmentAdapter.DataViewHolderText dataViewHolderText = (GroupHomeWallFragmentAdapter.DataViewHolderText) holder;

                dataViewHolderText.txtvw_user_name.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getPost_by().getProfile_details().getFirstname(), movieResults.get(position).getPost_by().getProfile_details().getLastname()));

                dataViewHolderText.txtvw_time_stamp.setText(movieResults.get(position).getPosted_at());
                dataViewHolderText.txtvw_post_details.setText(movieResults.get(position).getPost_content());
                if (movieResults.get(position).getLike_count().equalsIgnoreCase("1") || movieResults.get(position).getLike_count().equalsIgnoreCase("0")) {

                    dataViewHolderText.txtvw_no_of_likes.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getLike_count(), res.getString(R.string.like)));
                } else {
                    dataViewHolderText.txtvw_no_of_likes.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getLike_count(), res.getString(R.string.likes)));
                }

                if (movieResults.get(position).getComment_count().equalsIgnoreCase("1") || movieResults.get(position).getComment_count().equalsIgnoreCase("0")) {

                    dataViewHolderText.txtvw_no_of_comments.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getComment_count(), res.getString(R.string.comment)));
                } else {
                    dataViewHolderText.txtvw_no_of_comments.setText(String.format(res.getString(R.string.no_of_like), movieResults.get(position).getComment_count(), res.getString(R.string.comments)));
                }

                if (movieResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.MALE)) {
                    Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_male_avatar).thumbnail(0.1f).into(dataViewHolderText.img_vw_user_profile);
                } else if (movieResults.get(position).getPost_by().getProfile_details().getGender().equalsIgnoreCase(Constants.FEMALE)) {
                    Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.mipmap.ic_placeholder_female_avatar).thumbnail(0.1f).into(dataViewHolderText.img_vw_user_profile);

                }
                //Glide.with(context).load(movieResults.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(dataViewHolderText.img_vw_user_profile);


                if (movieResults.get(position).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {

                    int imgResource = R.drawable.ic_liked;
                    dataViewHolderText.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                } else {
                    int imgResource = R.drawable.prayer_like_icon;
                    dataViewHolderText.txtvw_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                }


                break;


            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return movieResults == null ? 0 : movieResults.size();
    }

    @Override
    public int getItemViewType(int position) {

        int returnType = 1;

        if (position == movieResults.size() - 1 && isLoadingAdded) {
            returnType = LOADING;
        } else if (movieResults.get(position).getMedia() != null && movieResults.get(position).getMedia().size() > 0) {
            if (movieResults.get(position).getMedia().get(0).getType().equalsIgnoreCase(Constants.IMAGE)) {
                returnType = IMAGE;
            } else if (movieResults.get(position).getMedia().get(0).getType().equalsIgnoreCase(Constants.VIDEO)) {
                returnType = VIDEO;
            }
        } else {
            returnType = TEXT;

        }

        return returnType;
        /*return (position == movieResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;*/
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(GroupHomeWallModal.DetailBean.PostBean r) {
        movieResults.add(r);
        notifyItemInserted(movieResults.size() - 1);
    }

    public void addAll(List<GroupHomeWallModal.DetailBean.PostBean> moveResults) {
        for (GroupHomeWallModal.DetailBean.PostBean result : moveResults) {
            add(result);
        }
    }

    public void remove(GroupHomeWallModal.DetailBean.PostBean r) {
        int position = movieResults.indexOf(r);
        if (position > -1) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeAll() {
        if (movieResults.size() > 0) {
            movieResults.clear();
            notifyDataSetChanged();
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new GroupHomeWallModal.DetailBean.PostBean());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = movieResults.size() - 1;
        GroupHomeWallModal.DetailBean.PostBean result = getItem(position);

        if (result != null) {
            movieResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public GroupHomeWallModal.DetailBean.PostBean getItem(int position) {
        return movieResults.get(position);
    }


    /*
    View Holders
    _________________________________________________________________________________________________
     */
    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


    protected class DataViewHolderText extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;


        public DataViewHolderText(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }


        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);

                if (movieResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    movieResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    movieResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(movieResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);

                } else {
                    movieResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    movieResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(movieResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);
                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {

            Intent i = new Intent(context, GroupsCommentsListActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.GROUP_POST_ID, String.valueOf(movieResults.get(getAdapterPosition()).getId()));
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }


        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(movieResults.get(getAdapterPosition()).getId()), movieResults.get(getAdapterPosition()).getPost_content(), movieResults.get(getAdapterPosition()).getPosted_at(), "", "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    GroupHomeWallPostLike groupHomeWallPostLike;


                    switch (tag) {

                        case APIServerResponse.GROUP_POST_LIKE:

                            groupHomeWallPostLike = (GroupHomeWallPostLike) response.body();

                            //if (groupHomeWallPostLike.getStatus().equalsIgnoreCase("1")) {
                            //movieResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                            //movieResults.get(getAdapterPosition()).setLike_count(movieResults.get(getAdapterPosition()).getLike_count() + "1");
                            //notifyItemChanged(getAdapterPosition());
                            //}
                            //((BaseActivity) mContext).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            //((BaseActivity) mContext).hideLoading();
            throwable.printStackTrace();
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    protected class DataViewHolderImage extends RecyclerView.ViewHolder implements APIServerResponse {


        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        @BindView(R.id.iv_grou_post_image)
        AppCompatImageView iv_grou_post_image;
        @BindView(R.id.movie_progress)
        ProgressBar mProgress;


        public DataViewHolderImage(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.iv_grou_post_image)
        public void viewImageFullScree() {
            try {
                Intent intentFullscreenImage = new Intent(context, ViewFullScreenImage.class);
                Bundle b = new Bundle();
                b.putString(Constants.IMAGE, movieResults.get(getAdapterPosition()).getMedia().get(0).getFile());
                intentFullscreenImage.putExtras(b);
                context.startActivity(intentFullscreenImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);

                if (movieResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    movieResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    movieResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(movieResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);

                } else {
                    movieResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    movieResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(movieResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);
                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {

            Intent i = new Intent(context, GroupsCommentsListActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.GROUP_POST_ID, String.valueOf(movieResults.get(getAdapterPosition()).getId()));
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }


        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(movieResults.get(getAdapterPosition()).getId()), movieResults.get(getAdapterPosition()).getPost_content(), movieResults.get(getAdapterPosition()).getPosted_at(), movieResults.get(getAdapterPosition()).getMedia().get(0).getFile(), "", "");


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:


                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            //((BaseActivity) mContext).hideLoading();
            throwable.printStackTrace();
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    protected class DataViewHolderVideo extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView txtvw_time_stamp;
        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.txtvw_post_details)
        AppCompatTextView txtvw_post_details;
        @BindView(R.id.txtvw_no_of_likes)
        AppCompatTextView txtvw_no_of_likes;
        @BindView(R.id.txtvw_no_of_comments)
        AppCompatTextView txtvw_no_of_comments;
        @BindView(R.id.txtvw_like)
        AppCompatTextView txtvw_like;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_share)
        AppCompatTextView txtvw_share;

        /*@BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;*/

        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView img_vw_user_profile;

        @BindView(R.id.iv_group_video_post)
        AppCompatImageView iv_group_video_post;

        @BindView(R.id.iv_group_video_play)
        AppCompatImageView iv_group_video_play;

        @BindView(R.id.movie_progress)
        ProgressBar mProgress;


        public DataViewHolderVideo(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }


        @OnClick(R.id.card_video)
        public void viewVideoFullScree() {
            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(movieResults.get(getAdapterPosition()).getMedia().get(0).getFile());
            playMusicModal.setArtworkUrl(movieResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());
            playMusicModal.setThumbnailUrl(movieResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());
            playMusicModal.setAlbum("");
            playMusicModal.setArtist("");
            playMusicModal.setAudio(false);
            playMusicModal.setTitle("Video");
            playMusicModal.setPlaylistId(movieResults.get(getAdapterPosition()).getId());

            albumSongsList.add(playMusicModal);


            Intent intent = new Intent(context, FullScreenVideoPlayerActivity.class);
            intent.putExtra(VideoPlayerActivity.EXTRA_INDEX, 0);
            intent.putExtra(VideoPlayerActivity.PLAYLIST_FLAG, movieResults.get(getAdapterPosition()).getId());
            Bundle b = new Bundle();
            b.putParcelableArrayList(Constants.ALBUM_SONGS_LIST, albumSongsList);
            intent.putExtra("video_bundle", b);
            context.startActivity(intent);
        }

        @OnClick(R.id.txtvw_like)
        public void likeGroupPost() {

            try {

                if (txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(false);
                }
                //ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);

                if (movieResults.get(getAdapterPosition()).getLike_flag().equalsIgnoreCase(Constants.LIKED)) {
                    movieResults.get(getAdapterPosition()).setLike_flag(Constants.NOT_LIKED);
                    movieResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(movieResults.get(getAdapterPosition()).getLike_count()) - 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);

                } else {
                    movieResults.get(getAdapterPosition()).setLike_flag(Constants.LIKED);
                    movieResults.get(getAdapterPosition()).setLike_count("" + (Integer.parseInt(movieResults.get(getAdapterPosition()).getLike_count()) + 1));
                    notifyItemChanged(getAdapterPosition());
                    ServerAPI.getInstance().likeGroupPost(APIServerResponse.GROUP_POST_LIKE, ((BaseActivity) context).getUserSessionId(), String.valueOf(movieResults.get(getAdapterPosition()).getId()), this);
                }


                if (!txtvw_like.isEnabled()) {
                    txtvw_like.setEnabled(true);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @OnClick(R.id.txtvw_comment)
        public void commentOnPost() {

            Intent i = new Intent(context, GroupsCommentsListActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.GROUP_POST_ID, String.valueOf(movieResults.get(getAdapterPosition()).getId()));
            b.putInt(Constants.POSITION, getAdapterPosition());
            i.putExtras(b);
            context.startActivity(i);
        }

        @OnClick(R.id.txtvw_share)
        public void sharePost() {
            try {
                openConfirmationDialog(String.valueOf(movieResults.get(getAdapterPosition()).getId()), movieResults.get(getAdapterPosition()).getPost_content(), movieResults.get(getAdapterPosition()).getPosted_at(), "", movieResults.get(getAdapterPosition()).getMedia().get(0).getFile(), movieResults.get(getAdapterPosition()).getMedia().get(0).getThumbnail_file());


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                          /*  joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                            } else {
                                Intent player = new Intent(mContext, JoyMusicSubscriptionActivity.class);
                                mContext.startActivity(player);
                            }
                            ((BaseActivity) mContext).hideLoading();
                            break;*/
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            //((BaseActivity) mContext).hideLoading();
            throwable.printStackTrace();
        }




        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    public void updateASingleItem(int position) {
        try {
            movieResults.get(position).setComment_count("" + (Integer.parseInt(movieResults.get(position).getComment_count()) + 1));
            notifyItemChanged(position);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void openConfirmationDialog(String postId, String post_text, String timeStamp, String post_image, String postVideoUrl, String postVideoThumbnail) {

        Intent shareDialog = new Intent(context, SharePostActivity.class);
        Bundle b = new Bundle();
        b.putString(Constants.POSTTYPE, "GROUP_POST");
        b.putString(Constants.POSTIMAGE, post_image);
        b.putString(Constants.POSTTEXT, post_text);
        b.putString(Constants.POSTTIMESTAMP, timeStamp);
        b.putString(Constants.POSTID, postId);
        b.putString(Constants.POSTVIDEO, postVideoUrl);
        b.putString(Constants.POSTTHUMBNAIL, postVideoThumbnail);
        shareDialog.putExtras(b);
        context.startActivity(shareDialog);


    }

    /*protected class MovieVH extends RecyclerView.ViewHolder implements APIServerResponse {
        //private TextView mMovieTitle;
        // private TextView mMovieDesc;
        // private TextView mYear; // displays "year | language"
        // private ImageView mPosterImg;
        private ProgressBar mProgress;

        AppCompatTextView atv_Name;

        AppCompatImageView atv_AlbumCover;

        LinearLayout ll_songItem;

        public MovieVH(View itemView) {
            super(itemView);

            atv_Name = (AppCompatTextView) itemView.findViewById(R.id.atv_Name);
            *//*mMovieDesc = (TextView) itemView.findViewById(R.id.movie_desc);
            mYear = (TextView) itemView.findViewById(R.id.movie_year);*//*
            ll_songItem = (LinearLayout) itemView.findViewById(R.id.ll_songItem);
            atv_AlbumCover = (AppCompatImageView) itemView.findViewById(R.id.atv_AlbumCover);
            mProgress = (ProgressBar) itemView.findViewById(R.id.movie_progress);

            ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    playAudio(getAdapterPosition());

                  *//*  if (movieResults.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase(Constants.PAID)) {
                        if (((BaseActivity) context).isConnectedToInternet()) {
                            ((BaseActivity) context).showLoading();
                            ServerAPI.getInstance().checkjoyMusicSubscription(APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY, ((BaseActivity) context).getUserSessionId(), GroupHomeWallFragmentAdapter.MovieVH.this);
                        }
                    } else {
                        playAudio(getAdapterPosition());
                    }*//*
                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                           *//* joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                                playAudio(getAdapterPosition());
                            } else {
                                Intent player = new Intent(context, JoyMusicSubscriptionActivity.class);
                                context.startActivity(player);
                            }
                            ((BaseActivity) context).hideLoading();*//*
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            //((BaseActivity) context).hideLoading();
        }
    }*/


    /*private void setPic(ImageView imageView, String imgPath, final ProgressBar progressBar) {

		*//* There isn't enough memory to open up more than a couple camera photos *//*
        *//* So pre-scale the target bitmap into which the file is decoded *//*

		*//* Get the size of the ImageView *//*

        int targetW = imageView.getWidth();
        *//*img_view_event_pic.getHeight();*//*
        int targetH = imageView.getWidth();

		*//* Get the size of the image *//*
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		*//* Figure out which way needs to be reduced less *//*
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		*//* Set bitmap options to scale the image decode target *//*
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

		*//* Decode the JPEG file into a Bitmap *//*
        Bitmap bitmap = BitmapFactory.decodeFile(imgPath, bmOptions);

		*//* Associate the Bitmap to the ImageView *//*
        progressBar.setVisibility(View.GONE);
        imageView.setImageBitmap(bitmap);
        //Glide.with(context).load(bitmap).placeholder(R.drawable.ic_me).thumbnail(0.1f).into(imageView);


        *//*Glide.with(context).load(bitmap).listener(new RequestListener<Bitmap, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, Bitmap model, Target<GlideDrawable> target, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, Bitmap model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image
                .thumbnail(0.1f)
                .centerCrop()
                .crossFade()
                .into(imageView);*//*



        *//*img_view_event_pic.setVisibility(View.VISIBLE);*//*
    }*/

}

