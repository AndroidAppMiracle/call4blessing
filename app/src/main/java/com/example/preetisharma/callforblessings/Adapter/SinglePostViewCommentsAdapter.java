package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.CommentsListModal;
import com.example.preetisharma.callforblessings.Server.Modal.SinglePostViewModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kshitiz Bali on 5/4/2017.
 */

public class SinglePostViewCommentsAdapter extends RecyclerView.Adapter<SinglePostViewCommentsAdapter.DataViewHolder> {
    public List<SinglePostViewModal.DetailBean.CommentsBean> list;
    Context mContext;


    public SinglePostViewCommentsAdapter(Context mContext, List<SinglePostViewModal.DetailBean.CommentsBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<SinglePostViewModal.DetailBean.CommentsBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public SinglePostViewCommentsAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_comments, parent, false);
        SinglePostViewCommentsAdapter.DataViewHolder dataView = new SinglePostViewCommentsAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final SinglePostViewCommentsAdapter.DataViewHolder holder, final int position) {
        holder.txtvw_user_name.setText(list.get(position).getPost_by().getUsername());
        holder.txtvw_comment.setText(list.get(position).getComment());
        Glide.with(mContext).load(list.get(position).getPost_by().getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).centerCrop().into(holder.user_profile_pic);
        holder.txtvw_time_stamp_post.setText(list.get(position).getCreated_at());
    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }


    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtvw_user_name)
        AppCompatTextView txtvw_user_name;
        @BindView(R.id.user_profile_pic)
        AppCompatImageView user_profile_pic;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_time_stamp_post)
        AppCompatTextView txtvw_time_stamp_post;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }
    }
}

