package com.example.preetisharma.callforblessings.demo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.JoyMusicMyDownloadsAdapter;
import com.example.preetisharma.callforblessings.Adapter.JoyMusicMyDownloadsPhoneAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DownloadedMusicPhoneModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicMyDownloadsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by satoti.garg on 5/22/2017.
 */

public class JoyMusicMyDownloads extends BaseActivity implements APIServerResponse {
    AppCompatTextView txtvw_header_title;
    AppCompatImageView img_view_back, img_view_change_password;
    RecyclerView rv_viewAllList;
    GridLayoutManager gridLayoutManager;
    List<DownloadedMusicPhoneModal> downloadList = new ArrayList<>();
    MediaMetadataRetriever metaRetriver;
    byte[] art;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joy_music_view_all);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        txtvw_header_title.setText(getString(R.string.my_downloads));
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setVisibility(View.GONE);

        rv_viewAllList = (RecyclerView) findViewById(R.id.rv_viewAllList);
        gridLayoutManager = new GridLayoutManager(this, 2);
        rv_viewAllList.setLayoutManager(gridLayoutManager);
        rv_viewAllList.setItemAnimator(new DefaultItemAnimator());


        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        loadFromPhoneMemory();

       /* if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().myDownloads(APIServerResponse.MY_DOWNLOADS, getUserSessionId(), this);
        }*/
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {

                JoyMusicMyDownloadsModal joyMusicMyDownloadsModal;

                switch (tag) {

                    case APIServerResponse.MY_DOWNLOADS:

                        joyMusicMyDownloadsModal = (JoyMusicMyDownloadsModal) response.body();
                        if (joyMusicMyDownloadsModal.getStatus().equalsIgnoreCase("1")) {

                            List<JoyMusicMyDownloadsModal.ListBean> list = new ArrayList<>();
                            list = joyMusicMyDownloadsModal.getList();
                            JoyMusicMyDownloadsAdapter adapter = new JoyMusicMyDownloadsAdapter(JoyMusicMyDownloads.this, list);
                            rv_viewAllList.setAdapter(adapter);

                        }

                        hideLoading();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();

    }


    private void loadFromPhoneMemory() {
        try {
            File finalFile = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + File.separator + "JoyMusic");
            if (finalFile.exists()) {
                metaRetriver = new MediaMetadataRetriever();
                if (!downloadList.isEmpty()) {
                    downloadList.clear();
                }
                String finalPath = finalFile.getAbsolutePath();
                Log.i("DIRECTORY", finalPath);
                File f = new File(finalPath);
                File[] files = f.listFiles();
                Log.i("DIRECTORY size", " " + files.length);
                if (files.length > 0) {
                    for (int i = 0; i < files.length; i++) {
                        //here populate your listview
                        Log.i("DIRECTORY FileName", "FileName:" + files[i].getName());
                        //metaRetriver.setDataSource(files[i].getAbsolutePath());
                        //DownloadedMusicPhoneModal downloadedMusicPhoneModal = new DownloadedMusicPhoneModal();

                        retriveAudioMetadata(files[i].getAbsolutePath());

                    }

                    JoyMusicMyDownloadsPhoneAdapter adapter = new JoyMusicMyDownloadsPhoneAdapter(JoyMusicMyDownloads.this, downloadList);
                    rv_viewAllList.setAdapter(adapter);

                }
            } else {
                if (isConnectedToInternet()) {
                    showLoading();
                    ServerAPI.getInstance().myDownloads(APIServerResponse.MY_DOWNLOADS, getUserSessionId(), this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }

            //retriveAudioMetadata();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void retriveAudioMetadata(String mediaSource) {

        String songAlbum = "", songArtist = "", songGenre = "", songName = "", songArt = "";
        metaRetriver = new MediaMetadataRetriever();
        metaRetriver.setDataSource(mediaSource);

        if (metaRetriver.getEmbeddedPicture() != null && metaRetriver.getEmbeddedPicture().length != 0) {
            byte[] art = metaRetriver.getEmbeddedPicture();
            songArt = Base64.encodeToString(art, Base64.DEFAULT);
            /*Bitmap songImage = BitmapFactory
                    .decodeByteArray(art, 0, art.length);*/

        } else {
            songArt = "";
        }

        if (metaRetriver
                .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM) != null && metaRetriver
                .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM).length() != 0) {
            songAlbum = metaRetriver
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        } else {
            songAlbum = "Unknown Album";
        }

        if (metaRetriver
                .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST) != null && metaRetriver
                .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST).length() != 0) {
            songArtist = metaRetriver
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        } else {
            songArtist = "Unknown Artist";
        }

        if (metaRetriver
                .extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE) != null &&
                metaRetriver
                        .extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE).length() != 0) {
            songGenre = metaRetriver
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
        } else {
            songGenre = "Unknown Genre";
        }

        if (metaRetriver
                .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE) != null && metaRetriver
                .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE).length() != 0) {
            songName = metaRetriver
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);

        } else {
            songName = "Unknown Title";
        }

        DownloadedMusicPhoneModal downloadedMusicPhoneModal = new DownloadedMusicPhoneModal();
        downloadedMusicPhoneModal.setName(songName);
        downloadedMusicPhoneModal.setFilePath(mediaSource);
        downloadedMusicPhoneModal.setCoverPhoto(songArt);
        downloadedMusicPhoneModal.setAlbumName(songAlbum);
        downloadedMusicPhoneModal.setArtist(songArtist);
        downloadedMusicPhoneModal.setGenre(songGenre);

        downloadList.add(downloadedMusicPhoneModal);


        Log.i("Metadata", songName + " " + songAlbum + " " + songGenre + " " + songArtist);
    }
}
